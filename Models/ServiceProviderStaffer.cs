using System.ComponentModel.DataAnnotations;
using PatientsRecordsManager.Constants.Enums;

namespace PatientsRecordsManager.Models
{
    public class ServiceProviderStaffer
    {
        [Required]
        public int ServiceProviderId { get; set; }
        public ServiceProvider ServiceProvider { get; set; }
        [Required]
        public int ServiceStafferId { get; set; }
        public ServiceStaffer Staffer { get; set; }
        [Required]
        [EnumDataType(typeof(ServiceProviderStafferStatus))]
        public ServiceProviderStafferStatus Status { get; set; }
    }
}