using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PatientsRecordsManager.Constants;
using PatientsRecordsManager.Data.Dtos.Prescribed.Surgeries;
using PatientsRecordsManager.Data.Repositories;
using PatientsRecordsManager.Extensions;
using PatientsRecordsManager.Resources;

namespace PatientsRecordsManager.Controllers
{
    [ApiController]
    [Route(APIs.VisitSurgeriesBaseUrl)]
    [Authorize]
    public class VisitSurgeriesController : PrmController
    {
        private readonly IVisitSurgeryRepository _repository;
        private readonly Messages _messages;

        public VisitSurgeriesController(IVisitSurgeryRepository repository, Messages messages)
        {
            _messages = messages;
            _repository = repository;
        }

        [HttpPost(APIs.AddSurgery)]
        public IActionResult AddVisitSurgery(string visitCode, PrescribedSurgeryForCreateDto surgeryForCreate)
        {
            return HttpContext.Success(_repository.AddSurgery(visitCode, surgeryForCreate), _messages.GetString("CompleteAddSurgery"));
        }

        [HttpPut(APIs.ModifySurgery)]
        public IActionResult ModifyVisitSurgery(string visitCode, string sCode, PrescribedSurgeryForUpdateDto surgeryForUpdate)
        {
            return HttpContext.Success(_repository.ModifySurgery(visitCode, sCode, surgeryForUpdate), _messages.GetString("CompleteUpdateSurgery"));
        }

        [HttpDelete(APIs.DeleteSurgery)]
        public IActionResult DeleteVisitSurgery(string visitCode, string sCode)
        {
            return HttpContext.Success(_repository.RemoveSurgery(visitCode, sCode), _messages.GetString("CompleteDeleteSurgery"));
        }

        [HttpGet(APIs.GetSurgeries)]
        public IActionResult GetSurgeries(string visitCode)
        {
            return HttpContext.Success(_repository.GetSurgeries(visitCode), _messages.GetString("CompleteFetchSurgeries"));
        }

        [HttpGet(APIs.GetSurgery)]
        public IActionResult GetVisitSurgery(string visitCode, string surgeryCode)
        {
            return HttpContext.Success(_repository.GetSurgeryDetails(visitCode, surgeryCode), _messages.GetString("CompleteFetchSurgeries"));
        }
    }
}