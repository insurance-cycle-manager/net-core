using PatientsRecordsManager.Constants.Enums;
using PatientsRecordsManager.Data.Dtos.InsuranceCompanies.Packages;

namespace PatientsRecordsManager.Data.Dtos.Patients.Packages
{
    public class PatientPackageForReturnDto
    {
        public PatientForReturnDto Patient { get; set; }
        public InsurancePackageForReturnDto InsurancePackage { get; set; }
        public PatientPackageStatus Status { get; set; }
    }
}