using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PatientsRecordsManager.Models
{
    public class PrescribedSurgery
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string PrescribedSurgeryCode { get; set; }

        [Required]
        public int VisitId { get; set; }
        public Visit Visit { get; set; }

        [Required]
        public int SurgeryId { get; set; }
        public Surgery Surgery { get; set; }

        [Required]
        public DateTime ScheduledDate { get; set; }

        public string Note { get; set; }

        public List<SurgeryPurchase> Purchases { get; set; }
    }
}