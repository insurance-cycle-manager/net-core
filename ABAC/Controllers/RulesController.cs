using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PatientsRecordsManager.ABAC.Data;
using PatientsRecordsManager.ABAC.Data.Dtos;
using PatientsRecordsManager.Extensions;
using PatientsRecordsManager.Resources;

namespace PatientsRecordsManager.ABAC.Controllers
{
    [ApiController]
    [Route(BaseRoute)]
    // [Authorize(Roles = "SupervisoryCommission")]
    [AllowAnonymous]
    public class RulesController : ControllerBase
    {
        private const string BaseRoute = "abac/[controller]";
        private const string FetchRulesRoute = "";
        private const string PostDboRulesRoute = "dbo";
        private const string PostActionRulesRoute = "act";
        private const string RemoveRulesRoute = "{ruleId}";
        private const string UpdateRulesRoute = "{ruleType}/{ruleId}";
        private const string DetailsRulesRoute = "{ruleType}/{ruleId}/attributes";

        private readonly RulesRepository rules;
        private readonly Messages messages;

        public RulesController(RulesRepository rules, Messages messages)
        {
            this.messages = messages;
            this.rules = rules;
        }

        [HttpGet(FetchRulesRoute)]
        public IActionResult GetRules([FromQuery] string subject)
        {
            return HttpContext.Success(rules.GetRules(subject), messages.GetString("rulesFetched"));
        }

        [HttpPost(PostDboRulesRoute)]
        public IActionResult AddDbObjectRule(DboRuleForCreateDto rule)
        {
            return HttpContext.Success(rules.AddDboTypeRule(rule), messages.GetString("ruleAdded"));
        }

        [HttpPost(PostActionRulesRoute)]
        public IActionResult AddActionRule(RuleForCreateDto rule)
        {
            return HttpContext.Success(rules.AddActionTypeRule(rule), messages.GetString("ruleAdded"));
        }

        [HttpPut(UpdateRulesRoute)]
        public IActionResult ModifyDbObjectRule(string ruleType, int ruleId, RuleForModifyDto rule)
        {
            return HttpContext.Success(rules.ModifyRule(ruleId, ruleType, rule), messages.GetString("ruleModified", ruleId));
        }

        [HttpDelete(RemoveRulesRoute)]
        public IActionResult RemoveRule(int ruleId)
        {
            return HttpContext.Success(rules.RemoveRule(ruleId), messages.GetString("ruleRemoved", ruleId));
        }
        
        [HttpGet(DetailsRulesRoute)]
        public IActionResult DetailsRule(string ruleType, int ruleId)
        {
            return HttpContext.Success(rules.DetailsRule(ruleId, ruleType), messages.GetString("ruleDetailsFetched", ruleId));
        }
    }
}