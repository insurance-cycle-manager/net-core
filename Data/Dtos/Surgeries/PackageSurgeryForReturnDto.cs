namespace PatientsRecordsManager.Data.Dtos.Surgeries
{
    public class PackageSurgeryForReturnDto
    {
        public string SurgeryCode { get; set; }
    }
}