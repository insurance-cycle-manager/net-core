using System;
using System.Collections.Generic;
using PatientsRecordsManager.Data.Dtos.Addresses;
using PatientsRecordsManager.Data.Dtos.Users.Addresses;
using PatientsRecordsManager.Data.Dtos.Users.Emails;
using PatientsRecordsManager.Data.Dtos.Users.Phones;
using PatientsRecordsManager.Data.Dtos.Users.Roles;

namespace PatientsRecordsManager.Data.Dtos.Users.Accounts
{
    public class UserAccountForReturnDto
    {
        public Uri PhotoUrl { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public IEnumerable<UserEmailForReturnDto> UserEmails { get; set; }
        public IEnumerable<UserPhoneForReturnDto> UserPhoneNumbers { get; set; }
        public IEnumerable<UserRoleForReturnDto> UserRoles { get; set; }
        public IEnumerable<UserAddressForReturnDto> UserAddresses { get; set; }
    }
}