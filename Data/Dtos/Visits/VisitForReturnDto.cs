using System;
using System.Collections.Generic;
using PatientsRecordsManager.Data.Dtos.Addresses;
using PatientsRecordsManager.Data.Dtos.Diagnosis;
using PatientsRecordsManager.Data.Dtos.InsuranceCompanies.Packages;
using PatientsRecordsManager.Data.Dtos.Patients;
using PatientsRecordsManager.Data.Dtos.Patients.Packages;
using PatientsRecordsManager.Data.Dtos.ServiceProviders;
using PatientsRecordsManager.Data.Dtos.ServiceProviders.Connections;
using PatientsRecordsManager.Data.Dtos.ServiceProviders.Staffers;
using PatientsRecordsManager.Data.Dtos.Visits.Claims;

namespace PatientsRecordsManager.Data.Dtos.Visits
{
    public class VisitForReturnDto
    {
        public string VisitCode { get; set; }
        public string Type { get; set; }
        public double Cost { get; set; }
        public DateTime VisitDate { get; set; }
        public VisitForReturnDto BaseVisit { get; set; }
        public PatientForReturnDto Patient { get; set; }
        public ServiceProviderForReturnDto ServiceProvider { get; set; }
        public StafferForReturnDto ServiceStaffer { get; set; }
        public SpConnectionForReturnDto ServiceProviderConnection { get; set; }
        public PatientPackageForReturnDto PatientPackage { get; set; }
        public AddressForReturnDto Address { get; set; }
        public VisitClaimForReturnDto Calim { get; set; }
        public DiagnosisForReturnDto Diagnosis { get; set; }
        public IEnumerable<VisitForReturnDto> SubVisits { get; set; }
    }
}