using System.ComponentModel.DataAnnotations;

namespace PatientsRecordsManager.Data.Dtos.Addresses
{
    public class AddressForReturnDto
    {
        public double? Longitude { get; set; }
        public double? Latitude { get; set; }
        [Required]
        public string Title { get; set; }
        public string AddressCode { get; set; }
    }
}