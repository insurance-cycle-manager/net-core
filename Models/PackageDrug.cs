namespace PatientsRecordsManager.Models
{
    public class PackageDrug
    {
        public int InsurancePackageId { get; set; }
        public int DrugId { get; set; }

        public InsurancePackage InsurancePackage { get; set; }
        public Drug Drug { get; set; }
    }
}