using PatientsRecordsManager.Data.Dtos.Patients;
using PatientsRecordsManager.Data.Dtos.Patients.Packages;
using PatientsRecordsManager.Data.Dtos.InsuranceCompanies.Packages;

namespace PatientsRecordsManager.Data.Repositories
{
    public interface IPatientRepository
    {
        // 
        // CRUD
        // 
        System.Collections.Generic.IEnumerable<PatientForReturnDto> GetPatients(string name);
        PatientForReturnDto AddPatient(PatientForCreateDto patient);
        PatientForReturnDto RemovePatient(int userId);
        PatientForReturnDto GetPatientDetails(int userId);
        PatientForReturnDto GetPatientDetails(string patientCode);
        PatientForReturnDto ModifyPatient(PatientForUpdateDto patient, int id);
        PatientForReturnDto ModifyPatient(PatientForUpdateDto patient, string patientCode);
        // 
        // Packages
        // 
        System.Collections.Generic.IEnumerable<InsurancePackageForReturnDto> GetPatientPackages(int userId);
        System.Collections.Generic.IEnumerable<InsurancePackageForReturnDto> GetPatientPackages(string patientCode);
        PatientPackageForReturnDto AddPatientPackage(string patientCode, PatientPackageForAddDto package);
        PatientPackageForReturnDto AddPatientPackage(int userId, PatientPackageForAddDto package);
        PatientPackageForReturnDto SuspendPatientPackage(int userId, string packageCode);
    }
}