using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PatientsRecordsManager.Constants;
using PatientsRecordsManager.Constants.Enums;
using PatientsRecordsManager.Data.Dtos.ServiceProviders;
using PatientsRecordsManager.Data.Dtos.ServiceProviders.Staff;
using PatientsRecordsManager.Data.Repositories;
using PatientsRecordsManager.Extensions;
using PatientsRecordsManager.Resources;

namespace PatientsRecordsManager.Controllers
{
    [ApiController]
    [Route(APIs.ServiceProvidersBaseUrl)]
    [Authorize]
    public class ServiceProvidersController : PrmController
    {
        private readonly IServiceProviderRepository _providersRepository;
        private readonly IServiceProviderStaffRepository _staffRepository;
        private readonly Messages _messages;

        public ServiceProvidersController(IServiceProviderRepository providerRepository,
                                          IServiceProviderStaffRepository staffRepository,
                                          Messages messages)
        {
            _messages = messages;
            _providersRepository = providerRepository;
            _staffRepository = staffRepository;
        }

        ///
        /// CRUD over service providers
        ///
        [HttpGet(APIs.GetServiceProviders)]
        public IActionResult GetServiceProvidersByType(ServiceProviderType spType, [FromQuery] string title)
        {
            return HttpContext.Success(_providersRepository.GetServiceProviders(spType, title), _messages.GetString("ServiceProvidersFetched"));
        }

        [HttpGet(APIs.GetMyServiceProviderDetails, Order = 1)]
        public IActionResult GetMyServiceProvider(ServiceProviderType spType)
        {
            return HttpContext.Success(_providersRepository.GetServiceProviderDetails(spType, HttpContext.GetUserId()), _messages.GetString("ServiceProviderFetched"));
        }

        [HttpGet(APIs.GetServiceProviderDetails, Order = 2)]
        [Authorize(Roles = RolesNames.Administrator)]
        public IActionResult GetServiceProvider(ServiceProviderType spType, string spCode)
        {
            return HttpContext.Success(_providersRepository.GetServiceProviderDetails(spType, spCode), _messages.GetString("ServiceProviderFetched"));
        }

        [HttpPut(APIs.ModifyMeServiceProvider, Order = 1)]
        [Authorize(Roles = RolesNames.GetProvidersCommaDelimited)]
        public IActionResult ModifyMeServiceProvider(ServiceProviderType spType, ServiceProviderForUpdateDto serviceProvider)
        {
            return HttpContext.Success(_providersRepository.ModifyServiceProvider(spType, HttpContext.GetUserId(), serviceProvider), _messages.GetString("ServiceProviderModifyDone"));
        }

        [HttpPut(APIs.ModifyServiceProvider, Order = 2)]
        [Authorize(Roles = RolesNames.Administrator)]
        public IActionResult ModifyServiceProvider(ServiceProviderType spType, string spCode, ServiceProviderForUpdateDto serviceProvider)
        {
            return HttpContext.Success(_providersRepository.ModifyServiceProvider(spType, spCode, serviceProvider), _messages.GetString("ServiceProviderModifyDone"));
        }

        [HttpPut(APIs.ChangeServiceProviderStatus)]
        [Authorize(Roles = RolesNames.Administrator)]
        public IActionResult ChangeServiceProviderStatus(ServiceProviderType spType, string spCode, StatusType newStatus)
        {
            return HttpContext.Success(
                _providersRepository.ChangeServiceProviderStatus(spType, spCode, newStatus),
                _messages.GetString("ServiceProviderStatusChangeDone"));
        }
        ///
        /// Finish CRUD region
        ///


        ///
        /// Service provider staff
        ///
        [HttpGet(APIs.GetProviderMeStaff, Order = 1)]
        public IActionResult GetStaffMeMembers()
        {
            return HttpContext.Success(
                _staffRepository.GetStaffByProvider(HttpContext.GetUserId()),
                _messages.GetString("ProviderStaffFetchDone", ""));
        }

        [HttpGet(APIs.GetProviderStaff, Order = 2)]
        public IActionResult GetStaffMembers(string spCode)
        {
            return HttpContext.Success(_staffRepository.GetStaffByProvider(spCode), _messages.GetString("ProviderStaffFetchDone", spCode));
        }

        [HttpPost(APIs.AddStaffMember)]
        [Authorize(Roles = RolesNames.GetProvidersCommaDelimitedWithAdmin)]
        public IActionResult AddStaffMember(string spCode, MemberForAddDto member)
        {
            var id = HttpContext.GetUserId();
            if (HttpContext.User.IsInRole(RolesNames.Administrator))
            {
                id = -1;
            }
            return HttpContext.Success(_staffRepository.AddStafferMember(id, spCode, member), _messages.GetString("MemberAddDone"));
        }

        [HttpDelete(APIs.DeleteStaffMember)]
        [Authorize(Roles = RolesNames.GetProvidersCommaDelimitedWithAdmin)]
        public IActionResult RemoveMember(string spCode, string stCode)
        {
            var id = HttpContext.GetUserId();
            if (HttpContext.User.IsInRole(RolesNames.Administrator))
            {
                id = -1;
            }
            return HttpContext.Success(_staffRepository.RemoveStaffer(id, spCode, stCode), _messages.GetString("MemberRemoveDone"));
        }
    }
}