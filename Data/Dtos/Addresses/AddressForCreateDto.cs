using System.ComponentModel.DataAnnotations;

namespace PatientsRecordsManager.Data.Dtos.Addresses
{
    public class AddressForCreateDto
    {
        public double? Longitude { get; set; }
        public double? Latitude { get; set; }
        [Required]
        public string Title { get; set; }
    }
}