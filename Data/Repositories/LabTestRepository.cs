using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using PatientsRecordsManager.ABAC.Extensions;
using PatientsRecordsManager.ABAC.Extensions.EF;
using PatientsRecordsManager.Data.Dtos.LabTests;
using PatientsRecordsManager.Models;
using PatientsRecordsManager.Resources;

namespace PatientsRecordsManager.Data.Repositories
{
    public class LabTestRepository : ILabTestRepository
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        private readonly Messages _messages;

        public LabTestRepository(DataContext context, IMapper mapper, Messages messages)
        {
            _mapper = mapper;
            _context = context;
            _messages = messages;
        }

        public LabTestForReturnDto AddLabTest(LabTestForCreateDto labTestForCreate)
        {
            var labTest = _mapper.Map<LabTest>(labTestForCreate);
            _context.LabTests.Add(labTest);

            if (_context.SaveChanges() <= 0)
            {
                throw new Exception(_messages.GetString("saveFailed"));
            }
            return _mapper.Map<LabTestForReturnDto>(labTest);
        }

        public bool DeleteLabTest(string labTestCode)
        {
            var labTest = GetLabTest(labTestCode);
            if (labTest == null)
            {
                return false;
            }
            _context.LabTests.Remove(labTest);
            if (_context.SaveChanges() <= 0)
            {
                return false;
            }
            return true;
        }

        public LabTestForReturnDto GetLabTestDetails(string labTestCode)
        {
            return _mapper.Map<LabTestForReturnDto>(GetLabTest(labTestCode));
        }

        public IEnumerable<LabTestForReturnDto> GetLabTests(string name)
        {
            var tests = _context.LabTests.AsQueryable();
            if (!string.IsNullOrWhiteSpace(name))
            {
                tests = tests.Where(t => t.NormalizedName.ToUpper().Contains(name.ToUpper()));
            }
            return _mapper.Map<List<LabTestForReturnDto>>(tests);
        }

        public LabTestForReturnDto ModifyLabTest(string labTestCode, LabTestForUpdateDto labTestForUpdate)
        {
            var retrievedLabTest = GetLabTest(labTestCode);
            var labTest = _mapper.Map<LabTestForUpdateDto, LabTest>(labTestForUpdate, retrievedLabTest);

            if (_context.SaveChanges() <= 0)
            {
                throw new Exception(_messages.GetString("saveFailed"));
            }
            return _mapper.Map<LabTestForReturnDto>(labTest);
        }

        private LabTest GetLabTest(string labTestCode)
        {
            return _context.LabTests
                .FirstOrDefault(e => e.LabTestCode.ToUpper() == (labTestCode.ToUpper()))
                ?? throw new Exception(_messages.GetString("labTestNotFound", labTestCode));
        }
    }
}