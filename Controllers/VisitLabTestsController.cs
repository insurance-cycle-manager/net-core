using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PatientsRecordsManager.Constants;
using PatientsRecordsManager.Data.Dtos.Prescribed.LabTests;
using PatientsRecordsManager.Data.Repositories;
using PatientsRecordsManager.Extensions;
using PatientsRecordsManager.Resources;

namespace PatientsRecordsManager.Controllers
{
    [ApiController]
    [Route(APIs.VisitLabTestsBaseUrl)]
    [Authorize]
    public class VisitLabTestsController : PrmController
    {
        private readonly IVisitLabTestRepository _repository;
        private readonly Messages _messages;

        public VisitLabTestsController(IVisitLabTestRepository repository, Messages messages)
        {
            _messages = messages;
            _repository = repository;
        }

        [HttpPost(APIs.PostLabTest)]
        public IActionResult AddLabTest(string visitCode, PrescribedLabTestForCreateDto labTest)
        {
            return HttpContext.Success(_repository.AddLabTest(visitCode, labTest), _messages.GetString("CompleteAddVisitLabTest"));
        }

        [HttpPut(APIs.ModifyLabTest)]
        public IActionResult ModifyLabTest(string visitCode, string labTestCode, PrescribedLabTestForUpdateDto labTest)
        {
            return HttpContext.Success(_repository.ModifyLabTest(visitCode, labTestCode, labTest), _messages.GetString("CompleteUpdateVisitLabTest"));
        }

        [HttpDelete(APIs.DeleteLabTest)]
        public IActionResult DeleteLabTest(string visitCode, string labTestCode)
        {
            return HttpContext.Success(_repository.RemoveLabTest(visitCode, labTestCode), _messages.GetString("CompleteDeleteVisitLabTest"));
        }

        [HttpGet(APIs.GetLabTests)]
        public IActionResult GetLabTests(string visitCode)
        {
            return HttpContext.Success(_repository.GetLabTests(visitCode), _messages.GetString("CompleteFetchVisitLabTests"));
        }

        [HttpGet(APIs.GetLabTestDetails)]
        public IActionResult GetLabTest(string visitCode, string labTestCode)
        {
            return HttpContext.Success(_repository.GetLabTestDetails(visitCode, labTestCode), _messages.GetString("CompleteFetchVisitLabTestDetails", labTestCode));
        }
    }
}