using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace PatientsRecordsManager.ABAC.Extensions.EF
{
    public static class EFQueryableExtensions
    {
        public static Task<bool> AllAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.AllAsync(source.Visit(Evaluate.Action.READ), predicate, cancellationToken);
        }

        public static Task<bool> AnyAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.AnyAsync(source.Visit(Evaluate.Action.READ), predicate, cancellationToken);
        }

        public static Task<bool> AnyAsync<TSource>(this IQueryable<TSource> source, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.AnyAsync(source.Visit(Evaluate.Action.READ), cancellationToken);
        }

        public static IAsyncEnumerable<TSource> AsAsyncEnumerable<TSource>(this IQueryable<TSource> source)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.AsAsyncEnumerable(source.Visit(Evaluate.Action.READ));
        }

        public static IQueryable<TEntity> AsNoTracking<TEntity>(this IQueryable<TEntity> source) where TEntity : class
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.AsNoTracking(source.Visit(Evaluate.Action.READ));
        }

        public static IQueryable<TEntity> AsTracking<TEntity>(this IQueryable<TEntity> source, Microsoft.EntityFrameworkCore.QueryTrackingBehavior track) where TEntity : class
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.AsTracking(source.Visit(Evaluate.Action.READ), track);
        }

        public static IQueryable<TEntity> AsTracking<TEntity>(this IQueryable<TEntity> source) where TEntity : class
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.AsTracking(source.Visit(Evaluate.Action.READ));
        }

        public static Task<decimal> AverageAsync(this IQueryable<decimal> source, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.AverageAsync(source.Visit(Evaluate.Action.READ), cancellationToken);
        }

        public static Task<decimal> AverageAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, decimal>> selector, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.AverageAsync(source.Visit(Evaluate.Action.READ), selector, cancellationToken);
        }

        public static Task<decimal?> AverageAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, decimal?>> selector, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.AverageAsync(source.Visit(Evaluate.Action.READ), selector, cancellationToken);
        }

        public static Task<double> AverageAsync(this IQueryable<int> source, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.AverageAsync(source, cancellationToken);
        }

        public static Task<double?> AverageAsync(this IQueryable<int?> source, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.AverageAsync(source, cancellationToken);
        }

        public static Task<double> AverageAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, int>> selector, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.AverageAsync<TSource>(source.Visit(Evaluate.Action.READ), selector, cancellationToken);
        }

        public static Task<double?> AverageAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, int?>> selector, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.AverageAsync<TSource>(source.Visit(Evaluate.Action.READ), selector, cancellationToken);
        }

        public static Task<double> AverageAsync(this IQueryable<long> source, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.AverageAsync(source.Visit(Evaluate.Action.READ), cancellationToken);
        }

        public static Task<double?> AverageAsync(this IQueryable<long?> source, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.AverageAsync(source, cancellationToken);
        }

        public static Task<double> AverageAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, long>> selector, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.AverageAsync<TSource>(source.Visit(Evaluate.Action.READ), selector, cancellationToken);
        }

        public static Task<double> AverageAsync(this IQueryable<double> source, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.AverageAsync(source.Visit(Evaluate.Action.READ), cancellationToken);
        }

        public static Task<decimal?> AverageAsync(this IQueryable<decimal?> source, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.AverageAsync(source, cancellationToken);
        }

        public static Task<double?> AverageAsync(this IQueryable<double?> source, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.AverageAsync(source, cancellationToken);
        }

        public static Task<double> AverageAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, double>> selector, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.AverageAsync(source.Visit(Evaluate.Action.READ), selector, cancellationToken);
        }

        public static Task<double?> AverageAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, double?>> selector, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.AverageAsync<TSource>(source.Visit(Evaluate.Action.READ), selector, cancellationToken);
        }

        public static Task<float> AverageAsync(this IQueryable<float> source, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.AverageAsync(source, cancellationToken);
        }

        public static Task<float?> AverageAsync(this IQueryable<float?> source, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.AverageAsync(source, cancellationToken);
        }

        public static Task<float> AverageAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, float>> selector, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.AverageAsync<TSource>(source.Visit(Evaluate.Action.READ), selector, cancellationToken);
        }

        public static Task<float?> AverageAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, float?>> selector, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.AverageAsync<TSource>(source.Visit(Evaluate.Action.READ), selector, cancellationToken);
        }

        public static Task<double?> AverageAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, long?>> selector, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.AverageAsync<TSource>(source.Visit(Evaluate.Action.READ), selector, cancellationToken);
        }

        public static Task<bool> ContainsAsync<TSource>(this IQueryable<TSource> source, TSource item, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.ContainsAsync<TSource>(source.Visit(Evaluate.Action.READ), item, cancellationToken);
        }

        public static Task<int> CountAsync<TSource>(this IQueryable<TSource> source, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.CountAsync<TSource>(source.Visit(Evaluate.Action.READ), cancellationToken);
        }

        public static Task<int> CountAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.CountAsync<TSource>(source.Visit(Evaluate.Action.READ), predicate, cancellationToken);
        }

        public static Task<TSource> FirstAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.FirstAsync<TSource>(source.Visit(Evaluate.Action.READ), predicate, cancellationToken);
        }

        public static Task<TSource> FirstAsync<TSource>(this IQueryable<TSource> source, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.FirstAsync<TSource>(source.Visit(Evaluate.Action.READ), cancellationToken);
        }

        public static Task<TSource> FirstOrDefaultAsync<TSource>(this IQueryable<TSource> source, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.FirstOrDefaultAsync<TSource>(source.Visit(Evaluate.Action.READ), cancellationToken);
        }

        public static Task<TSource> FirstOrDefaultAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.FirstOrDefaultAsync<TSource>(source.Visit(Evaluate.Action.READ), predicate, cancellationToken);
        }

        public static Task ForEachAsync<T>(this IQueryable<T> source, Action<T> action, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.ForEachAsync<T>(source.Visit(Evaluate.Action.READ), action, cancellationToken);
        }

        public static IQueryable<TEntity> IgnoreQueryFilters<TEntity>(this IQueryable<TEntity> source) where TEntity : class
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.IgnoreQueryFilters<TEntity>(source.Visit(Evaluate.Action.READ));
        }

        public static IQueryable<TEntity> Include<TEntity>(this IQueryable<TEntity> source, [Microsoft.EntityFrameworkCore.Query.NotParameterized] string navigationPropertyPath) where TEntity : class
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.Include<TEntity>(source.Visit(Evaluate.Action.READ), navigationPropertyPath);
        }

        public static Microsoft.EntityFrameworkCore.Query.IIncludableQueryable<TEntity, TProperty> Include<TEntity, TProperty>(this IQueryable<TEntity> source, Expression<Func<TEntity, TProperty>> navigationPropertyPath) where TEntity : class
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.Include<TEntity, TProperty>(source.Visit(Evaluate.Action.READ), navigationPropertyPath);
        }

        public static Task<TSource> LastAsync<TSource>(this IQueryable<TSource> source, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.LastAsync<TSource>(source.Visit(Evaluate.Action.READ), cancellationToken);
        }

        public static Task<TSource> LastAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.LastAsync<TSource>(source.Visit(Evaluate.Action.READ), predicate, cancellationToken);
        }

        public static Task<TSource> LastOrDefaultAsync<TSource>(this IQueryable<TSource> source, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.LastOrDefaultAsync<TSource>(source.Visit(Evaluate.Action.READ), cancellationToken);
        }

        public static Task<TSource> LastOrDefaultAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.LastOrDefaultAsync<TSource>(source.Visit(Evaluate.Action.READ), predicate, cancellationToken);
        }

        public static void Load<TSource>(this IQueryable<TSource> source)
        {
            Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.Load<TSource>(source.Visit(Evaluate.Action.READ));
        }

        public static Task LoadAsync<TSource>(this IQueryable<TSource> source, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.LoadAsync<TSource>(source.Visit(Evaluate.Action.READ), cancellationToken);
        }

        public static Task<long> LongCountAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.LongCountAsync<TSource>(source.Visit(Evaluate.Action.READ), predicate, cancellationToken);
        }

        public static Task<long> LongCountAsync<TSource>(this IQueryable<TSource> source, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.LongCountAsync<TSource>(source.Visit(Evaluate.Action.READ), cancellationToken);
        }
        public static Task<TSource> MaxAsync<TSource>(this IQueryable<TSource> source, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.MaxAsync<TSource>(source.Visit(Evaluate.Action.READ), cancellationToken);
        }

        public static Task<TResult> MaxAsync<TSource, TResult>(this IQueryable<TSource> source, Expression<Func<TSource, TResult>> selector, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.MaxAsync<TSource, TResult>(source.Visit(Evaluate.Action.READ), selector, cancellationToken);
        }

        public static Task<TSource> MinAsync<TSource>(this IQueryable<TSource> source, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.MinAsync<TSource>(source.Visit(Evaluate.Action.READ), cancellationToken);
        }

        public static Task<TResult> MinAsync<TSource, TResult>(this IQueryable<TSource> source, Expression<Func<TSource, TResult>> selector, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.MinAsync<TSource, TResult>(source.Visit(Evaluate.Action.READ), selector, cancellationToken);
        }

        public static Task<TSource> SingleAsync<TSource>(this IQueryable<TSource> source, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.SingleAsync<TSource>(source.Visit(Evaluate.Action.READ), cancellationToken);
        }

        public static Task<TSource> SingleAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.SingleAsync<TSource>(source.Visit(Evaluate.Action.READ), predicate, cancellationToken);
        }

        public static Task<TSource> SingleOrDefaultAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.SingleOrDefaultAsync<TSource>(source.Visit(Evaluate.Action.READ), predicate, cancellationToken);
        }

        public static Task<TSource> SingleOrDefaultAsync<TSource>(this IQueryable<TSource> source, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.SingleOrDefaultAsync<TSource>(source.Visit(Evaluate.Action.READ), cancellationToken);
        }

        public static Task<int> SumAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, int>> selector, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.SumAsync<TSource>(source.Visit(Evaluate.Action.READ), selector, cancellationToken);
        }

        public static Task<int?> SumAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, int?>> selector, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.SumAsync<TSource>(source.Visit(Evaluate.Action.READ), selector, cancellationToken);
        }

        public static Task<decimal?> SumAsync(this IQueryable<decimal?> source, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.SumAsync(source, cancellationToken);
        }

        public static Task<long> SumAsync(this IQueryable<long> source, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.SumAsync(source, cancellationToken);
        }

        public static Task<long?> SumAsync(this IQueryable<long?> source, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.SumAsync(source, cancellationToken);
        }

        public static Task<long> SumAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, long>> selector, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.SumAsync<TSource>(source.Visit(Evaluate.Action.READ), selector, cancellationToken);
        }

        public static Task<long?> SumAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, long?>> selector, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.SumAsync<TSource>(source.Visit(Evaluate.Action.READ), selector, cancellationToken);
        }

        public static Task<double> SumAsync(this IQueryable<double> source, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.SumAsync(source, cancellationToken);
        }

        public static Task<double?> SumAsync(this IQueryable<double?> source, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.SumAsync(source, cancellationToken);
        }

        public static Task<decimal> SumAsync(this IQueryable<decimal> source, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.SumAsync(source, cancellationToken);
        }

        public static Task<double?> SumAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, double?>> selector, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.SumAsync<TSource>(source.Visit(Evaluate.Action.READ), selector, cancellationToken);
        }

        public static Task<double> SumAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, double>> selector, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.SumAsync<TSource>(source.Visit(Evaluate.Action.READ), selector, cancellationToken);
        }

        public static Task<float> SumAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, float>> selector, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.SumAsync<TSource>(source, selector, cancellationToken);
        }

        public static Task<decimal> SumAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, decimal>> selector, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.SumAsync<TSource>(source.Visit(Evaluate.Action.READ), selector, cancellationToken);
        }

        public static Task<decimal?> SumAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, decimal?>> selector, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.SumAsync<TSource>(source.Visit(Evaluate.Action.READ), selector, cancellationToken);
        }

        public static Task<float> SumAsync(this IQueryable<float> source, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.SumAsync(source.Visit(Evaluate.Action.READ), cancellationToken);
        }

        public static Task<float?> SumAsync(this IQueryable<float?> source, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.SumAsync(source.Visit(Evaluate.Action.READ), cancellationToken);
        }

        public static Task<float?> SumAsync<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, float?>> selector, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.SumAsync<TSource>(source.Visit(Evaluate.Action.READ), selector, cancellationToken);
        }

        public static Task<int?> SumAsync(this IQueryable<int?> source, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.SumAsync(source, cancellationToken);
        }

        public static Task<int> SumAsync(this IQueryable<int> source, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.SumAsync(source, cancellationToken);
        }

        public static IQueryable<T> TagWith<T>(this IQueryable<T> source, [Microsoft.EntityFrameworkCore.Query.NotParameterized] string tag)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.TagWith<T>(source.Visit(Evaluate.Action.READ), tag);
        }

        public static Microsoft.EntityFrameworkCore.Query.IIncludableQueryable<TEntity, TProperty> ThenInclude<TEntity, TPreviousProperty, TProperty>(this Microsoft.EntityFrameworkCore.Query.IIncludableQueryable<TEntity, TPreviousProperty> source, Expression<Func<TPreviousProperty, TProperty>> navigationPropertyPath) where TEntity : class
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.ThenInclude<TEntity, TPreviousProperty, TProperty>(source.Visit(Evaluate.Action.READ), navigationPropertyPath);
        }

        public static Microsoft.EntityFrameworkCore.Query.IIncludableQueryable<TEntity, TProperty> ThenInclude<TEntity, TPreviousProperty, TProperty>(this Microsoft.EntityFrameworkCore.Query.IIncludableQueryable<TEntity, IEnumerable<TPreviousProperty>> source, Expression<Func<TPreviousProperty, TProperty>> navigationPropertyPath) where TEntity : class
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.ThenInclude<TEntity, TPreviousProperty, TProperty>(source.Visit(Evaluate.Action.READ), navigationPropertyPath);
        }

        public static Task<TSource[]> ToArrayAsync<TSource>(this IQueryable<TSource> source, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.ToArrayAsync<TSource>(source.Visit(Evaluate.Action.READ), cancellationToken);
        }

        public static Task<Dictionary<TKey, TSource>> ToDictionaryAsync<TSource, TKey>(this IQueryable<TSource> source, Func<TSource, TKey> keySelector, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.ToDictionaryAsync<TSource, TKey>(source.Visit(Evaluate.Action.READ), keySelector, cancellationToken);
        }

        public static Task<Dictionary<TKey, TSource>> ToDictionaryAsync<TSource, TKey>(this IQueryable<TSource> source, Func<TSource, TKey> keySelector, IEqualityComparer<TKey> comparer, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.ToDictionaryAsync<TSource, TKey>(source.Visit(Evaluate.Action.READ), keySelector, comparer, cancellationToken);
        }

        public static Task<Dictionary<TKey, TElement>> ToDictionaryAsync<TSource, TKey, TElement>(this IQueryable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.ToDictionaryAsync<TSource, TKey, TElement>(source.Visit(Evaluate.Action.READ), keySelector, elementSelector, cancellationToken);
        }

        public static Task<Dictionary<TKey, TElement>> ToDictionaryAsync<TSource, TKey, TElement>(this IQueryable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector, IEqualityComparer<TKey> comparer, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.ToDictionaryAsync<TSource, TKey, TElement>(source.Visit(Evaluate.Action.READ), keySelector, elementSelector, comparer, cancellationToken);

        }

        public static Task<List<TSource>> ToListAsync<TSource>(this IQueryable<TSource> source, CancellationToken cancellationToken = default)
        {
            return Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions.ToListAsync<TSource>(source.Visit(Evaluate.Action.READ), cancellationToken);
        }
    }
}