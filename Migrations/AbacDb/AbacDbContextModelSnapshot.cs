﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;
using PatientsRecordsManager.ABAC.Data;

namespace PatientsRecordsManager.Migrations.AbacDb
{
    [DbContext(typeof(AbacDbContext))]
    partial class AbacDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn)
                .HasAnnotation("ProductVersion", "3.1.1")
                .HasAnnotation("Relational:MaxIdentifierLength", 63);

            modelBuilder.Entity("Casbin.NET.Adapter.EFCore.CasbinRule<int>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<string>("PType")
                        .HasColumnType("text");

                    b.Property<string>("V0")
                        .HasColumnType("text");

                    b.Property<string>("V1")
                        .HasColumnType("text");

                    b.Property<string>("V2")
                        .HasColumnType("text");

                    b.Property<string>("V3")
                        .HasColumnType("text");

                    b.Property<string>("V4")
                        .HasColumnType("text");

                    b.Property<string>("V5")
                        .HasColumnType("text");

                    b.HasKey("Id");

                    b.ToTable("CasbinRule");
                });

            modelBuilder.Entity("PatientsRecordsManager.ABAC.Data.Models.DataAccessRequest", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<string>("Action")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<string[]>("Attributes")
                        .HasColumnType("text[]");

                    b.Property<DateTime>("DateOfAccess")
                        .HasColumnType("timestamp without time zone");

                    b.Property<string>("Object")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<int>("RequestId")
                        .HasColumnType("integer");

                    b.Property<bool>("Result")
                        .HasColumnType("boolean");

                    b.Property<string[]>("Roles")
                        .IsRequired()
                        .HasColumnType("text[]");

                    b.HasKey("Id");

                    b.HasIndex("RequestId");

                    b.ToTable("DataAccessRequests");
                });

            modelBuilder.Entity("PatientsRecordsManager.ABAC.Data.Models.Request", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<DateTime>("DateOfRequest")
                        .HasColumnType("timestamp without time zone");

                    b.Property<string>("Value")
                        .IsRequired()
                        .HasColumnType("text");

                    b.HasKey("Id");

                    b.ToTable("Requests");
                });

            modelBuilder.Entity("PatientsRecordsManager.ABAC.Data.Models.RuleAttributes", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<string>("Attributes")
                        .IsRequired()
                        .HasColumnType("text");

                    b.HasKey("Id");

                    b.ToTable("RuleAttributes");
                });

            modelBuilder.Entity("PatientsRecordsManager.ABAC.Data.Models.DataAccessRequest", b =>
                {
                    b.HasOne("PatientsRecordsManager.ABAC.Data.Models.Request", "Request")
                        .WithMany("Items")
                        .HasForeignKey("RequestId");
                });
#pragma warning restore 612, 618
        }
    }
}
