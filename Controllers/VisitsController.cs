using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PatientsRecordsManager.Constants;
using PatientsRecordsManager.Constants.Enums;
using PatientsRecordsManager.Data.Dtos.Visits;
using PatientsRecordsManager.Data.Repositories;
using PatientsRecordsManager.Extensions;
using PatientsRecordsManager.Resources;

namespace PatientsRecordsManager.Controllers
{
    [ApiController]
    [Route(APIs.DefaultRoute)]
    [Authorize]
    public class VisitsController : PrmController
    {
        private readonly IMapper _mapper;
        private readonly IVisitRepository _repository;
        private readonly Messages _messages;
        private readonly ILogger<VisitsController> _logger;

        public VisitsController(IMapper mapper, IVisitRepository repository, Messages messages, ILogger<VisitsController> logger)
        {
            _logger = logger;
            _messages = messages;
            _repository = repository;
            _mapper = mapper;
        }

        [HttpPost(APIs.AddVisit)]
        [Authorize(Roles = RolesNames.GetStaffersCommaDelimited)]
        public IActionResult AddVisit(ServiceProviderStafferType stType, VisitForCreateDto visitForCreate)
        {
            return HttpContext.Success(
                _repository.AddVisit(HttpContext.GetUserId(), stType, visitForCreate),
                _messages.GetString("VisitCreated"));
        }

        [HttpGet(APIs.GetVisits)]
        public IActionResult GetVisits([FromQuery] VisitType visitType)
        {
            return HttpContext.Success(_repository.GetVisits(visitType), _messages.GetString("VisitsFetched"));
        }

        [HttpGet(APIs.GetVisit)]
        public IActionResult GetVisit(string visitCode)
        {
            return HttpContext.Success(_repository.GetVisitDetails(visitCode), _messages.GetString("VisitFetched"));
        }

        [HttpDelete(APIs.DeleteVisit)]
        [Authorize(Roles = RolesNames.Administrator)]
        public IActionResult RemoveVisit(ServiceProviderStafferType stType, string stCode, string visitCode)
        {
            _logger.LogCritical(stCode);
            return HttpContext.Success(_repository.RemoveVisit(HttpContext.GetUserId(), stType, visitCode), _messages.GetString("VisitDeleted"));
        }

        [HttpPut(APIs.ModifyVisit)]
        [Authorize(Roles = RolesNames.GetStaffersCommaDelimited)]
        public IActionResult ModifyVisit(ServiceProviderStafferType stType, string visitCode, VisitForUpdateDto visitForUpdate)
        {
            return HttpContext.Success(_repository.ModifyVisit(HttpContext.GetUserId(), stType, visitCode, visitForUpdate), _messages.GetString("VisitUpdated"));
        }
    }
}