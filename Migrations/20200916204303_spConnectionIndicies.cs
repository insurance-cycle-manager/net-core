﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PatientsRecordsManager.Migrations
{
    public partial class spConnectionIndicies : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_InsuranceConnections_UserEmails_InsuranceCompanyEmailId",
                table: "InsuranceConnections");

            migrationBuilder.DropForeignKey(
                name: "FK_InsuranceConnections_UserEmails_ThirdPartyEmailId",
                table: "InsuranceConnections");

            migrationBuilder.DropIndex(
                name: "IX_ServiceProviderConnections_TpaId",
                table: "ServiceProviderConnections");

            migrationBuilder.DropIndex(
                name: "IX_InsuranceConnections_InsuranceCompanyEmailId",
                table: "InsuranceConnections");

            migrationBuilder.DropIndex(
                name: "IX_InsuranceConnections_ThirdPartyEmailId",
                table: "InsuranceConnections");

            migrationBuilder.DropColumn(
                name: "InsuranceCompanyEmailId",
                table: "InsuranceConnections");

            migrationBuilder.DropColumn(
                name: "ThirdPartyEmailId",
                table: "InsuranceConnections");

            migrationBuilder.AlterColumn<string>(
                name: "InsuranceConnectionCode",
                table: "InsuranceConnections",
                nullable: false,
                defaultValueSql: "'ICC'|| nextval('public.\"InsuranceConnectionsNumbers\"')",
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true,
                oldDefaultValueSql: "'ICC'|| nextval('public.\"InsuranceConnectionsNumbers\"')");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceStaffers_UserId_Type",
                table: "ServiceStaffers",
                columns: new[] { "UserId", "Type" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ServiceProviders_UserId_Type",
                table: "ServiceProviders",
                columns: new[] { "UserId", "Type" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ServiceProviderConnections_TpaId_ServiceProviderId",
                table: "ServiceProviderConnections",
                columns: new[] { "TpaId", "ServiceProviderId" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_ServiceStaffers_UserId_Type",
                table: "ServiceStaffers");

            migrationBuilder.DropIndex(
                name: "IX_ServiceProviders_UserId_Type",
                table: "ServiceProviders");

            migrationBuilder.DropIndex(
                name: "IX_ServiceProviderConnections_TpaId_ServiceProviderId",
                table: "ServiceProviderConnections");

            migrationBuilder.AlterColumn<string>(
                name: "InsuranceConnectionCode",
                table: "InsuranceConnections",
                type: "text",
                nullable: true,
                defaultValueSql: "'ICC'|| nextval('public.\"InsuranceConnectionsNumbers\"')",
                oldClrType: typeof(string),
                oldDefaultValueSql: "'ICC'|| nextval('public.\"InsuranceConnectionsNumbers\"')");

            migrationBuilder.AddColumn<int>(
                name: "InsuranceCompanyEmailId",
                table: "InsuranceConnections",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ThirdPartyEmailId",
                table: "InsuranceConnections",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_ServiceProviderConnections_TpaId",
                table: "ServiceProviderConnections",
                column: "TpaId");

            migrationBuilder.CreateIndex(
                name: "IX_InsuranceConnections_InsuranceCompanyEmailId",
                table: "InsuranceConnections",
                column: "InsuranceCompanyEmailId");

            migrationBuilder.CreateIndex(
                name: "IX_InsuranceConnections_ThirdPartyEmailId",
                table: "InsuranceConnections",
                column: "ThirdPartyEmailId");

            migrationBuilder.AddForeignKey(
                name: "FK_InsuranceConnections_UserEmails_InsuranceCompanyEmailId",
                table: "InsuranceConnections",
                column: "InsuranceCompanyEmailId",
                principalTable: "UserEmails",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_InsuranceConnections_UserEmails_ThirdPartyEmailId",
                table: "InsuranceConnections",
                column: "ThirdPartyEmailId",
                principalTable: "UserEmails",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
