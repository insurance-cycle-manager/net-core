using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using PatientsRecordsManager.ABAC.Extensions;
using PatientsRecordsManager.ABAC.Extensions.EF;
using PatientsRecordsManager.Constants;
using PatientsRecordsManager.Data;
using PatientsRecordsManager.Data.Dtos.InsuranceCompanies.Packages;
using PatientsRecordsManager.Data.Dtos.Patients.Packages;
using PatientsRecordsManager.Exceptions;
using PatientsRecordsManager.Extensions;
using PatientsRecordsManager.Models;
using PatientsRecordsManager.Resources;
using PatientsRecordsManager.Utils;
using PatientsRecordsManager.Utils.Exceptions;

namespace PatientsRecordsManager.Controllers
{
    [Route(APIs.InsurancePackagesBaseUrl)]
    [ApiController]
    [Authorize]
    public class InsurancePackagesController : PrmController
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;
        private readonly Messages _messages;

        public InsurancePackagesController(DataContext dataContext, IMapper mapper, Messages messages)
        {
            _messages = messages;
            _mapper = mapper;
            _dataContext = dataContext;
        }

        [HttpPost(APIs.AddInsurancePackage)]
        [Authorize(Roles = RolesNames.Administrator)]
        public async Task<IActionResult> AddInsurancePackage(string companyCode, InsurancePackageForCreateDto packageForCreate)
        {
            Assert.NotNull(packageForCreate);
            InsurancePackage package;

            var ic = await _dataContext.InsuranceCompanies
                .FirstOrDefaultAsync(c => c.InsuranceCompanyCode.ToUpper() == companyCode.ToUpper())
                .ConfigureAwait(false);

            if (ic == null)
            {
                throw new KeyNotFoundException(_messages.GetString("InsuranceCompanyNotFound"));
            }

            package = _mapper.Map<InsurancePackage>(packageForCreate);
            package.InsuranceCompany = ic;
            package.PackageDate = packageForCreate.PackageDate ?? DateTime.Now;
            await AddDrugsSurgeriesRadiographsLabTests(packageForCreate, package)
                .ConfigureAwait(false);

            await _dataContext.InsurancePackages
                .AddAsync(package).ConfigureAwait(false);
            if (await _dataContext.SaveChangesAsync().ConfigureAwait(false) < 1)
            {
                throw new Exception(_messages.GetString("InsurancePackageCreateError"));
            }

            return HttpContext.Success(_mapper.Map<InsurancePackageForReturnDto>(package), _messages.GetString("InsurancePackageCreated"));
        }

        private async Task AddDrugsSurgeriesRadiographsLabTests(InsurancePackageForCreateDto packageForCreate, InsurancePackage package)
        {
            await AddDrugs(packageForCreate, package).ConfigureAwait(false);
            await AddSurgeries(packageForCreate, package).ConfigureAwait(false);
            await AddRadiographs(packageForCreate, package).ConfigureAwait(false);
            await AddLabTests(packageForCreate, package).ConfigureAwait(false);
        }

        private async Task AddLabTests(InsurancePackageForCreateDto packageForCreate, InsurancePackage package)
        {
            var labTests = await _dataContext.LabTests
                            .Where(lt => packageForCreate.LabTests.Contains(lt.LabTestCode))
                            .ToListAsync().ConfigureAwait(false);

            labTests.ForEach(lt => _dataContext.PackageLabTests.Add(
                new PackageLabTest
                {
                    InsurancePackage = package,
                    LabTest = lt
                }
            ));
        }

        private async Task AddRadiographs(InsurancePackageForCreateDto packageForCreate, InsurancePackage package)
        {
            var radiographs = await _dataContext.Radiographs
                            .Where(r => packageForCreate.Radiographs.Contains(r.RadiographCode))
                            .ToListAsync().ConfigureAwait(false);

            radiographs.ForEach(r => _dataContext.PackageRadiographs.Add(
                new PackageRadiograph
                {
                    Radiograph = r,
                    InsurancePackage = package
                }
                )
            );
        }

        private async Task AddSurgeries(InsurancePackageForCreateDto packageForCreate, InsurancePackage package)
        {
            var surgeries = await _dataContext.Surgeries
                                        .Where(s => packageForCreate.Surgeries.Contains(s.SurgeryCode))
                                        .ToListAsync().ConfigureAwait(false);

            surgeries.ForEach(s => _dataContext.PackageSrugeries.Add(
                new PackageSurgery
                {
                    Surgery = s,
                    InsurancePackage = package
                }
            ));
        }
        private async Task AddDrugs(InsurancePackageForCreateDto packageForCreate, InsurancePackage package)
        {
            var drugs = await _dataContext.Drugs
                            .Where(d => packageForCreate.Drugs.Contains(d.DrugCode))
                            .ToListAsync().ConfigureAwait(false);

            drugs.ForEach(drug => _dataContext.PackageDrugs.Add(
                new PackageDrug
                {
                    InsurancePackage = package,
                    Drug = drug
                }
            ));
        }

        [HttpGet(APIs.GetPackages)]
        public async Task<IActionResult> GetInsurancePackages()
        {
            var packages = from p in
                 (await _dataContext.InsurancePackages
                 .Include(pkg => pkg.Price)
                .Include(pkg => pkg.InsuranceCompany)
                .Include(pkg => pkg.Drugs)
                .ThenInclude(pd => pd.Drug)
                .Include(pkg => pkg.LabTests)
                .ThenInclude(lt => lt.LabTest)
                .Include(pkg => pkg.Radiographs)
                .ThenInclude(r => r.Radiograph)
                .Include(pkg => pkg.Surgeries)
                .ThenInclude(s => s.Surgery)
                .ToListAsync()
                .ConfigureAwait(false))
                           select _mapper.Map<InsurancePackageForReturnDto>(p);

            return HttpContext.Success(packages, _messages.GetString("InsurancePackagesFetched"));
        }

        [HttpGet(APIs.GetInsurancePackage)]
        public async Task<IActionResult> GetInsurancePackage(string companyCode, string packageCode)
        {
            InsuranceCompany ic;
            InsurancePackage package;

            ic = await _dataContext.InsuranceCompanies
                .Include(i => i.InsurancePackages)
                .FirstOrDefaultAsync(c => c.InsuranceCompanyCode.ToUpper() == companyCode.ToUpper())
                .ConfigureAwait(false);

            if (ic == null)
            {
                throw new KeyNotFoundException(_messages.GetString("InsuranceCompanyNotFound"));
            }

            package = ic.InsurancePackages
                    .AsQueryable<InsurancePackage>()
                    .Include(pkg => pkg.Price)
                    .Include(pkg => pkg.Drugs)
                    .ThenInclude(pd => pd.Drug)
                    .Include(pkg => pkg.LabTests)
                    .ThenInclude(lt => lt.LabTest)
                    .Include(pkg => pkg.Radiographs)
                    .ThenInclude(r => r.Radiograph)
                    .Include(pkg => pkg.Surgeries)
                    .ThenInclude(s => s.Surgery)
                    .FirstOrDefault(p => p.InsurancePackageCode.ToUpper() == packageCode.ToUpper());

            if (package == null)
            {
                throw new KeyNotFoundException(_messages.GetString("InsurancePackageNotFound"));
            }

            return HttpContext.Success(
                _mapper.Map<InsurancePackageForReturnDto>(package),
                _messages.GetString("InsurancePackagesFetched"));
        }

        [HttpDelete(APIs.DeleteInsurancePackage)]
        [Authorize(Roles = RolesNames.Administrator)]
        public async Task<IActionResult> DeleteInsurancePackage(string companyCode, string packageCode)
        {
            InsuranceCompany ic; InsurancePackage package;

            ic = await _dataContext.InsuranceCompanies
                .FirstOrDefaultAsync(c => c.InsuranceCompanyCode.ToUpper() == companyCode.ToUpper())
                .ConfigureAwait(false);

            if (ic == null)
            {
                throw new KeyNotFoundException(_messages.GetString("InsuranceCompanyNotFound"));
            }

            package = await _dataContext.InsurancePackages
                .FirstOrDefaultAsync(p => p.InsuranceCompany == ic && p.InsurancePackageCode == packageCode)
                .ConfigureAwait(false);

            if (package == null)
            {
                throw new KeyNotFoundException(_messages.GetString("InsurancePackageNotFound"));
            }
            _dataContext.InsurancePackages.Remove(package);

            if (await _dataContext.SaveChangesAsync().ConfigureAwait(false) < 1)
            {
                throw new Exception(_messages.GetString("InsurancePackageDeleteError"));
            }

            return HttpContext.Success(
                new { PackageCode = packageCode, InsuranceCompany = companyCode },
                _messages.GetString("InsurancePackageDeleted"));
        }

        [HttpPut(APIs.ModifyInsurancePackage)]
        [Authorize(Roles = RolesNames.Administrator)]
        public async Task<IActionResult> ModifyInsurancePackage(string companyCode,
                                    string packageCode,
                                    [FromBody] InsurancePackageForUpdateDto newPackageForUpdate)
        {
            Assert.NotNull(newPackageForUpdate);

            InsurancePackage package;
            var ic = await _dataContext.InsuranceCompanies
                .FirstOrDefaultAsync(c => c.InsuranceCompanyCode.ToUpper() == companyCode.ToUpper())
                .ConfigureAwait(false);

            if (ic == null) throw new KeyNotFoundException(message: _messages.GetString("InsuranceCompanyNotFound"));

            package = await _dataContext.InsurancePackages
            .Include(pkg => pkg.Price)
            .Include(pkg => pkg.Drugs)
            .ThenInclude(pd => pd.Drug)
            .Include(pkg => pkg.LabTests)
            .ThenInclude(lt => lt.LabTest)
            .Include(pkg => pkg.Radiographs)
            .ThenInclude(r => r.Radiograph)
            .Include(pkg => pkg.Surgeries)
            .ThenInclude(s => s.Surgery)
            .FirstOrDefaultAsync(p => p.InsuranceCompany == ic && p.InsurancePackageCode == packageCode)
            .ConfigureAwait(false);

            if (package == null) throw new KeyNotFoundException(message: _messages.GetString("InsurancePackageNotFound"));


            var updatedPackage = _mapper.Map<InsurancePackage>(newPackageForUpdate);
            package.PackageDate = updatedPackage.PackageDate;


            UpdatePrice(newPackageForUpdate, package, updatedPackage);
            await UpdateDrugsSurgeriesRadiographsLabTests(package, newPackageForUpdate).ConfigureAwait(false);

            if (await _dataContext.SaveChangesAsync().ConfigureAwait(false) < 1)
                throw new Exception(_messages.GetString("InsurancePackageUpdateError"));

            return HttpContext.Success(newPackageForUpdate, _messages.GetString("InsurancePackageUpdated"));
        }

        [HttpGet(APIs.GetIcPackages)]
        public async Task<IActionResult> GetIcPackages(string companyCode)
        {
            InsuranceCompany ic = await _dataContext.InsuranceCompanies
                .FirstOrDefaultAsync(c => c.InsuranceCompanyCode.ToUpper() == (companyCode.ToUpper()))
                .ConfigureAwait(false);

            if (ic == null) throw new KeyNotFoundException(_messages.GetString("InsuranceCompanyNotFound"));

            var packages = await _dataContext.InsurancePackages
                .Include(pkg => pkg.Price)
                .Include(pkg => pkg.Drugs)
                .ThenInclude(pd => pd.Drug)
                .Include(pkg => pkg.LabTests)
                .ThenInclude(lt => lt.LabTest)
                .Include(pkg => pkg.Radiographs)
                .ThenInclude(r => r.Radiograph)
                .Include(pkg => pkg.Surgeries)
                .ThenInclude(s => s.Surgery)
                .Where(p => p.InsuranceCompany == ic)
                .Select(p => _mapper.Map<InsurancePackageForReturnDto>(p))
                .ToListAsync().ConfigureAwait(false);

            return HttpContext.Success(packages, _messages.GetString("InsurancePackagesFetched"));
        }

        [HttpGet(APIs.GetPackagePatients)]
        public IActionResult GetPackagePatients(string companyCode, string packageCode)
        {
            var ic = _dataContext.InsuranceCompanies
                .Include(ic => ic.InsurancePackages)
                .ThenInclude(p => p.PatientPackages)
                .ThenInclude(pp => pp.Patient)
                .FirstOrDefault(ic => ic.InsuranceCompanyCode.ToUpper() == (companyCode.ToUpper()) && ic.UserId == HttpContext.GetUserId());

            if (ic == null)
                throw new NoDataFoundException(_messages.GetString("InsuranceCompanyNotFound"));

            var package = ic.InsurancePackages
                .FirstOrDefault(p => p.InsurancePackageCode.ToUpper() == (packageCode.ToUpper()));

            if (package == null)
                throw new NoDataFoundException(_messages.GetString("InsurancePackageNotFound"));

            return HttpContext.Success(
                _mapper.Map<IEnumerable<PatientPackageForReturnDto>>(package.PatientPackages),
                _messages.GetString("patientPackageFetchDone"));
        }

        [HttpPut(APIs.ManagePatientPackage)]
        public IActionResult ManagePatientPackage(ManagePatientPackageRequestDto manage)
        {
            Assert.NotNull(manage);
            int userId = HttpContext.GetUserId();

            var ic = _dataContext.InsuranceCompanies
                .Include(ic => ic.InsurancePackages)
                .ThenInclude(icp => icp.PatientPackages)
                .ThenInclude(pp => pp.Patient)
                .FirstOrDefault(ic => ic.UserId == userId);
            if (ic == null)
                throw new NoDataFoundException(_messages.GetString("InsuranceCompanyNotFound"));

            var icp = ic.InsurancePackages
                .Where(icp => icp.InsurancePackageCode.ToUpper() == (manage.PackageCode.ToUpper()))
                .FirstOrDefault();
            if (icp == null)
                throw new NoDataFoundException(_messages.GetString("InsurancePackageNotFound"));

            var pp = icp.PatientPackages
                .Where(pp => pp.Patient.PatientCode.ToUpper() == (manage.PatientCode.ToUpper()))
                .FirstOrDefault();
            if (pp == null)
                throw new NoDataFoundException(_messages.GetString("PatientPackageNotFound", manage.PatientCode, manage.PackageCode));

            if (manage.Status.Value != Constants.Enums.PatientPackageStatus.Approved && manage.Status.Value != Constants.Enums.PatientPackageStatus.Rejected)
            {
                throw new ArgumentBadException(_messages.GetString("PatientPackageStatusBad"));
            }

            pp.Status = manage.Status.Value;

            if (_dataContext.SaveChanges() < 1)
            {
                throw new SaveChangesFailedException();
            }

            return HttpContext.Success(
                _mapper.Map<PatientPackageForReturnDto>(pp),
                _messages.GetString("patientPackageManageDone"));
        }

        private void UpdatePrice(InsurancePackageForUpdateDto newPackageForUpdate, InsurancePackage package, InsurancePackage updatedPackage)
        {
            if (newPackageForUpdate.Price != null)
            {
                var price = _mapper.Map<Price>(newPackageForUpdate.Price);
                _dataContext.Prices.Add(price);
                updatedPackage.Price = price;
                package.Price = updatedPackage.Price;
            }
        }

        private async Task UpdateDrugsSurgeriesRadiographsLabTests(InsurancePackage package, InsurancePackageForUpdateDto newPackageForUpdate)
        {
            await UpdateSurgeries(package, newPackageForUpdate).ConfigureAwait(false);
            await UpdateRadiographs(package, newPackageForUpdate).ConfigureAwait(false);
            await UpdateDrugs(package, newPackageForUpdate).ConfigureAwait(false);
            await UpdateLabTests(package, newPackageForUpdate).ConfigureAwait(false);
        }

        private async Task UpdateSurgeries(InsurancePackage package, InsurancePackageForUpdateDto newPackageForUpdate)
        {
            var newSurgeries = package.Surgeries
                .Select(ps => ps.Surgery.SurgeryCode)
                .Intersect(newPackageForUpdate.Surgeries);

            var surgeries = await _dataContext.Surgeries
                .Where(s => newSurgeries.Contains(s.SurgeryCode)).ToListAsync().ConfigureAwait(false);

            surgeries.ForEach(s => _dataContext.PackageSrugeries.Add(
               new PackageSurgery
               {
                   Surgery = s,
                   InsurancePackage = package
               }
           ));

        }

        private async Task UpdateRadiographs(InsurancePackage package, InsurancePackageForUpdateDto newPackageForUpdate)
        {
            var newRadiographs = package.Radiographs
                .Select(pr => pr.Radiograph.RadiographCode)
                .Intersect(newPackageForUpdate.Radiographs);

            var radiographs = await _dataContext.Radiographs
                .Where(r => newRadiographs.Contains(r.RadiographCode)).ToListAsync().ConfigureAwait(false);

            radiographs.ForEach(r => _dataContext.PackageRadiographs.Add(
               new PackageRadiograph
               {
                   Radiograph = r,
                   InsurancePackage = package
               }
           ));

        }

        private async Task UpdateDrugs(InsurancePackage package, InsurancePackageForUpdateDto newPackageForUpdate)
        {
            var newDrugs = package.Drugs
                .Select(pd => pd.Drug.DrugCode)
                .Intersect(newPackageForUpdate.Drugs);

            var drugs = await _dataContext.Drugs
                .Where(d => newDrugs.Contains(d.DrugCode)).ToListAsync().ConfigureAwait(false);

            drugs.ForEach(d => _dataContext.PackageDrugs.Add(
               new PackageDrug
               {
                   Drug = d,
                   InsurancePackage = package
               }
           ));

        }

        private async Task UpdateLabTests(InsurancePackage package, InsurancePackageForUpdateDto newPackageForUpdate)
        {
            var newLabTests = package.LabTests
                .Select(plt => plt.LabTest.LabTestCode)
                .Intersect(newPackageForUpdate.LabTests);

            var drugs = await _dataContext.LabTests
                .Where(lt => newLabTests.Contains(lt.LabTestCode)).ToListAsync().ConfigureAwait(false);

            drugs.ForEach(plt => _dataContext.PackageLabTests.Add(
               new PackageLabTest
               {
                   LabTest = plt,
                   InsurancePackage = package
               }
           ));

        }
    }
}