using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PatientsRecordsManager.Models
{
    public class PrescribedRadiograph
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string PrescribedRadiographCode { get; set; }

        [Required]
        public int VisitId { get; set; }
        public Visit Visit { get; set; }

        [Required]
        public int RadiographId { get; set; }
        public Radiograph Radiograph { get; set; }

        [Required]
        public DateTime ScheduledDate { get; set; }

        public string Note { get; set; }

        public List<RadiographPurchase> Purchases { get; set; }
    }
}