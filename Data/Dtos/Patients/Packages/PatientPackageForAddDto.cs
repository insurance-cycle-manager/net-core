using System.ComponentModel.DataAnnotations;

namespace PatientsRecordsManager.Data.Dtos.Patients.Packages
{
    public class PatientPackageForAddDto
    {
        [Required]
        public string PackageCode { get; set; }
    }
}