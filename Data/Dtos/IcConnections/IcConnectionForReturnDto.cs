using PatientsRecordsManager.Data.Dtos.InsuranceCompanies;
using PatientsRecordsManager.Data.Dtos.TPAs;

namespace PatientsRecordsManager.Data.Dtos.InsuranceConnections
{
    public class IcConnectionForReturnDto
    {
        public string ConnectionCode { get; set; }
        public TpaForReturnDto ThirdPartyAdministrator { get; set; }
        public InsuranceCompanyForReturnDto InsuranceCompany { get; set; }
    }
}