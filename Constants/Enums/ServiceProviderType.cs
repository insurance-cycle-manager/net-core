namespace PatientsRecordsManager.Constants.Enums
{
    public enum ServiceProviderType
    {
        Hospital,
        Pharmacy,
        Laboratory,
        RadiographOffice,
        Clinic
    }
}