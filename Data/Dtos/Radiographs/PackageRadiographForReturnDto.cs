namespace PatientsRecordsManager.Data.Dtos.Radiographs
{
    public class PackageRadiographForReturnDto
    {
        public string RadiographCode { get; set; }
    }
}