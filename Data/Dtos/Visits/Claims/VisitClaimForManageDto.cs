using System.ComponentModel.DataAnnotations;
using PatientsRecordsManager.Constants.Enums;

namespace PatientsRecordsManager.Data.Dtos.Visits.Claims
{
    public class VisitClaimForManageDto
    {
        [Required]
        public ClaimStatus? Status { get; set; }
    }
}