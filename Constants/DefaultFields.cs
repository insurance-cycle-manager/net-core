using PatientsRecordsManager.Constants.Enums;
using PatientsRecordsManager.Models;

namespace PatientsRecordsManager.Constants
{
    public static class DefaultFields
    {
        public const int DefaultSupervisoryCommision = 1;
        public static Status InactiveStatus { get { return new Status { Name = StatusType.InActive }; } }
    }
}