using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using PatientsRecordsManager.Constants.Enums;

namespace PatientsRecordsManager.Models
{
    public class Visit : Data.PrmModel
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string VisitCode { get; set; }

        public int? BaseVisitId { get; set; }
        public Visit BaseVisit { get; set; }

        public int? VisitClaimId { get; set; }
        public VisitClaim VisitClaim { get; set; }

        [Required]
        public int PatientId { get; set; }
        public Patient Patient { get; set; }

        [Required]
        [ForeignKey("ServiceProviders")]
        public int ServiceProviderId { get; set; }
        public ServiceProvider ServiceProvider { get; set; }

        [Required]
        public int ServiceStafferId { get; set; }
        public ServiceStaffer ServiceStaffer { get; set; }

        public ServiceProviderStaffer ServiceProviderStaffer { get; set; }

        [Required]
        public int ServiceProviderConnectionId { get; set; }
        public ServiceProviderConnection ServiceProviderConnection { get; set; }

        [Required]
        public int PatientPackageId { get; set; }
        public PatientPackage PatientPackage { get; set; }

        [Required]
        [EnumDataType(typeof(VisitType))]
        public VisitType Type { get; set; }

        [Required]
        public double Cost { get; set; }

        [Required]
        public DateTime VisitDate { get; set; }

        [ForeignKey("Diagnosises")]
        public int? DiagnosisId { get; set; }
        public VisitDiagnosis Diagnosis { get; set; }

        public List<Visit> SubVisits { get; set; }
        public List<Prescription> Prescriptions { get; set; }
        public List<PrescribedSurgery> Surgeries { get; set; }
        public List<PrescribedLabTest> LabTests { get; set; }
        public List<PrescribedRadiograph> Radiographs { get; set; }

        public List<RadiographPurchase> RadiographPurchases { get; set; }
        public List<SurgeryPurchase> SurgeryPurchases { get; set; }
        public List<LabTestPurchase> LabTestPurchases { get; set; }
        public List<PrescriptionPurchase> PrescriptionPurchases { get; set; }
    }
}