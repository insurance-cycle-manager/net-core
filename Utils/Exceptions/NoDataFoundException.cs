using System;
using System.Collections.Generic;
using System.Globalization;

namespace PatientsRecordsManager.Utils.Exceptions
{
    public class NoDataFoundException : KeyNotFoundException
    {
        public NoDataFoundException() : base()
        {
        }

        public NoDataFoundException(string message) : base(string.Format(CultureInfo.CurrentCulture, "Data not found {0}", message))
        {
        }

        public NoDataFoundException(string message, Exception inner) : base(string.Format(CultureInfo.CurrentCulture, "Data not found {0}", message), inner)
        {
        }
    }
}