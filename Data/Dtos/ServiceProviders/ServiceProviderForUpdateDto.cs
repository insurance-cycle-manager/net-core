using System.ComponentModel.DataAnnotations;

namespace PatientsRecordsManager.Data.Dtos.ServiceProviders
{
    public class ServiceProviderForUpdateDto
    {
        [Required]
        public string Title { get; set; }
        public string UserAddressCode { get; set; }
        public string UserEmailCode { get; set; }
        public string UserPhoneCode { get; set; }
    }
}