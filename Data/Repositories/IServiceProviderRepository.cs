using System.Collections.Generic;
using PatientsRecordsManager.Constants.Enums;
using PatientsRecordsManager.Data.Dtos.ServiceProviders;

namespace PatientsRecordsManager.Data.Repositories
{
    public interface IServiceProviderRepository
    {
        List<ServiceProviderForReturnDto> GetServiceProviders(ServiceProviderType type, string title = null);
        ServiceProviderForReturnDto GetServiceProviderDetails(ServiceProviderType type, string code);
        ServiceProviderForReturnDto GetServiceProviderDetails(ServiceProviderType type, int userId);
        ServiceProviderForReturnDto AddServiceProvider(ServiceProviderForCreateDto serviceProviderForCreate);
        ServiceProviderForReturnDto ModifyServiceProvider(ServiceProviderType type, string code, ServiceProviderForUpdateDto serviceProviderForUpdate);
        ServiceProviderForReturnDto ModifyServiceProvider(ServiceProviderType type, int userId, ServiceProviderForUpdateDto serviceProviderForUpdate);
        ServiceProviderForReturnDto DeleteServiceProvider(ServiceProviderType type, int userId);
        ServiceProviderForReturnDto ChangeServiceProviderStatus(ServiceProviderType type, string code, StatusType status);
    }
}