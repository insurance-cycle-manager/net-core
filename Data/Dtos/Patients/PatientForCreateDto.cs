using System;
using System.ComponentModel.DataAnnotations;
using PatientsRecordsManager.Constants.Enums;
using PatientsRecordsManager.Data.Dtos.Addresses;

namespace PatientsRecordsManager.Data.Dtos.Patients
{
    public class PatientForCreateDto
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public DateTime DateOfBirth { get; set; }
        [EnumDataType(typeof(Gender))]
        [Required]
        public Gender Gender { get; set; }
        [Required]
        public int AddressId { get; set; }
        [Required]
        public int EmailId { get; set; }
        [Required]
        public int PhoneNumberId { get; set; }
        [Required]
        public int UserId { get; set; }
        [Required]
        public int RoleId { get; set; }
    }
}