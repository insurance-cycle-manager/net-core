using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using PatientsRecordsManager.ABAC.Extensions;
using PatientsRecordsManager.ABAC.Extensions.EF;
using PatientsRecordsManager.Constants.Enums;
using PatientsRecordsManager.Data.Dtos.ServiceProviders.Connections;
using PatientsRecordsManager.Exceptions;
using PatientsRecordsManager.Models;
using PatientsRecordsManager.Resources;
using PatientsRecordsManager.Utils;
using PatientsRecordsManager.Utils.Exceptions;

namespace PatientsRecordsManager.Data.Repositories
{
    public class SpConnectionsRepository : ISpConnectionsRepository
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        private readonly Messages _messages;

        public SpConnectionsRepository(DataContext context, IMapper mapper, Messages messages)
        {
            _context = context;
            _mapper = mapper;
            _messages = messages;
        }

        public SpConnectionForReturnDto AddConnectionBySp(int providerId, SpConnectionForAddDto connection)
        {
            Assert.NotNull(connection);

            var provider = _context.ServiceProviders
                .FirstOrDefault(sp => sp.ServiceProviderCode.ToUpper() == (connection.ServiceProviderCode.ToUpper()) && sp.UserId == providerId);

            if (provider == null)
                throw new NoDataFoundException(_messages.GetString("serviceProviderNotFound", providerId));

            return AddConnection(provider, GetTpaByCode(connection.TpaCode), SpConnectionStatus.Pending1, connection.CreatedAt, connection.InsuranceCompanies);
        }

        public SpConnectionForReturnDto AddConnectionByTpa(int tpaId, SpConnectionForAddDto connection)
        {
            Assert.NotNull(connection);

            var tpa = _context.ThirdPartyAdministrators
                .Include(t => t.InsuranceConnections)
                .ThenInclude(c => c.InsuranceCompany)
                .FirstOrDefault(t => t.ThirdPartyCode.ToUpper() == (connection.TpaCode.ToUpper()) && t.UserId == tpaId);

            if (tpa == null)
                throw new NoDataFoundException(_messages.GetString("TpaNotFound", tpaId));

            return AddConnection(GetSpByCode(connection.ServiceProviderCode), tpa, SpConnectionStatus.Pending2, connection.CreatedAt, connection.InsuranceCompanies);
        }

        private SpConnectionForReturnDto AddConnection(ServiceProvider sp,
                                                       ThirdPartyAdministrator tpa,
                                                       SpConnectionStatus status,
                                                       DateTime createdAt,
                                                       IEnumerable<string> icsCodes)
        {
            var connection = new ServiceProviderConnection
            {
                Status = status,
                Tpa = tpa,
                ServiceProvider = sp,
                CreatedAt = createdAt,
                InsuranceCompanies = GenerateIcConnections(tpa, icsCodes)
            };
            _context.ServiceProviderConnections.Add(connection);

            if (_context.SaveChanges() <= 0)
            {
                throw new SaveChangesFailedException();
            }
            return _mapper.Map<SpConnectionForReturnDto>(connection);
        }

        public IEnumerable<SpConnectionForReturnDto> GetConnections(SpConnectionStatus? status, string sp, string tpa)
        {
            return GetConnections(status, sp, null, null, null, tpa, null);
        }

        public IEnumerable<SpConnectionForReturnDto> GetConnections(int providerId, ServiceProviderType type)
        {
            return GetConnections(null, null, null, providerId, type, null, null);
        }

        public IEnumerable<SpConnectionForReturnDto> GetConnections(string providerCode, ServiceProviderType type)
        {
            return GetConnections(null, null, providerCode, null, type, null, null);
        }

        public IEnumerable<SpConnectionForReturnDto> GetConnections(int tpaId)
        {
            return GetConnections(null, null, null, null, null, null, tpaId);
        }

        private IEnumerable<SpConnectionForReturnDto> GetConnections(SpConnectionStatus? status,
                                                                     string providerTitle,
                                                                     string providerCode,
                                                                     int? providerId,
                                                                     ServiceProviderType? type,
                                                                     string tpaName,
                                                                     int? tpaId)
        {
            var connections = _context.ServiceProviderConnections
                .Include(c => c.ServiceProvider)
                .Include(c => c.Tpa)
                .AsQueryable();

            if (status != null)
                connections = connections.Where(c => c.Status == status);

            if (providerTitle != null)
                connections = connections.Where(c => c.ServiceProvider.Title.ToUpper().Contains(providerTitle.ToUpper()));
            if (providerCode != null)
                connections = connections.Where(c => c.ServiceProvider.ServiceProviderCode == providerCode);
            if (providerId != null)
                connections = connections.Where(c => c.ServiceProvider.UserId == providerId);
            if (type != null)
                connections = connections.Where(c => c.ServiceProvider.Type == type);

            if (tpaName != null)
                connections = connections.Where(c => c.Tpa.Name.ToUpper() == (tpaName.ToUpper()));
            if (tpaId != null)
                connections = connections.Where(c => c.Tpa.UserId == tpaId);

            return _mapper.Map<IEnumerable<SpConnectionForReturnDto>>(connections.ToList());
        }

        public SpConnectionForReturnDto ManageConnectionStatusBySp(int providerId, string connectionCode, SpConnectionStatusDto status)
        {
            Assert.NotNull(status);

            var retreived = GetConnection(connectionCode);

            if (retreived == null || retreived.ServiceProvider.UserId != providerId)
            {
                throw new NoDataFoundException(_messages.GetString("connectionsSpTpaCodeNotFound"));
            }

            switch (retreived.Status)
            {
                case SpConnectionStatus.Pending2:
                    if (status.Status == SpConnectionStatus.Approved || status.Status == SpConnectionStatus.Rejected)
                    {
                        retreived.Status = status.Status.Value;
                    }
                    else throw new ArgumentBadException(_messages.GetString("connectionStatusNotAllowed", status.Status));
                    break;
                case SpConnectionStatus.Approved:
                    if (status.Status == SpConnectionStatus.SpDeactivated)
                    {
                        retreived.Status = status.Status.Value;
                    }
                    else throw new ArgumentBadException(_messages.GetString("connectionStatusNotAllowed", status.Status));
                    break;
                default:
                    throw new ArgumentBadException(_messages.GetString("connectionStatusNotAllowed", status.Status));
            }

            if (_context.SaveChanges() <= 0)
            {
                throw new SaveChangesFailedException();
            }
            return _mapper.Map<SpConnectionForReturnDto>(retreived);
        }

        public SpConnectionForReturnDto ManageConnectionStatusByTpa(int tpaId, string connectionCode, SpConnectionStatusDto status)
        {
            Assert.NotNull(status);

            var retreived = GetConnection(connectionCode);

            if (retreived == null || retreived.Tpa.UserId != tpaId)
            {
                throw new NoDataFoundException(_messages.GetString("connectionsSpTpaCodeNotFound"));
            }

            switch (retreived.Status)
            {
                case SpConnectionStatus.Pending1:
                    if (status.Status == SpConnectionStatus.Approved || status.Status == SpConnectionStatus.Rejected)
                    {
                        retreived.Status = status.Status.Value;
                    }
                    else throw new ArgumentBadException(_messages.GetString("connectionStatusNotAllowed", status.Status));
                    break;
                case SpConnectionStatus.Approved:
                    if (status.Status == SpConnectionStatus.TpaDeactivated)
                    {
                        retreived.Status = status.Status.Value;
                    }
                    else throw new ArgumentBadException(_messages.GetString("connectionStatusNotAllowed", status.Status));
                    break;
                default:
                    throw new ArgumentBadException(_messages.GetString("connectionStatusNotAllowed", status.Status));
            }

            if (_context.SaveChanges() <= 0)
            {
                throw new SaveChangesFailedException();
            }
            return _mapper.Map<SpConnectionForReturnDto>(retreived);
        }

        public SpConnectionForReturnDto ModifyConnectionBySp(int providerId, string connectionCode, SpConnectionForModifyDto connection)
        {
            Assert.NotNull(connection);

            var retreived = GetConnection(connectionCode);

            if (retreived == null || retreived.ServiceProvider.UserId != providerId)
            {
                throw new NoDataFoundException(_messages.GetString("connectionsSpTpaModifyFailed"));
            }
            retreived.InsuranceCompanies = GenerateIcConnections(retreived.Tpa, connection.InsuranceCompanies);
            retreived.Status = SpConnectionStatus.Pending1;

            if (_context.SaveChanges() <= 0)
            {
                throw new SaveChangesFailedException();
            }
            return _mapper.Map<SpConnectionForReturnDto>(retreived);
        }

        public SpConnectionForReturnDto ModifyConnectionByTpa(int tpaId, string connectionCode, SpConnectionForModifyDto connection)
        {
            Assert.NotNull(connection);

            var retreived = GetConnection(connectionCode);
            if (retreived == null || retreived.Tpa.UserId != tpaId)
            {
                throw new NoDataFoundException(_messages.GetString("connectionsSpTpaModifyFailed"));
            }
            retreived.InsuranceCompanies = GenerateIcConnections(retreived.Tpa, connection.InsuranceCompanies);
            retreived.Status = SpConnectionStatus.Pending2;

            if (_context.SaveChanges() <= 0)
            {
                throw new SaveChangesFailedException();
            }
            return _mapper.Map<SpConnectionForReturnDto>(retreived);
        }

        private ServiceProviderConnection GetConnection(string connectionCode)
        {
            return _context.ServiceProviderConnections
                .Include(c => c.ServiceProvider)
                .Include(c => c.Tpa)
                .FirstOrDefault(c => c.ConnectionCode.ToUpper() == (connectionCode.ToUpper()))
                ?? throw new NoDataFoundException(connectionCode);
        }

        private ServiceProvider GetSpByCode(string spCode)
        {
            return _context.ServiceProviders
                .FirstOrDefault(sp => sp.ServiceProviderCode.ToUpper() == (spCode.ToUpper()))
                ?? throw new NoDataFoundException(spCode);
        }

        private ThirdPartyAdministrator GetTpaByCode(string tpaCode)
        {
            return _context.ThirdPartyAdministrators
                .Include(t => t.InsuranceConnections)
                .ThenInclude(c => c.InsuranceCompany)
                .FirstOrDefault(t => t.ThirdPartyCode.ToUpper() == (tpaCode.ToUpper()))
                ?? throw new NoDataFoundException(tpaCode);
        }

        private List<ServiceProviderConnectionInsuranceCompanies> GenerateIcConnections(ThirdPartyAdministrator tpa, IEnumerable<string> iccodes)
        {
            var ics = tpa.InsuranceConnections
                .Where(ic => iccodes.Any(c => ic.InsuranceCompany.InsuranceCompanyCode == c))
                .Select(ic => new ServiceProviderConnectionInsuranceCompanies { InsuranceCompany = ic.InsuranceCompany })
                .ToList();

            if (ics.Count != iccodes.Count())
                throw new Exception(_messages.GetString("invalidInsuranceCompaniesCodes", tpa.ThirdPartyCode));
            return ics;
        }
    }
}