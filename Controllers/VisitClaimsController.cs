using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PatientsRecordsManager.ABAC.Extensions;
using PatientsRecordsManager.ABAC.Extensions.EF;
using PatientsRecordsManager.Constants;
using PatientsRecordsManager.Constants.Enums;
using PatientsRecordsManager.Data;
using PatientsRecordsManager.Data.Dtos.Visits.Claims;
using PatientsRecordsManager.Exceptions;
using PatientsRecordsManager.Extensions;
using PatientsRecordsManager.Models;
using PatientsRecordsManager.Utils;

namespace PatientsRecordsManager.Controllers
{
    [ApiController]
    [Authorize]
    public class VisitClaimsController : PrmController
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        private readonly Resources.Messages _messages;

        public VisitClaimsController(DataContext context, IMapper mapper, Resources.Messages messages)
        {
            _mapper = mapper;
            _context = context;
            _messages = messages;
        }

        [HttpPost(APIs.VisitClaim)]
        public IActionResult ClaimVisit(string visitCode)
        {
            var visit = GetVisit(visitCode);

            if (visit.VisitClaimId != null && visit.VisitClaimId.Value > 0)
            {
                throw new Exception(_messages.GetString("visitClaimedOrRequested", visitCode));
            }

            var claim = new VisitClaim
            {
                ServiceProviderId = visit.ServiceProviderId,
                ThirdPartyAdministratorId = visit.ServiceProviderConnection.TpaId,
                InsuranceCompanyId = visit.PatientPackage.InsurancePackage.InsuranceCompanyId,
                DateOfClaiming = DateTime.Now
            };

            visit.VisitClaim = claim;

            if (_context.SaveChanges() <= 0)
            {
                throw new SaveChangesFailedException();
            }
            return HttpContext.Success(_mapper.Map<VisitClaimForReturnDto>(claim), _messages.GetString("visitClaimAddDone"));
        }

        [HttpPut(APIs.ManageClaim)]
        public IActionResult ManageClaim(string requester, string claimCode, VisitClaimForManageDto claimForManage)
        {
            Assert.NotNull(claimForManage);

            if (!IsInRole(requester))
            {
                throw new Exception(_messages.GetString("calimsFetchDenied", requester));
            }

            if (RolesNames.IsServiceProvider(requester))
            {
                if (claimForManage.Status != ClaimStatus.Canceled)
                    throw new ArgumentBadException(_messages.GetString("visitClaimStatusNotAllowed", claimForManage.Status));
            }
            else if (RolesNames.IsTpa(requester))
            {
                if (claimForManage.Status != ClaimStatus.Accepted && claimForManage.Status != ClaimStatus.Rejected)
                    throw new ArgumentBadException(_messages.GetString("visitClaimStatusNotAllowed", claimForManage.Status));
            }
            else
            {
                throw new ArgumentBadException(_messages.GetString("visitClaimStatusNotAllowed", claimForManage.Status));
            }

            var claim = GetVisitClaim(claimCode);

            if (claim.Status != ClaimStatus.Pending)
            {
                throw new ArgumentBadException(_messages.GetString("visitClaimStatusOnlyPending"));
            }

            claim.Status = claimForManage.Status.Value;

            if (_context.SaveChanges() <= 0)
                throw new SaveChangesFailedException();

            return HttpContext.Success(
                _mapper.Map<VisitClaimForReturnDto>(claim),
                _messages.GetString("visitClaimManageDone"));
        }

        [HttpGet(APIs.GetClaims)]
        public IActionResult GetClaims([FromRoute] string requester,
                                       [FromQuery] string tpaCode,
                                       [FromQuery] string spCode,
                                       [FromQuery] string icCode,
                                       [FromQuery] string vCode)
        {
            int? tpaUserId = null, spUserId = null, icUserId = null;

            if (!IsInRole(requester))
            {
                throw new Exception(_messages.GetString("calimsFetchDenied", requester));
            }

            if (RolesNames.IsServiceProvider(requester))
            {
                spUserId = HttpContext.GetUserId();
            }
            else if (RolesNames.IsTpa(requester))
            {
                tpaUserId = HttpContext.GetUserId();
            }
            else if (RolesNames.IsInsuranceCompany(requester))
            {
                icUserId = HttpContext.GetUserId();
            }

            return HttpContext.Success(
                _mapper.Map<List<VisitClaimForReturnDto>>(GetVisitsClaims(tpaUserId, tpaCode, spUserId, spCode, icUserId, icCode, vCode)),
                _messages.GetString("visitClaimsGetDone"));
        }

        private bool IsInRole(string roleName)
        {
            return _context.Users
                .Include(u => u.UserRoles)
                .ThenInclude(ur => ur.Role)
                .FirstOrDefault(u => u.Id == HttpContext.GetUserId())
                .UserRoles
                .FirstOrDefault(ur => ur.Role.NormalizedName.ToUpper() == (roleName.ToUpper())) != null;
        }

        private Visit GetVisit(string visitCode)
        {
            var visit = _context.Visits
                .Include(v => v.ServiceProviderConnection)
                .Include(v => v.PatientPackage)
                .ThenInclude(pp => pp.InsurancePackage)
                .ThenInclude(ppip => ppip.InsuranceCompany)
                .FirstOrDefault(v => v.VisitCode.ToUpper() == (visitCode.ToUpper()));

            if (visit == null)
                throw new ArgumentBadException(_messages.GetString("visitNotFound", visitCode));
            return visit;
        }

        private IQueryable<VisitClaim> GetVisitsClaims(int? tpaUserId = null,
                                                       string tpaCode = null,
                                                       int? spUserId = null,
                                                       string spCode = null,
                                                       int? icUserId = null,
                                                       string icCode = null,
                                                       string vCode = null)
        {
            var claims = _context.VisitClaims.AsQueryable();

            if (tpaUserId != null && tpaUserId.Value > 0 || !string.IsNullOrWhiteSpace(tpaCode))
            {
                claims = claims.Include(c => c.ThirdPartyAdministrator);

                if (tpaUserId != null && tpaUserId.Value > 0)
                {
                    claims = claims.Where(c => c.ThirdPartyAdministrator.UserId == tpaUserId);
                }
                if (!string.IsNullOrWhiteSpace(tpaCode))
                {
                    claims = claims.Where(c => c.ThirdPartyAdministrator.ThirdPartyCode.ToUpper().Contains(tpaCode.ToUpper()));
                }
            }

            if (spUserId != null && spUserId.Value > 0 || !string.IsNullOrWhiteSpace(spCode))
            {
                claims = claims.Include(c => c.ServiceProvider);

                if (spUserId != null && spUserId.Value > 0)
                {
                    claims = claims.Where(c => c.ServiceProvider.UserId == spUserId);
                }
                if (!string.IsNullOrWhiteSpace(spCode))
                {
                    claims = claims.Where(c => c.ServiceProvider.ServiceProviderCode.ToUpper().Contains(spCode.ToUpper()));
                }
            }

            if (icUserId != null && icUserId.Value > 0 || !string.IsNullOrWhiteSpace(icCode))
            {
                claims = claims.Include(c => c.InsuranceCompany);

                if (icUserId != null && icUserId.Value > 0)
                {
                    claims = claims.Where(c => c.InsuranceCompany.UserId == icUserId);
                }
                if (!string.IsNullOrWhiteSpace(icCode))
                {
                    claims = claims.Where(c => c.InsuranceCompany.InsuranceCompanyCode.ToUpper().Contains(icCode.ToUpper()));
                }
            }

            if (!string.IsNullOrWhiteSpace(vCode))
            {
                claims = claims
                    .Include(c => c.Visit)
                    .Where(c => c.Visit.VisitCode.ToUpper().Contains(vCode.ToUpper()));
            }
            return claims;
        }

        private VisitClaim GetVisitClaim(string claimCode)
        {
            var claim = _context.VisitClaims
                .FirstOrDefault(c => c.VisitClaimCode.ToUpper() == (claimCode.ToUpper()));
            return claim;
        }
    }
}