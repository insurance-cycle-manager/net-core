using System.ComponentModel.DataAnnotations;

namespace PatientsRecordsManager.Data.Dtos.Users.Phones
{
    public class UserPhoneForReturnDto
    {
        [Phone]
        [Required]
        public string Number { get; set; }
        [Required]
        public bool Confirmed { get; set; }
        [Required]
        public string PhoneCode { get; set; }
        public string UserName { get; set; }
    }
}