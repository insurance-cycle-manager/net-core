using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PatientsRecordsManager.Models
{
    public class UserPhoneNumber
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string PhoneCode { get; set; }

        [Phone]
        [Required]
        public string Number { get; set; }

        public bool Confirmed { get; set; } = false;

        [Required]
        [ForeignKey("Users")]
        public int UserId { get; set; }
        public User User { get; set; }
    }
}