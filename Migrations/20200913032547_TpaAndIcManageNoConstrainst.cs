﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PatientsRecordsManager.Migrations
{
    public partial class TpaAndIcManageNoConstrainst : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_InsuranceCompanies_Addresses_AddressId",
                table: "InsuranceCompanies");

            migrationBuilder.DropForeignKey(
                name: "FK_InsuranceCompanies_AspNetUsers_UserId",
                table: "InsuranceCompanies");

            migrationBuilder.DropForeignKey(
                name: "FK_ThirdPartyAdministrators_Addresses_AddressId",
                table: "ThirdPartyAdministrators");

            migrationBuilder.DropForeignKey(
                name: "FK_ThirdPartyAdministrators_AspNetUsers_UserId",
                table: "ThirdPartyAdministrators");

            migrationBuilder.DropIndex(
                name: "IX_ThirdPartyAdministrators_AddressId",
                table: "ThirdPartyAdministrators");

            migrationBuilder.DropIndex(
                name: "IX_InsuranceCompanies_AddressId",
                table: "InsuranceCompanies");

            migrationBuilder.AlterColumn<int>(
                name: "UserId",
                table: "ThirdPartyAdministrators",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<string>(
                name: "ThirdPartyCode",
                table: "ThirdPartyAdministrators",
                nullable: false,
                defaultValueSql: "'TPA'|| nextval('public.\"ThirdPartyAdministratorsNumbers\"')",
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true,
                oldDefaultValueSql: "'TPA'|| nextval('public.\"ThirdPartyAdministratorsNumbers\"')");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "ThirdPartyAdministrators",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "AddressId1",
                table: "ThirdPartyAdministrators",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "AddressUserId",
                table: "ThirdPartyAdministrators",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "EmailId",
                table: "ThirdPartyAdministrators",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PhoneNumberId",
                table: "ThirdPartyAdministrators",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "RoleId",
                table: "ThirdPartyAdministrators",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserRoleRoleId",
                table: "ThirdPartyAdministrators",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserRoleUserId",
                table: "ThirdPartyAdministrators",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "UserId",
                table: "InsuranceCompanies",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AddColumn<int>(
                name: "AddressId1",
                table: "InsuranceCompanies",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "AddressUserId",
                table: "InsuranceCompanies",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "EmailId",
                table: "InsuranceCompanies",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PhoneNumberId",
                table: "InsuranceCompanies",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "RoleId",
                table: "InsuranceCompanies",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserRoleRoleId",
                table: "InsuranceCompanies",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserRoleUserId",
                table: "InsuranceCompanies",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ThirdPartyAdministrators_EmailId",
                table: "ThirdPartyAdministrators",
                column: "EmailId");

            migrationBuilder.CreateIndex(
                name: "IX_ThirdPartyAdministrators_PhoneNumberId",
                table: "ThirdPartyAdministrators",
                column: "PhoneNumberId");

            migrationBuilder.CreateIndex(
                name: "IX_ThirdPartyAdministrators_RoleId",
                table: "ThirdPartyAdministrators",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_ThirdPartyAdministrators_AddressUserId_AddressId1",
                table: "ThirdPartyAdministrators",
                columns: new[] { "AddressUserId", "AddressId1" });

            migrationBuilder.CreateIndex(
                name: "IX_ThirdPartyAdministrators_UserRoleUserId_UserRoleRoleId",
                table: "ThirdPartyAdministrators",
                columns: new[] { "UserRoleUserId", "UserRoleRoleId" });

            migrationBuilder.CreateIndex(
                name: "IX_InsuranceCompanies_EmailId",
                table: "InsuranceCompanies",
                column: "EmailId");

            migrationBuilder.CreateIndex(
                name: "IX_InsuranceCompanies_PhoneNumberId",
                table: "InsuranceCompanies",
                column: "PhoneNumberId");

            migrationBuilder.CreateIndex(
                name: "IX_InsuranceCompanies_RoleId",
                table: "InsuranceCompanies",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_InsuranceCompanies_AddressUserId_AddressId1",
                table: "InsuranceCompanies",
                columns: new[] { "AddressUserId", "AddressId1" });

            migrationBuilder.CreateIndex(
                name: "IX_InsuranceCompanies_UserRoleUserId_UserRoleRoleId",
                table: "InsuranceCompanies",
                columns: new[] { "UserRoleUserId", "UserRoleRoleId" });

            migrationBuilder.AddForeignKey(
                name: "FK_InsuranceCompanies_UserEmails_EmailId",
                table: "InsuranceCompanies",
                column: "EmailId",
                principalTable: "UserEmails",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_InsuranceCompanies_UserPhoneNumbers_PhoneNumberId",
                table: "InsuranceCompanies",
                column: "PhoneNumberId",
                principalTable: "UserPhoneNumbers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_InsuranceCompanies_AspNetRoles_RoleId",
                table: "InsuranceCompanies",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_InsuranceCompanies_AspNetUsers_UserId",
                table: "InsuranceCompanies",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_InsuranceCompanies_UserAddresses_AddressUserId_AddressId1",
                table: "InsuranceCompanies",
                columns: new[] { "AddressUserId", "AddressId1" },
                principalTable: "UserAddresses",
                principalColumns: new[] { "UserId", "AddressId" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_InsuranceCompanies_AspNetUserRoles_UserRoleUserId_UserRoleR~",
                table: "InsuranceCompanies",
                columns: new[] { "UserRoleUserId", "UserRoleRoleId" },
                principalTable: "AspNetUserRoles",
                principalColumns: new[] { "UserId", "RoleId" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ThirdPartyAdministrators_UserEmails_EmailId",
                table: "ThirdPartyAdministrators",
                column: "EmailId",
                principalTable: "UserEmails",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ThirdPartyAdministrators_UserPhoneNumbers_PhoneNumberId",
                table: "ThirdPartyAdministrators",
                column: "PhoneNumberId",
                principalTable: "UserPhoneNumbers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ThirdPartyAdministrators_AspNetRoles_RoleId",
                table: "ThirdPartyAdministrators",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ThirdPartyAdministrators_AspNetUsers_UserId",
                table: "ThirdPartyAdministrators",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ThirdPartyAdministrators_UserAddresses_AddressUserId_Addres~",
                table: "ThirdPartyAdministrators",
                columns: new[] { "AddressUserId", "AddressId1" },
                principalTable: "UserAddresses",
                principalColumns: new[] { "UserId", "AddressId" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ThirdPartyAdministrators_AspNetUserRoles_UserRoleUserId_Use~",
                table: "ThirdPartyAdministrators",
                columns: new[] { "UserRoleUserId", "UserRoleRoleId" },
                principalTable: "AspNetUserRoles",
                principalColumns: new[] { "UserId", "RoleId" },
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_InsuranceCompanies_UserEmails_EmailId",
                table: "InsuranceCompanies");

            migrationBuilder.DropForeignKey(
                name: "FK_InsuranceCompanies_UserPhoneNumbers_PhoneNumberId",
                table: "InsuranceCompanies");

            migrationBuilder.DropForeignKey(
                name: "FK_InsuranceCompanies_AspNetRoles_RoleId",
                table: "InsuranceCompanies");

            migrationBuilder.DropForeignKey(
                name: "FK_InsuranceCompanies_AspNetUsers_UserId",
                table: "InsuranceCompanies");

            migrationBuilder.DropForeignKey(
                name: "FK_InsuranceCompanies_UserAddresses_AddressUserId_AddressId1",
                table: "InsuranceCompanies");

            migrationBuilder.DropForeignKey(
                name: "FK_InsuranceCompanies_AspNetUserRoles_UserRoleUserId_UserRoleR~",
                table: "InsuranceCompanies");

            migrationBuilder.DropForeignKey(
                name: "FK_ThirdPartyAdministrators_UserEmails_EmailId",
                table: "ThirdPartyAdministrators");

            migrationBuilder.DropForeignKey(
                name: "FK_ThirdPartyAdministrators_UserPhoneNumbers_PhoneNumberId",
                table: "ThirdPartyAdministrators");

            migrationBuilder.DropForeignKey(
                name: "FK_ThirdPartyAdministrators_AspNetRoles_RoleId",
                table: "ThirdPartyAdministrators");

            migrationBuilder.DropForeignKey(
                name: "FK_ThirdPartyAdministrators_AspNetUsers_UserId",
                table: "ThirdPartyAdministrators");

            migrationBuilder.DropForeignKey(
                name: "FK_ThirdPartyAdministrators_UserAddresses_AddressUserId_Addres~",
                table: "ThirdPartyAdministrators");

            migrationBuilder.DropForeignKey(
                name: "FK_ThirdPartyAdministrators_AspNetUserRoles_UserRoleUserId_Use~",
                table: "ThirdPartyAdministrators");

            migrationBuilder.DropIndex(
                name: "IX_ThirdPartyAdministrators_EmailId",
                table: "ThirdPartyAdministrators");

            migrationBuilder.DropIndex(
                name: "IX_ThirdPartyAdministrators_PhoneNumberId",
                table: "ThirdPartyAdministrators");

            migrationBuilder.DropIndex(
                name: "IX_ThirdPartyAdministrators_RoleId",
                table: "ThirdPartyAdministrators");

            migrationBuilder.DropIndex(
                name: "IX_ThirdPartyAdministrators_AddressUserId_AddressId1",
                table: "ThirdPartyAdministrators");

            migrationBuilder.DropIndex(
                name: "IX_ThirdPartyAdministrators_UserRoleUserId_UserRoleRoleId",
                table: "ThirdPartyAdministrators");

            migrationBuilder.DropIndex(
                name: "IX_InsuranceCompanies_EmailId",
                table: "InsuranceCompanies");

            migrationBuilder.DropIndex(
                name: "IX_InsuranceCompanies_PhoneNumberId",
                table: "InsuranceCompanies");

            migrationBuilder.DropIndex(
                name: "IX_InsuranceCompanies_RoleId",
                table: "InsuranceCompanies");

            migrationBuilder.DropIndex(
                name: "IX_InsuranceCompanies_AddressUserId_AddressId1",
                table: "InsuranceCompanies");

            migrationBuilder.DropIndex(
                name: "IX_InsuranceCompanies_UserRoleUserId_UserRoleRoleId",
                table: "InsuranceCompanies");

            migrationBuilder.DropColumn(
                name: "AddressId1",
                table: "ThirdPartyAdministrators");

            migrationBuilder.DropColumn(
                name: "AddressUserId",
                table: "ThirdPartyAdministrators");

            migrationBuilder.DropColumn(
                name: "EmailId",
                table: "ThirdPartyAdministrators");

            migrationBuilder.DropColumn(
                name: "PhoneNumberId",
                table: "ThirdPartyAdministrators");

            migrationBuilder.DropColumn(
                name: "RoleId",
                table: "ThirdPartyAdministrators");

            migrationBuilder.DropColumn(
                name: "UserRoleRoleId",
                table: "ThirdPartyAdministrators");

            migrationBuilder.DropColumn(
                name: "UserRoleUserId",
                table: "ThirdPartyAdministrators");

            migrationBuilder.DropColumn(
                name: "AddressId1",
                table: "InsuranceCompanies");

            migrationBuilder.DropColumn(
                name: "AddressUserId",
                table: "InsuranceCompanies");

            migrationBuilder.DropColumn(
                name: "EmailId",
                table: "InsuranceCompanies");

            migrationBuilder.DropColumn(
                name: "PhoneNumberId",
                table: "InsuranceCompanies");

            migrationBuilder.DropColumn(
                name: "RoleId",
                table: "InsuranceCompanies");

            migrationBuilder.DropColumn(
                name: "UserRoleRoleId",
                table: "InsuranceCompanies");

            migrationBuilder.DropColumn(
                name: "UserRoleUserId",
                table: "InsuranceCompanies");

            migrationBuilder.AlterColumn<int>(
                name: "UserId",
                table: "ThirdPartyAdministrators",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "ThirdPartyCode",
                table: "ThirdPartyAdministrators",
                type: "text",
                nullable: true,
                defaultValueSql: "'TPA'|| nextval('public.\"ThirdPartyAdministratorsNumbers\"')",
                oldClrType: typeof(string),
                oldDefaultValueSql: "'TPA'|| nextval('public.\"ThirdPartyAdministratorsNumbers\"')");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "ThirdPartyAdministrators",
                type: "text",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<int>(
                name: "UserId",
                table: "InsuranceCompanies",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ThirdPartyAdministrators_AddressId",
                table: "ThirdPartyAdministrators",
                column: "AddressId");

            migrationBuilder.CreateIndex(
                name: "IX_InsuranceCompanies_AddressId",
                table: "InsuranceCompanies",
                column: "AddressId");

            migrationBuilder.AddForeignKey(
                name: "FK_InsuranceCompanies_Addresses_AddressId",
                table: "InsuranceCompanies",
                column: "AddressId",
                principalTable: "Addresses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_InsuranceCompanies_AspNetUsers_UserId",
                table: "InsuranceCompanies",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ThirdPartyAdministrators_Addresses_AddressId",
                table: "ThirdPartyAdministrators",
                column: "AddressId",
                principalTable: "Addresses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ThirdPartyAdministrators_AspNetUsers_UserId",
                table: "ThirdPartyAdministrators",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
