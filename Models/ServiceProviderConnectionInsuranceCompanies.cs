namespace PatientsRecordsManager.Models
{
    public class ServiceProviderConnectionInsuranceCompanies
    {
        public int SpConnectionId { get; set; }
        public int InsuranceCompanyId { get; set; }

        public ServiceProviderConnection SpConnection { get; set; }
        public InsuranceCompany InsuranceCompany { get; set; }
    }
}