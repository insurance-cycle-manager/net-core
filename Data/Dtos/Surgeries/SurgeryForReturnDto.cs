using System.Collections.Generic;
using PatientsRecordsManager.Data.Dtos.Prescribed.Surgeries;
using PatientsRecordsManager.Data.Dtos.Prices;

namespace PatientsRecordsManager.Data.Dtos.Surgeries
{
    public class SurgeryForReturnDto
    {
        public string SurgeryCode { get; set; }
        public string Name { get; set; }
        public PriceForReturnDto Price { get; set; }
        public IEnumerable<PrescribedSurgeryForReturnDto> PrescribedSurgeries { get; set; }
        public IEnumerable<PackageSurgeryForReturnDto> Packages { get; set; }
        public IEnumerable<SurgeryPurchaseForReturnDto> Purchases { get; set; }
    }
}