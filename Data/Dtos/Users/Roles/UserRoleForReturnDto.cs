using System.Collections.Generic;
using PatientsRecordsManager.Data.Dtos.Permissions;

namespace PatientsRecordsManager.Data.Dtos.Users.Roles
{
    public class UserRoleForReturnDto
    {
        public string UserName { get; set; }
        public string RoleName { get; set; }
        public IEnumerable<PermissionForReturnDto> Permissions { get; set; }
    }
}