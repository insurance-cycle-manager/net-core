using System.Collections.Generic;
using PatientsRecordsManager.Data.Dtos.Prescribed.Surgeries;

namespace PatientsRecordsManager.Data.Repositories
{
    public interface IVisitSurgeryRepository
    {
        PrescribedSurgeryForReturnDto AddSurgery(string vCode, PrescribedSurgeryForCreateDto surgery);
        IEnumerable<PrescribedSurgeryForReturnDto> GetSurgeries(string vCode);
        PrescribedSurgeryForReturnDto GetSurgeryDetails(string vCode, string sCode);
        PrescribedSurgeryForReturnDto ModifySurgery(string vCode, string sCode, PrescribedSurgeryForUpdateDto surgery);
        PrescribedSurgeryForReturnDto RemoveSurgery(string vCode, string sCode);
    }
}