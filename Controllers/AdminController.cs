using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PatientsRecordsManager.Constants;
using PatientsRecordsManager.Data;

namespace PatientsRecordsManager.Controllers
{
    [ApiController]
    [Route(APIs.DefaultRoute)]
    [Authorize(Policy = Policies.Admin)]
    public class AdminController : PrmController
    {
        private readonly DataContext _context;

        public AdminController(DataContext context)
        {
            _context = context;
        }
    }
}