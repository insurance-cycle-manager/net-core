namespace PatientsRecordsManager.ABAC.Data.Dtos
{
    public class RuleForReturnDto
    {
        public int Id { get; set; }
        public string PType { get; set; }
        public string V0 { get; set; }
        public string V1 { get; set; }
        public string V2 { get; set; }
        public string V3 { get; set; }
        public object V4 { get; set; }
        public object V5 { get; set; }
    }
}