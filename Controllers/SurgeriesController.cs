using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PatientsRecordsManager.Constants;
using PatientsRecordsManager.Data.Dtos.Surgeries;
using PatientsRecordsManager.Data.Repositories;
using PatientsRecordsManager.Extensions;
using PatientsRecordsManager.Resources;

namespace PatientsRecordsManager.Controllers
{
    [ApiController]
    [Route(APIs.SurgeriesBaseUrl)]
    public class SurgeriesController : PrmController
    {
        private readonly IMapper _mapper;
        private readonly ISurgeryRespository _repository;
        private readonly Messages _messages;

        public SurgeriesController(ISurgeryRespository repository, IMapper mapper, Messages messages)
        {
            _messages = messages;
            _repository = repository;
            _mapper = mapper;
        }

        [HttpPost(APIs.AddSurgery)]
        [Authorize(Roles = RolesNames.Administrator)]
        public IActionResult AddSurgery(SurgeryForCreateDto surgeryForCreate)
        {
            return HttpContext.Success(_repository.AddSurgery(surgeryForCreate), _messages.GetString("CompleteAddSurgery"));
        }

        [HttpPut(APIs.ModifySurgery)]
        [Authorize(Roles = RolesNames.Administrator)]
        public IActionResult ModifySurgery(string surgeryCode, SurgeryForUpdateDto surgeryForUpdate)
        {
            return HttpContext.Success(_repository.ModifySurgery(surgeryCode, surgeryForUpdate), _messages.GetString("CompleteUpdateSurgery"));
        }

        [HttpDelete(APIs.DeleteSurgery)]
        public IActionResult DeleteSurgery(string surgeryCode)
        {
            return HttpContext.Success(_repository.RemoveSurgery(surgeryCode), _messages.GetString("CompleteDeleteSurgery"));
        }

        [HttpGet(APIs.GetSurgeries)]
        [AllowAnonymous]
        public IActionResult GetSurgeries()
        {
            return HttpContext.Success(_repository.GetSurgeries(), _messages.GetString("CompleteFetchSurgeries"));
        }

        [HttpGet(APIs.GetSurgery)]
        [AllowAnonymous]
        public IActionResult GetSurgery(string surgeryCode)
        {
            return HttpContext.Success(_repository.GetSurgeryDetails(surgeryCode), _messages.GetString("CompleteFetchSurgeries"));
        }
    }
}