using System.ComponentModel.DataAnnotations;
using PatientsRecordsManager.Constants.Enums;

namespace PatientsRecordsManager.Data.Dtos.InsuranceCompanies.Packages
{
    public class ManagePatientPackageRequestDto
    {
        [Required]
        public string PatientCode { get; set; }
        [Required]
        public string PackageCode { get; set; }
        [Required]
        [EnumDataType(typeof(PatientPackageStatus))]
        public PatientPackageStatus? Status { get; set; }
    }
}