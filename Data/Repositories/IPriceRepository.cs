using PatientsRecordsManager.Data.Dtos.Prices;

namespace PatientsRecordsManager.Data.Repositories
{
    public interface IPriceRepository
    {
        PriceForReturnDto AddPrice(PriceForAddDto price, string previous = null);
        PriceForReturnDto GetPrice(string code);
    }
}