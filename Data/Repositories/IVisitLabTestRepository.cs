using System.Collections.Generic;
using PatientsRecordsManager.Data.Dtos.Prescribed.LabTests;

namespace PatientsRecordsManager.Data.Repositories
{
    public interface IVisitLabTestRepository
    {
        PrescribedLabTestForReturnDto AddLabTest(string visitCode, PrescribedLabTestForCreateDto labTestForCreate);
        IEnumerable<PrescribedLabTestForReturnDto> GetLabTests(string visitCode);
        PrescribedLabTestForReturnDto GetLabTestDetails(string visitCode, string labTestCode);
        PrescribedLabTestForReturnDto ModifyLabTest(string visitCode, string labTestCode, PrescribedLabTestForUpdateDto labTestForUpdate);
        PrescribedLabTestForReturnDto RemoveLabTest(string visitCode, string labTestCode);
    }
}