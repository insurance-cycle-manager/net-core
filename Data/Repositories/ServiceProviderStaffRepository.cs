using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using PatientsRecordsManager.ABAC.Extensions;
using PatientsRecordsManager.ABAC.Extensions.EF;
using PatientsRecordsManager.Constants.Enums;
using PatientsRecordsManager.Data.Dtos.ServiceProviders.Staff;
using PatientsRecordsManager.Exceptions;
using PatientsRecordsManager.Models;
using PatientsRecordsManager.Resources;
using PatientsRecordsManager.Utils;

namespace PatientsRecordsManager.Data.Repositories
{
    public class ServiceProviderStaffRepository : IServiceProviderStaffRepository
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        private readonly Messages _messages;

        public ServiceProviderStaffRepository(DataContext context, IMapper mapper, Messages messages)
        {
            _messages = messages;
            _mapper = mapper;
            _context = context;
        }

        public MemberForReturnDto AddStafferMember(int userId, string providerCode, MemberForAddDto memberForAdd)
        {
            Assert.NotNull(memberForAdd);

            var staffer = GetServiceStaffer(stCode: memberForAdd.StafferCode);
            var provider = GetServiceProvider(spCode: providerCode, spUserId: userId);

            IsStafferMemberOrRequested(provider, memberForAdd.StafferCode);
            var member = new ServiceProviderStaffer
            {
                ServiceProvider = provider,
                Staffer = staffer,
                Status = ServiceProviderStafferStatus.Pending
            };
            _context.ServiceProviderStaffer.Add(member);

            if (_context.SaveChanges() <= 0)
            {
                throw new SaveChangesFailedException();
            }
            return _mapper.Map<MemberForReturnDto>(member);
        }

        public MemberForReturnDto ConfirmStafferMember(string providerCode, int userId, string stafferCode)
        {
            var member = GetMember(providerCode, stafferCode, stafferId: userId);
            member.Status = ServiceProviderStafferStatus.Confirmed;

            if (_context.SaveChanges() < 0)
            {
                throw new SaveChangesFailedException();
            }
            return _mapper.Map<MemberForReturnDto>(member);
        }

        public IEnumerable<MemberForReturnDto> GetStaffByProvider(string spCode)
        {
            return _mapper.Map<List<MemberForReturnDto>>(GetServiceProvider(spCode: spCode).Staff.AsEnumerable());
        }

        public IEnumerable<MemberForReturnDto> GetStaffByProvider(int spUserId)
        {
            return _mapper.Map<List<MemberForReturnDto>>(GetServiceProvider(spUserId: spUserId).Staff.AsEnumerable());
        }

        public IEnumerable<MemberForReturnDto> GetStaffByStaffer(string stCode)
        {
            return _mapper.Map<List<MemberForReturnDto>>(GetServiceStaffer(stCode: stCode).ServiceProviders.AsEnumerable());
        }

        public IEnumerable<MemberForReturnDto> GetStaffByStaffer(int stUserId)
        {
            return _mapper.Map<List<MemberForReturnDto>>(GetServiceStaffer(stUserId: stUserId).ServiceProviders.AsEnumerable());
        }

        public MemberForReturnDto RemoveStaffer(int userId, string providerCode, string stafferCode)
        {
            var member = GetMember(providerCode, stafferCode, providerId: userId);
            _context.ServiceProviderStaffer.Remove(member);

            if (_context.SaveChanges() <= 0)
            {
                throw new SaveChangesFailedException();
            }
            return _mapper.Map<MemberForReturnDto>(member);
        }

        private ServiceProvider GetServiceProvider(string spCode = null, int? spUserId = null)
        {
            var sp = _context.ServiceProviders
                .Include(sp => sp.Staff)
                .ThenInclude(e => e.Staffer)
                .AsQueryable();

            if (!string.IsNullOrEmpty(spCode))
                sp = sp.Where(e => e.ServiceProviderCode.ToUpper() == (spCode.ToUpper()));

            if (spUserId.HasValue)
                sp = sp.Where(e => e.UserId == spUserId.Value);

            return sp.FirstOrDefault() ?? throw new ArgumentBadException(_messages.GetString("ServiceProviderNotFound"));
        }

        private ServiceStaffer GetServiceStaffer(string stCode = null, int? stUserId = null)
        {
            var st = _context.ServiceStaffers
                .Include(st => st.ServiceProviders)
                .ThenInclude(sts => sts.ServiceProvider)
                .AsQueryable();

            if (!string.IsNullOrEmpty(stCode))
                st = st.Where(e => e.ServiceStafferCode.ToUpper() == (stCode.ToUpper()));

            if (stUserId.HasValue)
                st = st.Where(e => e.UserId == stUserId.Value);

            return st.FirstOrDefault() ?? throw new ArgumentBadException(_messages.GetString("ServiceStafferNotFound"));
        }

        private ServiceProviderStaffer GetMember(string providerCode, string stafferCode, int providerId = -1, int stafferId = -1)
        {
            return _context.ServiceProviderStaffer
                .Include(sps => sps.ServiceProvider)
                .Include(sps => sps.Staffer)
                .FirstOrDefault(sps =>
                    sps.ServiceProvider.ServiceProviderCode.ToUpper() == (providerCode.ToUpper()) &&
                    (providerId == -1 || sps.ServiceProvider.UserId == providerId) &&
                    sps.Staffer.ServiceStafferCode.ToUpper() == (stafferCode.ToUpper()) &&
                    (stafferId == -1 || sps.Staffer.UserId == stafferId))
            ?? throw new ArgumentBadException(_messages.GetString("BadStafferMembership", providerCode, stafferCode));
        }

        private void IsStafferMemberOrRequested(ServiceProvider provider, string stafferCode)
        {
            var member = provider.Staff
                .FirstOrDefault(e => e.Staffer.ServiceStafferCode.ToUpper() == (stafferCode.ToUpper()));

            if (member != null)
            {
                throw new ArgumentBadException(_messages.GetString("BadStafferMembership", stafferCode, provider.ServiceProviderCode));
            }
        }
    }
}