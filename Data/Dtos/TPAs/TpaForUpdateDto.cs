using System;
using System.ComponentModel.DataAnnotations;
using PatientsRecordsManager.Data.Dtos.Addresses;

namespace PatientsRecordsManager.Data.Dtos.TPAs
{
    public class TpaForUpdateDto
    {
        [Required]
        public DateTime DateOfFounding { get; set; }
        [Required]
        public DateTime JoinedAt { get; set; }
    }
}