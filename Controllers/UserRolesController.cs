using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PatientsRecordsManager.ABAC.Extensions;
using PatientsRecordsManager.ABAC.Extensions.EF;
using PatientsRecordsManager.Constants;
using PatientsRecordsManager.Constants.Enums;
using PatientsRecordsManager.Data;
using PatientsRecordsManager.Data.Dtos.InsuranceCompanies;
using PatientsRecordsManager.Data.Dtos.Patients;
using PatientsRecordsManager.Data.Dtos.RolesRequests;
using PatientsRecordsManager.Data.Dtos.ServiceProviders;
using PatientsRecordsManager.Data.Dtos.ServiceProviders.Staffers;
using PatientsRecordsManager.Data.Dtos.TPAs;
using PatientsRecordsManager.Data.Dtos.Users.RolesRequests;
using PatientsRecordsManager.Data.Repositories;
using PatientsRecordsManager.Exceptions;
using PatientsRecordsManager.Extensions;
using PatientsRecordsManager.Models;
using PatientsRecordsManager.Utils.Exceptions;

namespace PatientsRecordsManager.Controllers
{

    [ApiController]
    public class UserRolesController : PrmController
    {
        private readonly UserManager<User> _userManager;
        private readonly IMapper _mapper;
        private readonly DataContext _context;
        private readonly Resources.Messages _messages;
        private readonly IServiceProviderRepository _serviceProvidersRepo;
        private readonly IServiceStafferRepository _serviceStaffersRepo;
        private readonly IPatientRepository _patientsRepo;
        private readonly IIcRepository _icsRepo;
        private readonly ITpaRepository _tpasRepo;

        public UserRolesController(UserManager<User> userManager, DataContext context,
            IServiceProviderRepository providersRepo,
            IServiceStafferRepository staffersRepo,
            IPatientRepository patientRepo,
            IIcRepository icsRepo,
            ITpaRepository tpasRepo,
            IMapper mapper,
            Resources.Messages messages)
        {
            _serviceProvidersRepo = providersRepo;
            _serviceStaffersRepo = staffersRepo;
            _patientsRepo = patientRepo;
            _icsRepo = icsRepo;
            _tpasRepo = tpasRepo;
            _context = context;
            _mapper = mapper;
            _userManager = userManager;
            _messages = messages;
        }

        [HttpGet(APIs.AdminPrefix + APIs.UserRolesPrefix + APIs.GetRolesRequests)]
        [Authorize(Roles = RolesNames.Administrator)]
        public IActionResult GetRolesRequests([FromQuery] RoleRequestStatus? status)
        {
            return HttpContext.Success(_mapper.Map<List<RoleRequestForReturnDto>>(FetchRolesRequests(status)), _messages.GetString("RolesRequestsFetchDone"));
        }


        [HttpGet(APIs.UserRolesPrefix + APIs.GetRolesRequests)]
        [Authorize]
        public IActionResult GetUserRolesRequests([FromQuery] RoleRequestStatus? status)
        {
            return HttpContext.Success(_mapper.Map<List<RoleRequestForReturnDto>>(FetchRolesRequests(status, HttpContext.GetUserId())), _messages.GetString("RolesRequestsFetchDone"));
        }

        [HttpPost(APIs.AdminPrefix + APIs.UserRolesPrefix + APIs.RequestUserRole)]
        [Authorize(Roles = RolesNames.Administrator)]
        public IActionResult GrantUserToRole(AddUserToRoleDto source)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            AddUserToRoleWithConfirm(GetUser(source.UserName), GetRole(source.RoleName), source);
            return HttpContext.Success(source, _messages.GetString("UserRoleAddDone", source.UserName, source.RoleName));
        }

        [HttpPost(APIs.UserRolesPrefix + APIs.RequestUserRole)]
        [Authorize]
        public IActionResult RequestUserRole(RequestUserRoleDto request)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            AddUserToRoleWithoutConfirm(GetUser(HttpContext.GetUserId()), GetRole(request.RoleName), request);
            return HttpContext.Success(request, _messages.GetString("UserRoleRequestDone"));
        }

        [HttpDelete(APIs.AdminPrefix + APIs.UserRolesPrefix + APIs.UnAssignUserFromRole)]
        [Authorize(Roles = RolesNames.Administrator)]
        public IActionResult UnAssignUserFromRole(string username, string rolename)
        {
            RemoveUserFromRole(GetUser(username), rolename).Wait();
            return HttpContext.Success(null, _messages.GetString("UserRoleDeleteDone", rolename));
        }

        [HttpDelete(APIs.UserRolesPrefix + APIs.DeleteUserRole)]
        [Authorize]
        public IActionResult DeleteUserRole(string rolename)
        {
            RemoveUserFromRole(GetUser(HttpContext.GetUserId()), rolename).Wait();
            return HttpContext.Success(null, _messages.GetString("UserRoleDeleteDone", rolename));
        }

        [HttpPost(APIs.AdminPrefix + APIs.UserRolesPrefix + APIs.ManageRoleRequest)]
        [Authorize(Roles = RolesNames.Administrator)]
        public IActionResult ManageRequestsForRole(ManageRoleRequestDto manage)
        {
            if (manage == null)
            {
                throw new ArgumentNullException(nameof(manage));
            }

            ConfirmUserRoleRequest(GetUser(manage.UserName), GetRole(manage.RoleName), manage.Response.GetValueOrDefault());
            return HttpContext.Success(manage, _messages.GetString("ManageUserRoleSucceeded"));
        }


        private User GetUser(string username)
        {
            return _context.Users
                .Include(u => u.UserEmails)
                .Include(u => u.UserPhoneNumbers)
                .Include(u => u.UserAddresses)
                .ThenInclude(ua => ua.Address)
                .Include(u => u.UserRoleRequests)
                .Include(u => u.UserRoles)
                .ThenInclude(ur => ur.Role)
                .FirstOrDefault(u => u.NormalizedUserName.ToUpper() == username.ToUpper()) ??
                throw new ArgumentBadException(_messages.GetString("InvalidUserName", username));
        }

        private User GetUser(int id)
        {
            return _context.Users
                .Include(u => u.UserEmails)
                .Include(u => u.UserPhoneNumbers)
                .Include(u => u.UserAddresses)
                .ThenInclude(ua => ua.Address)
                .Include(u => u.UserRoleRequests)
                .Include(u => u.UserRoles)
                .ThenInclude(ur => ur.Role)
                .FirstOrDefault(u => u.Id == id) ??
                throw new ArgumentBadException(_messages.GetString("InvalidUserName", id));
        }

        private Role GetRole(string roleName)
        {
            return _context.Roles
                .FirstOrDefault(r => r.NormalizedName.ToUpper().Contains(roleName.ToUpper())) ??
                throw new ArgumentBadException(_messages.GetString("UnknownRoleName"));
        }

        private UserRoleRequest GetPendingRequest(User user, int roleId)
        {
            var request = user.UserRoleRequests.FirstOrDefault(req =>
                req.RoleId == roleId && req.Status == RoleRequestStatus.Pending);
            if (request == null)
            {
                throw new ArgumentBadException(_messages.GetString("NoPendingRequests", user.UserName));
            }
            return request;
        }

        private IQueryable<UserRoleRequest> FetchRolesRequests(RoleRequestStatus? status, int? userId = null)
        {
            var requests = _context.UserRoleRequests
                .Include(req => req.User)
                .Include(req => req.Role)
                .Include(req => req.UserAddress)
                .Include(req => req.UserEmail)
                .Include(req => req.UserPhoneNumber)
                .AsQueryable();

            if (status != null)
            {
                requests = requests.Where(req => req.Status.Equals(status));
            }

            if (userId != null)
            {
                requests = requests.Where(req => req.UserId == userId);
            }
            return requests;
        }

        private void IsUserHasRole(User user, string roleName)
        {
            if (user.UserRoles.Any(r => r.Role.NormalizedName.ToUpper() == (roleName.ToUpper())))
            {
                throw new ArgumentBadException(_messages.GetString("UserRoleAlreadyExisting", user.UserName, roleName));
            }
        }

        private void DidUserRequestRole(User user, string roleName)
        {
            var requests = user.UserRoleRequests
                .AsQueryable()
                .Include(r => r.Role)
                .Where(r => r.Status != RoleRequestStatus.Removed)
                .ToList();
            if (requests.Any(req => req.Role.NormalizedName == roleName && req.Status == RoleRequestStatus.Pending))
            {
                throw new ArgumentBadException(_messages.GetString("UserRoleRequested", user.UserName, roleName));
            }
        }

        private void AddUserToRoleWithoutConfirm(User user, Role role, RequestUserRoleDto requestDto)
        {
            IsUserHasRole(user, requestDto.RoleName);
            DidUserRequestRole(user, requestDto.RoleName);

            var request = new UserRoleRequest
            {
                User = user,
                Role = role,
                Title = requestDto.Title,
                UserAddress = GetUserAddress(user, requestDto.PreferredAddressCode),
                UserEmail = GetUserEmail(user, requestDto.PreferredEmail),
                UserPhoneNumber = GetUserPhoneNumber(user, requestDto.PreferredPhoneNumber),
            };
            _context.UserRoleRequests.Add(request);

            if (_context.SaveChanges() <= 0)
            {
                throw new SaveChangesFailedException();
            }
        }

        private void ConfirmUserRoleRequest(User user, Role role, RoleRequestResponse response)
        {
            UserRoleRequest request = GetPendingRequest(user, role.Id);

            switch (response)
            {
                case RoleRequestResponse.Approve:
                    request.Status = RoleRequestStatus.Approved;
                    AddUserToRole(user, role, request).Wait();
                    break;
                case RoleRequestResponse.Reject:
                    request.Status = RoleRequestStatus.Rejected;
                    _context.SaveChanges();
                    break;
            }
        }

        private void AddUserToRoleWithConfirm(User user, Role role, RequestUserRoleDto request)
        {
            AddUserToRoleWithoutConfirm(user, role, request);
            ConfirmUserRoleRequest(user, role, RoleRequestResponse.Approve);
        }

        private async Task AddUserToRole(User user, Role role, UserRoleRequest request)
        {
            var result = await _userManager.AddToRoleAsync(user, role.Name).ConfigureAwait(false);
            if (result.Succeeded)
            {
                if (RolesNames.IsServiceProvider(role.Name))
                {
                    var serviceProviderDto = new ServiceProviderForCreateDto
                    {
                        Type = DetectServiceProviderTypeFromName(role.Name),
                        NormalizedUserName = user.NormalizedUserName,
                        UserId = user.Id,
                        RoleId = role.Id,
                        Title = request.Title,
                        AddressId = request.AddressId,
                        EmailId = request.UserEmailId,
                        PhoneNumberId = request.UserPhoneNumberId,
                    };
                    _serviceProvidersRepo.AddServiceProvider(serviceProviderDto);
                }
                else if (RolesNames.IsServiceStaffer(role.Name))
                {
                    var serviceStafferDto = new StafferForCreateDto
                    {
                        Type = DetectServiceStafferTypeFromName(role.Name),
                        NormalizedUserName = user.NormalizedUserName,
                        UserId = user.Id,
                        RoleId = role.Id,
                        Title = request.Title,
                        AddressId = request.AddressId,
                        EmailId = request.UserEmailId,
                        PhoneNumberId = request.UserPhoneNumberId,
                    };
                    _serviceStaffersRepo.AddServiceStaffer(serviceStafferDto);
                }
                else if (RolesNames.IsPatient(role.Name))
                {
                    var patientDto = new PatientForCreateDto
                    {
                        Name = request.Title,
                        AddressId = request.AddressId,
                        EmailId = request.UserEmailId,
                        PhoneNumberId = request.UserPhoneNumberId,
                        UserId = user.Id,
                        RoleId = role.Id,
                        Gender = Gender.Default
                    };
                    _patientsRepo.AddPatient(patientDto);
                }
                else if (RolesNames.IsTpa(role.Name))
                {
                    var tpaDto = new TpaForCreateDto
                    {
                        Name = request.Title,
                        AddressId = request.AddressId,
                        EmailId = request.UserEmailId,
                        PhoneNumberId = request.UserPhoneNumberId,
                        UserId = user.Id,
                        RoleId = role.Id,
                        JoinedAt = DateTime.Now,
                        SupervisoryComissionId = DefaultFields.DefaultSupervisoryCommision,
                        Status = StatusType.Active
                    };
                    _tpasRepo.AddTpa(tpaDto);
                }
                else if (RolesNames.IsInsuranceCompany(role.Name))
                {
                    var icDto = new InsuranceCompanyForCreateDto
                    {
                        Name = request.Title,
                        AddressId = request.AddressId,
                        EmailId = request.UserEmailId,
                        PhoneNumberId = request.UserPhoneNumberId,
                        UserId = user.Id,
                        RoleId = role.Id,
                        JoinedAt = DateTime.Now,
                        SupervisoryComissionId = DefaultFields.DefaultSupervisoryCommision,
                        Status = StatusType.Active
                    };
                    _icsRepo.AddInsurnceCompany(icDto);
                }
                else throw new Exception(_messages.GetString("UnknownRoleName"));
            }
            else throw new SaveChangesFailedException(result.Errors.First().Description);
        }

        private async Task RemoveUserFromRole(User user, string roleName)
        {
            var role = user.UserRoles.Find(r => r.Role.NormalizedName.ToUpper() == (roleName.ToUpper()));

            if (role == null || role.Role == null)
            {
                throw new NoDataFoundException(_messages.GetString("UserRoleNotFound"));
            }

            var request = user.UserRoleRequests
                .FirstOrDefault(r => r.RoleId == role.RoleId);
            request.Status = RoleRequestStatus.Removed;

            var result = await _userManager.RemoveFromRoleAsync(user, role.Role.Name).ConfigureAwait(false);
            if (result.Succeeded)
            {
                if (RolesNames.IsServiceProvider(roleName))
                {
                    _serviceProvidersRepo.DeleteServiceProvider(DetectServiceProviderTypeFromName(roleName), user.Id);
                }
                else if (RolesNames.IsServiceStaffer(roleName))
                {
                    _serviceStaffersRepo.DeleteServiceStaffer(DetectServiceStafferTypeFromName(roleName), user.Id);
                }
                else if (RolesNames.IsPatient(roleName))
                {
                    _patientsRepo.RemovePatient(user.Id);
                }
                else if (RolesNames.IsTpa(roleName))
                {
                    _tpasRepo.RemoveTpa(user.Id);
                }
                else if (RolesNames.IsInsuranceCompany(roleName))
                {
                    _icsRepo.RemoveInsuranceCompany(user.Id);
                }
                else throw new Exception(_messages.GetString("UnknownRoleName"));
            }
            throw new SaveChangesFailedException(result.Errors.First().Description);
        }

        private ServiceProviderType DetectServiceProviderTypeFromName(string rolename)
        {
            switch (rolename)
            {
                case RolesNames.Hospital:
                    return ServiceProviderType.Hospital;
                case RolesNames.Clinic:
                    return ServiceProviderType.Clinic;
                case RolesNames.Pharmacy:
                    return ServiceProviderType.Pharmacy;
                case RolesNames.Laboratory:
                    return ServiceProviderType.Laboratory;
                case RolesNames.RadiographOffice:
                    return ServiceProviderType.RadiographOffice;
                default:
                    throw new ArgumentBadException(_messages.GetString("RoleWithSpMatchFailed", rolename));
            }
        }

        private ServiceProviderStafferType DetectServiceStafferTypeFromName(string rolename)
        {
            switch (rolename)
            {
                case RolesNames.Doctor:
                    return ServiceProviderStafferType.Doctor;
                case RolesNames.Nurse:
                    return ServiceProviderStafferType.Nurse;
                case RolesNames.Pharmacist:
                    return ServiceProviderStafferType.Pharmacist;
                case RolesNames.LabSpecialist:
                    return ServiceProviderStafferType.LabSpecialist;
                case RolesNames.Radiographer:
                    return ServiceProviderStafferType.Radiographer;
                case RolesNames.Receptionist:
                    return ServiceProviderStafferType.Receptionist;
                default:
                    throw new ArgumentBadException(_messages.GetString("RoleWithSpMatchFailed", rolename));
            }
        }

        private UserAddress GetUserAddress(User user, string addressCode)
        {
            var address = user.UserAddresses
                .Where(ua => ua.Confirmed)
                .FirstOrDefault(ua => ua.Address.AddressCode.ToUpper() == (addressCode.ToUpper()));

            if (address == null)
            {
                throw new ArgumentBadException(_messages.GetString("userAddressCodeNotFound", user.UserName, addressCode));
            }
            return address;
        }

        private UserEmail GetUserEmail(User user, string emailAddress)
        {
            var email = user.UserEmails
                .Where(ue => ue.Confirmed)
                .FirstOrDefault(ue => ue.Address.ToUpper() == (emailAddress.ToUpper()));

            if (email == null)
            {
                throw new ArgumentBadException(_messages.GetString("UserEmailAddressNotFound", user.UserName, emailAddress));
            }
            return email;
        }

        private UserPhoneNumber GetUserPhoneNumber(User user, string phoneNumber)
        {
            var phone = user.UserPhoneNumbers
                .Where(up => up.Confirmed)
                .FirstOrDefault(ph => ph.Number.ToUpper() == (phoneNumber.ToUpper()));

            if (phone == null)
            {
                throw new ArgumentBadException(_messages.GetString("UserPhoneNumberNotFound", user.UserName, phoneNumber));
            }
            return phone;
        }
    }
}