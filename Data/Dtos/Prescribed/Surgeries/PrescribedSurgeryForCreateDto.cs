using System;
using System.ComponentModel.DataAnnotations;

namespace PatientsRecordsManager.Data.Dtos.Prescribed.Surgeries
{
    public class PrescribedSurgeryForCreateDto
    {
        [Required]
        public string SurgeryCode { get; set; }
        [Required]
        public DateTime? ScheduledDate { get; set; }
        [Required]
        public string Note { get; set; }
    }
}