using System;

namespace PatientsRecordsManager.Utils
{
    public static class Assert
    {
        public static void NotNull(object data)
        {
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }
        }
    }
}