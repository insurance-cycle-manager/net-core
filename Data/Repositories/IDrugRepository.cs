using System.Collections.Generic;
using PatientsRecordsManager.Data.Dtos.Drugs;

namespace PatientsRecordsManager.Data.Repositories
{
    public interface IDrugRepository
    {
        DrugForReturnDto AddDrug(DrugForCreateDto drug);
        DrugForReturnDto ModifyDrug(string drugCode, DrugForUpdateDto drug);
        bool DeleteDrug(string drugCode);
        DrugForReturnDto GetDrugDetails(string drugCode);
        IEnumerable<DrugForReturnDto> GetDrugs(string name, string manufacturer);
    }
}