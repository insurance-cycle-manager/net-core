using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Resources;
using Newtonsoft.Json;

namespace PatientsRecordsManager.Resources
{
    public class Messages
    {
        private readonly Dictionary<string, string> _data;

        public Messages()
        {
            // Initialize resource manager
            // _manager = new ResourceManager("PatientsRecordsManager.Resources", typeof(Messages).Assembly);

            // Fetch messages data
            var assembly = Assembly.GetEntryAssembly();
            var resourceStream = assembly.GetManifestResourceStream("PatientsRecordsManager.Resources.en.json");
            using var reader = new StreamReader(resourceStream);
            _data = JsonConvert.DeserializeObject<Dictionary<string, string>>(reader.ReadToEnd());
        }

        private string GetStringFromDictionary(string key)
        {
            if (!_data.ContainsKey(key))
            {
                throw new KeyNotFoundException("The given key was not present in the dictionary. " + key);
            }
            return _data[key];
        }

        public string GetString(string key, params object[] args)
        {
            var value = string.Format(CultureInfo.CurrentCulture, GetStringFromDictionary(key), args);
            // return _manager.GetString(value, CultureInfo.CurrentCulture);
            return value;
        }
    }
}