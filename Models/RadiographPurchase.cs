using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PatientsRecordsManager.Models
{
    public class RadiographPurchase
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public int PrescribedRadiographId { get; set; }
        public PrescribedRadiograph PrescribedRadiograph { get; set; }
        [Required]
        public DateTime DateOfPurchasing { get; set; }

        [ForeignKey("Prices")]
        public int PriceId { get; set; }
        public Price Price { get; set; }

        public int VisitId { get; set; }
        public Visit Visit { get; set; }
    }
}