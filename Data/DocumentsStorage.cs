using System;
using System.Globalization;
using System.IO;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using PatientsRecordsManager.Extensions;
using PatientsRecordsManager.Utils;

namespace PatientsRecordsManager.Data
{
    public class DocumentsStorage
    {
        private readonly string _baseUrl;
        private readonly UploaderSettings _configuration;
        private readonly IMapper _mapper;

        public DocumentsStorage(IMapper mapper, IOptions<UploaderSettings> configuration)
        {
            _mapper = mapper;
            if (configuration == null)
            {
                throw new ArgumentNullException(nameof(configuration));
            }
            _configuration = configuration.Value;
            _baseUrl = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
        }

        public async Task<string> AddDocumentToStorage(DocumentType type, IFormFile file)
        {
            if (file == null)
            {
                throw new ArgumentNullException(nameof(file));
            }
            var subPath = file.GenerateFileName();
            var path = Path.Combine(_baseUrl, _configuration.Internal, Enum.GetName(type.GetType(), type), subPath);

            using (var stream = System.IO.File.Create(path))
            {
                await file.CopyToAsync(stream).ConfigureAwait(false);
                return Path.Combine(_configuration.External, subPath);
            }
        }

        public Tuple<string, FileStream> GetDocumentFromStorage(DocumentType type, string file)
        {
            string typeName = Enum.GetName(type.GetType(), type).ToLower(CultureInfo.CurrentCulture);
            string path = Path.Combine(_baseUrl, _configuration.Internal, typeName, file);

            var contentType = MimeTypeUtil.GetValue(Path.GetExtension(path).ToLower(CultureInfo.CurrentCulture));
            // var contentType = string.Format("{0}/{1}", typeName, "jpeg");
            using(var stream = System.IO.File.OpenRead(path))
            {
                return new Tuple<string, FileStream>(contentType, stream);
            }
        }

        public enum DocumentType
        {
            IMAGE,
            VIDEO,
            TEXT
        }
    }
}