using PatientsRecordsManager.Data.Dtos.Prices;

namespace PatientsRecordsManager.Data.Dtos.Surgeries
{
    public class SurgeryForUpdateDto
    {
        public string Name { get; set; }
        public PriceForAddDto Price { get; set; }
    }
}