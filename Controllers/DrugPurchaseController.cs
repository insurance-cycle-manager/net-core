using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PatientsRecordsManager.ABAC.Extensions.EF;
using PatientsRecordsManager.Constants;
using PatientsRecordsManager.Data;
using PatientsRecordsManager.Data.Dtos.Drugs;
using PatientsRecordsManager.Extensions;
using PatientsRecordsManager.Models;
using PatientsRecordsManager.Resources;
using PatientsRecordsManager.Utils;
using PatientsRecordsManager.Utils.Exceptions;

namespace PatientsRecordsManager.Controllers
{
    [Authorize]
    [ApiController]
    [Route(APIs.DefaultRoute)]
    public class DrugPurchaseController : PrmController
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        private readonly Messages _messages;

        public DrugPurchaseController(DataContext dataContext, IMapper mapper, Messages messages)
        {
            _mapper = mapper;
            _messages = messages;
            _context = dataContext;
        }

        [HttpGet]
        public IActionResult GetDurgPurchases()
        {
            var drugs = _context.DrugPurchases.Select(dp => _mapper.Map<DrugPurchaseForReturnDto>(dp));
            return HttpContext.Success(drugs, _messages.GetString("DrugPurchasesFetched"));
        }

        [HttpGet(APIs.GetDrugPurchase)]
        public IActionResult GetDurgPurchase(string prescriptionCode, string drugCode)
        {
            DrugPurchase drugPurchase;

            var prescriptionPurchase = FetchPrescriptionPurchase(prescriptionCode);
            drugPurchase = FetchDrugPurchase(prescriptionPurchase, drugCode);

            return HttpContext.Success(_mapper.Map<DrugPurchaseForReturnDto>(drugPurchase), _messages.GetString("DrugPurchaseFetched"));
        }


        [HttpPost(APIs.AddDrugPurchase)]
        public IActionResult AddDrugPurchase(string prescriptionCode,
                                                         [FromBody] DrugPurchaseForAddDto purchaseToAdd)
        {
            Assert.NotNull(purchaseToAdd);

            var prescriptionPurchase = FetchPrescriptionPurchase(prescriptionCode);
            FetchAndInsertDrugs(purchaseToAdd, prescriptionPurchase);

            if (_context.SaveChanges() < 1)
                throw new Exception(_messages.GetString("DrugPurchaseAddFailed"));

            return HttpContext.Success(new
            {
                Purchase = purchaseToAdd,
                PrescriptionCode = prescriptionCode
            }, _messages.GetString("DrugPurchaseCreated"));
        }

        private void FetchAndInsertDrugs(DrugPurchaseForAddDto purchaseToAdd, PrescriptionPurchase prescriptionPurchase)
        {
            var drugsFetched = FetchDrugs(purchaseToAdd.DrugCodes);
            var visit = FetchVisit(purchaseToAdd.VisitCode);

            foreach (var drug in drugsFetched)
            {
                var durgPurchase = _mapper.Map<DrugPurchase>(purchaseToAdd);
                // durgPurchase.DrugCode = drug.DrugCode;
                durgPurchase.Drug = drug;
                durgPurchase.PrescriptionPurchase = prescriptionPurchase;
                _context.DrugPurchases.Add(durgPurchase);
            }
        }

        private PrescriptionPurchase FetchPrescriptionPurchase(string prescriptionCode)
        {
            return _context.PrescriptionPurchases
                .Include(pp => pp.Prescription)
                .ThenInclude(p => p.Drugs)
                .ThenInclude(pd => pd.Drug)
                .FirstOrDefault(pp => pp.Prescription.PrescriptionCode.ToUpper() == prescriptionCode.ToUpper())
                ?? throw new NoDataFoundException(_messages.GetString("PrescriptionPurchaseNotFound"));
        }

        private DrugPurchase FetchDrugPurchase(PrescriptionPurchase prescriptionPurchase, string drugCode)
        {
            return _context.DrugPurchases
                .FirstOrDefault(dp =>
                    dp.PrescriptionPurchase == prescriptionPurchase && dp.Drug.DrugCode.ToUpper() == drugCode.ToUpper())
                    ?? throw new NoDataFoundException(_messages.GetString("DrugPurchaseNotFound"));
        }

        private IEnumerable<Drug> FetchDrugs(IEnumerable<string> drugCodes)
        {
            var drugs = _context.Drugs
                .Where(d => drugCodes.Contains(d.DrugCode))
                .DefaultIfEmpty();

            if (drugs == null) throw new NoDataFoundException(_messages.GetString("DrugNotFound2"));

            return drugs;
        }

        private Visit FetchVisit(string visitCode)
        {
            return _context.Visits
               .FirstOrDefault(v => v.VisitCode.ToUpper() == visitCode.ToUpper())
               ?? throw new NoDataFoundException(_messages.GetString("VisitNotFound2"));
        }
    }
}