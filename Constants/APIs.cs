﻿namespace PatientsRecordsManager.Constants
{
    public static class APIs
    {
        public const string DefaultRoute = "[controller]";
        public const string Auth = "auth";
        public const string Login = "login";
        public const string Register = "register";
        public const string UsersWithRoles = "usersWithRoles";
        // 
        // Insurance Companies
        // 
        public const string InsuranceCompaniesBaseUrl = DefaultRoute;
        public const string GetInsuranceCompanies = "";
        public const string GetInsuranceCompanyMe = "me";
        public const string GetInsuranceCompany = "{icCode}";
        public const string ModifyInsuranceCompanyMe = "me";
        public const string ModifyInsuranceCompany = "{icCode}";
        // 
        // Patients
        //
        public const string PatientsBaseUrl = "Patients";
        public const string GetPatients = "";
        public const string GetPatientDetails = "{patientCode}";
        public const string GetPatientMeDetails = "me";
        public const string ModifyPatient = "{patientCode}";
        public const string ModifyPatientMe = "me";
        public const string AddPatientPackage = "{patientCode}/packages";
        public const string AddPatientMePackage = "me/packages";
        public const string GetPatientPackage = "{patientCode}/packages";
        public const string GetPatientMePackage = "me/packages";
        public const string SuspendPatientPackage = "me/packages/{packageCode}";
        //
        // TPAs
        // 
        public const string TpasBaseUrl = DefaultRoute;
        public const string GetTpas = "";
        public const string GetTpaMe = "me";
        public const string GetTpa = "{tpaCode}";
        public const string ModifyTpaMe = "me";
        public const string ModifyTpa = "{tpaCode}";
        //
        // Insurance Connections
        // 
        public const string InsuranceConnectionsBaseUrl = DefaultRoute;
        public const string AddConnection = "";
        public const string DeleteConnection = "{connectionCode}";
        // 
        // Insurance Packages
        // 
        public const string InsurancePackagesBaseUrl = DefaultRoute;
        public const string GetPackages = "";
        public const string IcPackagesBaseUrl = "{companyCode}";
        public const string GetIcPackages = IcPackagesBaseUrl;
        public const string AddInsurancePackage = IcPackagesBaseUrl;
        public const string ManagePatientPackage = "me/manage";
        public const string IcPackageBaseUrl = "{companyCode}/{packageCode}";
        public const string GetPackagePatients = IcPackageBaseUrl + "/Patients";
        public const string GetInsurancePackage = IcPackageBaseUrl + "";
        public const string DeleteInsurancePackage = IcPackageBaseUrl + "";
        public const string ModifyInsurancePackage = IcPackageBaseUrl + "";
        // 
        // Service providers
        // 
        public const string ServiceProvidersBaseUrl = "serviceProviders/{spType}/";
        public const string GetServiceProviders = "";
        public const string GetServiceProviderDetails = "{spCode}";
        public const string GetMyServiceProviderDetails = "me";
        public const string ModifyServiceProvider = "{spCode}";
        public const string ModifyMeServiceProvider = "me";
        public const string ChangeServiceProviderStatus = "{spCode}/changeStatus/{newStatus}";
        // 
        // Service staffers
        // 
        public const string ServiceStaffersBaseUrl = "serviceStaffers/{stType}/";
        public const string GetServiceStaffers = "";

        public const string GetServiceStafferDetails = "{stCode}";
        public const string GetMeServiceStafferDetails = "me";
        public const string ModifyServiceStaffer = "{stCode}";
        public const string ModifyMeServiceStaffer = "me";
        public const string ChangeServiceStafferStatus = "{stCode}/changeStatus/{newStatus}";
        // 
        // Staff
        // 
        public const string GetStafferStaff = "{stCode}/Staff";
        public const string GetStafferMeStaff = "me/Staff";
        public const string GetProviderStaff = "{spCode}/Staff";
        public const string GetProviderMeStaff = "me/Staff";
        public const string AddStaffMember = "{spCode}/Staff";
        public const string ConfirmStaffMember = "{stCode}/Staff/{spCode}";
        public const string DeleteStaffMember = "{spCode}/Staff/{stCode}";
        // 
        // SP Connections
        // 
        public const string SPsConnectionsBaseUrl = "spConnections";
        public const string ConnectionsFetch = "";
        public const string SpConnectionsBaseUrl = "sp/{spType}";
        public const string SpMeConnectionsFetch = SpConnectionsBaseUrl + "/me";
        public const string SpConnectionsFetch = SpConnectionsBaseUrl + "/{spCode}";
        public const string SpConnectionsInvite = SpConnectionsBaseUrl + "/me";
        public const string SpConnectionsModify = SpConnectionsBaseUrl + "/me/{connectionCode}";
        public const string SpConnectionsStatus = SpConnectionsBaseUrl + "/me/{connectionCode}/status";
        public const string TpaConnectionsBaseUrl = "tpa/me";
        public const string TpaConnectionsFetch = TpaConnectionsBaseUrl + "";
        public const string TpaConnectionsInvite = TpaConnectionsBaseUrl + "";
        public const string TpaConnectionsModify = TpaConnectionsBaseUrl + "/{connectionCode}";
        public const string TpaConnectionsStatus = TpaConnectionsBaseUrl + "/{connectionCode}/status";
        //
        // Visits
        // 
        public const string GetVisits = "";
        public const string GetVisit = "{visitCode}";
        public const string AddVisit = "{stType}/me";
        public const string ModifyVisit = "{stType}/me/{visitCode}";
        public const string DeleteVisit = "{stType}/{stCode}/{visitCode}";
        //
        // Diangosis
        // 
        public const string VisitDiagnosisBaseUrl = "Visits/{visitCode}/Diagnosis";
        public const string AddDiagnosis = "";
        public const string ModifyDiagnosis = "{diagnosisCode}";
        public const string DeleteDiagnosis = "{diagnosisCode}";
        //
        // Prescriptions
        // 
        public const string VisitPrescriptionsBaseUrl = "Visits/{visitCode}/Prescriptions";
        public const string GetPrescriptions = "";
        public const string GetPrescription = "{prescriptionCode}";
        public const string AddPrescription = "";
        public const string ModifyPrescription = "{prescriptionCode}";
        public const string DeletePrescription = "{prescriptionCode}";
        public const string PurchasedPrescriptionBaseUrl = VisitPrescriptionsBaseUrl + "/{prescriptionCode}/Purchase";
        public const string AddPrescriptionPurchase = PurchasedPrescriptionBaseUrl + "";
        public const string GetPrescriptionPurchase = PurchasedPrescriptionBaseUrl + "";

        // 
        // Drugs
        // 
        public const string GetDrugs = "";
        public const string GetDrugDetails = "{drugCode}";
        public const string PostDrug = "";
        public const string DeleteDrug = "{drugCode}";
        public const string ModifyDrug = "{drugCode}";
        public const string AddDrugPurchase = "{prescriptionCode}";
        public const string GetDrugPurchase = AddDrugPurchase;

        // 
        // Surgeries
        //
        public const string SurgeriesBaseUrl = DefaultRoute;
        public const string GetSurgeries = "";
        public const string GetSurgery = "{surgeryCode}";
        public const string AddSurgery = "";
        public const string ModifySurgery = "{surgeryCode}";
        public const string DeleteSurgery = "{surgeryCode}";
        public const string VisitSurgeriesBaseUrl = "Visits/{visitCode}/Surgeries";
        public const string PurchasedSurgeriesBaseUrl = VisitSurgeriesBaseUrl + "/{prescribedSurgeryCode}/Purchase";
        public const string AddSurgeryPurchase = PurchasedSurgeriesBaseUrl + "";
        public const string GetSurgeryPurchase = PurchasedSurgeriesBaseUrl + "";
        // 
        // Lab Tests
        // 
        public const string LabTestsBaseUrl = DefaultRoute;
        public const string VisitLabTestsBaseUrl = "Visits/{visitCode}/LabTests";
        public const string GetLabTests = "";
        public const string GetLabTestDetails = "{labTestCode}";
        public const string PostLabTest = "";
        public const string DeleteLabTest = "{labTestCode}";
        public const string ModifyLabTest = "{labTestCode}";
        public const string PurchasedLabTestsBaseUrl = VisitLabTestsBaseUrl + "/{prescribedLabTestCode}/Purchase";
        public const string AddLabTestPurchase = PurchasedLabTestsBaseUrl + "";
        public const string GetLabTestPurchase = PurchasedLabTestsBaseUrl + "";
        // 
        // Radiographs
        // 
        public const string RadiographsBaseUrl = DefaultRoute;
        public const string VisitRadiographsBaseUrl = "Visits/{visitCode}/Radiographs";
        public const string GetRadiographs = "";
        public const string GetRadiographDetails = "{radiographCode}";
        public const string PostRadiograph = "";
        public const string DeleteRadiograph = "{radiographCode}";
        public const string ModifyRadiograph = "{radiographCode}";
        public const string ChangeRadiographPrice = "{radiographCode}/price";
        public const string PurchasedRadiographsBaseUrl = VisitRadiographsBaseUrl + "/{prescribedRadiographCode}/Purchase";
        public const string AddRadiographPurchase = PurchasedRadiographsBaseUrl + "";
        public const string GetRadiographPurchase = PurchasedRadiographsBaseUrl + "";
        // 
        // User Account
        // 
        public const string UsersBaseUrl = DefaultRoute;
        public const string GetAccountInformation = "{username}";
        public const string GetAccountInformationMe = "me";
        public const string ModifyAccountInformation = "{username}";
        public const string ModifyAccountInformationMe = "me";
        // 
        // User roles
        //
        public const string AdminPrefix = "Admin/";
        public const string UserRolesPrefix = "UserRoles/";
        public const string GetRolesRequests = "";
        public const string AddUserRole = "";
        public const string RequestUserRole = "";
        public const string DeleteUserRole = "{rolename}";
        public const string UnAssignUserFromRole = "{username}/{rolename}";
        public const string ManageRoleRequest = "Manage";
        public const string GetUserRoles = "{username}";
        // 
        // User Phone Numbers
        // 
        public const string UserPhonesBaseUrl = "UserPhones";
        public const string GetUserPhones = "";
        public const string GetPendingUserPhones = "Pending";
        public const string PostUserPhone = "";
        public const string ModifyUserPhone = "{phoneCode}";
        public const string ConfirmUserPhone = "{userName}/{phoneCode}/Confirm";
        public const string DeleteUserPhone = "{phoneCode}";
        // 
        // User Email
        // 
        public const string UserEmailsBaseUrl = "UserEmails";
        public const string GetUserEmails = "";
        public const string GetPendingUserEmails = "Pending";
        public const string PostUserEmail = "";
        public const string ModifyUserEmail = "{emailCode}";
        public const string ConfirmUserEmail = "{userName}/{emailCode}/Confirm";
        public const string DeleteUserEmail = "{emailCode}";
        // 
        // User Addresses
        // 
        public const string UserAddressesBaseUrl = "UserAddresses";
        public const string GetUserAddresses = "";
        public const string GetPendingUserAddresses = "Pending";
        public const string PostUserAddress = "";
        public const string ModifyUserAddress = "{addressCode}";
        public const string ConfirmUserAddress = "{userName}/{addressCode}/Confirm";
        public const string DeleteUserAddress = "{addressCode}";
        //
        // File Upload
        // 
        public const string Upload = "Upload/{docType}";
        public const string Download = "Download/{docType}/{docPath}";
        //
        // Claims
        // 
        public const string VisitClaim = "Visits/{visitCode}/Claim";
        public const string ClaimsBaseUrl = "Claims/";
        public const string GetClaims = ClaimsBaseUrl + "{requester}";
        public const string ManageClaim = ClaimsBaseUrl + "{requester}/{claimCode}";
        // 
        // Packages
        // 
        public const string PackagesBaseUrl = "Packages";
        // 
        // Addresses
        // 
        public const string AddressesBaseUrl = DefaultRoute;
        public const string GetAddresses = "";
        public const string PostAddress = "";
        public const string ModifyAddress = "{addressCode}";
        ///
        //Stats
        //
        public const string StatsBaseUrl = DefaultRoute;
        public const string PurchaseCounts = "PurchaseCounts";
        public const string PurchaseAmounts = "PurchaseAmounts";
        public const string PatientGenders = "Genders";
        public const string VisitCounts = "VisitCounts";
        public const string SpCounts = "SPCounts";

        public const string AccessRequestCounts = "ACCounts";
        public const string PermittedDenied = "permitted";
        public const string MinMaxAvg = "MinMaxAvg";




        // 
        // 
        // 
        public const string RulesRequests = "requests";
    }
}
