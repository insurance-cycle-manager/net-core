using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using PatientsRecordsManager.ABAC.Exceptions;
using PatientsRecordsManager.ABAC.Extensions;
using PatientsRecordsManager.Utils;

namespace PatientsRecordsManager.Extensions
{
    public static class HttpResponseExtension
    {
        private static object Compose(HttpResponse response, object data, string message, string url, string expose, string origin)
        {
            response.Headers.Clear();
            response.Headers.Add("Access-Control-Expose-Headers", expose);
            response.Headers.Add("Access-Control-Allow-Origin", origin);
            // response.Headers.Add("Content-Type", "application/json; charset=utf-8");
            response.Headers.Add("Content-Type", "application/json");

            return new
            {
                Data = data,
                Message = message,
                Url = url
            };
        }

        public static IActionResult Success(this HttpContext context, object data, string message)
        {
            Assert.NotNull(context);

            var value = Compose(context.Response,
                data,
                message,
                context.Request.Path.Value,
                "Success",
                "*");

            var statusCode = StatusCodes.Status200OK;
            var result = new ResponseResult(value, statusCode);

            // context.Response.StatusCode = statusCode;
            return new Response(result);
        }

        public static IActionResult Error(this HttpContext context, object data, string message)
        {
            Assert.NotNull(context);

            var value = Compose(context.Response,
                data,
                message,
                context.Request.Path.Value,
                "User-Error",
                "*");

            var statusCode = StatusCodes.Status400BadRequest;
            var result = new ResponseResult(value, statusCode);

            // context.Response.StatusCode = statusCode;

            return new Response(result);
        }

        public static IActionResult InternalServerError(this HttpContext context, Exception exception)
        {
            Assert.NotNull(context);
            Assert.NotNull(exception);

            var value = Compose(context.Response,
                null,
                exception.Message,
                context.Request.Path.Value,
                "Application-Server-Error",
                "*");

            var statusCode = StatusCodes.Status500InternalServerError;
            var result = new ResponseResult(value, statusCode);

            // context.Response.StatusCode = statusCode;

            return new Response(result);
        }

        public static async Task InternalServerErrorAsync(this HttpContext context)
        {
            Assert.NotNull(context);

            var exceptionHandlerPathFeature =
                context.Features.Get<IExceptionHandlerPathFeature>();

            // Use exceptionHandlerPathFeature to process the exception 
            // (for example, logging), but do NOT expose sensitive error information 
            // directly to the client.

            var exception = exceptionHandlerPathFeature?.Error;
            if (exception is AbacEnforceException)
            {
                await context.UnauthorizedMiddleware(exception.Message).ConfigureAwait(false);
            }
            else
            {
                var value = Compose(context.Response,
                    null,
                    exception.Message,
                    context.Request.Path.Value,
                    "Application-Server-Error",
                    "*");
                context.Response.StatusCode = StatusCodes.Status400BadRequest;
                await context.Response.WriteAsync(JsonConvert.SerializeObject(value)).ConfigureAwait(false);
            }
        }

        public static IActionResult Unauthorized(this HttpContext context, string message)
        {
            Assert.NotNull(context);

            var value = Compose(context.Response,
                null,
                message,
                context.Request.Path.Value,
                "User-Unauthorized",
                "*");

            var statusCode = StatusCodes.Status401Unauthorized;
            var result = new ResponseResult(value, statusCode);

            // context.Response.StatusCode = statusCode;
            // await context.Response.Body.WriteAsync(System.Text.Encoding.UTF8.GetBytes(message));

            return new Response(result);
        }

        public static IActionResult NotFound(this HttpContext context, string message)
        {
            Assert.NotNull(context);

            var value = Compose(context.Response,
                null,
                message,
                context.Request.Path.Value,
                "Url-Not-Found",
                "*");

            var statusCode = StatusCodes.Status404NotFound;
            var result = new ResponseResult(value, statusCode);

            // context.Response.StatusCode = statusCode;

            return new Response(result);
        }

        public static int GetUserId(this HttpContext context)
        {
            Assert.NotNull(context);
            Assert.NotNull(context.User);

            string claim = context.User.FindFirstValue(ClaimTypes.NameIdentifier);
            if (string.IsNullOrEmpty(claim))
            {
                return int.MinValue;
            }
            return int.Parse(claim, System.Globalization.CultureInfo.CurrentCulture);
        }


        class Response : IActionResult
        {
            private ResponseResult _result;

            public Response(ResponseResult result)
            {
                _result = result;
            }

            public async Task ExecuteResultAsync(ActionContext context)
            {
                await _result.ExecuteResultAsync(context).ConfigureAwait(false);
            }
        }

        class ResponseResult : ObjectResult
        {
            public ResponseResult(object data, int statusCode) : base(data)
            {
                StatusCode = statusCode;
            }
        }
    }
}