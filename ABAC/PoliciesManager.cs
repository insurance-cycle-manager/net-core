using System;
using System.Collections.Generic;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using PatientsRecordsManager.ABAC.Enforcers;
using PatientsRecordsManager.ABAC.Exceptions;
using PatientsRecordsManager.ABAC.Extensions;

namespace PatientsRecordsManager.ABAC
{
    public class PoliciesManager
    {
        // public static PoliciesManager Instance { get; set; }
        // public static PoliciesManager Initialize(IServiceProvider provider)
        // {
        //     if (provider == null)
        //         throw new ArgumentNullException(nameof(provider));

        //     Instance = new PoliciesManager(provider);
        //     return Instance;
        // }
        // 
        // End static singleton area
        // 
        private readonly IServiceProvider _provider;

        public PoliciesManager(IServiceProvider provider)
        {
            _provider = provider;
        }

        // 
        // Check if requester is allowed to access system action
        // 
        public bool EvaluateRequestOfAction(string requestId, ClaimsPrincipal sub, object o, string act)
        {
            var en = _provider.GetService(typeof(ActionEnforcer)) as ActionEnforcer;
            var result = en.Evaluate(requestId, sub, o, act);

            return result;
        }
        //
        // 

        // 
        // Check if requester is allowed to access db objects
        // 
        public void EvaluateRequestOfDbo(string requestId, ClaimsPrincipal sub, string o, IEnumerable<string> attributes, string act)
        {
            var en = _provider.GetService(typeof(DboEnforcer)) as DboEnforcer;
            var result = en.Evaluate(requestId, sub, o, attributes, act);

            // var en = _provider.GetService(typeof(ActionEnforcer)) as ActionEnforcer;
            // var result = en.Evaluate(requestId, sub, o, act);

            if (!result)
            {
                throw new AbacEnforceException();
                //new HttpContextAccessor { }.HttpContext.UnauthorizedMiddleware("OKay");
            }
        }
    }
}