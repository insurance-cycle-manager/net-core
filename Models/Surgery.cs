using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PatientsRecordsManager.Models
{
    public class Surgery
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string NormalizedName { get; set; }
        [Required]
        public string SurgeryCode { get; set; }

        [ForeignKey("Prices")]
        public int? PriceId { get; set; }
        public Price Price { get; set; }

        public List<PackageSurgery> Packages { get; set; }
        public List<PrescribedSurgery> Surgeries { get; set; }
        public List<SurgeryPurchase> Purchases { get; set; }
    }
}