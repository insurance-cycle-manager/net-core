namespace PatientsRecordsManager.Data.Dtos.Permissions
{
    public class PermissionForReturnDto
    {
        public string Name { get; set; }
    }
}