using System.ComponentModel.DataAnnotations;
using PatientsRecordsManager.Constants.Enums;

namespace PatientsRecordsManager.Data.Dtos.Users.RolesRequests
{
    public class ManageRoleRequestDto
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        public string RoleName { get; set; }
        [Required]
        [EnumDataType(typeof(RoleRequestResponse))]
        public RoleRequestResponse? Response { get; set; } // reject or accept
    }
}