using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using PatientsRecordsManager.Constants.Enums;

namespace PatientsRecordsManager.Models
{
    public class ServiceProvider
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string ServiceProviderCode { get; set; }

        [Required]
        public string Title { get; set; }
        [Required]
        public string NormalizedTitle { get; set; }

        [ForeignKey("Statuses")]
        public int? StatusId { get; set; }
        public Status Status { get; set; }

        [Required]
        public DateTime JoinedAt { get; set; }

        [Required]
        [EnumDataType(typeof(ServiceProviderType))]
        public ServiceProviderType Type { get; set; }

        [Required]
        [ForeignKey("Users")]
        public int UserId { get; set; }
        public User User { get; set; }

        [Required]
        [ForeignKey("Roles")]
        public int RoleId { get; set; }
        public Role Role { get; set; }
        public UserRole UserRole { get; set; }

        [Required]
        [ForeignKey("Addresses")]
        public int AddressId { get; set; }
        public UserAddress Address { get; set; }

        [Required]
        [ForeignKey("UserEmails")]
        public int EmailId { get; set; }
        public UserEmail Email { get; set; }

        [Required]
        [ForeignKey("UserPhoneNumbers")]
        public int PhoneNumberId { get; set; }
        public UserPhoneNumber PhoneNumber { get; set; }


        public IEnumerable<ServiceProviderStaffer> Staff { get; set; }
        public IEnumerable<Visit> Visits { get; set; }
        public IEnumerable<ServiceProviderConnection> Connections { get; set; }
        public IEnumerable<VisitClaim> VisitClaims { get; set; }
    }
}