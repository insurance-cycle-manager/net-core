using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace PatientsRecordsManager.ABAC.Extensions.Linq
{
    public static class Queryable
    {
        public static TSource Aggregate<TSource>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, TSource, TSource>> func)
        {
            return System.Linq.Queryable.Aggregate<TSource>(source.Visit(Evaluate.Action.READ), func);
        }
        public static TAccumulate Aggregate<TSource, TAccumulate>(this System.Linq.IQueryable<TSource> source, TAccumulate seed, Expression<Func<TAccumulate, TSource, TAccumulate>> func)
        {
            return System.Linq.Queryable.Aggregate<TSource, TAccumulate>(source.Visit(Evaluate.Action.READ), seed, func);
        }
        public static TResult Aggregate<TSource, TAccumulate, TResult>(this System.Linq.IQueryable<TSource> source, TAccumulate seed, Expression<Func<TAccumulate, TSource, TAccumulate>> func, Expression<Func<TAccumulate, TResult>> selector)
        {
            return System.Linq.Queryable.Aggregate<TSource, TAccumulate, TResult>(source.Visit(Evaluate.Action.READ), seed, func, selector);
        }

        public static bool All<TSource>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate)
        {
            return System.Linq.Queryable.All<TSource>(source.Visit(Evaluate.Action.READ), predicate);
        }

        public static bool Any<TSource>(this System.Linq.IQueryable<TSource> source)
        {
            return System.Linq.Queryable.Any<TSource>(source.Visit(Evaluate.Action.READ));
        }

        public static bool Any<TSource>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate)
        {
            return System.Linq.Queryable.Any<TSource>(source.Visit(Evaluate.Action.READ), predicate);
        }

        public static System.Linq.IQueryable<TSource> Append<TSource>(this System.Linq.IQueryable<TSource> source, TSource element)
        {
            return System.Linq.Queryable.Append<TSource>(source.Visit(Evaluate.Action.READ), element);
        }

        public static System.Linq.IQueryable<TElement> AsQueryable<TElement>(this IEnumerable<TElement> source)
        {
            return System.Linq.Queryable.AsQueryable<TElement>(source.Visit(Evaluate.Action.READ));
        }

        public static System.Linq.IQueryable AsQueryable(this IEnumerable source)
        {
            return System.Linq.Queryable.AsQueryable(source.Visit(Evaluate.Action.READ));
        }

        public static double Average<TSource>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, double>> selector)
        {
            return System.Linq.Queryable.Average<TSource>(source.Visit(Evaluate.Action.READ), selector);
        }

        public static float? Average<TSource>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, float?>> selector)
        {
            return System.Linq.Queryable.Average<TSource>(source.Visit(Evaluate.Action.READ), selector);
        }

        public static double? Average<TSource>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, long?>> selector)
        {
            return System.Linq.Queryable.Average<TSource>(source.Visit(Evaluate.Action.READ), selector);
        }

        public static double? Average<TSource>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, int?>> selector)
        {
            return System.Linq.Queryable.Average<TSource>(source.Visit(Evaluate.Action.READ), selector);
        }

        public static double? Average<TSource>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, double?>> selector)
        {
            return System.Linq.Queryable.Average<TSource>(source.Visit(Evaluate.Action.READ), selector);
        }

        public static decimal? Average<TSource>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, decimal?>> selector)
        {
            return System.Linq.Queryable.Average<TSource>(source.Visit(Evaluate.Action.READ), selector);
        }

        public static double Average<TSource>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, long>> selector)
        {
            return System.Linq.Queryable.Average<TSource>(source.Visit(Evaluate.Action.READ), selector);
        }

        public static double Average<TSource>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, int>> selector)
        {
            return System.Linq.Queryable.Average<TSource>(source.Visit(Evaluate.Action.READ), selector);
        }

        public static decimal Average<TSource>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, decimal>> selector)
        {
            return System.Linq.Queryable.Average<TSource>(source.Visit(Evaluate.Action.READ), selector);
        }

        public static double? Average(this System.Linq.IQueryable<double?> source)
        {
            return System.Linq.Queryable.Average(source);
        }

        public static float? Average(this System.Linq.IQueryable<float?> source)
        {
            return System.Linq.Queryable.Average(source);
        }

        public static double? Average(this System.Linq.IQueryable<long?> source)
        {
            return System.Linq.Queryable.Average(source);
        }

        public static double? Average(this System.Linq.IQueryable<int?> source)
        {
            return System.Linq.Queryable.Average(source);
        }

        public static float Average<TSource>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, float>> selector)
        {
            return System.Linq.Queryable.Average<TSource>(source, selector);
        }

        public static decimal? Average(this System.Linq.IQueryable<decimal?> source)
        {
            return System.Linq.Queryable.Average(source);
        }

        public static double Average(this System.Linq.IQueryable<long> source)
        {
            return System.Linq.Queryable.Average(source);
        }

        public static double Average(this System.Linq.IQueryable<int> source)
        {
            return System.Linq.Queryable.Average(source);
        }

        public static double Average(this System.Linq.IQueryable<double> source)
        {
            return System.Linq.Queryable.Average(source);
        }

        public static decimal Average(this System.Linq.IQueryable<decimal> source)
        {
            return System.Linq.Queryable.Average(source);
        }

        public static float Average(this System.Linq.IQueryable<float> source)
        {
            return System.Linq.Queryable.Average(source);
        }

        public static System.Linq.IQueryable<TResult> Cast<TResult>(this System.Linq.IQueryable source)
        {
            return System.Linq.Queryable.Cast<TResult>(source.Visit(Evaluate.Action.READ));
        }

        public static System.Linq.IQueryable<TSource> Concat<TSource>(this System.Linq.IQueryable<TSource> source1, IEnumerable<TSource> source2)
        {
            return System.Linq.Queryable.Concat<TSource>(source1.Visit(Evaluate.Action.READ), source2.Visit(Evaluate.Action.READ));
        }

        public static bool Contains<TSource>(this System.Linq.IQueryable<TSource> source, TSource item)
        {
            return System.Linq.Queryable.Contains<TSource>(source.Visit(Evaluate.Action.READ), item);
        }

        public static bool Contains<TSource>(this System.Linq.IQueryable<TSource> source, TSource item, IEqualityComparer<TSource> comparer)
        {
            return System.Linq.Queryable.Contains<TSource>(source.Visit(Evaluate.Action.READ), item, comparer);
        }

        public static int Count<TSource>(this System.Linq.IQueryable<TSource> source)
        {
            return System.Linq.Queryable.Count<TSource>(source.Visit(Evaluate.Action.READ));
        }

        public static int Count<TSource>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate)
        {
            return System.Linq.Queryable.Count<TSource>(source.Visit(Evaluate.Action.READ), predicate);
        }

        public static System.Linq.IQueryable<TSource> DefaultIfEmpty<TSource>(this System.Linq.IQueryable<TSource> source)
        {
            return System.Linq.Queryable.DefaultIfEmpty<TSource>(source.Visit(Evaluate.Action.READ));
        }

        public static System.Linq.IQueryable<TSource> DefaultIfEmpty<TSource>(this System.Linq.IQueryable<TSource> source, TSource defaultValue)
        {
            return System.Linq.Queryable.DefaultIfEmpty<TSource>(source.Visit(Evaluate.Action.READ), defaultValue);
        }

        public static System.Linq.IQueryable<TSource> Distinct<TSource>(this System.Linq.IQueryable<TSource> source)
        {
            return System.Linq.Queryable.Distinct<TSource>(source.Visit(Evaluate.Action.READ));
        }

        public static System.Linq.IQueryable<TSource> Distinct<TSource>(this System.Linq.IQueryable<TSource> source, IEqualityComparer<TSource> comparer)
        {
            return System.Linq.Queryable.Distinct<TSource>(source.Visit(Evaluate.Action.READ), comparer);
        }

        public static TSource ElementAt<TSource>(this System.Linq.IQueryable<TSource> source, int index)
        {
            return System.Linq.Queryable.ElementAt<TSource>(source.Visit(Evaluate.Action.READ), index);
        }

        public static TSource ElementAtOrDefault<TSource>(this System.Linq.IQueryable<TSource> source, int index)
        {
            return System.Linq.Queryable.ElementAtOrDefault<TSource>(source.Visit(Evaluate.Action.READ), index);
        }

        public static System.Linq.IQueryable<TSource> Except<TSource>(this System.Linq.IQueryable<TSource> source1, IEnumerable<TSource> source2)
        {
            return System.Linq.Queryable.Except<TSource>(source1.Visit(Evaluate.Action.READ), source2.Visit(Evaluate.Action.READ));
        }

        public static System.Linq.IQueryable<TSource> Except<TSource>(this System.Linq.IQueryable<TSource> source1, IEnumerable<TSource> source2, IEqualityComparer<TSource> comparer)
        {
            return System.Linq.Queryable.Except<TSource>(source1.Visit(Evaluate.Action.READ), source2.Visit(Evaluate.Action.READ), comparer);
        }

        public static TSource First<TSource>(this System.Linq.IQueryable<TSource> source)
        {
            return System.Linq.Queryable.First<TSource>(source.Visit(Evaluate.Action.READ));
        }

        public static TSource First<TSource>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate)
        {
            return System.Linq.Queryable.First<TSource>(source.Visit(Evaluate.Action.READ), predicate);
        }

        public static TSource FirstOrDefault<TSource>(this System.Linq.IQueryable<TSource> source)
        {
            return System.Linq.Queryable.FirstOrDefault<TSource>(source.Visit(Evaluate.Action.READ));
        }

        public static TSource FirstOrDefault<TSource>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate)
        {
            return System.Linq.Queryable.FirstOrDefault<TSource>(source.Visit(Evaluate.Action.READ), predicate);
        }

        public static System.Linq.IQueryable<TResult> GroupBy<TSource, TKey, TElement, TResult>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, TKey>> keySelector, Expression<Func<TSource, TElement>> elementSelector, Expression<Func<TKey, IEnumerable<TElement>, TResult>> resultSelector, IEqualityComparer<TKey> comparer)
        {
            return System.Linq.Queryable.GroupBy<TSource, TKey, TElement, TResult>(source.Visit(Evaluate.Action.READ), keySelector, elementSelector, resultSelector, comparer);
        }

        public static System.Linq.IQueryable<TResult> GroupBy<TSource, TKey, TResult>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, TKey>> keySelector, Expression<Func<TKey, IEnumerable<TSource>, TResult>> resultSelector, IEqualityComparer<TKey> comparer)
        {
            return System.Linq.Queryable.GroupBy<TSource, TKey, TResult>(source.Visit(Evaluate.Action.READ), keySelector, resultSelector, comparer);
        }

        public static System.Linq.IQueryable<TResult> GroupBy<TSource, TKey, TResult>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, TKey>> keySelector, Expression<Func<TKey, IEnumerable<TSource>, TResult>> resultSelector)
        {
            return System.Linq.Queryable.GroupBy<TSource, TKey, TResult>(source.Visit(Evaluate.Action.READ), keySelector, resultSelector);
        }

        public static System.Linq.IQueryable<TResult> GroupBy<TSource, TKey, TElement, TResult>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, TKey>> keySelector, Expression<Func<TSource, TElement>> elementSelector, Expression<Func<TKey, IEnumerable<TElement>, TResult>> resultSelector)
        {
            return System.Linq.Queryable.GroupBy<TSource, TKey, TElement, TResult>(source.Visit(Evaluate.Action.READ), keySelector, elementSelector, resultSelector);
        }

        public static System.Linq.IQueryable<System.Linq.IGrouping<TKey, TElement>> GroupBy<TSource, TKey, TElement>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, TKey>> keySelector, Expression<Func<TSource, TElement>> elementSelector)
        {
            return System.Linq.Queryable.GroupBy<TSource, TKey, TElement>(source.Visit(Evaluate.Action.READ), keySelector, elementSelector);
        }

        public static System.Linq.IQueryable<System.Linq.IGrouping<TKey, TSource>> GroupBy<TSource, TKey>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, TKey>> keySelector, IEqualityComparer<TKey> comparer)
        {
            return System.Linq.Queryable.GroupBy<TSource, TKey>(source.Visit(Evaluate.Action.READ), keySelector, comparer);
        }

        public static System.Linq.IQueryable<System.Linq.IGrouping<TKey, TSource>> GroupBy<TSource, TKey>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, TKey>> keySelector)
        {
            return System.Linq.Queryable.GroupBy<TSource, TKey>(source.Visit(Evaluate.Action.READ), keySelector);
        }

        public static System.Linq.IQueryable<System.Linq.IGrouping<TKey, TElement>> GroupBy<TSource, TKey, TElement>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, TKey>> keySelector, Expression<Func<TSource, TElement>> elementSelector, IEqualityComparer<TKey> comparer)
        {
            return System.Linq.Queryable.GroupBy<TSource, TKey, TElement>(source.Visit(Evaluate.Action.READ), keySelector, elementSelector, comparer);
        }

        public static System.Linq.IQueryable<TResult> GroupJoin<TOuter, TInner, TKey, TResult>(this System.Linq.IQueryable<TOuter> outer, IEnumerable<TInner> inner, Expression<Func<TOuter, TKey>> outerKeySelector, Expression<Func<TInner, TKey>> innerKeySelector, Expression<Func<TOuter, IEnumerable<TInner>, TResult>> resultSelector)
        {
            return System.Linq.Queryable.GroupJoin<TOuter, TInner, TKey, TResult>(outer.Visit(Evaluate.Action.READ), inner.Visit(Evaluate.Action.READ), outerKeySelector, innerKeySelector, resultSelector);
        }

        public static System.Linq.IQueryable<TResult> GroupJoin<TOuter, TInner, TKey, TResult>(this System.Linq.IQueryable<TOuter> outer, IEnumerable<TInner> inner, Expression<Func<TOuter, TKey>> outerKeySelector, Expression<Func<TInner, TKey>> innerKeySelector, Expression<Func<TOuter, IEnumerable<TInner>, TResult>> resultSelector, IEqualityComparer<TKey> comparer)
        {
            return System.Linq.Queryable.GroupJoin<TOuter, TInner, TKey, TResult>(outer.Visit(Evaluate.Action.READ), inner.Visit(Evaluate.Action.READ), outerKeySelector, innerKeySelector, resultSelector, comparer);
        }

        public static System.Linq.IQueryable<TSource> Intersect<TSource>(this System.Linq.IQueryable<TSource> source1, IEnumerable<TSource> source2)
        {
            return System.Linq.Queryable.Intersect<TSource>(source1.Visit(Evaluate.Action.READ), source2.Visit(Evaluate.Action.READ));
        }

        public static System.Linq.IQueryable<TSource> Intersect<TSource>(this System.Linq.IQueryable<TSource> source1, IEnumerable<TSource> source2, IEqualityComparer<TSource> comparer)
        {
            return System.Linq.Queryable.Intersect<TSource>(source1.Visit(Evaluate.Action.READ), source2.Visit(Evaluate.Action.READ), comparer);
        }

        public static System.Linq.IQueryable<TResult> Join<TOuter, TInner, TKey, TResult>(this System.Linq.IQueryable<TOuter> outer, IEnumerable<TInner> inner, Expression<Func<TOuter, TKey>> outerKeySelector, Expression<Func<TInner, TKey>> innerKeySelector, Expression<Func<TOuter, TInner, TResult>> resultSelector, IEqualityComparer<TKey> comparer)
        {
            return System.Linq.Queryable.Join<TOuter, TInner, TKey, TResult>(outer.Visit(Evaluate.Action.READ), inner.Visit(Evaluate.Action.READ), outerKeySelector, innerKeySelector, resultSelector, comparer);
        }

        public static System.Linq.IQueryable<TResult> Join<TOuter, TInner, TKey, TResult>(this System.Linq.IQueryable<TOuter> outer, IEnumerable<TInner> inner, Expression<Func<TOuter, TKey>> outerKeySelector, Expression<Func<TInner, TKey>> innerKeySelector, Expression<Func<TOuter, TInner, TResult>> resultSelector)
        {
            return System.Linq.Queryable.Join<TOuter, TInner, TKey, TResult>(outer.Visit(Evaluate.Action.READ), inner.Visit(Evaluate.Action.READ), outerKeySelector, innerKeySelector, resultSelector);
        }

        public static TSource Last<TSource>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate)
        {
            return System.Linq.Queryable.Last<TSource>(source.Visit(Evaluate.Action.READ), predicate);
        }

        public static TSource Last<TSource>(this System.Linq.IQueryable<TSource> source)
        {
            return System.Linq.Queryable.Last<TSource>(source.Visit(Evaluate.Action.READ));
        }

        public static TSource LastOrDefault<TSource>(this System.Linq.IQueryable<TSource> source)
        {
            return System.Linq.Queryable.LastOrDefault<TSource>(source.Visit(Evaluate.Action.READ));
        }

        public static TSource LastOrDefault<TSource>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate)
        {
            return System.Linq.Queryable.LastOrDefault<TSource>(source.Visit(Evaluate.Action.READ), predicate);
        }

        public static long LongCount<TSource>(this System.Linq.IQueryable<TSource> source)
        {
            return System.Linq.Queryable.LongCount<TSource>(source.Visit(Evaluate.Action.READ));
        }

        public static long LongCount<TSource>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate)
        {
            return System.Linq.Queryable.LongCount<TSource>(source.Visit(Evaluate.Action.READ), predicate);
        }

        public static TSource Max<TSource>(this System.Linq.IQueryable<TSource> source)
        {
            return System.Linq.Queryable.Max<TSource>(source.Visit(Evaluate.Action.READ));
        }

        public static TResult Max<TSource, TResult>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, TResult>> selector)
        {
            return System.Linq.Queryable.Max<TSource, TResult>(source.Visit(Evaluate.Action.READ), selector);
        }

        public static TSource Min<TSource>(this System.Linq.IQueryable<TSource> source)
        {
            return System.Linq.Queryable.Min<TSource>(source.Visit(Evaluate.Action.READ));
        }

        public static TResult Min<TSource, TResult>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, TResult>> selector)
        {
            return System.Linq.Queryable.Min<TSource, TResult>(source.Visit(Evaluate.Action.READ), selector);
        }

        public static System.Linq.IQueryable<TResult> OfType<TResult>(this System.Linq.IQueryable source)
        {
            return System.Linq.Queryable.OfType<TResult>(source.Visit(Evaluate.Action.READ));
        }

        public static System.Linq.IOrderedQueryable<TSource> OrderBy<TSource, TKey>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, TKey>> keySelector)
        {
            return System.Linq.Queryable.OrderBy<TSource, TKey>(source.Visit(Evaluate.Action.READ), keySelector);
        }

        public static System.Linq.IOrderedQueryable<TSource> OrderBy<TSource, TKey>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, TKey>> keySelector, IComparer<TKey> comparer)
        {
            return System.Linq.Queryable.OrderBy<TSource, TKey>(source.Visit(Evaluate.Action.READ), keySelector, comparer);
        }

        public static System.Linq.IOrderedQueryable<TSource> OrderByDescending<TSource, TKey>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, TKey>> keySelector)
        {
            return System.Linq.Queryable.OrderByDescending<TSource, TKey>(source.Visit(Evaluate.Action.READ), keySelector);
        }

        public static System.Linq.IOrderedQueryable<TSource> OrderByDescending<TSource, TKey>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, TKey>> keySelector, IComparer<TKey> comparer)
        {
            return System.Linq.Queryable.OrderByDescending<TSource, TKey>(source.Visit(Evaluate.Action.READ), keySelector, comparer);
        }

        public static System.Linq.IQueryable<TSource> Prepend<TSource>(this System.Linq.IQueryable<TSource> source, TSource element)
        {
            return System.Linq.Queryable.Prepend<TSource>(source.Visit(Evaluate.Action.READ), element);
        }

        public static System.Linq.IQueryable<TSource> Reverse<TSource>(this System.Linq.IQueryable<TSource> source)
        {
            return System.Linq.Queryable.Reverse<TSource>(source.Visit(Evaluate.Action.READ));
        }

        public static System.Linq.IQueryable<TResult> Select<TSource, TResult>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, TResult>> selector)
        {
            return System.Linq.Queryable.Select<TSource, TResult>(source, selector);
        }

        public static System.Linq.IQueryable<TResult> Select<TSource, TResult>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, int, TResult>> selector)
        {
            return System.Linq.Queryable.Select<TSource, TResult>(source.Visit(Evaluate.Action.READ), selector);
        }

        public static System.Linq.IQueryable<TResult> SelectMany<TSource, TCollection, TResult>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, int, IEnumerable<TCollection>>> collectionSelector, Expression<Func<TSource, TCollection, TResult>> resultSelector)
        {
            return System.Linq.Queryable.SelectMany<TSource, TCollection, TResult>(source.Visit(Evaluate.Action.READ), collectionSelector, resultSelector);
        }

        public static System.Linq.IQueryable<TResult> SelectMany<TSource, TCollection, TResult>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, IEnumerable<TCollection>>> collectionSelector, Expression<Func<TSource, TCollection, TResult>> resultSelector)
        {
            return System.Linq.Queryable.SelectMany<TSource, TCollection, TResult>(source.Visit(Evaluate.Action.READ), collectionSelector, resultSelector);
        }

        public static System.Linq.IQueryable<TResult> SelectMany<TSource, TResult>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, int, IEnumerable<TResult>>> selector)
        {
            return System.Linq.Queryable.SelectMany<TSource, TResult>(source.Visit(Evaluate.Action.READ), selector);
        }

        public static System.Linq.IQueryable<TResult> SelectMany<TSource, TResult>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, IEnumerable<TResult>>> selector)
        {
            return System.Linq.Queryable.SelectMany<TSource, TResult>(source.Visit(Evaluate.Action.READ), selector);
        }

        public static bool SequenceEqual<TSource>(this System.Linq.IQueryable<TSource> source1, IEnumerable<TSource> source2)
        {
            return System.Linq.Queryable.SequenceEqual<TSource>(source1.Visit(Evaluate.Action.READ), source2.Visit(Evaluate.Action.READ));
        }

        public static bool SequenceEqual<TSource>(this System.Linq.IQueryable<TSource> source1, IEnumerable<TSource> source2, IEqualityComparer<TSource> comparer)
        {
            return System.Linq.Queryable.SequenceEqual<TSource>(source1.Visit(Evaluate.Action.READ), source2.Visit(Evaluate.Action.READ), comparer);
        }

        public static TSource Single<TSource>(this System.Linq.IQueryable<TSource> source)
        {
            return System.Linq.Queryable.Single<TSource>(source.Visit(Evaluate.Action.READ));
        }

        public static TSource Single<TSource>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate)
        {
            return System.Linq.Queryable.Single<TSource>(source.Visit(Evaluate.Action.READ), predicate);
        }

        public static TSource SingleOrDefault<TSource>(this System.Linq.IQueryable<TSource> source)
        {
            return System.Linq.Queryable.SingleOrDefault<TSource>(source.Visit(Evaluate.Action.READ));
        }

        public static TSource SingleOrDefault<TSource>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate)
        {
            return System.Linq.Queryable.SingleOrDefault<TSource>(source.Visit(Evaluate.Action.READ), predicate);
        }

        public static System.Linq.IQueryable<TSource> Skip<TSource>(this System.Linq.IQueryable<TSource> source, int count)
        {
            return System.Linq.Queryable.Skip<TSource>(source.Visit(Evaluate.Action.READ), count);
        }

        public static System.Linq.IQueryable<TSource> SkipLast<TSource>(this System.Linq.IQueryable<TSource> source, int count)
        {
            return System.Linq.Queryable.SkipLast<TSource>(source.Visit(Evaluate.Action.READ), count);
        }

        public static System.Linq.IQueryable<TSource> SkipWhile<TSource>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate)
        {
            return System.Linq.Queryable.SkipWhile<TSource>(source.Visit(Evaluate.Action.READ), predicate);
        }

        public static System.Linq.IQueryable<TSource> SkipWhile<TSource>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, int, bool>> predicate)
        {
            return System.Linq.Queryable.SkipWhile<TSource>(source.Visit(Evaluate.Action.READ), predicate);
        }

        public static long Sum<TSource>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, long>> selector)
        {
            return System.Linq.Queryable.Sum<TSource>(source.Visit(Evaluate.Action.READ), selector);
        }

        public static decimal? Sum<TSource>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, decimal?>> selector)
        {
            return System.Linq.Queryable.Sum<TSource>(source.Visit(Evaluate.Action.READ), selector);
        }

        public static double? Sum<TSource>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, double?>> selector)
        {
            return System.Linq.Queryable.Sum<TSource>(source.Visit(Evaluate.Action.READ), selector);
        }

        public static int? Sum<TSource>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, int?>> selector)
        {
            return System.Linq.Queryable.Sum<TSource>(source.Visit(Evaluate.Action.READ), selector);
        }

        public static double Sum(this System.Linq.IQueryable<double> source)
        {
            return System.Linq.Queryable.Sum(source);
        }

        public static float? Sum<TSource>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, float?>> selector)
        {
            return System.Linq.Queryable.Sum<TSource>(source.Visit(Evaluate.Action.READ), selector);
        }

        public static float Sum<TSource>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, float>> selector)
        {
            return System.Linq.Queryable.Sum<TSource>(source.Visit(Evaluate.Action.READ), selector);
        }

        public static int Sum<TSource>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, int>> selector)
        {
            return System.Linq.Queryable.Sum<TSource>(source.Visit(Evaluate.Action.READ), selector);
        }

        public static long? Sum<TSource>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, long?>> selector)
        {
            return System.Linq.Queryable.Sum<TSource>(source.Visit(Evaluate.Action.READ), selector);
        }

        public static double Sum<TSource>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, double>> selector)
        {
            return System.Linq.Queryable.Sum<TSource>(source.Visit(Evaluate.Action.READ), selector);
        }

        public static decimal Sum(this System.Linq.IQueryable<decimal> source)
        {
            return System.Linq.Queryable.Sum(source);
        }

        public static float Sum(this System.Linq.IQueryable<float> source)
        {
            return System.Linq.Queryable.Sum(source);
        }

        public static float? Sum(this System.Linq.IQueryable<float?> source)
        {
            return System.Linq.Queryable.Sum(source);
        }

        public static long? Sum(this System.Linq.IQueryable<long?> source)
        {
            return System.Linq.Queryable.Sum(source);
        }

        public static int? Sum(this System.Linq.IQueryable<int?> source)
        {
            return System.Linq.Queryable.Sum(source);
        }

        public static double? Sum(this System.Linq.IQueryable<double?> source)
        {
            return System.Linq.Queryable.Sum(source);
        }

        public static decimal? Sum(this System.Linq.IQueryable<decimal?> source)
        {
            return System.Linq.Queryable.Sum(source);
        }

        public static long Sum(this System.Linq.IQueryable<long> source)
        {
            return System.Linq.Queryable.Sum(source);
        }

        public static int Sum(this System.Linq.IQueryable<int> source)
        {
            return System.Linq.Queryable.Sum(source);
        }

        public static decimal Sum<TSource>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, decimal>> selector)
        {
            return System.Linq.Queryable.Sum<TSource>(source.Visit(Evaluate.Action.READ), selector);
        }

        public static System.Linq.IQueryable<TSource> Take<TSource>(this System.Linq.IQueryable<TSource> source, int count)
        {
            return System.Linq.Queryable.Take<TSource>(source.Visit(Evaluate.Action.READ), count);
        }

        public static System.Linq.IQueryable<TSource> TakeLast<TSource>(this System.Linq.IQueryable<TSource> source, int count)
        {
            return System.Linq.Queryable.TakeLast<TSource>(source.Visit(Evaluate.Action.READ), count);
        }

        public static System.Linq.IQueryable<TSource> TakeWhile<TSource>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate)
        {
            return System.Linq.Queryable.TakeWhile<TSource>(source, predicate);
        }

        public static System.Linq.IQueryable<TSource> TakeWhile<TSource>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, int, bool>> predicate)
        {
            return System.Linq.Queryable.TakeWhile<TSource>(source.Visit(Evaluate.Action.READ), predicate);
        }

        public static System.Linq.IOrderedQueryable<TSource> ThenBy<TSource, TKey>(this System.Linq.IOrderedQueryable<TSource> source, Expression<Func<TSource, TKey>> keySelector)
        {
            return System.Linq.Queryable.ThenBy<TSource, TKey>(source.Visit(Evaluate.Action.READ), keySelector);
        }

        public static System.Linq.IOrderedQueryable<TSource> ThenBy<TSource, TKey>(this System.Linq.IOrderedQueryable<TSource> source, Expression<Func<TSource, TKey>> keySelector, IComparer<TKey> comparer)
        {
            return System.Linq.Queryable.ThenBy<TSource, TKey>(source.Visit(Evaluate.Action.READ), keySelector, comparer);
        }

        public static System.Linq.IOrderedQueryable<TSource> ThenByDescending<TSource, TKey>(this System.Linq.IOrderedQueryable<TSource> source, Expression<Func<TSource, TKey>> keySelector)
        {
            return System.Linq.Queryable.ThenByDescending<TSource, TKey>(source.Visit(Evaluate.Action.READ), keySelector);
        }

        public static System.Linq.IOrderedQueryable<TSource> ThenByDescending<TSource, TKey>(this System.Linq.IOrderedQueryable<TSource> source, Expression<Func<TSource, TKey>> keySelector, IComparer<TKey> comparer)
        {
            return System.Linq.Queryable.ThenByDescending<TSource, TKey>(source.Visit(Evaluate.Action.READ), keySelector, comparer);
        }

        public static System.Linq.IQueryable<TSource> Union<TSource>(this System.Linq.IQueryable<TSource> source1, IEnumerable<TSource> source2)
        {
            return System.Linq.Queryable.Union<TSource>(source1.Visit(Evaluate.Action.READ), source2.Visit(Evaluate.Action.READ));
        }

        public static System.Linq.IQueryable<TSource> Union<TSource>(this System.Linq.IQueryable<TSource> source1, IEnumerable<TSource> source2, IEqualityComparer<TSource> comparer)
        {
            return System.Linq.Queryable.Union<TSource>(source1.Visit(Evaluate.Action.READ), source2.Visit(Evaluate.Action.READ), comparer);
        }

        public static System.Linq.IQueryable<TSource> Where<TSource>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate)
        {
            return System.Linq.Queryable.Where<TSource>(source.Visit(Evaluate.Action.READ), predicate);
        }

        public static System.Linq.IQueryable<TSource> Where<TSource>(this System.Linq.IQueryable<TSource> source, Expression<Func<TSource, int, bool>> predicate)
        {
            return System.Linq.Queryable.Where<TSource>(source.Visit(Evaluate.Action.READ), predicate);
        }

        public static System.Linq.IQueryable<(TFirst First, TSecond Second)> Zip<TFirst, TSecond>(this System.Linq.IQueryable<TFirst> source1, IEnumerable<TSecond> source2)
        {
            return System.Linq.Queryable.Zip<TFirst, TSecond>(source1.Visit(Evaluate.Action.READ), source2.Visit(Evaluate.Action.READ));
        }

        public static System.Linq.IQueryable<TResult> Zip<TFirst, TSecond, TResult>(this System.Linq.IQueryable<TFirst> source1, IEnumerable<TSecond> source2, Expression<Func<TFirst, TSecond, TResult>> resultSelector)
        {
            return System.Linq.Queryable.Zip<TFirst, TSecond, TResult>(source1.Visit(Evaluate.Action.READ), source2.Visit(Evaluate.Action.READ), resultSelector);
        }
    }
}