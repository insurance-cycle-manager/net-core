﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PatientsRecordsManager.Data.Dtos.Users
{
    public class UserForRegisterDto
    {
        [Required]
        public string UserName { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        [StringLength(maximumLength: 24, MinimumLength = 8)]
        public string Password { get; set; }

        // [Required]
        // public string RoleName { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

        public UserForRegisterDto()
        {
            CreatedAt = DateTime.Now;
            UpdatedAt = DateTime.Now;
        }
    }
}
