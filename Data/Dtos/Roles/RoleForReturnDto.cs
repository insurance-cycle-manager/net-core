using System.Collections.Generic;
using PatientsRecordsManager.Data.Dtos.Permissions;

namespace PatientsRecordsManager.Data.Dtos.Roles
{
    public class RoleForReturnDto
    {
        public string Name { get; set; }
        public IEnumerable<PermissionForReturnDto> Permissions { get; set; }
    }
}