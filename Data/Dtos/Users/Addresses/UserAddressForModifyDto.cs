using System.ComponentModel.DataAnnotations;

namespace PatientsRecordsManager.Data.Dtos.Users.Addresses
{
    public class UserAddressForModifyDto
    {
        [Required]
        public string AddressCode { get; set; }
    }
}