using System.Collections.Generic;
using PatientsRecordsManager.Data.Dtos.InsuranceCompanies;

namespace PatientsRecordsManager.Data.Repositories
{
    public interface IIcRepository
    {
        IEnumerable<InsuranceCompanyForReturnDto> GetInsuranceCompanies();
        InsuranceCompanyForReturnDto AddInsurnceCompany(InsuranceCompanyForCreateDto data);
        InsuranceCompanyForReturnDto RemoveInsuranceCompany(int userId);
        InsuranceCompanyForReturnDto ModifyInsuranceCompany(int userId, InsuranceCompanyForUpdateDto data);
        InsuranceCompanyForReturnDto ModifyInsuranceCompany(string code, InsuranceCompanyForUpdateDto data);
        InsuranceCompanyForReturnDto GetInsuranceCompanyDetails(int userId);
        InsuranceCompanyForReturnDto GetInsuranceCompanyDetails(string code);
    }
}