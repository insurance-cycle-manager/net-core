using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using PatientsRecordsManager.Constants.Enums;

namespace PatientsRecordsManager.Models
{
    public class Patient
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string PatientCode { get; set; }

        [Required]
        public string Name { get; set; }
        [Required]
        public string NormalizedName { get; set; }

        [Required]
        public DateTime DateOfBirth { get; set; }

        [EnumDataType(typeof(Gender))]
        [Required]
        public Gender Gender { get; set; } = Gender.Default;

        [Required]
        [ForeignKey("Addresses")]
        public int AddressId { get; set; }
        public UserAddress Address { get; set; }

        [Required]
        [ForeignKey("UserEmails")]
        public int EmailId { get; set; }
        public UserEmail Email { get; set; }

        [Required]
        [ForeignKey("UserPhoneNumbers")]
        public int PhoneNumberId { get; set; }
        public UserPhoneNumber PhoneNumber { get; set; }

        [Required]
        [ForeignKey("Users")]
        public int UserId { get; set; }
        public User User { get; set; }
        [Required]
        [ForeignKey("Roles")]
        public int RoleId { get; set; }
        public Role Role { get; set; }
        public UserRole UserRole { get; set; }

        public List<Visit> Visits { get; set; }
        public List<PatientPackage> PatientPackages { get; set; }
    }
}