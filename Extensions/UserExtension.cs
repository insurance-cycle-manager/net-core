using System;
using System.Collections.Generic;
using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using PatientsRecordsManager.Models;
using PatientsRecordsManager.Utils;

namespace PatientsRecordsManager.Extensions
{
    public static class UserExtension
    {
        public static string GenerateJwtMethod(this User user, IConfiguration configuration, UserManager<User> userManager)
        {
            // Generating the JWT
            Assert.NotNull(user);
            Assert.NotNull(configuration);
            Assert.NotNull(userManager);

            var Id = user.Id.ToString(CultureInfo.CurrentCulture);
            var userName = user.UserName;

            // First Section
            var claims = new List<Claim> {
                new Claim (ClaimTypes.NameIdentifier, Id),
                new Claim (ClaimTypes.Name, userName)
            };

            var roles = userManager.GetRolesAsync(user).Result;
            foreach (var role in roles)
            {
                claims.Add(new Claim(ClaimTypes.Role, role));
            }

            // Second Section
            var key = new SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes(configuration.GetSection("App:Token").Value));

            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.Now.AddDays(1),
                SigningCredentials = credentials,
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(token);
        }
    }
}