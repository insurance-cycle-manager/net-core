using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using PatientsRecordsManager.ABAC.Data;
using PatientsRecordsManager.ABAC.Data.Models;
using PatientsRecordsManager.ABAC.Functions;

namespace PatientsRecordsManager.ABAC.Enforcers
{
    public class DboEnforcer : MyEnforcer
    {
        public DboEnforcer(AbacDbContext context, ILogger<DboEnforcer> logger) : base(".\\ABAC\\Models\\dbo.config", context, logger)
        {
            this.AddFunction("attrsMatch", new MatchDboAttributes("attrsMatch", context));
            this.AddFunction("subMatch", new MatchSubjects("subMatch"));
            this.EnableEnforce(true);
        }

        protected override DataAccessRequestDetails ComposeRequestData(params object[] rValues)
        {
            // ClaimsPrincipal sub, string o, IEnumerable<string> attributes, string act
            if (rValues.Length != 4)
                throw new ArgumentException(ComposeRequestDataValuesLength);

            IEnumerable<string> roles = (rValues[0] as ClaimsPrincipal).Identities.Select(c => c.Name);
            string o = $"{rValues[1]}";
            IEnumerable<string> attrs = (rValues[2] as IEnumerable<string>);
            string act = $"{rValues[3]}";

            return new DataAccessRequestDetails
            {
                Roles = roles.ToArray(),
                Object = o,
                Attributes = attrs.ToArray(),
                Action = act
            };
        }
    }
}