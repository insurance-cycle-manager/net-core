using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PatientsRecordsManager.Data.Dtos.ServiceProviders.Connections
{
    public class SpConnectionForAddDto
    {
        public DateTime CreatedAt { get; set; }
        [Required]
        public string TpaCode { get; set; }
        [Required]
        public string ServiceProviderCode { get; set; }
        [Required, MinLength(1)]
        public IEnumerable<string> InsuranceCompanies { get; set; }

        public SpConnectionForAddDto()
        {
            CreatedAt = DateTime.Now;
        }
    }
}