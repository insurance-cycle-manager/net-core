using PatientsRecordsManager.Data.Dtos.Prices;

namespace PatientsRecordsManager.Data.Dtos.LabTests
{
    public class LabTestForUpdateDto
    {
        public string Name { get; set; }
        public PriceForAddDto Price { get; set; }
    }
}