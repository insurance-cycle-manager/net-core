using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using PatientsRecordsManager.ABAC.Extensions;
using PatientsRecordsManager.ABAC.Extensions.EF;
using PatientsRecordsManager.Data.Dtos.InsuranceCompanies;
using PatientsRecordsManager.Exceptions;
using PatientsRecordsManager.Models;
using PatientsRecordsManager.Resources;
using PatientsRecordsManager.Utils;
using PatientsRecordsManager.Utils.Exceptions;

namespace PatientsRecordsManager.Data.Repositories
{
    public class IcRepository : IIcRepository
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        private readonly Messages _messages;

        public IcRepository(DataContext context, IMapper mapper, Messages messages)
        {
            _context = context;
            _mapper = mapper;
            _messages = messages;
        }

        public InsuranceCompanyForReturnDto AddInsurnceCompany(InsuranceCompanyForCreateDto icData)
        {
            var insuranceCompany = _mapper.Map<InsuranceCompany>(icData);
            _context.InsuranceCompanies.Add(insuranceCompany);

            if (_context.SaveChanges() <= 0)
            {
                throw new SaveChangesFailedException(_messages.GetString("InsuranceCompanyCreateError"));
            }
            return _mapper.Map<InsuranceCompanyForReturnDto>(insuranceCompany);
        }

        public IEnumerable<InsuranceCompanyForReturnDto> GetInsuranceCompanies()
        {
            return _context.InsuranceCompanies
                .Include(tpa => tpa.Email)
                .Include(tpa => tpa.PhoneNumber)
                .Include(tpa => tpa.Address)
                .ThenInclude(ua => ua.Address)
                .Include(insuranceCompany => insuranceCompany.SupervisoryComission)
                .Include(insuranceCompany => insuranceCompany.Status)
                .ToList()
                .Select(ic => _mapper.Map<InsuranceCompanyForReturnDto>(ic));
        }

        public InsuranceCompanyForReturnDto GetInsuranceCompanyDetails(string code)
        {
            return _mapper.Map<InsuranceCompanyForReturnDto>(GetInsuranceCompany(code: code));
        }

        public InsuranceCompanyForReturnDto GetInsuranceCompanyDetails(int userId)
        {
            return _mapper.Map<InsuranceCompanyForReturnDto>(GetInsuranceCompany(userId: userId));
        }

        public InsuranceCompanyForReturnDto ModifyInsuranceCompany(int icUserId, InsuranceCompanyForUpdateDto data)
        {
            return ModifyInsuranceCompany(data, userId: icUserId);
        }

        public InsuranceCompanyForReturnDto ModifyInsuranceCompany(string code, InsuranceCompanyForUpdateDto data)
        {
            return ModifyInsuranceCompany(data, code: code);
        }

        public InsuranceCompanyForReturnDto RemoveInsuranceCompany(int icUserId)
        {
            var ic = GetInsuranceCompany(userId: icUserId);

            _context.InsuranceCompanies.Remove(ic);

            if (_context.SaveChanges() <= 0)
            {
                throw new SaveChangesFailedException(_messages.GetString("icDeleteFailed", ic.InsuranceCompanyCode));
            }
            return _mapper.Map<InsuranceCompanyForReturnDto>(ic);
        }

        private InsuranceCompany GetInsuranceCompany(string code = null, int? userId = null)
        {
            var ics = _context.InsuranceCompanies
                .Include(c => c.Status)
                .Include(tpa => tpa.Email)
                .Include(tpa => tpa.PhoneNumber)
                .Include(tpa => tpa.Address)
                .ThenInclude(ua => ua.Address)
                .Include(c => c.SupervisoryComission)
                .Include(c => c.InsuranceConnections)
                .Include(c => c.SpConnections)
                .AsQueryable();

            if (!string.IsNullOrWhiteSpace(code))
            {
                ics = ics.Where(c => c.InsuranceCompanyCode.ToUpper() == (code.ToUpper()));
            }

            if (userId != null && userId.GetValueOrDefault() > 0)
            {
                ics = ics.Where(c => c.UserId == userId.Value);
            }

            return ics.FirstOrDefault()
                ?? throw new NoDataFoundException(_messages.GetString("InsuranceCompanyNotFound"));
        }

        private InsuranceCompanyForReturnDto ModifyInsuranceCompany(InsuranceCompanyForUpdateDto data, string code = null, int? userId = null)
        {
            var ic = GetInsuranceCompany(code, userId);
            _mapper.Map<InsuranceCompanyForUpdateDto, InsuranceCompany>(data, ic);

            if (_context.SaveChanges() <= 0)
            {
                throw new SaveChangesFailedException(_messages.GetString("icModifyFailed", ic.InsuranceCompanyCode));
            }
            return _mapper.Map<InsuranceCompanyForReturnDto>(ic);
        }
    }
}