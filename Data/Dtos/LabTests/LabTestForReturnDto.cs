using PatientsRecordsManager.Data.Dtos.Prices;

namespace PatientsRecordsManager.Data.Dtos.LabTests
{
    public class LabTestForReturnDto
    {
        public string Name { get; set; }
        public string LabTestCode { get; set; }
        public PriceForReturnDto Price { get; set; }
    }
}