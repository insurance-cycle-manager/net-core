using System.Collections.Generic;
using PatientsRecordsManager.Data.Dtos.Prescribed.Radiographs;

namespace PatientsRecordsManager.Data.Repositories
{
    public interface IVisitRadiographRepository
    {
        PrescribedRadiographForReturnDto AddRadiograph(string visitCode, PrescribedRadiographForCreateDto radiographForCreate);
        IEnumerable<PrescribedRadiographForReturnDto> GetRadiographs(string visitCode);
        PrescribedRadiographForReturnDto GetRadiographDetails(string visitCode, string radiographCode);
        PrescribedRadiographForReturnDto ModifyRadiograph(string visitCode, string radiographCode, PrescribedRadiographForUpdateDto radiographForUpdate);
        PrescribedRadiographForReturnDto RemoveRadiograph(string visitCode, string radiographCode);
    }
}