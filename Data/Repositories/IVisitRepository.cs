using System.Collections.Generic;
using System.Threading.Tasks;
using PatientsRecordsManager.Constants.Enums;
using PatientsRecordsManager.Data.Dtos.Visits;

namespace PatientsRecordsManager.Data.Repositories
{
    public interface IVisitRepository
    {
        VisitForReturnDto AddVisit(int userId, ServiceProviderStafferType stType, VisitForCreateDto visit);
        VisitForReturnDto GetVisitDetails(string visitCode);
        IEnumerable<VisitForReturnDto> GetVisits(VisitType? visitType);
        VisitForReturnDto ModifyVisit(int userId, ServiceProviderStafferType stType, string visitCode, VisitForUpdateDto visit);
        VisitForReturnDto RemoveVisit(int userId, ServiceProviderStafferType stType, string visitCode);
    }
}