using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using PatientsRecordsManager.Data.Dtos.Patients;
using PatientsRecordsManager.Data.Dtos.Patients.Packages;
using PatientsRecordsManager.Data.Dtos.InsuranceCompanies.Packages;
using PatientsRecordsManager.Exceptions;
using PatientsRecordsManager.Models;
using PatientsRecordsManager.Resources;
using PatientsRecordsManager.Utils.Exceptions;
using PatientsRecordsManager.ABAC.Extensions.EF;
using PatientsRecordsManager.ABAC.Extensions;

namespace PatientsRecordsManager.Data.Repositories
{
    public class PatientRepository : IPatientRepository
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        private readonly Messages _messages;

        public PatientRepository(DataContext context, IMapper mapper, Messages messages)
        {
            _messages = messages;
            _mapper = mapper;
            _context = context;
        }

        public PatientForReturnDto GetPatientDetails(int userId)
        {
            return _mapper.Map<PatientForReturnDto>(GetPatient(userId: userId));
        }

        public PatientForReturnDto GetPatientDetails(string patientCode)
        {
            return _mapper.Map<PatientForReturnDto>(GetPatient(patientCode: patientCode));
        }

        public IEnumerable<PatientForReturnDto> GetPatients(string name)
        {
            var patients = _context.Patients.AsQueryable();

            if (!string.IsNullOrWhiteSpace(name))
            {
                patients = patients.Where(p => p.NormalizedName.ToUpper().Contains(name.ToUpper()));
            }
            return _mapper.Map<IEnumerable<PatientForReturnDto>>(patients);
        }

        public PatientForReturnDto AddPatient(PatientForCreateDto patientForCreate)
        {
            var patient = _mapper.Map<Patient>(patientForCreate);
            _context.Patients.Add(patient);

            if (_context.SaveChanges() <= 0)
            {
                throw new SaveChangesFailedException();
            }
            return _mapper.Map<PatientForReturnDto>(patient);
        }

        public PatientForReturnDto RemovePatient(int userId)
        {
            var patient = GetPatient(userId: userId);
            _context.Patients.Remove(patient);

            if (_context.SaveChanges() <= 0)
            {
                throw new SaveChangesFailedException();
            }
            return _mapper.Map<PatientForReturnDto>(patient);
        }

        private Patient GetPatient(int? userId = null, string patientCode = null)
        {
            var patients = _context.Patients
                .Include(p => p.Address)
                .ThenInclude(pa => pa.Address)
                .Include(p => p.Email)
                .Include(p => p.PhoneNumber)
                .Include(p => p.PatientPackages)
                .ThenInclude(pp => pp.InsurancePackage)
                .Include(p => p.Visits)
                .AsQueryable();

            if (!string.IsNullOrWhiteSpace(patientCode))
                patients = patients.Where(p => p.PatientCode.ToUpper() == patientCode.ToUpper());

            if (userId != null && userId.Value > 0)
                patients = patients.Where(p => p.UserId == userId);

            return patients.FirstOrDefault() ??
                throw new ArgumentBadException(_messages.GetString("patientNotFound", patientCode));
        }

        public PatientPackageForReturnDto AddPatientPackage(string patientCode, PatientPackageForAddDto package)
        {
            return AddPatientPackage(GetPatient(patientCode: patientCode), package);
        }

        public PatientPackageForReturnDto AddPatientPackage(int userId, PatientPackageForAddDto package)
        {
            return AddPatientPackage(GetPatient(userId: userId), package);
        }

        public IEnumerable<InsurancePackageForReturnDto> GetPatientPackages(string patientCode)
        {
            return GetPatientPackages(GetPatient(patientCode: patientCode));
        }

        public IEnumerable<InsurancePackageForReturnDto> GetPatientPackages(int userId)
        {
            return GetPatientPackages(GetPatient(userId: userId));
        }

        private IEnumerable<InsurancePackageForReturnDto> GetPatientPackages(Patient patient)
        {
            return _mapper.Map<IEnumerable<InsurancePackageForReturnDto>>(patient.PatientPackages
                .Where(pp => pp.Status == Constants.Enums.PatientPackageStatus.Approved)
                .Select(pp => pp.InsurancePackage));
        }

        private PatientPackageForReturnDto AddPatientPackage(Patient patient, PatientPackageForAddDto packageData)
        {
            var icPackage = _context.InsurancePackages
                .FirstOrDefault(ip => ip.InsurancePackageCode.ToUpper() == (packageData.PackageCode.ToUpper()));

            if (icPackage == null || patient.PatientPackages.Any(pp => pp.InsurancePackageId == icPackage.Id))
            {
                throw new NoDataFoundException(_messages.GetString("badPackageCodeToAddToPatient"));
            }

            var package = new PatientPackage
            {
                Patient = patient,
                InsurancePackage = icPackage,
                Status = Constants.Enums.PatientPackageStatus.Requested
            };
            _context.PatientPackages.Add(package);

            if (_context.SaveChanges() <= 0)
            {
                throw new SaveChangesFailedException();
            }
            return _mapper.Map<PatientPackageForReturnDto>(package);
        }

        public PatientPackageForReturnDto SuspendPatientPackage(int userId, string packageCode)
        {
            var patient = GetPatient(userId: userId);
            var package = patient.PatientPackages.FirstOrDefault(pp => pp.InsurancePackage.InsurancePackageCode.ToUpper() == (packageCode.ToUpper()));

            if (package == null)
            {
                throw new NoDataFoundException(_messages.GetString("badPackageCodeToAddToPatient"));
            }
            package.Status = Constants.Enums.PatientPackageStatus.Suspended;

            if (_context.SaveChanges() <= 0)
            {
                throw new SaveChangesFailedException();
            }
            return _mapper.Map<PatientPackageForReturnDto>(package);
        }

        public PatientForReturnDto ModifyPatient(PatientForUpdateDto patientDto, int userId)
        {
            return ModifyPatient(patientDto, userId: userId);
        }

        public PatientForReturnDto ModifyPatient(PatientForUpdateDto patientDto, string patientCode)
        {
            return ModifyPatient(patientDto, patientCode: patientCode);
        }

        private PatientForReturnDto ModifyPatient(PatientForUpdateDto patientDto, int? id = null, string patientCode = null)
        {
            var patient = GetPatient(id, patientCode);
            _mapper.Map<PatientForUpdateDto, Patient>(patientDto, patient);

            if (_context.SaveChanges() <= 0)
                throw new SaveChangesFailedException();

            return _mapper.Map<PatientForReturnDto>(patient);
        }
    }
}