﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PatientsRecordsManager.Migrations
{
    public partial class changeSPConnectionCode : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_ServiceProviderConnections_ServiceProviderConnectionCode",
                table: "ServiceProviderConnections");

            migrationBuilder.DropColumn(
                name: "ServiceProviderConnectionCode",
                table: "ServiceProviderConnections");

            migrationBuilder.AddColumn<string>(
                name: "ConnectionCode",
                table: "ServiceProviderConnections",
                nullable: false,
                defaultValueSql: "'SPC'|| nextval('public.\"ServiceProviderConnectionsNumbers\"')");

            migrationBuilder.AddColumn<string>(
                name: "ConnectionStatus",
                table: "ServiceProviderConnections",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceProviderConnections_ConnectionCode",
                table: "ServiceProviderConnections",
                column: "ConnectionCode",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_ServiceProviderConnections_ConnectionCode",
                table: "ServiceProviderConnections");

            migrationBuilder.DropColumn(
                name: "ConnectionCode",
                table: "ServiceProviderConnections");

            migrationBuilder.DropColumn(
                name: "ConnectionStatus",
                table: "ServiceProviderConnections");

            migrationBuilder.AddColumn<string>(
                name: "ServiceProviderConnectionCode",
                table: "ServiceProviderConnections",
                type: "text",
                nullable: false,
                defaultValueSql: "'SPC'|| nextval('public.\"ServiceProviderConnectionsNumbers\"')");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceProviderConnections_ServiceProviderConnectionCode",
                table: "ServiceProviderConnections",
                column: "ServiceProviderConnectionCode",
                unique: true);
        }
    }
}
