using System;

namespace PatientsRecordsManager.ABAC.Exceptions
{
    public class AbacEnforceException : Exception
    {
        public AbacEnforceException() : base()
        {
        }

        public AbacEnforceException(string message) : base(message)
        {
        }

        public AbacEnforceException(string message, Exception inner) : base(message, inner)
        {
        }
    }
}