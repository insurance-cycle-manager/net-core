using System.ComponentModel.DataAnnotations;
using PatientsRecordsManager.Constants.Enums;

namespace PatientsRecordsManager.Data.Dtos.ServiceProviders.Connections
{
    public class SpConnectionStatusDto
    {
        [Required]
        public SpConnectionStatus? Status { get; set; }
    }
}