using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using PatientsRecordsManager.ABAC.Extensions;
using PatientsRecordsManager.ABAC.Extensions.EF;
using PatientsRecordsManager.Data.Dtos.TPAs;
using PatientsRecordsManager.Exceptions;
using PatientsRecordsManager.Models;
using PatientsRecordsManager.Resources;
using PatientsRecordsManager.Utils.Exceptions;

namespace PatientsRecordsManager.Data.Repositories
{
    public class TpaRepository : ITpaRepository
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        private readonly Messages _messages;

        public TpaRepository(DataContext context, IMapper mapper, Messages messages)
        {
            _context = context;
            _mapper = mapper;
            _messages = messages;
        }

        public TpaForReturnDto AddTpa(TpaForCreateDto tpaData)
        {
            var tpa = _mapper.Map<ThirdPartyAdministrator>(tpaData);
            _context.ThirdPartyAdministrators.Add(tpa);

            if (_context.SaveChanges() <= 0)
            {
                throw new SaveChangesFailedException(_messages.GetString("saveFailed"));
            }
            return _mapper.Map<TpaForReturnDto>(tpa);
        }

        public TpaForReturnDto GetTpaDetails(string code)
        {
            return _mapper.Map<TpaForReturnDto>(GetTpa(code: code));
        }

        public TpaForReturnDto GetTpaDetails(int tpaUserId)
        {
            return _mapper.Map<TpaForReturnDto>(GetTpa(userId: tpaUserId));
        }

        public IEnumerable<TpaForReturnDto> GetTPAs()
        {
            return _context.ThirdPartyAdministrators
                .Include(tpa => tpa.Email)
                .Include(tpa => tpa.PhoneNumber)
                .Include(tpa => tpa.Address)
                .ThenInclude(ua => ua.Address)
                .Include(tpa => tpa.SupervisoryComission)
                .Include(tpa => tpa.Status)
                .Select(tpa => _mapper.Map<TpaForReturnDto>(tpa));
        }

        public TpaForReturnDto ModifyTpa(int tpaUserId, TpaForUpdateDto data)
        {
            return ModifyTpa(data, userId: tpaUserId);
        }

        public TpaForReturnDto ModifyTpa(string tpaCode, TpaForUpdateDto data)
        {
            return ModifyTpa(data, code: tpaCode);
        }

        public TpaForReturnDto RemoveTpa(int tpaUserId)
        {
            var tpa = GetTpa(userId: tpaUserId);

            _context.ThirdPartyAdministrators.Remove(tpa);

            if (_context.SaveChanges() <= 0)
            {
                throw new SaveChangesFailedException(_messages.GetString("TpaDeleteFailed", tpa.ThirdPartyCode));
            }
            return _mapper.Map<TpaForReturnDto>(tpa);
        }

        private ThirdPartyAdministrator GetTpa(string code = null, int? userId = null)
        {
            var tpas = _context.ThirdPartyAdministrators
                .Include(tpa => tpa.Status)
                .Include(tpa => tpa.Email)
                .Include(tpa => tpa.PhoneNumber)
                .Include(tpa => tpa.Address)
                .ThenInclude(ua => ua.Address)
                .Include(tpa => tpa.SupervisoryComission)
                .Include(t => t.InsuranceConnections)
                .ThenInclude(c => c.InsuranceCompany)
                .Include(t => t.ServiceProvidersConnections)
                .AsQueryable();

            if (!string.IsNullOrWhiteSpace(code))
            {
                tpas = tpas.Where(c => c.ThirdPartyCode.ToUpper() == (code.ToUpper()));
            }

            if (userId != null && userId.GetValueOrDefault() > 0)
            {
                tpas = tpas.Where(c => c.UserId == userId.Value);
            }

            return tpas.FirstOrDefault()
                ?? throw new NoDataFoundException(_messages.GetString("TpaNotFound"));
        }

        private TpaForReturnDto ModifyTpa(TpaForUpdateDto data, string code = null, int? userId = null)
        {
            var tpa = GetTpa(code, userId);
            _mapper.Map<TpaForUpdateDto, ThirdPartyAdministrator>(data, tpa);

            if (_context.SaveChanges() <= 0)
            {
                throw new SaveChangesFailedException(_messages.GetString("tpaModifyFailed", tpa.ThirdPartyCode));
            }
            return _mapper.Map<TpaForReturnDto>(tpa);
        }
    }
}