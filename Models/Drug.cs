using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PatientsRecordsManager.Models
{
    public class Drug
    {
        [Key]
        public int Id { get; set; }
        public string DrugCode { get; set; }

        [Required]
        public string Name { get; set; }
        [Required]
        public string NormalizedName { get; set; }
        [Required]
        public string Manufacturer { get; set; }

        [ForeignKey("Prices")]
        public int? PriceId { get; set; }
        public Price Price { get; set; }

        public DateTime CreatedAt { get; set; }

        public IEnumerable<PrescriptionDrug> Prescriptions { get; set; }
        public IEnumerable<DrugPurchase> Purchases { get; set; }
        public IEnumerable<PatientPackage> PatientPackages { get; set; }
    }
}