using System.ComponentModel.DataAnnotations;

namespace PatientsRecordsManager.Data.Dtos.Users.Phones
{
    public class UserPhoneForAddDto
    {
        [Required]
        [Phone]
        public string Number { get; set; }
    }
}