using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace PatientsRecordsManager.ABAC.Extensions.EF
{
    public static class DbSetExtensions
    {
        public static EntityEntry<TEntity> Add<TEntity>(this Microsoft.EntityFrameworkCore.DbSet<TEntity> source, TEntity entity) where TEntity : class
        {
            return Evaluate.Visit(source, Evaluate.Action.WRITE).Add(entity);
        }

        // public static ValueTask<EntityEntry<TEntity>> AddAsync<TEntity>(TEntity entity, CancellationToken cancellationToken = default)
        // {

        // }

        // public static void AddRange<TEntity>(System.Collections.Generic.IEnumerable<TEntity> entities)
        // {

        // }

        // public static void AddRange<TEntity>(params TEntity[] entities)
        // {

        // }

        // public static Task AddRangeAsync<TEntity>(System.Collections.Generic.IEnumerable<TEntity> entities, CancellationToken cancellationToken = default)
        // {

        // }

        // public static Task AddRangeAsync<TEntity>(params TEntity[] entities)
        // {

        // }

        // public static System.Collections.Generic.IAsyncEnumerable<TEntity> AsAsyncEnumerable<TEntity>()
        // {

        // }

        // public static System.Linq.IQueryable<TEntity> AsQueryable<TEntity>()
        // {

        // }

        // public static EntityEntry<TEntity> Attach<TEntity>(TEntity entity)
        // {

        // }

        // public static void AttachRange<TEntity>(System.Collections.Generic.IEnumerable<TEntity> entities)
        // {

        // }

        // public static void AttachRange<TEntity>(params TEntity[] entities)
        // {

        // }

        // public static TEntity Find<TEntity>(params object[] keyValues)
        // {

        // }

        // public static ValueTask<TEntity> FindAsync<TEntity>(object[] keyValues, CancellationToken cancellationToken)
        // {

        // }

        // public static ValueTask<TEntity> FindAsync<TEntity>(params object[] keyValues)
        // {

        // }

        // public static EntityEntry<TEntity> Remove<TEntity>(TEntity entity)
        // {

        // }

        // public static void RemoveRange<TEntity>(params TEntity[] entities)
        // {

        // }

        // public static void RemoveRange<TEntity>(System.Collections.Generic.IEnumerable<TEntity> entities)
        // {

        // }

        // public static EntityEntry<TEntity> Update<TEntity>(TEntity entity)
        // {

        // }

        // public static void UpdateRange<TEntity>(params TEntity[] entities)
        // {

        // }

        // public static void UpdateRange<TEntity>(System.Collections.Generic.IEnumerable<TEntity> entities)
        // {

        // }
    }
}