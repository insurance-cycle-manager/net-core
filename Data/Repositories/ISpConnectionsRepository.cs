using System.Collections.Generic;
using PatientsRecordsManager.Constants.Enums;
using PatientsRecordsManager.Data.Dtos.ServiceProviders.Connections;

namespace PatientsRecordsManager.Data.Repositories
{
    public interface ISpConnectionsRepository
    {
        IEnumerable<SpConnectionForReturnDto> GetConnections(SpConnectionStatus? status, string sp, string tpa);
        IEnumerable<SpConnectionForReturnDto> GetConnections(int providerId, ServiceProviderType type);
        IEnumerable<SpConnectionForReturnDto> GetConnections(string providerCode, ServiceProviderType type);
        IEnumerable<SpConnectionForReturnDto> GetConnections(int tpaId);

        SpConnectionForReturnDto AddConnectionBySp(int providerId, SpConnectionForAddDto connection);
        SpConnectionForReturnDto AddConnectionByTpa(int tpaId, SpConnectionForAddDto connection);

        SpConnectionForReturnDto ModifyConnectionBySp(int providerId, string connectionCode, SpConnectionForModifyDto connection);
        SpConnectionForReturnDto ModifyConnectionByTpa(int tpaId, string connectionCode, SpConnectionForModifyDto connection);

        SpConnectionForReturnDto ManageConnectionStatusBySp(int providerId, string connectionCode, SpConnectionStatusDto confirm);
        SpConnectionForReturnDto ManageConnectionStatusByTpa(int tpaId, string connectionCode, SpConnectionStatusDto confirm);
    }
}