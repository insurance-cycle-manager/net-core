namespace PatientsRecordsManager.Data.Dtos.LabTests
{
    public class PackageLabTestForReturnDto
    {
        public string LabTestCode { get; set; }
    }
}