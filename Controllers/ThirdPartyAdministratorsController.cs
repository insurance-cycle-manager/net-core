using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PatientsRecordsManager.Constants;
using PatientsRecordsManager.Data.Dtos.TPAs;
using PatientsRecordsManager.Data.Repositories;
using PatientsRecordsManager.Extensions;
using PatientsRecordsManager.Resources;

namespace PatientsRecordsManager.Controllers
{
    [ApiController]
    [Authorize]
    [Route(APIs.TpasBaseUrl)]
    public class ThirdPartyAdministratorsController : PrmController
    {
        private readonly ITpaRepository _tpas;
        private readonly IMapper _mapper;
        private readonly Messages _messages;

        public ThirdPartyAdministratorsController(ITpaRepository tpaRepository, IMapper mapper, Messages messages)
        {
            _mapper = mapper;
            _tpas = tpaRepository;
            _messages = messages;
        }

        [HttpGet(APIs.GetTpas)]
        public IActionResult GetTpas()
        {
            return HttpContext.Success(
                _tpas.GetTPAs(),
                _messages.GetString("ThirdPartyAdministratorsFetched"));
        }

        [HttpGet(APIs.GetTpaMe, Order = 1)]
        public IActionResult GetTpaMe()
        {
            return HttpContext.Success(
                _tpas.GetTpaDetails(HttpContext.GetUserId()),
                _messages.GetString("ThirdPartyAdministratorFetched"));
        }

        [HttpGet(APIs.GetTpa, Order = 2)]
        [Authorize(Roles = RolesNames.Administrator)]
        public IActionResult GetTpa(string tpaCode)
        {
            return HttpContext.Success(
                _tpas.GetTpaDetails(tpaCode),
                _messages.GetString("ThirdPartyAdministratorFetched"));
        }

        [HttpPut(APIs.ModifyTpaMe, Order = 1)]
        public IActionResult ModifyTPAMe([FromBody] TpaForUpdateDto data)
        {
            return HttpContext.Success(
                _tpas.ModifyTpa(HttpContext.GetUserId(), data),
                _messages.GetString("TpaUpdated"));
        }

        [HttpPut(APIs.ModifyTpa, Order = 2)]
        [Authorize(Roles = RolesNames.Administrator)]
        public IActionResult ModifyTPA(string tpaCode, [FromBody] TpaForUpdateDto data)
        {
            return HttpContext.Success(
                _tpas.ModifyTpa(tpaCode, data),
                _messages.GetString("TpaUpdated"));
        }
    }

}