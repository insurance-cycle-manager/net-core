using System;
using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PatientsRecordsManager.ABAC.Extensions;
using PatientsRecordsManager.ABAC.Extensions.EF;
using PatientsRecordsManager.ABAC.Extensions.Linq;
using PatientsRecordsManager.Constants;
using PatientsRecordsManager.Data;
using PatientsRecordsManager.Data.Dtos.Users.Accounts;
using PatientsRecordsManager.Data.Dtos.Users.Addresses;
using PatientsRecordsManager.Data.Dtos.Users.Emails;
using PatientsRecordsManager.Data.Dtos.Users.Phones;
using PatientsRecordsManager.Extensions;
using PatientsRecordsManager.Models;
using PatientsRecordsManager.Resources;
using PatientsRecordsManager.Utils;
using PatientsRecordsManager.Utils.Exceptions;

namespace PatientsRecordsManager.Controllers
{

    [ApiController]
    [Route(APIs.UsersBaseUrl)]
    public class UsersController : PrmController
    {
        private readonly IMapper _mapper;
        private readonly Messages _messages;
        private readonly DataContext _context;

        public UsersController(IMapper mapper, Messages messages, DataContext context)
        {
            _context = context;
            _messages = messages;
            _mapper = mapper;
        }

        [HttpGet("rulesManager")]
        [Authorize(Roles = RolesNames.Administrator)]
        public IActionResult GetUsers()
        {
            return HttpContext.Success(
                _context.Users.Select(u => new { Id = u.Id, Name = u.UserName }),
                _messages.GetString("UserAccountFetched"));
        }

        [HttpGet(APIs.GetAccountInformation, Order = 2)]
        [Authorize(Roles = RolesNames.Administrator)]
        public IActionResult GetAccountInformation(string username)
        {
            return HttpContext.Success(
                _mapper.Map<UserAccountForReturnDto>(GetUser(username: username)),
                _messages.GetString("UserAccountFetched"));
        }

        [HttpGet(APIs.GetAccountInformationMe, Order = 1)]
        public IActionResult GetMyAccountInformation()
        {
            return HttpContext.Success(
                _mapper.Map<UserAccountForReturnDto>(GetUser(id: HttpContext.GetUserId())),
                _messages.GetString("UserAccountFetched"));
        }

        [HttpPut(APIs.ModifyAccountInformation, Order = 2)]
        [Authorize(Roles = RolesNames.Administrator)]
        public IActionResult ModifyAccountInformation(string username, UserAccountForUpdateDto account)
        {
            Assert.NotNull(account);
            return ModifyUserInstance(GetUser(username: username), account);
        }

        [HttpPut(APIs.ModifyAccountInformationMe, Order = 1)]
        public IActionResult ModifyMyAccountInformation(UserAccountForUpdateDto account)
        {
            Assert.NotNull(account);
            return ModifyUserInstance(GetUser(id: HttpContext.GetUserId()), account);
        }

        private IActionResult ModifyUserInstance(User user, UserAccountForUpdateDto account)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(account.PhotoUrl.AbsolutePath))
                {
                    user.PhotoUrl = account.PhotoUrl.AbsolutePath;
                }
                tryModiyUserEmails(user, account.UserEmails, account.Email);
                tryModiyUserPhones(user, account.UserPhones, account.PhoneNumber);
                tryModiyUserAddresses(user, account.UserAddresses, _context.Addresses.AsQueryable());
            }
            catch (Exception e)
            {
                throw new Exception(_messages.GetString("UserAccountModifyFailed"), e);
            }
            return HttpContext.Success(_mapper.Map<UserAccountForReturnDto>(user), _messages.GetString("UserAccountModifyDone"));
        }

        private static void tryModiyUserEmails(User user, List<UserEmailForModifyDto> emails, string defaultEmail)
        {
            if (emails != null && emails.Count > 0)
            {
                emails.ForEach(e =>
                {
                    if (!user.UserEmails.Any(ue => ue.Address == e.Address))
                    {
                        user.UserEmails.Add(new UserEmail { Address = e.Address, Confirmed = false });
                    }
                });

                user.UserEmails.ForEach(ue =>
                {
                    if (!emails.Any(e => ue.Address == e.Address))
                        user.UserEmails.Remove(ue);
                });
            }

            if (!string.IsNullOrWhiteSpace(defaultEmail))
            {
                var defaultUserEmail = user.UserEmails.Find(ue => ue.Address == defaultEmail);
                user.Email = defaultEmail;
                user.EmailConfirmed = defaultUserEmail.Confirmed;
            }
        }

        private static void tryModiyUserPhones(User user, List<UserPhoneForModifyDto> phones, string defaultPhone)
        {
            if (phones != null && phones.Count > 0)
            {
                phones.ForEach(e =>
                {
                    if (!user.UserPhoneNumbers.Any(up => up.Number == e.Number))
                    {
                        user.UserPhoneNumbers.Add(new UserPhoneNumber { Number = e.Number, Confirmed = false });
                    }
                });

                user.UserPhoneNumbers.ForEach(up =>
                {
                    if (!phones.Any(e => up.Number == e.Number))
                        user.UserPhoneNumbers.Remove(up);
                });
            }

            if (!string.IsNullOrWhiteSpace(defaultPhone))
            {
                var phone = user.UserPhoneNumbers.Find(up => up.Number == defaultPhone);
                user.PhoneNumber = defaultPhone;
                user.PhoneNumberConfirmed = phone.Confirmed;
            }
        }

        private void tryModiyUserAddresses(User user, List<UserAddressForModifyDto> addresses, IEnumerable<Address> addressesList)
        {
            if (addresses != null && addresses.Count > 0)
            {
                addresses.ForEach(a =>
                {
                    if (!user.UserAddresses.Any(ua => ua.Address.AddressCode == a.AddressCode))
                        user.UserAddresses.Add(new UserAddress { Address = tryGetAddress(addressesList, a.AddressCode), Confirmed = false });
                });

                user.UserAddresses.ForEach(ua =>
                {
                    if (!addresses.Any(e => ua.Address.AddressCode == e.AddressCode))
                        user.UserAddresses.Remove(ua);
                });
            }
        }

        private Address tryGetAddress(IEnumerable<Address> addresses, string addressCode)
        {
            return addresses.FirstOrDefault(a => a.AddressCode.ToUpper() == (addressCode.ToUpper()))
                ?? throw new NoDataFoundException(_messages.GetString("AddressCodeNotFound", addressCode));
        }

        private User GetUser(int? id = null, string username = null)
        {
            var users = _context.Users
                .Include(u => u.UserAddresses)
                .ThenInclude(ua => ua.Address)
                .Include(u => u.UserPhoneNumbers)
                .Include(u => u.UserEmails)
                .Include(u => u.UserRoles)
                .ThenInclude(ur => ur.Role)
                .AsQueryable();

            if (!string.IsNullOrWhiteSpace(username))
            {
                users = users.Where(u => u.NormalizedUserName.ToUpper() == (username.ToUpper()));
            }

            if (id != null && id.Value > 0)
            {
                users = users.Where(u => u.Id == id.Value);
            }

            return users.FirstOrDefault() ?? throw new KeyNotFoundException(_messages.GetString("UnknownUserId"));
        }
    }
}