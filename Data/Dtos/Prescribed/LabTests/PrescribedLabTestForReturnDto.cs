using System;
using System.ComponentModel.DataAnnotations;
using PatientsRecordsManager.Data.Dtos.LabTests;
using PatientsRecordsManager.Data.Dtos.Visits;

namespace PatientsRecordsManager.Data.Dtos.Prescribed.LabTests
{
    public class PrescribedLabTestForReturnDto
    {
        [Required]
        public string PrescribedLabTestCode { get; set; }
        
        [Required]
        public string VisitCode { get; set; }
        public VisitForReturnDto Visit { get; set; }

        [Required]
        public string LabTestCode { get; set; }
        public LabTestForReturnDto LabTest { get; set; }

        [Required]
        public DateTime ScheduledDate { get; set; }
        
        [Required]
        public string Note { get; set; }
    }
}