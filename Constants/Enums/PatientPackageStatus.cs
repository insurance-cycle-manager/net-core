namespace PatientsRecordsManager.Constants.Enums
{
    public enum PatientPackageStatus
    {
        // Patient requested the package
        Requested,
        //  IC approved the request
        Approved,
        // IC rejected the request
        Rejected,
        // Suspended
        Suspended
    }
}