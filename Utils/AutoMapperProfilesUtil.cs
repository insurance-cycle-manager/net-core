using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using PatientsRecordsManager.Data.Dtos.Drugs;
using PatientsRecordsManager.Data.Dtos.Patients;
using PatientsRecordsManager.Data.Dtos.Surgeries;
using PatientsRecordsManager.Data.Dtos.Prescribed.Surgeries;
using PatientsRecordsManager.Data.Dtos.InsuranceCompanies;
using PatientsRecordsManager.Data.Dtos.TPAs;
using PatientsRecordsManager.Data.Dtos.InsuranceCompanies.Packages;
using PatientsRecordsManager.Data.Dtos.Diagnosis;
using PatientsRecordsManager.Data.Dtos.ServiceProviders;
using PatientsRecordsManager.Models;
using PatientsRecordsManager.Data.Dtos.Users;
using PatientsRecordsManager.Data.Dtos.Roles;
using PatientsRecordsManager.Data.Dtos.Addresses;
using PatientsRecordsManager.Data.Dtos.Users.Roles;
using PatientsRecordsManager.Data.Dtos.Visits;
using PatientsRecordsManager.Data.Dtos.Users.Accounts;
using PatientsRecordsManager.Data.Dtos.Users.Emails;
using PatientsRecordsManager.Data.Dtos.Permissions;
using PatientsRecordsManager.Data.Dtos.ServiceProviders.Connections;
using PatientsRecordsManager.Data.Dtos.Prescribed.Prescriptions;
using PatientsRecordsManager.Data.Dtos.LabTests;
using PatientsRecordsManager.Data.Dtos.Radiographs;
using PatientsRecordsManager.Data.Dtos.Prices;
using PatientsRecordsManager.Data.Dtos.Prescribed.LabTests;
using PatientsRecordsManager.Data.Dtos.Prescribed.Radiographs;
using PatientsRecordsManager.Data.Dtos.ServiceProviders.Staffers;
using PatientsRecordsManager.Data.Dtos.RolesRequests;
using PatientsRecordsManager.Data.Dtos.Users.Phones;
using PatientsRecordsManager.Data.Dtos.Users.Addresses;
using PatientsRecordsManager.Data.Dtos.ServiceProviders.Staff;
using System.ComponentModel;
using System.Reflection;
using PatientsRecordsManager.Constants;
using System.Globalization;
using PatientsRecordsManager.Data.Dtos.Patients.Packages;
using PatientsRecordsManager.Data.Dtos.Visits.Claims;
using PatientsRecordsManager.Data.Dtos.InsuranceConnections;
using System;
using PatientsRecordsManager.ABAC.Data.Models;
using PatientsRecordsManager.ABAC.Data.Dtos;
using Newtonsoft.Json;
using System.Text;
using Casbin.NET.Adapter.EFCore;

namespace PatientsRecordsManager.Utils
{
    public class AutoMapperProfilesUtil : Profile
    {
        public AutoMapperProfilesUtil()
        {
            AllowNullCollections = true;
            AllowNullDestinationValues = true;

            CreateMaps();
        }

        private bool Filter(object member)
        {
            // NOTE: The code below does NOT find the ReadOnlyAttribute
            var readOnlyAttr = member?.GetType().GetCustomAttribute<ReadOnlyAttribute>();
            var isReadOnly = readOnlyAttr?.IsReadOnly ?? false;
            return member != null || !isReadOnly;
        }

        private void CreateAbacMaps()
        {
            CreateMap<Type, Entity>()
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.FullName))
                .ForMember(dest => dest.Properties, opt => opt.MapFrom(src => src.GetProperties().Select(p => p.Name)));

            CreateMap<CasbinRule<int>, RuleForReturnDto>()
                .ForMember(dest => dest.V4, opt => opt.MapFrom(src => JsonConvert.DeserializeObject(Encoding.UTF8.GetString(Convert.FromBase64String(src.V4)))));
        }

        private void CreateMaps()
        {

            CreateMap<UserForLoginDto, User>();
            CreateMap<UserForRegisterDto, User>();
            CreateUserForReturnMap();
            CreateUserAccountForReturnMap();

            CreateUserRolesRequestMap();

            CreateMap<Role, RoleForReturnDto>();
            CreateMap<RoleForCreateDto, Role>();
            CreateMap<RoleForUpdateDto, Role>();
            CreateMap<InsuranceCompanyForCreateDto, InsuranceCompany>()
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => new Status { Name = src.Status }));
            CreateIcForReturnMap();
            CreateMap<TpaForCreateDto, ThirdPartyAdministrator>()
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => new Status { Name = src.Status }));
            CreateTpaForReturnMap();

            CreateMap<AddressForCreateDto, Address>()
                .ForMember(dest => dest.NormalizedTitle, opt => opt.MapFrom(src =>
                    src.Title.ToUpper(CultureInfo.CurrentCulture)));
            CreateMap<AddressForUpdateDto, Address>()
                .ForMember(dest => dest.NormalizedTitle, opt => opt.MapFrom(src =>
                    src.Title.ToUpper(CultureInfo.CurrentCulture)));
            CreateMap<Address, AddressForReturnDto>();

            CreateMap<InsuranceCompanyForUpdateDto, InsuranceCompany>();
            CreateMap<TpaForUpdateDto, ThirdPartyAdministrator>();
            CreateInsurancePackageMaps();

            CreateSpMap();
            CreateSpConnectionMap();
            CreatePatientMap();
            CreatePriceMap();

            // Visit related maps
            CreateVisitRelatedMaps();
        }

        private void CreateInsurancePackageMaps()
        {
            CreateMap<InsurancePackage, InsurancePackageForReturnDto>();

            CreatePackageForCreateMap();
            CreateMap<InsurancePackageForUpdateDto, InsurancePackage>();
        }

        private void CreatePackageForCreateMap()
        {
            CreateMap<InsurancePackageForCreateDto, InsurancePackage>()
                .ForMember(dest => dest.Drugs, act => act.Ignore())
                .ForMember(dest => dest.LabTests, act => act.Ignore())
                .ForMember(dest => dest.Surgeries, act => act.Ignore())
                .ForMember(dest => dest.Radiographs, act => act.Ignore())
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name.Normalize()))
                .BeforeMap((src, dest) =>
                {
                    var currentPrice = dest.Price;
                    dest.Price = new Price
                    {
                        Value = src.Price.Value,
                        PreviousPrice = currentPrice
                    };
                });
        }

        private void CreateVisitRelatedMaps()
        {
            CreateVisitsMap();
            CreateDiagnosisesMap();
            CreatePrescriptionMaps();
            CreateSurgeryMaps();
            CreateLabTestMaps();
            CreateRadiographMaps();

            CreateMap<VisitClaim, VisitClaimForReturnDto>();
        }

        private void CreateUserRolesRequestMap()
        {
            CreateMap<Permission, PermissionForReturnDto>();

            CreateMap<UserRole, UserRoleForReturnDto>()
                .ForMember(urr => urr.UserName, opt => opt.MapFrom(ur => ur.User.UserName))
                .ForMember(urr => urr.RoleName, opt => opt.MapFrom(ur => ur.Role.Name))
                .ForMember(urr => urr.Permissions, opt => opt.MapFrom(ur => from urp in ur.Role.Permissions select urp.Permission));

            CreateMap<RequestUserRoleDto, UserRoleRequest>();
            CreateMap<UserRoleRequest, RoleRequestForReturnDto>();
        }

        private void CreateSpConnectionMap()
        {
            CreateMap<SpConnectionForAddDto, ServiceProviderConnection>();
            CreateMap<SpConnectionForModifyDto, ServiceProviderConnection>();
            CreateMap<ServiceProviderConnection, SpConnectionForReturnDto>()
                .ForMember(dest => dest.InsuranceCompanies, opt => opt.MapFrom(src => src.InsuranceCompanies.Select(ic => ic.InsuranceCompany)));
        }

        private void CreateVisitsMap()
        {
            CreateMap<VisitForCreateDto, Visit>();
            CreateMap<VisitForUpdateDto, Visit>()
                .ReverseMap();
            CreateMap<Visit, VisitForReturnDto>();
        }

        private void CreateDiagnosisesMap()
        {
            CreateMap<DiagnosisForCreateDto, VisitDiagnosis>();
            CreateMap<DiagnosisForUpdateDto, VisitDiagnosis>();
            CreateMap<VisitDiagnosis, DiagnosisForReturnDto>();
        }

        private void CreatePrescriptionMaps()
        {
            CreateMap<PrescriptionForCreateDto, Prescription>()
                .ForMember(dest => dest.Drugs, opt => opt.MapFrom(src => new List<PrescriptionDrug>()));
            CreateMap<PrescriptionForUpdateDto, Prescription>()
                .ForMember(dest => dest.Drugs, opt => opt.MapFrom(src => new List<PrescriptionDrug>()));
            CreateMap<Prescription, PrescriptionForReturnDto>()
                .ForMember(dest => dest.Drugs, opt => opt.MapFrom(src => from d in src.Drugs select d.Drug));

            CreateMap<DrugForCreateDto, Drug>()
                .ForMember(dest => dest.NormalizedName, opt => opt.MapFrom(src => src.Name.Normalize()))
                .BeforeMap((src, dest) =>
                {
                    var currentPrice = dest.Price;
                    dest.Price = new Price
                    {
                        Value = src.Price.Value,
                        PreviousPrice = currentPrice
                    };
                });

            CreateMap<DrugForUpdateDto, Drug>()
                .ForMember(dest => dest.NormalizedName, opt => opt.MapFrom(src => src.Name.Normalize()))
                .BeforeMap((src, dest) =>
                {
                    if (src != null && src.Price != null && src.Price.Value != dest.Price.Value)
                    {
                        var current = dest.Price;
                        dest.Price = new Price
                        {
                            Value = src.Price.Value,
                            PreviousPrice = current
                        };
                    }
                });
            CreateMap<Drug, DrugForReturnDto>();
            CreateMap<PackageDrug, PackageDrugForReturnDto>()
                .ForMember(dest => dest.DrugCode, opt => opt.MapFrom(src => src.Drug.DrugCode));

            CreateMap<PrescriptionPurchaseForAddDto, PrescriptionPurchase>();
            CreateMap<PrescriptionPurchase, PrescriptionPurchaseForReturnDto>();

            CreateMap<Drug, DrugForReturnDto>();
            CreateMap<DrugPurchase, DrugForReturnDto>();

            CreateMap<DrugPurchaseForAddDto, DrugPurchase>()
            .BeforeMap((src, dest) =>
                {
                    var currentPrice = dest.Price;
                    dest.Price = new Price
                    {
                        Value = src.Price.Value,
                        PreviousPrice = currentPrice
                    };
                });
        }

        private void CreateSurgeryMaps()
        {
            CreateMap<SurgeryForCreateDto, Surgery>()
                .ForMember(dest => dest.NormalizedName, opt => opt.MapFrom(src => src.Name.Normalize()))
                .BeforeMap((src, dest) =>
                {
                    var currentPrice = dest.Price;
                    dest.Price = new Price
                    {
                        Value = src.Price.Value,
                        PreviousPrice = currentPrice
                    };
                });
            CreateMap<SurgeryForUpdateDto, Surgery>()
                .ForMember(dest => dest.NormalizedName, opt => opt.MapFrom(src => src.Name.Normalize()))
                .BeforeMap((src, dest) =>
                {
                    var currentPrice = dest.Price;
                    dest.Price = new Price
                    {
                        Value = src.Price.Value,
                        PreviousPrice = currentPrice
                    };
                });
            CreateMap<Surgery, SurgeryForReturnDto>();

            CreateMap<PrescribedSurgeryForCreateDto, PrescribedSurgery>();
            CreateMap<PrescribedSurgeryForUpdateDto, PrescribedSurgery>();
            CreateMap<PrescribedSurgery, PrescribedSurgeryForReturnDto>();

            // CreateMap<SurgeryPurchaseForAddDto, SurgeryPurchase>()
            // .BeforeMap((src, dest) =>
            //     {
            //         var currentPrice = dest.Price;
            //         dest.Price = new Price
            //         {
            //             Value = src.Price.Value,
            //             PreviousPrice = currentPrice
            //         };
            //     });

            CreateMap<SurgeryPurchase, SurgeryPurchaseForReturnDto>();
            CreateMap<PackageSurgery, PackageSurgeryForReturnDto>()
                .ForMember(dest => dest.SurgeryCode, opt => opt.MapFrom(src => src.Surgery.SurgeryCode));
        }

        private void CreateLabTestMaps()
        {
            CreateMap<LabTestForCreateDto, LabTest>()
                .ForMember(dest => dest.NormalizedName, opt => opt.MapFrom(src => src.Name.Normalize()))
                .BeforeMap((src, dest) =>
                {
                    var currentPrice = dest.Price;
                    dest.Price = new Price
                    {
                        Value = src.Price.Value,
                        PreviousPrice = currentPrice
                    };
                });
            CreateMap<LabTestForUpdateDto, LabTest>()
                .ForMember(dest => dest.NormalizedName, opt => opt.MapFrom(src => src.Name.Normalize()))
                .BeforeMap((src, dest) =>
                {
                    var currentPrice = dest.Price;
                    dest.Price = new Price
                    {
                        Value = src.Price.Value,
                        PreviousPrice = currentPrice
                    };
                });
            CreateMap<LabTest, LabTestForReturnDto>();
            CreateMap<PackageLabTest, PackageLabTestForReturnDto>()
                .ForMember(dest => dest.LabTestCode, opt => opt.MapFrom(src => src.LabTest.LabTestCode));

            CreateMap<PrescribedLabTestForCreateDto, PrescribedLabTest>();
            CreateMap<PrescribedLabTestForUpdateDto, PrescribedLabTest>();
            CreateMap<PrescribedLabTest, PrescribedLabTestForReturnDto>();

            // CreateMap<LabTestPurchaseForAddDto, LabTestPurchase>()
            // .BeforeMap((src, dest) =>
            //     {
            //         var currentPrice = dest.Price;
            //         dest.Price = new Price
            //         {
            //             Value = src.Price.Value,
            //             PreviousPrice = currentPrice
            //         };
            //     });

            CreateMap<LabTestPurchase, LabTestPurchaseForReturnDto>();
        }

        private void CreateRadiographMaps()
        {
            CreateMap<RadiographForCreateDto, Radiograph>()
                .ForMember(dest => dest.NormalizedName, opt => opt.MapFrom(src => src.Name.Normalize()))
                .BeforeMap((src, dest) =>
                {
                    var currentPrice = dest.Price;
                    dest.Price = new Price
                    {
                        Value = src.Price.Value,
                        PreviousPrice = currentPrice
                    };
                });
            CreateMap<RadiographForUpdateDto, Radiograph>()
                .ForMember(dest => dest.NormalizedName, opt => opt.MapFrom(src => src.Name.Normalize()))
                .BeforeMap((src, dest) =>
                {
                    var currentPrice = dest.Price;
                    dest.Price = new Price
                    {
                        Value = src.Price.Value,
                        PreviousPrice = currentPrice
                    };
                });
            CreateMap<Radiograph, RadiographForReturnDto>();

            CreateMap<PrescribedRadiographForCreateDto, PrescribedRadiograph>();
            CreateMap<PrescribedRadiographForUpdateDto, PrescribedRadiograph>();
            CreateMap<PrescribedRadiograph, PrescribedRadiographForReturnDto>();

            CreateMap<PackageRadiograph, PackageRadiographForReturnDto>()
            .ForMember(dest => dest.RadiographCode, opt => opt.MapFrom(src => src.Radiograph.RadiographCode));

            CreateMap<RadiographPurchase, RadiographPurchaseForReturnDto>();

            CreateMap<RadiographPurchaseForAddDto, RadiographPurchase>()
            .BeforeMap((src, dest) =>
                {
                    var currentPrice = dest.Price;
                    dest.Price = new Price
                    {
                        Value = src.Price.Value,
                        PreviousPrice = currentPrice
                    };
                });
        }

        private void CreateSpMap()
        {
            CreateMap<ServiceProviderForCreateDto, ServiceProvider>()
                .ForMember(dest => dest.NormalizedTitle, opt => opt.MapFrom(src => src.Title.Normalize()));
            CreateMap<ServiceProviderForUpdateDto, ServiceProvider>()
                .ForMember(dest => dest.NormalizedTitle, opt => opt.MapFrom(src => src.Title.Normalize()))
                .ForAllMembers(opts => opts.Condition((src, dest, srcMember) => srcMember != null));
            CreateMap<ServiceProvider, ServiceProviderForReturnDto>()
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.Status.Name))
                .ForMember(dest => dest.Address, opt => opt.MapFrom(src => src.Address.Address))
                .ForMember(dest => dest.Staff, opt => opt.MapFrom(src => src.Staff.Select(e => e.Staffer)));

            CreateMap<StafferForCreateDto, ServiceStaffer>()
                .ForMember(dest => dest.NormalizedTitle, opt => opt.MapFrom(src => src.Title.Normalize()));
            CreateMap<StafferForUpdateDto, ServiceStaffer>()
                .ForMember(dest => dest.NormalizedTitle, opt => opt.MapFrom(src => src.Title.Normalize()))
                .ForAllMembers(opts => opts.Condition((src, dest, srcMember) => srcMember != null));
            CreateMap<ServiceStaffer, StafferForReturnDto>()
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.Status.Name))
                .ForMember(dest => dest.Address, opt => opt.MapFrom(src => src.Address.Address))
                .ForMember(dest => dest.ServiceProviders, opt => opt.MapFrom(src => src.ServiceProviders.Select(e => e.ServiceProvider)));

            CreateMap<ServiceProviderStaffer, MemberForReturnDto>();
        }

        private void CreatePatientMap()
        {
            CreateMap<PatientForCreateDto, Patient>()
                .ForMember(dest => dest.NormalizedName, opt => opt.MapFrom(src => src.Name.ToUpper(CultureInfo.CurrentCulture)));
            CreateMap<PatientForUpdateDto, Patient>();
            CreateMap<Patient, PatientForReturnDto>()
                .ForMember(dest => dest.Packages, opt => opt.MapFrom(src => src.PatientPackages.Select(pp => pp.InsurancePackage)));

            CreateMap<PatientPackage, PatientPackageForReturnDto>();
        }

        private void CreateUserAccountForReturnMap()
        {
            CreateMap<User, UserAccountForReturnDto>();
        }

        private void CreateTpaForReturnMap()
        {
            CreateMap<ThirdPartyAdministrator, TpaForReturnDto>()
                .ForMember(dest => dest.SupervisoryComission, opt => opt.MapFrom(src => src.SupervisoryComission.Name))
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.Status.Name));

            CreateMap<InsuranceConnection, IcConnectionForReturnDto>();
        }

        private void CreateIcForReturnMap()
        {
            CreateMap<InsuranceCompany, InsuranceCompanyForReturnDto>()
                .ForMember(dest => dest.SupervisoryComission, opt => opt.MapFrom(src => src.SupervisoryComission.Name))
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.Status.Name));
        }

        private void CreatePriceMap()
        {
            CreateMap<PriceForAddDto, Price>();
            CreateMap<Price, PriceForReturnDto>();
        }

        private void CreateUserForReturnMap()
        {
            CreateMap<UserEmailForAddDto, UserEmail>();
            CreateMap<UserEmailForModifyDto, UserEmail>();
            CreateMap<UserEmail, UserEmailForReturnDto>()
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.User.UserName));

            CreateMap<UserPhoneForAddDto, UserPhoneNumber>();
            CreateMap<UserPhoneForModifyDto, UserPhoneNumber>();
            CreateMap<UserPhoneNumber, UserPhoneForReturnDto>()
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.User.UserName));

            CreateMap<UserAddressForAddDto, UserAddress>();
            CreateMap<UserAddressForModifyDto, UserAddress>();
            CreateMap<UserAddress, UserAddressForReturnDto>()
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.User.UserName));

            CreateMap<User, UserForReturnDto>();
        }
    }

}