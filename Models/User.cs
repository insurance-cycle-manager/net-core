﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Identity;

namespace PatientsRecordsManager.Models
{
    public class User : IdentityUser<int>
    {
        public string PhotoUrl { get; set; }

        public DateTime DateOfBirth { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public List<UserRoleRequest> UserRoleRequests { get; set; }
        public List<UserRole> UserRoles { get; set; }
        public List<UserAddress> UserAddresses { get; set; }
        public List<UserEmail> UserEmails { get; set; }
        public List<UserPhoneNumber> UserPhoneNumbers { get; set; }
    }
}
