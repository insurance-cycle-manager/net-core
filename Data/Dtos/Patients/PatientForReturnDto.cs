using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using PatientsRecordsManager.Constants.Enums;
using PatientsRecordsManager.Data.Dtos.InsuranceCompanies.Packages;
using PatientsRecordsManager.Data.Dtos.Patients.Packages;
using PatientsRecordsManager.Data.Dtos.Users.Addresses;
using PatientsRecordsManager.Data.Dtos.Users.Emails;
using PatientsRecordsManager.Data.Dtos.Users.Phones;
using PatientsRecordsManager.Data.Dtos.Visits;

namespace PatientsRecordsManager.Data.Dtos.Patients
{
    public class PatientForReturnDto
    {
        [Required]
        public string PatientCode { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public DateTime DateOfBirth { get; set; }
        [Required]
        [EnumDataType(typeof(Gender))]
        public Gender Gender { get; set; }
        public UserAddressForReturnDto Address { get; set; }
        public UserEmailForReturnDto Email { get; set; }
        public UserPhoneForReturnDto PhoneNumber { get; set; }
        public IEnumerable<VisitForReturnDto> Visits { get; set; }
        public IEnumerable<InsurancePackageForReturnDto> Packages { get; set; }
    }
}