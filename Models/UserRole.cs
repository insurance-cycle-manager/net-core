using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace PatientsRecordsManager.Models
{
    public class UserRole : IdentityUserRole<int>
    {
        public User User { get; set; }
        public Role Role { get; set; }

        public ServiceProvider ServiceProvider { get; set; }
        public ServiceStaffer ServiceStaffer { get; set; }
        public Patient Patient { get; set; }
        public ThirdPartyAdministrator ThirdPartyAdministrator { get; set; }
        public InsuranceCompany InsuranceCompany { get; set; }
    }
}