using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PatientsRecordsManager.Constants;
using PatientsRecordsManager.Data.Dtos.Patients;
using PatientsRecordsManager.Data.Dtos.Patients.Packages;
using PatientsRecordsManager.Data.Repositories;
using PatientsRecordsManager.Extensions;
using PatientsRecordsManager.Resources;

namespace PatientsRecordsManager.Controllers
{
    [ApiController]
    [Route(APIs.PatientsBaseUrl)]
    public class PatientsController : PrmController
    {
        private readonly IPatientRepository _repository;
        private readonly Messages _messages;

        public PatientsController(IPatientRepository repository, Messages messages)
        {
            _messages = messages;
            _repository = repository;
        }

        //
        // CRUD
        // 
        [HttpGet(APIs.GetPatients)]
        public IActionResult GetPatients([FromQuery] string title)
        {
            return HttpContext.Success(_repository.GetPatients(title), _messages.GetString("PatientsFetched"));
        }

        [HttpGet(APIs.GetPatientMeDetails, Order = 1)]
        [Authorize(Roles = RolesNames.Patient)]
        public IActionResult GetPatientMeDetails()
        {
            return HttpContext.Success(
                _repository.GetPatientDetails(HttpContext.GetUserId()),
                _messages.GetString("PatientsFetched"));
        }

        [HttpGet(APIs.GetPatientDetails, Order = 2)]
        [Authorize(Roles = RolesNames.Administrator)]
        public IActionResult GetPatientDetails(string patientCode)
        {
            return HttpContext.Success(
                _repository.GetPatientDetails(patientCode),
                _messages.GetString("PatientsFetched"));
        }

        [HttpPut(APIs.ModifyPatientMe, Order = 1)]
        public IActionResult ModifyPatientMeDetails(PatientForUpdateDto patient)
        {
            return HttpContext.Success(
                _repository.ModifyPatient(patient, HttpContext.GetUserId()),
                _messages.GetString("PatientsFetched"));
        }

        [HttpPut(APIs.ModifyPatient, Order = 2)]
        [Authorize(Roles = RolesNames.Administrator)]
        public IActionResult ModifyPatientDetails(string patientCode, PatientForUpdateDto patient)
        {
            return HttpContext.Success(
                _repository.ModifyPatient(patient, patientCode),
                _messages.GetString("PatientsFetched"));
        }

        //
        // Patient Packages
        // 
        [HttpPost(APIs.AddPatientMePackage, Order = 1)]
        public IActionResult AddPatientMePackage(PatientPackageForAddDto package)
        {
            return HttpContext.Success(
                _repository.AddPatientPackage(HttpContext.GetUserId(), package),
                _messages.GetString("patientPackageAddDone"));
        }

        [HttpGet(APIs.GetPatientMePackage, Order = 1)]
        public IActionResult GetPatientMePackages()
        {
            return HttpContext.Success(
                _repository.GetPatientPackages(HttpContext.GetUserId()),
                _messages.GetString("patientPackageAddDone"));
        }

        [HttpPost(APIs.AddPatientPackage, Order = 2)]
        [Authorize(Roles = RolesNames.Administrator)]
        public IActionResult AddPatientPackage(string patientCode, PatientPackageForAddDto package)
        {
            return HttpContext.Success(
                _repository.AddPatientPackage(patientCode, package),
                _messages.GetString("patientPackageAddDone"));
        }

        [HttpGet(APIs.GetPatientPackage, Order = 2)]
        [Authorize(Roles = RolesNames.GetStaffersCommaDelimitedWithAdmin)]
        public IActionResult GetPatientPackages(string patientCode)
        {
            return HttpContext.Success(
                _repository.GetPatientPackages(patientCode),
                _messages.GetString("patientPackageAddDone"));
        }

        [HttpPut(APIs.SuspendPatientPackage)]
        public IActionResult SuspendPatientPackage(string packageCode)
        {
            return HttpContext.Success(
                _repository.SuspendPatientPackage(HttpContext.GetUserId(), packageCode),
                _messages.GetString("patientPackageSuspendDone"));
        }
    }
}