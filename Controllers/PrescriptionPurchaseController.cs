using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PatientsRecordsManager.ABAC.Extensions;
using PatientsRecordsManager.ABAC.Extensions.EF;
using PatientsRecordsManager.Constants;
using PatientsRecordsManager.Data;
using PatientsRecordsManager.Data.Dtos.Prescribed.Prescriptions;
using PatientsRecordsManager.Extensions;
using PatientsRecordsManager.Models;
using PatientsRecordsManager.Resources;
using PatientsRecordsManager.Utils.Exceptions;

namespace PatientsRecordsManager.Controllers
{
    [Authorize]
    [ApiController]
    public class PrescriptionPurchaseController : PrmController
    {
        private readonly IMapper _mapper;
        private readonly DataContext _dataContext;
        private readonly Messages _messages;
        public PrescriptionPurchaseController(DataContext dataContext, IMapper mapper, Messages messages)
        {
            _messages = messages;
            _mapper = mapper;
            _dataContext = dataContext;
        }

        [HttpGet(APIs.DefaultRoute)]
        public IActionResult GetPresciptionPurchases()
        {
            var prescriptionPurchases = _dataContext.PrescriptionPurchases
                .Select(pp => _mapper.Map<PrescriptionPurchaseForReturnDto>(pp));

            return HttpContext.Success(prescriptionPurchases,
                _messages.GetString("PrescriptionPurchasesFetched"));

        }

        [HttpPost(APIs.AddPrescriptionPurchase)]
        public IActionResult AddPrescriptionPurchase(string visitCode, string prescriptionCode, [FromBody] PrescriptionPurchaseForAddDto addDto)
        {
            Utils.Assert.NotNull(addDto);
            var visit = FetchVisit(visitCode);
            var prescribed = visit.Prescriptions
                .FirstOrDefault(l => l.PrescriptionCode.ToUpper() == (prescriptionCode.ToUpper()));

            var drugs = GetPurchasedDrugs(prescribed, addDto.PurchasedDrugs);
            var price = CalculatePurchasedDrugs(drugs);

            var purchase = new PrescriptionPurchase
            {
                Visit = visit,
                Prescription = prescribed,
                PurchasedDrugs = drugs,
                Price = price,
                DateOfPurchasing = DateTime.Now
            };
            prescribed.PrescriptionPurchases.Add(purchase);

            if (_dataContext.SaveChanges() < 1)
                throw new Exception(_messages.GetString("LabTestPurchaseAddFailed"));

            return HttpContext.Success(
                _mapper.Map<PrescriptionPurchaseForReturnDto>(purchase),
                _messages.GetString("PrescriptionPurchaseCreated"));
        }

        [HttpGet(APIs.GetPrescriptionPurchase)]
        public IActionResult GetPresciptionPurchase(string visitCode, string prescriptionCode)
        {
            var purchases = _dataContext.PrescriptionPurchases
                .Include(pp => pp.Price)
                .Include(pp => pp.Prescription)
                .Include(pp => pp.Visit)
                // .Include(pp => pp.PurchasedDrugs)
                // .ThenInclude(pppd => pppd.Drug)
                .Where(pp => pp.Visit.VisitCode.ToUpper() == (visitCode.ToUpper()) && pp.Prescription.PrescriptionCode.ToUpper() == (prescriptionCode.ToUpper()))
                .ToList();

            return HttpContext.Success(
                _mapper.Map<IEnumerable<PrescriptionPurchaseForReturnDto>>(purchases),
                _messages.GetString("PrescriptionPurchaseFetched"));
        }

        private Visit FetchVisit(string visitCode)
        {
            return _dataContext.Visits
                .Include(v => v.Prescriptions)
                .ThenInclude(p => p.PrescriptionPurchases)
                .Include(v => v.Prescriptions)
                .ThenInclude(p => p.Drugs)
                .ThenInclude(pd => pd.Drug)
                .ThenInclude(pd => pd.Price)
                .FirstOrDefault(v => v.VisitCode.ToUpper() == (visitCode.ToUpper()))
                ?? throw new NoDataFoundException(_messages.GetString("VisitNotFound2"));
        }

        private static List<DrugPurchase> GetPurchasedDrugs(Prescription prescription, IEnumerable<string> drugs)
        {
            List<DrugPurchase> purchased = new List<DrugPurchase>();

            foreach (var code in drugs)
            {
                var drug = prescription.Drugs.Find(d => d.Drug.DrugCode.Equals(code, StringComparison.CurrentCultureIgnoreCase));

                if (drug != null)
                {
                    purchased.Add(new DrugPurchase
                    {
                        Drug = drug.Drug,
                        Price = drug.Drug.Price,
                        DateOfPurchasing = DateTime.Now,
                    });
                }
            }
            return purchased;
        }

        private static Price CalculatePurchasedDrugs(IEnumerable<DrugPurchase> drugPurchases)
        {
            return new Price { Value = drugPurchases.Sum(dp => dp.Price.Value) };
        }
    }
}