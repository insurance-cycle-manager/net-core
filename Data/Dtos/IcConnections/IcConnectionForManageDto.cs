using System.ComponentModel.DataAnnotations;

namespace PatientsRecordsManager.Data.Dtos.InsuranceConnections
{
    public class IcConnectionForManageDto
    {
        [Required]
        public string InsuranceCompanyCode { get; set; }
        [Required]
        public string ThirdPartyCode { get; set; }
    }
}