namespace PatientsRecordsManager.ABAC.Extensions.EF
{
    public static class DbFunctionsExtensions
    {
        public static bool ILike(this Microsoft.EntityFrameworkCore.DbFunctions source, string match, string pattern)
        {
            return Microsoft.EntityFrameworkCore.NpgsqlDbFunctionsExtensions.ILike(source, match, pattern);
        }

        public static bool ILike(this Microsoft.EntityFrameworkCore.DbFunctions source, string matchExpression, string pattern, string escapeCharacter)
        {
            return Microsoft.EntityFrameworkCore.NpgsqlDbFunctionsExtensions.ILike(source, matchExpression, pattern, escapeCharacter);
        }
    }
}