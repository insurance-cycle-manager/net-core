namespace PatientsRecordsManager.Data.Dtos.Diagnosis
{
    public class DiagnosisForReturnDto
    {
        public string Description { get; set; }
        public string DiagnosisCode { get; set; }
    }
}