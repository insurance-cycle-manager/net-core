using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using AutoMapper;
using PatientsRecordsManager.ABAC.Extensions;
using PatientsRecordsManager.ABAC.Extensions.EF;
using PatientsRecordsManager.Constants.Enums;
using PatientsRecordsManager.Data.Dtos.Visits;
using PatientsRecordsManager.Exceptions;
using PatientsRecordsManager.Models;
using PatientsRecordsManager.Resources;
using PatientsRecordsManager.Utils;

namespace PatientsRecordsManager.Data.Repositories
{
    public class VisitRepository : IVisitRepository
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        private readonly Messages _messages;

        public VisitRepository(DataContext context, IMapper mapper, Messages messages)
        {
            _context = context;
            _mapper = mapper;
            _messages = messages;
        }

        public VisitForReturnDto AddVisit(int userId, ServiceProviderStafferType stType, VisitForCreateDto visitForCreate)
        {
            Assert.NotNull(visitForCreate);

            var visit = _mapper.Map<Visit>(visitForCreate);

            // 1- Get Patient
            var p = GetPatient(visitForCreate.PatientCode);
            // 2- Get Staffer from id and type
            var staffMembership = GetStaffMembership(userId, stType, visitForCreate.ServiceProviderCode);

            visit.Patient = p;
            visit.ServiceProvider = staffMembership.ServiceProvider;
            visit.ServiceStaffer = staffMembership.Staffer;
            visit.ServiceProviderConnection = GetServiceProviderConnection(visit.ServiceProvider.ServiceProviderCode, visitForCreate.ServiceProviderConnectionCode);
            visit.PatientPackage = GetPatientPackage(
                visit.Patient,
                visit.ServiceProviderConnection.InsuranceCompanies.Select(i => i.InsuranceCompany.InsuranceCompanyCode),
                visitForCreate.PatientPackageCode);

            if (!string.IsNullOrEmpty(visitForCreate.BaseVisitCode))
            {
                visit.BaseVisit = GetVisit(visitForCreate.BaseVisitCode);

                if (visit.BaseVisit.PatientId != visit.Patient.Id)
                {
                    throw new Exception(_messages.GetString("badBaseVisitPatientMismatch", visitForCreate.BaseVisitCode));
                }
            }

            _context.Visits.Add(visit);

            if (_context.SaveChanges() < 1)
            {
                throw new SaveChangesFailedException(_messages.GetString("visitAddFailed"));
            }
            return _mapper.Map<VisitForReturnDto>(visit);
        }

        public VisitForReturnDto GetVisitDetails(string visitCode)
        {
            var visit = _context.Visits
                .Include(v => v.ServiceProvider)
                .ThenInclude(sp => sp.Address)
                .Include(v => v.ServiceStaffer)
                .ThenInclude(st => st.Address)
                .Include(v => v.ServiceProviderConnection)
                .Include(v => v.Patient)
                .Include(v => v.VisitClaim)
                .Include(v => v.BaseVisit)
                .Include(v => v.SubVisits)
                .Include(v => v.Diagnosis)
                // .Include(v => v.Prescriptions)
                // .Include(v => v.Surgeries)
                // .Include(v => v.LabTests)
                // .Include(v => v.Radiographs)
                // .Include(v => v.RadiographPurchases)
                // .Include(v => v.DrugPurchases)
                // .Include(v => v.SurgeryPurchases)
                // .Include(v => v.LabTestPurchases)
                .FirstOrDefault(v => v.VisitCode.ToUpper() == visitCode.ToUpper());

            if (visit == null)
            {
                throw new KeyNotFoundException(_messages.GetString("visitNotFound", visitCode));
            }
            return _mapper.Map<VisitForReturnDto>(visit);
        }

        public IEnumerable<VisitForReturnDto> GetVisits(VisitType? visitType)
        {
            var visits = _context.Visits
                .Include(v => v.Patient)
                .AsQueryable();

            if (visitType == null)
            {
                visits = visits.Where(v => v.Type == visitType);
            }
            return _mapper.Map<IEnumerable<VisitForReturnDto>>(visits.ToList());
        }

        public VisitForReturnDto ModifyVisit(int stUserId, ServiceProviderStafferType type, string visitCode, VisitForUpdateDto visitForUpdate)
        {
            var visit = GetVisit(visitCode);
            _mapper.Map<VisitForUpdateDto, Visit>(visitForUpdate, visit);

            if (_context.SaveChanges() < 1)
            {
                throw new SaveChangesFailedException(_messages.GetString("modifyVisitFailed", visitCode));
            }
            return _mapper.Map<VisitForReturnDto>(visit);
        }

        public VisitForReturnDto RemoveVisit(int stUserId, ServiceProviderStafferType type, string visitCode)
        {
            var visit = GetVisit(visitCode);

            _context.Visits.Remove(visit);
            if (_context.SaveChanges() < 1)
            {
                throw new SaveChangesFailedException(_messages.GetString("removeVisitFailed", visitCode));
            }
            return _mapper.Map<VisitForReturnDto>(visit);
        }

        private Visit GetVisit(string visitCode)
        {
            return _context.Visits
                .FirstOrDefault(v => v.VisitCode.ToUpper() == visitCode.ToUpper()) ??
                throw new KeyNotFoundException(_messages.GetString("visitNotFound", visitCode));
        }

        private Patient GetPatient(string patientCode)
        {
            return _context.Patients
                .Include(p => p.PatientPackages)
                .ThenInclude(pp => pp.InsurancePackage)
                .ThenInclude(ppip => ppip.InsuranceCompany)
                .FirstOrDefault(p => p.PatientCode.ToUpper() == patientCode.ToUpper())
                ?? throw new KeyNotFoundException(_messages.GetString("patientNotFound", patientCode));
        }

        private ServiceProvider GetServiceProvider(string providerCode)
        {
            var provider = _context.ServiceProviders
                .Include(sp => sp.Status)
                .Where(p => p.Status.Name == StatusType.Active && p.ServiceProviderCode.ToUpper() == providerCode.ToUpper())
                .FirstOrDefault();

            if (provider == null)
            {
                throw new KeyNotFoundException(_messages.GetString("serviceProviderNotFound", providerCode));
            }
            return provider;
        }

        private ServiceStaffer GetServiceStaffer(string stafferCode)
        {
            var staffer = _context.ServiceStaffers
                .Include(sp => sp.Status)
                .Where(s => s.Status.Name == StatusType.Active && s.ServiceStafferCode.ToUpper() == stafferCode.ToUpper())
                .FirstOrDefault();

            if (staffer == null)
            {
                throw new KeyNotFoundException(_messages.GetString("serviceStafferNotFound", stafferCode));
            }
            return staffer;
        }

        private ServiceProviderStaffer GetStaffMembership(int stUserId, ServiceProviderStafferType stType, string spCode)
        {
            return _context.ServiceProviderStaffer
                .Include(sps => sps.ServiceProvider)
                .ThenInclude(sp => sp.Status)
                .Include(sps => sps.Staffer)
                .ThenInclude(st => st.Status)
                .Where(sps =>
                    sps.Status == ServiceProviderStafferStatus.Confirmed &&
                    sps.Staffer.UserId == stUserId &&
                    sps.Staffer.Type == stType &&
                    sps.Staffer.Status.Name == StatusType.Active &&
                    sps.ServiceProvider.ServiceProviderCode.ToUpper() == spCode.ToUpper() &&
                    sps.ServiceProvider.Status.Name == StatusType.Active)
                .FirstOrDefault()
            ?? throw new KeyNotFoundException(_messages.GetString("stafferMembershipNotRecognized", stType, spCode));
        }

        private ServiceProviderConnection GetServiceProviderConnection(string spCode, string connectionCode)
        {
            var connection = _context.ServiceProviderConnections
                .Include(spc => spc.ServiceProvider)
                .Include(spc => spc.InsuranceCompanies)
                .ThenInclude(icc => icc.InsuranceCompany)
                .Where(p =>
                    p.Status == SpConnectionStatus.Approved &&
                    p.ServiceProvider.ServiceProviderCode.ToUpper() == spCode &&
                    p.ConnectionCode.ToUpper() == connectionCode.ToUpper())
                .FirstOrDefault();

            if (connection == null)
            {
                throw new KeyNotFoundException(_messages.GetString("spConnectionNotFoundOrNotApproved", connection));
            }
            return connection;
        }

        private PatientPackage GetPatientPackage(Patient patient, IEnumerable<string> insuranceCompanies, string packageCode)
        {
            var package = patient
                .PatientPackages
                .Where(p =>
                    p.Status == PatientPackageStatus.Approved &&
                    p.InsurancePackage.InsurancePackageCode.ToUpper() == packageCode.ToUpper())
                .ToList()
                .FirstOrDefault(p => insuranceCompanies.Contains(p.InsurancePackage.InsuranceCompany.InsuranceCompanyCode));

            if (package == null)
            {
                throw new KeyNotFoundException(_messages.GetString("patientPackageNotFoundOrNotApproved"));
            }
            return package;
        }
    }
}