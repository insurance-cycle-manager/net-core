using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PatientsRecordsManager.ABAC.Extensions;
using PatientsRecordsManager.ABAC.Extensions.EF;
using PatientsRecordsManager.Constants;
using PatientsRecordsManager.Data;
using PatientsRecordsManager.Data.Dtos.Users.Emails;
using PatientsRecordsManager.Exceptions;
using PatientsRecordsManager.Extensions;
using PatientsRecordsManager.Models;
using PatientsRecordsManager.Resources;

namespace PatientsRecordsManager.Controllers
{
    [ApiController]
    [Authorize]
    [Route(APIs.UserEmailsBaseUrl)]
    public class UserEmailsController : PrmController
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        private readonly Messages _messages;

        public UserEmailsController(DataContext context, IMapper mapper, Messages messages)
        {
            _messages = messages;
            _mapper = mapper;
            _context = context;
        }

        [HttpGet(APIs.GetPendingUserEmails)]
        [Authorize(Roles = RolesNames.Administrator)]
        public IActionResult GetPendingUserEmails([FromQuery] string userName)
        {
            return HttpContext.Success(GetAllEmails(false, userName), _messages.GetString("PendingUserEmailsFetchDone"));
        }

        [HttpGet(APIs.GetUserEmails)]
        public IActionResult GetUserEmails()
        {
            var user = GetUser(HttpContext.GetUserId());
            return HttpContext.Success(GetEmails(user), _messages.GetString("UserEmailsFetchDone", user.UserName));
        }

        [HttpPost(APIs.PostUserEmail)]
        public IActionResult PostUserEmail(UserEmailForAddDto email)
        {
            return HttpContext.Success(AddEmail(GetUser(HttpContext.GetUserId()), email), _messages.GetString("UserEmailPostDone"));
        }

        [HttpPut(APIs.ModifyUserEmail)]
        public IActionResult ModifyUserEmail(string emailCode, UserEmailForModifyDto email)
        {
            return HttpContext.Success(ModifyEmail(GetUser(HttpContext.GetUserId()), emailCode, email), _messages.GetString("UserEmailModifyDone"));
        }

        [HttpPut(APIs.ConfirmUserEmail)]
        public IActionResult ConfirmUserEmail(string userName, string emailCode)
        {
            return HttpContext.Success(ConfirmEmail(GetUser(userName), emailCode), _messages.GetString("UserEmailConfirmDone"));
        }

        [HttpDelete(APIs.DeleteUserEmail)]
        public IActionResult DeleteUserEmail(string emailCode)
        {
            return HttpContext.Success(DeleteEmail(GetUser(HttpContext.GetUserId()), emailCode), _messages.GetString("UserEmailDeletedDone"));
        }

        private User GetUser(string userName)
        {
            return _context.Users
                .Include(u => u.UserEmails)
                .FirstOrDefault(u => u.NormalizedUserName.ToUpper() == userName.ToUpper()) ??
                throw new ArgumentBadException(_messages.GetString("InvalidUserName", userName));
        }

        private User GetUser(int userId)
        {
            return _context.Users
                .Include(u => u.UserEmails)
                .FirstOrDefault(u => u.Id.Equals(userId)) ??
                throw new ArgumentBadException(_messages.GetString("InvalidUserName", userId));
        }

        private UserEmail GetEmailByCode(User user, string emailCode)
        {
            return user.UserEmails
                .FirstOrDefault(e => e.EmailCode.ToUpper() == emailCode.ToUpper()) ??
                throw new ArgumentBadException(_messages.GetString("UserEmailCodeNotFound"));
        }

        private List<UserEmailForReturnDto> GetAllEmails(bool confirmed, string userName = null)
        {
            var emails = _context.UserEmails
                .Include(e => e.User)
                .Where(e => e.Confirmed == confirmed);

            if (!string.IsNullOrWhiteSpace(userName))
            {
                emails = emails.Where(e => e.User.UserName.ToUpper().Contains(userName.ToUpper()));
            }
            return _mapper.Map<List<UserEmailForReturnDto>>(emails);
        }

        private List<UserEmailForReturnDto> GetEmails(User user)
        {
            return _mapper.Map<List<UserEmailForReturnDto>>(user.UserEmails);
        }

        private UserEmailForReturnDto AddEmail(User user, UserEmailForAddDto userEmailDto)
        {
            var email = _mapper.Map<UserEmail>(userEmailDto);
            user.UserEmails.Add(email);

            if (_context.SaveChanges() <= 0)
            {
                throw new SaveChangesFailedException();
            }
            return _mapper.Map<UserEmailForReturnDto>(email);
        }

        private UserEmailForReturnDto ModifyEmail(User user, string emailCode, UserEmailForModifyDto userEmailDto)
        {
            var retrievedEmail = GetEmailByCode(user, emailCode);
            var email = _mapper.Map<UserEmailForModifyDto, UserEmail>(userEmailDto, retrievedEmail);
            email.Confirmed = false;

            if (_context.SaveChanges() <= 0)
            {
                throw new SaveChangesFailedException();
            }
            return _mapper.Map<UserEmailForReturnDto>(email);
        }

        private UserEmailForReturnDto ConfirmEmail(User user, string emailCode)
        {
            var email = GetEmailByCode(user, emailCode);
            if (email.Confirmed)
            {
                throw new SaveChangesFailedException(_messages.GetString("UserEmailAlreadyConfirmed"));
            }
            email.Confirmed = true;

            if (_context.SaveChanges() <= 0)
            {
                throw new SaveChangesFailedException();
            }
            return _mapper.Map<UserEmailForReturnDto>(email);
        }

        private bool DeleteEmail(User user, string emailCode)
        {
            var email = GetEmailByCode(user, emailCode);
            user.UserEmails.Remove(email);

            if (_context.SaveChanges() <= 0)
            {
                throw new SaveChangesFailedException();
            }
            return true;
        }
    }
}