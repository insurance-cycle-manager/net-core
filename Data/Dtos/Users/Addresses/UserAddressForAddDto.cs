using System.ComponentModel.DataAnnotations;

namespace PatientsRecordsManager.Data.Dtos.Users.Addresses
{
    public class UserAddressForAddDto
    {
        [Required]
        public string AddressCode { get; set; }
    }
}