using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PatientsRecordsManager.Models
{
    public class LabTestPurchase
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public int PrescribedLabTestId { get; set; }
        public PrescribedLabTest PrescribedLabTest { get; set; }

        public DateTime DateOfPurchasing { get; set; }
        [ForeignKey("Prices")]
        public int PriceId { get; set; }
        public Price Price { get; set; }

        public int VisitId { get; set; }
        public Visit Visit { get; set; }
    }
}