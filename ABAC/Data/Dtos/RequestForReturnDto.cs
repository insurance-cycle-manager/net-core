using System;
using Newtonsoft.Json.Linq;

namespace PatientsRecordsManager.ABAC.Data.Dtos
{
    public class RequestForReturnDto
    {
        public DateTime RequestingDate { get; set; }
        public JObject Params { get; set; }
        public JObject ExecutionTree { get; set; }
        public bool Result { get; set; }
    }
}