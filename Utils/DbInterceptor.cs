using System.Data;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Logging;
using PatientsRecordsManager.Controllers;

namespace PatientsRecordsManager.Utils
{
    public class DbInterceptor : DbCommandInterceptor
    {
        private readonly IHttpContextAccessor _accessor;

        public DbInterceptor()
        {
            _accessor = new HttpContextAccessor();
        }

        public override DbCommand CommandCreated(CommandEndEventData eventData, DbCommand result)
        {
            // System.Console.WriteLine("CommandCreated {0} {1}", eventData?.Command?.CommandText, _accessor?.HttpContext?.User?.Identity?.Name);
            return base.CommandCreated(eventData, result);
        }
        public override InterceptionResult<DbCommand> CommandCreating(CommandCorrelatedEventData eventData, InterceptionResult<DbCommand> result)
        {
            // System.Console.WriteLine("CommandCreating {0} {1}", eventData?.CommandId, _accessor?.HttpContext?.User?.Identity?.Name);
            return base.CommandCreating(eventData, result);
        }
        public override void CommandFailed(DbCommand command, CommandErrorEventData eventData)
        {
            // System.Console.WriteLine("CommandFailed {0} {1}", eventData?.Command?.CommandText, _accessor?.HttpContext?.User?.Identity?.Name);

            base.CommandFailed(command, eventData);
        }
        public override Task CommandFailedAsync(DbCommand command, CommandErrorEventData eventData, CancellationToken cancellationToken = default)
        {
            // System.Console.WriteLine("CommandFailedAsync {0} {1}", eventData?.Command?.CommandText, _accessor?.HttpContext?.User?.Identity?.Name);
            return base.CommandFailedAsync(command, eventData, cancellationToken);
        }
        public override InterceptionResult DataReaderDisposing(DbCommand command, DataReaderDisposingEventData eventData, InterceptionResult result)
        {
            // System.Console.WriteLine("DataReaderDisposing {0} {1}", eventData?.Command?.CommandText, _accessor?.HttpContext?.User?.Identity?.Name);
            return base.DataReaderDisposing(command, eventData, result);
        }
        public override int NonQueryExecuted(DbCommand command, CommandExecutedEventData eventData, int result)
        {
            // System.Console.WriteLine("NonQueryExecuted {0} {1}", eventData?.Command?.CommandText, _accessor?.HttpContext?.User?.Identity?.Name);
            return base.NonQueryExecuted(command, eventData, result);
        }
        public override Task<int> NonQueryExecutedAsync(DbCommand command, CommandExecutedEventData eventData, int result, CancellationToken cancellationToken = default)
        {
            // System.Console.WriteLine("NonQueryExecutedAsync {0} {1}", eventData?.Command?.CommandText, _accessor?.HttpContext?.User?.Identity?.Name);
            return base.NonQueryExecutedAsync(command, eventData, result, cancellationToken);
        }
        public override InterceptionResult<int> NonQueryExecuting(DbCommand command, CommandEventData eventData, InterceptionResult<int> result)
        {
            // System.Console.WriteLine("NonQueryExecuting {0} {1}", eventData?.Command?.CommandText, _accessor?.HttpContext?.User?.Identity?.Name);
            return base.NonQueryExecuting(command, eventData, result);
        }
        public override Task<InterceptionResult<int>> NonQueryExecutingAsync(DbCommand command, CommandEventData eventData, InterceptionResult<int> result, CancellationToken cancellationToken = default)
        {
            // System.Console.WriteLine("NonQueryExecutingAsync {0} {1}", eventData?.Command?.CommandText, _accessor?.HttpContext?.User?.Identity?.Name);
            return base.NonQueryExecutingAsync(command, eventData, result);
        }
        public override DbDataReader ReaderExecuted(DbCommand command, CommandExecutedEventData eventData, DbDataReader result)
        {
            // System.Console.WriteLine("ReaderExecuted {0} {1}", eventData?.Command?.CommandText, _accessor?.HttpContext?.User?.Identity?.Name);
            return base.ReaderExecuted(command, eventData, result);
        }
        public override Task<DbDataReader> ReaderExecutedAsync(DbCommand command, CommandExecutedEventData eventData, DbDataReader result, CancellationToken cancellationToken = default)
        {
            // System.Console.WriteLine("ReaderExecutedAsync {0} {1}", eventData?.Command?.CommandText, _accessor?.HttpContext?.User?.Identity?.Name);
            return base.ReaderExecutedAsync(command, eventData, result, cancellationToken);
        }
        public override InterceptionResult<DbDataReader> ReaderExecuting(DbCommand command, CommandEventData eventData, InterceptionResult<DbDataReader> result)
        {
            // System.Console.WriteLine("ReaderExecuting {0} {1}", eventData?.Command?.CommandText, _accessor?.HttpContext?.User?.Identity?.Name);
            return base.ReaderExecuting(command, eventData, result);
        }
        public override Task<InterceptionResult<DbDataReader>> ReaderExecutingAsync(DbCommand command, CommandEventData eventData, InterceptionResult<DbDataReader> result, CancellationToken cancellationToken = default)
        {
            // System.Console.WriteLine("ReaderExecutingAsync {0} {1}", eventData?.Command?.CommandText, _accessor?.HttpContext?.User?.Identity?.Name);
            return base.ReaderExecutingAsync(command, eventData, result, cancellationToken);
        }
        public override object ScalarExecuted(DbCommand command, CommandExecutedEventData eventData, object result)
        {
            // System.Console.WriteLine("ScalarExecuted {0} {1}", eventData?.Command?.CommandText, _accessor?.HttpContext?.User?.Identity?.Name);
            return base.ScalarExecuted(command, eventData, result);
        }
        public override Task<object> ScalarExecutedAsync(DbCommand command, CommandExecutedEventData eventData, object result, CancellationToken cancellationToken = default)
        {
            // System.Console.WriteLine("ScalarExecutedAsync {0} {1}", eventData?.Command?.CommandText, _accessor?.HttpContext?.User?.Identity?.Name);
            return base.ScalarExecutedAsync(command, eventData, result, cancellationToken);
        }
        public override InterceptionResult<object> ScalarExecuting(DbCommand command, CommandEventData eventData, InterceptionResult<object> result)
        {
            // System.Console.WriteLine("ScalarExecuting {0} {1}", eventData?.Command?.CommandText, _accessor?.HttpContext?.User?.Identity?.Name);
            return base.ScalarExecuting(command, eventData, result);
        }
        public override Task<InterceptionResult<object>> ScalarExecutingAsync(DbCommand command, CommandEventData eventData, InterceptionResult<object> result, CancellationToken cancellationToken = default)
        {
            // System.Console.WriteLine("ScalarExecutingAsync {0} {1}", eventData?.Command?.CommandText, _accessor?.HttpContext?.User?.Identity?.Name);
            return base.ScalarExecutingAsync(command, eventData, result, cancellationToken);
        }
    }
}