using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using AutoMapper;
using Casbin.NET.Adapter.EFCore;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PatientsRecordsManager.ABAC.Data.Dtos;
using PatientsRecordsManager.ABAC.Data.Models;
using PatientsRecordsManager.ABAC.Extensions;
using PatientsRecordsManager.Resources;

namespace PatientsRecordsManager.ABAC.Data
{
    public class RulesRepository
    {
        private readonly AbacDbContext context;
        private readonly IMapper mapper;
        private readonly Messages messages;

        public RulesRepository(AbacDbContext context, IMapper mapper, Messages messages)
        {
            this.messages = messages;
            this.context = context;
            this.mapper = mapper;
        }

        public RuleForReturnDto AddDboTypeRule(DboRuleForCreateDto r)
        {
            if (r == null)
                throw new ArgumentNullException(nameof(r));
            return this.AddRule(AbacDbContext.pType, r.V0, r.V1, r.V2, r.V3, AddRuleAttributes(r.V4).Id.ToString(), AbacDbContext.dbo);
        }

        private RuleAttributes AddRuleAttributes(object attributes)
        {
            if (attributes == null)
                throw new ArgumentNullException(nameof(attributes));

            var ruleAttributes = new RuleAttributes
            {
                Attributes = JObject.Parse(JsonConvert.SerializeObject(attributes))
            };
            this.context.RuleAttributes.Add(ruleAttributes);
            this.context.SaveChanges();
            return ruleAttributes;
        }

        public RuleForReturnDto AddActionTypeRule(RuleForCreateDto r)
        {
            if (r == null)
                throw new ArgumentNullException(nameof(r));
            return this.AddRule(AbacDbContext.pType, r.V0, r.V1, r.V2, r.V3, r.V4, AbacDbContext.act);
        }

        private RuleForReturnDto AddRule(string pType, string v0, string v1, string v2, string v3, string v4, string v5)
        {
            if (string.IsNullOrEmpty(pType))
            {
                throw new ArgumentNullException(nameof(pType));
            }

            var rule = new CasbinRule<int>
            {
                PType = pType,
                V0 = v0,
                V1 = v1,
                V2 = v2,
                V3 = v3,
                V4 = v4 != null ? v4 : null,
                V5 = v5
            };

            this.context.CasbinRule.Add(rule);

            if (this.context.SaveChanges() <= 0)
            {
                throw new Exception(this.messages.GetString("ruleAddFailed"));
            }

            return new RuleForReturnDto
            {
                Id = rule.Id,
                PType = rule.PType,
                V0 = rule.V0,
                V1 = rule.V1,
                V2 = rule.V2,
                V3 = rule.V3,
                V4 = rule.V4 != null ? JsonConvert.DeserializeObject(rule.V4) : null,
                V5 = rule.V5,
            };
        }

        public IEnumerable<RuleForReturnDto> GetRules(string subject = null, string type = null)
        {
            var rules = this.context.CasbinRule
                .AsQueryable();

            if (!string.IsNullOrEmpty(subject))
                rules = rules.Where(r => r.V0.ToUpper() == subject.ToUpper());

            if (!string.IsNullOrEmpty(type))
                rules = rules.Where(r => r.V5.ToUpper() == type.ToUpper());

            return rules.Select(e => new RuleForReturnDto
            {
                Id = e.Id,
                PType = e.PType,
                V0 = e.V0,
                V1 = e.V1,
                V2 = e.V2,
                V3 = e.V3,
                V4 = e.V4 != null ? JsonConvert.DeserializeObject(e.V4) : null,
                V5 = e.V5,
            });
        }

        public IEnumerable<Request> GetRequests()
        {
            return this.context.Requests
                .Include(e => e.Items)
                .ToList();
        }

        public RuleForReturnDto RemoveRule(int ruleId)
        {
            var rule = GetRule(ruleId);

            this.context.CasbinRule.Remove(rule);
            if (this.context.SaveChanges() <= 0)
            {
                throw new Exception(this.messages.GetString("ruleRemoveFailed", ruleId));
            }
            return new RuleForReturnDto
            {
                Id = rule.Id,
                PType = rule.PType,
                V0 = rule.V0,
                V1 = rule.V1,
                V2 = rule.V2,
                V3 = rule.V3,
                V4 = rule.V4 != null ? JsonConvert.DeserializeObject(rule.V4) : null,
                V5 = rule.V5,
            };
        }

        public RuleForReturnDto ModifyRule(int ruleId, string ruleType, RuleForModifyDto ruleDto)
        {
            var rule = GetRule(ruleId);

            if (rule.V5.ToUpper() != ruleType.ToUpper())
            {
                throw new Exception(this.messages.GetString("ruleCodeNotFound", ruleId));
            }

            if (!string.IsNullOrEmpty(ruleDto.V0))
                rule.V0 = ruleDto.V0;
            if (!string.IsNullOrEmpty(ruleDto.V1))
                rule.V1 = ruleDto.V1;
            if (!string.IsNullOrEmpty(ruleDto.V2))
                rule.V2 = ruleDto.V2;
            if (!string.IsNullOrEmpty(ruleDto.V3))
                rule.V3 = ruleDto.V3;

            if (ruleDto.V4 != null)
                rule.V4 = AddRuleAttributes(ruleDto.V4).Id.ToString();

            this.context.CasbinRule.Update(rule);
            if (this.context.SaveChanges() <= 0)
            {
                throw new Exception(this.messages.GetString("ruleUpdateFailed", ruleId));
            }
            return new RuleForReturnDto
            {
                Id = rule.Id,
                PType = rule.PType,
                V0 = rule.V0,
                V1 = rule.V1,
                V2 = rule.V2,
                V3 = rule.V3,
                V4 = rule.V4 != null ? JsonConvert.DeserializeObject(rule.V4) : null,
                V5 = rule.V5,
            };
        }

        public JObject DetailsRule(int ruleId, string ruleType)
        {
            var rule = GetRule(ruleId);

            if (rule.V5.ToUpper() != ruleType.ToUpper())
            {
                throw new Exception(this.messages.GetString("ruleCodeNotFound", ruleId));
            }

            var attributes = this.context
                .RuleAttributes
                .FirstOrDefault(r => r.Id == int.Parse(rule.V4));

            if (attributes == null)
            {
                throw new Exception(this.messages.GetString("ruleCodeNotFound", ruleId));
            }
            return attributes.Attributes;
        }

        private CasbinRule<int> GetRule(int ruleId)
        {
            return this.context
                .CasbinRule
                .FirstOrDefault(r => r.Id == ruleId) ??
                throw new Exception(this.messages.GetString("ruleCodeNotFound", ruleId));
        }
    }
}