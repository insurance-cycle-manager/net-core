using System.ComponentModel.DataAnnotations;
using PatientsRecordsManager.Constants.Enums;

namespace PatientsRecordsManager.Models
{

    public class PatientPackage
    {
        [Required]
        public int PatientId { get; set; }
        public Patient Patient { get; set; }

        [Required]
        public int InsurancePackageId { get; set; }
        public InsurancePackage InsurancePackage { get; set; }
        
        [Required]
        public PatientPackageStatus Status { get; set; }
    }
}