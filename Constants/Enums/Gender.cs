namespace PatientsRecordsManager.Constants.Enums
{
    public enum Gender
    {
        Default,
        Male,
        Female,
        Unknown
    }
}