﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PatientsRecordsManager.Migrations
{
    public partial class icPackages : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Visits_ServiceStaffers_ServiceStafferId",
                table: "Visits");

            migrationBuilder.DropForeignKey(
                name: "FK_Visits_ServiceProviderStaffer_ServiceProviderStafferService~",
                table: "Visits");

            migrationBuilder.DropColumn(
                name: "ServiceProviderStafferId",
                table: "Visits");

            migrationBuilder.CreateSequence<int>(
                name: "InsurancePackagesNumbers");

            migrationBuilder.AlterColumn<int>(
                name: "ServiceStafferId",
                table: "Visits",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ServiceProviderStafferServiceStafferId",
                table: "Visits",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<int>(
                name: "ServiceProviderStafferServiceProviderId",
                table: "Visits",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AddColumn<int>(
                name: "Status",
                table: "PatientPackages",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<string>(
                name: "InsurancePackageCode",
                table: "InsurancePackages",
                nullable: true,
                defaultValueSql: "'ICP'|| nextval('public.\"InsurancePackagesNumbers\"')",
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Visits_ServiceStaffers_ServiceStafferId",
                table: "Visits",
                column: "ServiceStafferId",
                principalTable: "ServiceStaffers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Visits_ServiceProviderStaffer_ServiceProviderStafferService~",
                table: "Visits",
                columns: new[] { "ServiceProviderStafferServiceProviderId", "ServiceProviderStafferServiceStafferId" },
                principalTable: "ServiceProviderStaffer",
                principalColumns: new[] { "ServiceProviderId", "ServiceStafferId" },
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Visits_ServiceStaffers_ServiceStafferId",
                table: "Visits");

            migrationBuilder.DropForeignKey(
                name: "FK_Visits_ServiceProviderStaffer_ServiceProviderStafferService~",
                table: "Visits");

            migrationBuilder.DropSequence(
                name: "InsurancePackagesNumbers");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "PatientPackages");

            migrationBuilder.AlterColumn<int>(
                name: "ServiceStafferId",
                table: "Visits",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "ServiceProviderStafferServiceStafferId",
                table: "Visits",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ServiceProviderStafferServiceProviderId",
                table: "Visits",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ServiceProviderStafferId",
                table: "Visits",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<string>(
                name: "InsurancePackageCode",
                table: "InsurancePackages",
                type: "text",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true,
                oldDefaultValueSql: "'ICP'|| nextval('public.\"InsurancePackagesNumbers\"')");

            migrationBuilder.AddForeignKey(
                name: "FK_Visits_ServiceStaffers_ServiceStafferId",
                table: "Visits",
                column: "ServiceStafferId",
                principalTable: "ServiceStaffers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Visits_ServiceProviderStaffer_ServiceProviderStafferService~",
                table: "Visits",
                columns: new[] { "ServiceProviderStafferServiceProviderId", "ServiceProviderStafferServiceStafferId" },
                principalTable: "ServiceProviderStaffer",
                principalColumns: new[] { "ServiceProviderId", "ServiceStafferId" },
                onDelete: ReferentialAction.Cascade);
        }
    }
}
