using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using PatientsRecordsManager.ABAC.Extensions;
using PatientsRecordsManager.ABAC.Extensions.EF;
using PatientsRecordsManager.Data.Dtos.Surgeries;
using PatientsRecordsManager.Models;
using PatientsRecordsManager.Resources;

namespace PatientsRecordsManager.Data.Repositories
{
    public class SurgeryRepository : ISurgeryRespository
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        private readonly Messages _messages;

        public SurgeryRepository(DataContext context, IMapper mapper, Messages messages)
        {
            _mapper = mapper;
            _context = context;
            _messages = messages;
        }

        public SurgeryForReturnDto AddSurgery(SurgeryForCreateDto surgeryForCreate)
        {
            var surgery = _mapper.Map<Surgery>(surgeryForCreate);
            _context.Surgeries.Add(surgery);

            if (_context.SaveChanges() <= 0)
            {
                throw new Exception(_messages.GetString("saveFailed"));
            }
            return _mapper.Map<SurgeryForReturnDto>(surgery);
        }

        public IEnumerable<SurgeryForReturnDto> GetSurgeries()
        {
            return from s in _context.Surgeries.ToList() select _mapper.Map<SurgeryForReturnDto>(s);
        }

        public SurgeryForReturnDto GetSurgeryDetails(string surgeryCode)
        {
            var surgery = _context.Surgeries
                .Include(s => s.Price)
                .Include(s => s.Packages)
                .Include(s => s.Purchases)
                .Include(s => s.Surgeries)
                .FirstOrDefault(s => s.SurgeryCode.ToUpper() == (surgeryCode.ToUpper()));

            if (surgery == null)
            {
                throw new Exception(_messages.GetString("surgeryCodeNotFound", surgeryCode));
            }
            return _mapper.Map<SurgeryForReturnDto>(surgery);
        }

        public SurgeryForReturnDto ModifySurgery(string surgeryCode, SurgeryForUpdateDto surgeryForUpdate)
        {
            var retrievedSurgery = GetSurgery(surgeryCode);
            var surgery = _mapper.Map<SurgeryForUpdateDto, Surgery>(surgeryForUpdate, retrievedSurgery);

            if (_context.SaveChanges() <= 0)
            {
                throw new Exception(_messages.GetString("saveFailed"));
            }
            return _mapper.Map<SurgeryForReturnDto>(surgery);
        }

        public SurgeryForReturnDto RemoveSurgery(string surgeryCode)
        {
            var surgery = GetSurgery(surgeryCode);
            _context.Surgeries.Remove(surgery);

            if (_context.SaveChanges() <= 0)
            {
                throw new Exception(_messages.GetString("saveFailed"));
            }
            return _mapper.Map<SurgeryForReturnDto>(surgery);
        }

        private Surgery GetSurgery(string surgeryCode)
        {
            return _context.Surgeries
                .FirstOrDefault(s => s.SurgeryCode.ToUpper() == (surgeryCode.ToUpper()))
                ?? throw new Exception(_messages.GetString("surgeryCodeNotFound", surgeryCode));
        }
    }
}