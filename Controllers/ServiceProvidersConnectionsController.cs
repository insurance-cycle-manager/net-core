using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PatientsRecordsManager.Constants;
using PatientsRecordsManager.Constants.Enums;
using PatientsRecordsManager.Data.Dtos.ServiceProviders.Connections;
using PatientsRecordsManager.Data.Repositories;
using PatientsRecordsManager.Extensions;
using PatientsRecordsManager.Resources;

namespace PatientsRecordsManager.Controllers
{
    [ApiController]
    [Route(APIs.SPsConnectionsBaseUrl)]
    [Authorize]
    public class ServiceProvidersConnectionsController : PrmController
    {
        private readonly ISpConnectionsRepository _connections;
        private readonly Messages _messages;

        public ServiceProvidersConnectionsController(ISpConnectionsRepository connections, Messages messages)
        {
            _connections = connections;
            _messages = messages;
        }

        [HttpGet(APIs.ConnectionsFetch)]
        [Authorize(Roles = RolesNames.GetProvidersCommaDelimitedWithTpaWithAdmin)]
        public IActionResult GetConnections([FromQuery] SpConnectionStatus status,
                                            [FromQuery] string sp,
                                            [FromQuery] string tpa)
        {
            return HttpContext.Success(
                _connections.GetConnections(status, sp, tpa),
                _messages.GetString("SPsConnectionsFetchDone"));
        }

        [HttpGet(APIs.SpMeConnectionsFetch)]
        [Authorize(Roles = RolesNames.GetProvidersCommaDelimited)]
        public IActionResult GetConnections([FromRoute] ServiceProviderType type)
        {
            return HttpContext.Success(
                _connections.GetConnections(HttpContext.GetUserId(), type),
                _messages.GetString("SPsConnectionsFetchDone"));
        }

        [HttpGet(APIs.SpConnectionsFetch)]
        [Authorize(Roles = RolesNames.GetStaffersCommaDelimited)]
        public IActionResult GetConnections([FromRoute] ServiceProviderType type, [FromRoute] string spCode)
        {
            return HttpContext.Success(
                _connections.GetConnections(spCode, type),
                _messages.GetString("SPsConnectionsFetchDone"));
        }


        [HttpGet(APIs.TpaConnectionsFetch)]
        [Authorize(Roles = RolesNames.TPA)]
        public IActionResult GetConnections()
        {
            return HttpContext.Success(
                _connections.GetConnections(HttpContext.GetUserId()),
                _messages.GetString("SPsConnectionsFetchDone"));
        }

        [HttpPost(APIs.SpConnectionsInvite)]
        public IActionResult InviteTpaToConnection(SpConnectionForAddDto connection)
        {
            return HttpContext.Success(
                _connections.AddConnectionBySp(HttpContext.GetUserId(), connection),
                _messages.GetString("SPsConnectionsInviteDone"));
        }

        [HttpPost(APIs.TpaConnectionsInvite)]
        public IActionResult InviteSpToConnection(SpConnectionForAddDto connection)
        {
            return HttpContext.Success(
                _connections.AddConnectionByTpa(HttpContext.GetUserId(), connection),
                _messages.GetString("SPsConnectionsInviteDone"));
        }

        [HttpPut(APIs.SpConnectionsModify)]
        public IActionResult ModifyConnectionBySp(string connectionCode, SpConnectionForModifyDto connection)
        {
            return HttpContext.Success(
                _connections.ModifyConnectionBySp(HttpContext.GetUserId(), connectionCode, connection),
                _messages.GetString("SPsConnectionsModifyDone"));
        }

        [HttpPut(APIs.TpaConnectionsModify)]
        public IActionResult ModifyConnectionByTpa(string connectionCode, SpConnectionForModifyDto connection)
        {
            return HttpContext.Success(
                _connections.ModifyConnectionByTpa(HttpContext.GetUserId(), connectionCode, connection),
                _messages.GetString("SPsConnectionsModifyDone"));
        }

        [HttpPut(APIs.SpConnectionsStatus)]
        public IActionResult ManageConnectionStatusBySp(string connectionCode, SpConnectionStatusDto status)
        {
            return HttpContext.Success(
                _connections.ManageConnectionStatusBySp(HttpContext.GetUserId(), connectionCode, status),
                _messages.GetString("SPsConnectionsStatusDone"));
        }

        [HttpPut(APIs.TpaConnectionsStatus)]
        public IActionResult ManageConnectionStatusByTpa(string connectionCode, SpConnectionStatusDto status)
        {
            return HttpContext.Success(
                _connections.ManageConnectionStatusByTpa(HttpContext.GetUserId(), connectionCode, status),
                _messages.GetString("SPsConnectionsStatusDone"));
        }
    }
}