using System;
using System.ComponentModel.DataAnnotations;

namespace PatientsRecordsManager.Data.Dtos.Prescribed.LabTests
{
    public class PrescribedLabTestForCreateDto
    {
        [Required]
        public string LabTestCode { get; set; }
        [Required]
        public DateTime? ScheduledDate { get; set; }
        [Required]
        public string Note { get; set; }
    }
}