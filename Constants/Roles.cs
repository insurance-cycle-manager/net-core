namespace PatientsRecordsManager.Constants
{
    public static class RolesNames
    {
        public const string Administrator = "Administrator";
        public const string User = "User";

        public const string InsuranceCompany = "InsuranceCompany";
        public const string TPA = "TPA";

        public const string Doctor = "Doctor";
        public const string Nurse = "Nurse";
        public const string Pharmacist = "Pharmacist";
        public const string Radiographer = "Radiographer";
        public const string LabSpecialist = "LabSpecialist";
        public const string Receptionist = "Receptionist";

        public const string Hospital = "Hospital";
        public const string Pharmacy = "Pharmacy";
        public const string Clinic = "Clinic";
        public const string Laboratory = "Laboratory";
        public const string RadiographOffice = "RadiographOffice";

        public const string Patient = "Patient";

        public static bool IsServiceProvider(string roleName)
        {
            return roleName == Hospital ||
                roleName == Pharmacy ||
                roleName == Clinic ||
                roleName == Laboratory ||
                roleName == RadiographOffice;
        }

        public static bool IsServiceStaffer(string roleName)
        {
            return roleName == Doctor ||
                roleName == Nurse ||
                roleName == Pharmacist ||
                roleName == Radiographer ||
                roleName == LabSpecialist ||
                roleName == Receptionist;
        }

        public static bool IsPatient(string roleName)
        {
            return roleName == Patient;
        }

        public static bool IsTpa(string roleName)
        {
            return roleName == TPA;
        }

        public static bool IsInsuranceCompany(string roleName)
        {
            return roleName == InsuranceCompany;
        }

        public const string GetProvidersCommaDelimited = "Hospital,Pharmacy,Clinic,Laboaratory,RadiographOffice";
        public const string GetProvidersCommaDelimitedWithAdmin = "Administrator,Hospital,Pharmacy,Clinic,Laboaratory,RadiographOffice";
        public const string GetProvidersCommaDelimitedWithTpaWithAdmin = "Administrator,TPA,Hospital,Pharmacy,Clinic,Laboaratory,RadiographOffice";

        public const string GetStaffersCommaDelimited = "Doctor,Nurse,Pharmacist,Radiographer,LabSpecialist,Receptionist";
        public const string GetStaffersCommaDelimitedWithAdmin = "Administrator,Doctor,Nurse,Pharmacist,Radiographer,LabSpecialist,Receptionist";
    }
}