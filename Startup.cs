using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using PatientsRecordsManager.ABAC.Extensions;
using PatientsRecordsManager.Data;
using PatientsRecordsManager.Data.Repositories;
using PatientsRecordsManager.Data.Seeds;
using PatientsRecordsManager.Extensions;
using PatientsRecordsManager.Resources;
using PatientsRecordsManager.Utils;

namespace PatientsRecordsManager
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<DataContext>(e =>
            {
                e.AddInterceptors(new DbInterceptor());
                e.UseNpgsql(Configuration.GetConnectionString("DefaultConnection"));
            });

            services.AddAbac();
            services.AddCustomizedIdentity(Configuration);

            services
              .AddControllers(opt =>
              {
                  var policy = new AuthorizationPolicyBuilder()
                    .RequireAuthenticatedUser()
                    .Build();

                  opt.Filters.Add(new AuthorizeFilter(policy));
              })
              .AddNewtonsoftJson(opt =>
              {
                  opt.SerializerSettings.ReferenceLoopHandling =
                      Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                  opt.SerializerSettings.NullValueHandling =
                      Newtonsoft.Json.NullValueHandling.Ignore;
              });
            services.AddCors();
            services.AddAutoMapper(typeof(Startup));

            services.Configure<UploaderSettings>(Configuration.GetSection("FileUploaderSettings"));

            services.AddTransient<Seeder>();
            // 
            // services.AddScoped<IAuthRepository, AuthRepository>();
            services.AddScoped<IPatientRepository, PatientRepository>();
            services.AddScoped<IAddressRepository, AddressRepository>();
            services.AddScoped<IServiceProviderRepository, ServiceProviderRepository>();
            services.AddScoped<IServiceStafferRepository, ServiceStafferRepository>();
            services.AddScoped<IServiceProviderStaffRepository, ServiceProviderStaffRepository>();
            services.AddScoped<IVisitRepository, VisitRepository>();
            services.AddScoped<IVisitPrescriptionRepository, VisitPrescriptionRepository>();
            services.AddScoped<IVisitSurgeryRepository, VisitSurgeryRepository>();
            services.AddScoped<IVisitLabTestRepository, VisitLabTestRepository>();
            services.AddScoped<IVisitRadiographRepository, VisitRadiographRepository>();
            services.AddScoped<ISurgeryRespository, SurgeryRepository>();
            services.AddScoped<IDrugRepository, DrugRepository>();
            services.AddScoped<ILabTestRepository, LabTestRepository>();
            services.AddScoped<IRadiographRepository, RadiographRepository>();
            services.AddScoped<IPriceRepository, PriceRepository>();
            services.AddScoped<ISpConnectionsRepository, SpConnectionsRepository>();
            services.AddScoped<ITpaRepository, TpaRepository>();
            services.AddScoped<IIcRepository, IcRepository>();
            // 
            services.AddTransient<DocumentsStorage>();
            services.AddTransient<Messages>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public static void Configure(IApplicationBuilder app, IHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler(builder =>
                {
                    builder.Run(async context =>
                    {
                        await context.InternalServerErrorAsync().ConfigureAwait(false);
                    });
                });
            }

            // seeder.SeedRoles();
            // seeder.SeedUsers();
            // seeder.SeedUserRoles();
            // seeder.SeedDrugs();
            // seeder.SeedRadiographs();
            // seeder.SeedDefaulValues();

            app.UseCors(x => x.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseAbacMiddleware();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
