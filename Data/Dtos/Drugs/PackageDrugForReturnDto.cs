namespace PatientsRecordsManager.Data.Dtos.Drugs
{
    public class PackageDrugForReturnDto
    {
        public string DrugCode { get; set; }
    }
}