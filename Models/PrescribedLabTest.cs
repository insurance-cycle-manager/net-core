using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PatientsRecordsManager.Models
{
    public class PrescribedLabTest
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string PrescribedLabTestCode { get; set; }

        [Required]
        public int VisitId { get; set; }
        public Visit Visit { get; set; }

        [Required]
        public int LabTestId { get; set; }
        public LabTest LabTest { get; set; }

        [Required]
        public DateTime ScheduledDate { get; set; }

        public string Note { get; set; }

        public List<LabTestPurchase> Purchases { get; set; }
    }
}