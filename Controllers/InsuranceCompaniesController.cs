using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PatientsRecordsManager.Constants;
using PatientsRecordsManager.Data.Dtos.InsuranceCompanies;
using PatientsRecordsManager.Data.Repositories;
using PatientsRecordsManager.Extensions;
using PatientsRecordsManager.Resources;

namespace PatientsRecordsManager.Controllers
{
    [ApiController]
    [Route(APIs.InsuranceCompaniesBaseUrl)]
    [Authorize]
    public class InsuranceCompaniesController : PrmController
    {
        private readonly IIcRepository _ics;
        private readonly IMapper _mapper;
        private readonly Messages _messages;

        public InsuranceCompaniesController(IIcRepository ics, IMapper mapper, Messages messages)
        {
            _messages = messages;
            _mapper = mapper;
            _ics = ics;
        }

        [HttpGet(APIs.GetInsuranceCompanies)]
        public IActionResult GetInsuranceCompanies()
        {
            return HttpContext.Success(
                _ics.GetInsuranceCompanies(),
                _messages.GetString("InsuranceCompaniesFetched"));
        }

        [HttpGet(APIs.GetInsuranceCompanyMe, Order = 1)]
        public IActionResult GetInsuranceCompanyMe()
        {
            return HttpContext.Success(
                _ics.GetInsuranceCompanyDetails(HttpContext.GetUserId()),
                _messages.GetString("InsuranceCompanyFetched"));
        }

        [HttpGet(APIs.GetInsuranceCompany, Order = 2)]
        [Authorize(Roles = RolesNames.Administrator)]
        public IActionResult GetInsuranceCompany(string icCode)
        {
            return HttpContext.Success(
                _ics.GetInsuranceCompanyDetails(icCode),
                _messages.GetString("InsuranceCompanyFetched"));
        }

        [HttpPut(APIs.ModifyInsuranceCompanyMe, Order = 1)]
        public IActionResult ModifyInsuranceCompanyMe(InsuranceCompanyForUpdateDto data)
        {
            return HttpContext.Success(
                _ics.ModifyInsuranceCompany(HttpContext.GetUserId(), data),
                _messages.GetString("InsuranceCompanyUpdated"));
        }

        [HttpPut(APIs.ModifyInsuranceCompany, Order = 2)]
        [Authorize(Roles = RolesNames.Administrator)]
        public IActionResult ModifyInsuranceCompany(string icCode, InsuranceCompanyForUpdateDto data)
        {
            return HttpContext.Success(
                _ics.ModifyInsuranceCompany(icCode, data),
                _messages.GetString("InsuranceCompanyUpdated"));
        }
    }
}