using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PatientsRecordsManager.Constants;
using PatientsRecordsManager.Data;
using PatientsRecordsManager.Extensions;
using PatientsRecordsManager.Resources;
using static PatientsRecordsManager.Data.DocumentsStorage;

namespace PatientsRecordsManager.Controllers
{
    [ApiController]
    [Authorize]
    public class DocumentsController : PrmController
    {
        private readonly DocumentsStorage _storage;
        private readonly Messages _messages;

        public DocumentsController(DocumentsStorage docsStorage, Messages messages)
        {
            _messages = messages;
            _storage = docsStorage;
        }

        [HttpPost(APIs.Upload)]
        public async Task<IActionResult> Upload(DocumentType docType, [FromForm] IFormFile file)
        {
            if (file == null)
            {
                throw new ArgumentNullException(_messages.GetString("AttachmentNull"));
            }

            string pathToMedia = await _storage.AddDocumentToStorage(docType, file).ConfigureAwait(false);
            return HttpContext.Success(pathToMedia, _messages.GetString("AttachmentUploaded"));
        }

        [HttpGet(APIs.Download)]
        public IActionResult Download(DocumentType docType, string docPath)
        {
            try
            {
                if (docPath == null || string.IsNullOrWhiteSpace(docPath))
                {
                    throw new ArgumentNullException(_messages.GetString("AttachmentNull"));
                }

                var result = _storage.GetDocumentFromStorage(docType, docPath);
                // return File(stream, string.Format("{0}/*", Enum.GetName(docType.GetType(), docType)));
                return File(result.Item2, result.Item1);
            }
            catch (ArgumentNullException e)
            {
                return HttpContext.Error(new
                {
                    Type = Enum.GetName(docType.GetType(), docType),
                    Path = docPath
                }, e.Message);
            }
        }
    }
}