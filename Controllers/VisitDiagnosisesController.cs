using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PatientsRecordsManager.ABAC.Extensions;
using PatientsRecordsManager.ABAC.Extensions.EF;
using PatientsRecordsManager.Constants;
using PatientsRecordsManager.Data;
using PatientsRecordsManager.Data.Dtos.Diagnosis;
using PatientsRecordsManager.Exceptions;
using PatientsRecordsManager.Extensions;
using PatientsRecordsManager.Models;

namespace PatientsRecordsManager.Controllers
{
    [ApiController]
    [Route(APIs.VisitDiagnosisBaseUrl)]
    [Authorize]
    public class VisitDiagnosisesController : PrmController
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        private readonly Resources.Messages _messages;

        public VisitDiagnosisesController(DataContext context, IMapper mapper, Resources.Messages messages)
        {
            _context = context;
            _mapper = mapper;
            _messages = messages;
        }

        [HttpPost(APIs.AddDiagnosis)]
        public async Task<IActionResult> AddDiagnosis(string visitCode, DiagnosisForCreateDto diagnosisForCreate)
        {
            var visit = _context.Visits.FirstOrDefault(v => v.VisitCode.ToUpper() == (visitCode.ToUpper()));
            if (visit == null)
            {
                throw new KeyNotFoundException(_messages.GetString("visitNotFound", visitCode));
            }

            if (visit.DiagnosisId != null)
            {
                throw new Exception(_messages.GetString("visitDiagnosisExists", visitCode));
            }

            var diagnosis = _mapper.Map<VisitDiagnosis>(diagnosisForCreate);
            _context.Diagnoses.Add(diagnosis);

            visit.Diagnosis = diagnosis;

            if (await _context.SaveChangesAsync().ConfigureAwait(false) <= 0)
            {
                throw new SaveChangesFailedException(_messages.GetString("diagnosisAddFailed"));
            }

            return HttpContext.Success(_mapper.Map<DiagnosisForReturnDto>(diagnosis), _messages.GetString("diagnosisAddDone"));
        }

        [HttpPut(APIs.ModifyDiagnosis)]
        public IActionResult ModifyDiagnosis(string visitCode, string diagnosisCode, DiagnosisForUpdateDto diagnosisForUpdate)
        {
            var visit = _context.Visits
                .Include(v => v.Diagnosis)
                .FirstOrDefault(v => v.VisitCode.ToUpper() == (visitCode.ToUpper()));
            if (visit == null)
            {
                throw new KeyNotFoundException(_messages.GetString("visitNotFound", visitCode));
            }

            if (!visit.Diagnosis.DiagnosisCode.Equals(diagnosisCode, StringComparison.CurrentCultureIgnoreCase))
            {
                throw new KeyNotFoundException(_messages.GetString("diagnosisNotFound", diagnosisCode));
            }

            visit.Diagnosis = _mapper.Map<DiagnosisForUpdateDto, VisitDiagnosis>(diagnosisForUpdate, visit.Diagnosis);

            if (_context.SaveChanges() <= 0)
            {
                throw new SaveChangesFailedException(_messages.GetString("diagnosisModifyFailed"));
            }
            return HttpContext.Success(diagnosisForUpdate, _messages.GetString("diagnosisModifyDone"));
        }

        [HttpDelete(APIs.DeleteDiagnosis)]
        public IActionResult DeleteDiagnosis(string visitCode, string diagnosisCode)
        {
            var visit = _context.Visits
                .Include(v => v.Diagnosis)
                .FirstOrDefault(v => v.VisitCode.ToUpper() == (visitCode.ToUpper()));
            if (visit == null)
            {
                throw new KeyNotFoundException(_messages.GetString("visitNotFound", visitCode));
            }

            if (!visit.Diagnosis.DiagnosisCode.Equals(diagnosisCode, StringComparison.CurrentCultureIgnoreCase))
            {
                throw new KeyNotFoundException(_messages.GetString("diagnosisNotFound", diagnosisCode));
            }

            _context.Diagnoses.Remove(visit.Diagnosis);

            if (_context.SaveChanges() <= 0)
            {
                throw new SaveChangesFailedException(_messages.GetString("diagnosisDeleteFailed"));
            }
            return HttpContext.Success(diagnosisCode, _messages.GetString("diagnosisDeleteDone"));
        }
    }
}