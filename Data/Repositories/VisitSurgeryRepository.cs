using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using PatientsRecordsManager.Resources;
using PatientsRecordsManager.Data.Dtos.Prescribed.Surgeries;
using PatientsRecordsManager.Exceptions;
using PatientsRecordsManager.Models;
using PatientsRecordsManager.Utils;
using PatientsRecordsManager.ABAC.Extensions.EF;
using PatientsRecordsManager.ABAC.Extensions;

namespace PatientsRecordsManager.Data.Repositories
{
    public class VisitSurgeryRepository : IVisitSurgeryRepository
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        private readonly Messages _messages;

        public VisitSurgeryRepository(DataContext context, IMapper mapper, Messages messages)
        {
            _mapper = mapper;
            _context = context;
            _messages = messages;
        }

        public PrescribedSurgeryForReturnDto AddSurgery(string visitCode, PrescribedSurgeryForCreateDto surgeryForCreate)
        {
            Assert.NotNull(surgeryForCreate);

            var visit = _context.Visits
                .Include(v => v.Surgeries)
                .FirstOrDefault(v => v.VisitCode.ToUpper() == (visitCode.ToUpper()));

            var surgery = _context
                .Surgeries
                .FirstOrDefault(s => s.SurgeryCode.ToUpper() == (surgeryForCreate.SurgeryCode.ToUpper()));

            if (visit == null || surgery == null)
            {
                throw new ArgumentBadException(_messages.GetString("visitNotFound", visitCode));
            }

            var surgeryToAdd = _mapper.Map<PrescribedSurgery>(surgeryForCreate);
            surgeryToAdd.Visit = visit;
            surgeryToAdd.Surgery = surgery;

            visit.Surgeries.Add(surgeryToAdd);

            if (_context.SaveChanges() <= 0)
            {
                throw new SaveChangesFailedException();
            }
            return _mapper.Map<PrescribedSurgeryForReturnDto>(surgeryToAdd);
        }

        public IEnumerable<PrescribedSurgeryForReturnDto> GetSurgeries(string visitCode)
        {
            var visit = _context.Visits
                .Include(v => v.Surgeries)
                .FirstOrDefault(v => v.VisitCode.ToUpper() == (visitCode.ToUpper()));

            return _mapper.Map<List<PrescribedSurgeryForReturnDto>>(visit.Surgeries.ToList());
        }

        public PrescribedSurgeryForReturnDto GetSurgeryDetails(string visitCode, string surgeryCode)
        {
            return _mapper.Map<PrescribedSurgeryForReturnDto>(GetSurgery(visitCode, surgeryCode));
        }

        public PrescribedSurgeryForReturnDto ModifySurgery(string visitCode, string surgeryCode, PrescribedSurgeryForUpdateDto surgeryForUpdate)
        {
            var retrievedSurgery = GetSurgery(visitCode, surgeryCode);
            var surgery = _mapper.Map<PrescribedSurgeryForUpdateDto, PrescribedSurgery>(surgeryForUpdate, retrievedSurgery);

            if (_context.SaveChanges() <= 0)
            {
                throw new SaveChangesFailedException();
            }
            return _mapper.Map<PrescribedSurgeryForReturnDto>(surgery);
        }

        public PrescribedSurgeryForReturnDto RemoveSurgery(string visitCode, string surgeryCode)
        {
            var surgery = GetSurgery(visitCode, surgeryCode);
            _context.PrescribedSurgeries.Remove(surgery);

            if (_context.SaveChanges() <= 0)
            {
                throw new SaveChangesFailedException();
            }
            return _mapper.Map<PrescribedSurgeryForReturnDto>(surgery);
        }

        private PrescribedSurgery GetSurgery(string visitCode, string surgeryCode)
        {
            return _context.PrescribedSurgeries
                .Include(s => s.Visit)
                .Where(s => s.Visit.VisitCode.ToUpper() == (visitCode.ToUpper()) && s.PrescribedSurgeryCode.ToUpper() == (surgeryCode.ToUpper()))
                .FirstOrDefault()
                ?? throw new ArgumentBadException(_messages.GetString("visitSurgeryNotFound", visitCode, surgeryCode));
        }
    }
}