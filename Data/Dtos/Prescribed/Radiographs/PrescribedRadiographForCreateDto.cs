using System;
using System.ComponentModel.DataAnnotations;

namespace PatientsRecordsManager.Data.Dtos.Prescribed.Radiographs
{
    public class PrescribedRadiographForCreateDto
    {
        [Required]
        public string RadiographCode { get; set; }
        [Required]
        public DateTime? ScheduledDate { get; set; }
        [Required]
        public string Note { get; set; }
    }
}