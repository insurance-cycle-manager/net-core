using System.ComponentModel.DataAnnotations;
using PatientsRecordsManager.Data.Dtos.Prices;

namespace PatientsRecordsManager.Data.Dtos.LabTests
{
    public class LabTestForCreateDto
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public PriceForAddDto Price { get; set; }
    }
}