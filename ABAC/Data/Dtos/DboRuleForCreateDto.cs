using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace PatientsRecordsManager.ABAC.Data.Dtos
{
    public class DboRuleForCreateDto : RuleForCreateDto
    {
        [Required]
        public new object V4 { get; set; }
    }
}