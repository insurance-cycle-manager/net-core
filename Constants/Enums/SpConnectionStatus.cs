namespace PatientsRecordsManager.Constants.Enums
{
    public enum SpConnectionStatus
    {
        // Pending means that TPA requested the connection and it's waiting for SP to respond
        Pending1,
        Pending2,
        // Approved means that SP responded with Yup
        Approved,
        // Rejected means that SP responded with Nop
        Rejected,
        // Means that TPA suspended the connection
        TpaDeactivated,
        // Means that TPA suspended the connection
        SpDeactivated,
    }
}