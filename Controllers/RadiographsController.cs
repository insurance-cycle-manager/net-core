using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PatientsRecordsManager.Constants;
using PatientsRecordsManager.Data.Dtos.Radiographs;
using PatientsRecordsManager.Data.Repositories;
using PatientsRecordsManager.Extensions;
using PatientsRecordsManager.Resources;

namespace PatientsRecordsManager.Controllers
{
    [ApiController]
    [Route(APIs.RadiographsBaseUrl)]
    [Authorize]
    public class RadiographsController : PrmController
    {
        private readonly IMapper _mapper;
        private readonly IRadiographRepository _repository;
        private readonly Messages _messages;

        public RadiographsController(IMapper mapper, IRadiographRepository repository, Messages messages)
        {
            _mapper = mapper;
            _repository = repository;
            _messages = messages;
        }

        [HttpGet(APIs.GetRadiographs)]
        public IActionResult GetRadiographs([FromQuery] string name)
        {
            return HttpContext.Success(_repository.GetRadiographs(name), _messages.GetString("RadiographsGetDone"));
        }

        [HttpGet(APIs.GetRadiographDetails)]
        public IActionResult GetRadiographDetails(string radiographCode)
        {
            return HttpContext.Success(_repository.GetRadiographDetails(radiographCode), _messages.GetString("RadiographDetailsGetDone", radiographCode));
        }

        [HttpPost(APIs.PostRadiograph)]
        [Authorize(Roles = RolesNames.Administrator)]
        public IActionResult AddRadiograph(RadiographForCreateDto radiograph)
        {
            return HttpContext.Success(_repository.AddRadiograph(radiograph), _messages.GetString("RadiographAddDone"));
        }

        [HttpDelete(APIs.DeleteRadiograph)]
        public IActionResult DeleteRadiograph(string radiographCode)
        {
            bool done = _repository.DeleteRadiograph(radiographCode);
            if (!done)
            {
                return HttpContext.Error(radiographCode, _messages.GetString("RadiographDeleteFailed", radiographCode));
            }
            return HttpContext.Success(radiographCode, _messages.GetString("RadiographDeleteDone", radiographCode));
        }

        [HttpPut(APIs.ModifyRadiograph)]
        [Authorize(Roles = RolesNames.Administrator)]
        public IActionResult ModifyRadiograph(string radiographCode, RadiographForUpdateDto radiograph)
        {
            return HttpContext.Success(_repository.ModifyRadiograph(radiographCode, radiograph), _messages.GetString("RadiographModifyDone", radiographCode));
        }
    }
}