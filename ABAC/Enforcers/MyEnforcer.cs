using System;
using System.Linq;
using Casbin.NET.Adapter.EFCore;
using Microsoft.Extensions.Logging;
using NetCasbin;
using Newtonsoft.Json.Linq;
using PatientsRecordsManager.ABAC.Data;
using PatientsRecordsManager.ABAC.Data.Models;

namespace PatientsRecordsManager.ABAC.Enforcers
{
    public abstract class MyEnforcer : Enforcer
    {
        protected const string ComposeRequestDataValuesLength = "Length of values passed not allowed";


        private readonly AbacDbContext context;
        private readonly ILogger logger;

        public MyEnforcer(string modelPath, AbacDbContext context, ILogger<MyEnforcer> logger) : base(modelPath, new CasbinDbAdapter<int>(context))
        {
            this.context = context;
            this.logger = logger;
        }

        protected abstract DataAccessRequestDetails ComposeRequestData(params object[] rValues);

        protected Request GetOrCreateRequest(string requestId)
        {
            var r = this.context.Requests.FirstOrDefault(x => x.Value == requestId);
            if (r == null)
            {
                r = new Request
                {
                    Value = requestId,
                    DateOfRequest = DateTime.Now,
                };
                this.context.Requests.Add(r);
                this.context.SaveChanges();
            }
            return r;
        }

        protected DataAccessRequest Find(DataAccessRequestDetails data)
        {
            return this.context
                .DataAccessRequests
                .Local
                .FirstOrDefault(r => r.CompareTo(data) == 0);
        }

        public bool Evaluate(string requestId, params object[] rValues)
        {
            var data = ComposeRequestData(rValues);
            var request = Find(data);

            if (request == null)
            {
                request = new DataAccessRequest
                {
                    // 
                    Object = data.Object,
                    Action = data.Action,
                    Roles = data.Roles,
                    Attributes = data.Attributes,
                    // 
                    Result = base.Enforce(rValues),
                    DateOfAccess = DateTime.Now,
                    Request = GetOrCreateRequest(requestId)
                };
                this.context.DataAccessRequests.Add(request);
                this.context.SaveChanges();
            }
            this.logger.LogWarning(request.ToString());
            return request.Result;
        }
    }
}