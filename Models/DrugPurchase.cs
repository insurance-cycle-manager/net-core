using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PatientsRecordsManager.Models
{
    public class DrugPurchase
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public DateTime DateOfPurchasing { get; set; }

        public int DrugId { get; set; }
        public Drug Drug { get; set; }

        public int PrescriptionPurchaseId { get; set; }
        public PrescriptionPurchase PrescriptionPurchase { get; set; }


        [ForeignKey("Prices")]
        public int PriceId { get; set; }
        public Price Price { get; set; }
    }
}