using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PatientsRecordsManager.Models
{
    public class Prescription
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string PrescriptionCode { get; set; }

        [Required]
        public int VisitId { get; set; }
        public Visit Visit { get; set; }

        public string Note { get; set; }
        
        public List<PrescriptionDrug> Drugs { get; set; }
        public List<PrescriptionPurchase> PrescriptionPurchases { get; set; }
    }
}