using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PatientsRecordsManager.Models
{
    public class Address
    {
        [Key]
        public int Id { get; set; }
        public double? Longitude { get; set; }
        public double? Latitude { get; set; }

        [Required]
        public string Title { get; set; }
        [Required]
        public string NormalizedTitle { get; set; }
        [Required]
        public string AddressCode { get; set; }

        public IEnumerable<UserAddress> UserAddresses { get; set; }
    }
}