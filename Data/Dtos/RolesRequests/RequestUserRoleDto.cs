using System.ComponentModel.DataAnnotations;

namespace PatientsRecordsManager.Data.Dtos.RolesRequests
{
    public class RequestUserRoleDto
    {
        [Required]
        public string RoleName { get; set; }
        [Required]
        [EmailAddress]
        public string PreferredEmail { get; set; }
        [Required]
        [Phone]
        public string PreferredPhoneNumber { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string PreferredAddressCode { get; set; }
    }
}