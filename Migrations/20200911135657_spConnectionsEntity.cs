﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PatientsRecordsManager.Migrations
{
    public partial class spConnectionsEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ConnectionStatus",
                table: "ServiceProviderConnections");

            migrationBuilder.AddColumn<int>(
                name: "Status",
                table: "ServiceProviderConnections",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Status",
                table: "ServiceProviderConnections");

            migrationBuilder.AddColumn<string>(
                name: "ConnectionStatus",
                table: "ServiceProviderConnections",
                type: "text",
                nullable: false,
                defaultValue: "");
        }
    }
}
