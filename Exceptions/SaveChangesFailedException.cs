using System;

namespace PatientsRecordsManager.Exceptions
{
    public class SaveChangesFailedException : Exception
    {
        public SaveChangesFailedException()
        {
        }

        public SaveChangesFailedException(string message) : base(message)
        {
            
        }

        public SaveChangesFailedException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}