﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace PatientsRecordsManager.Migrations.AbacDb
{
    public partial class abacFirstMigrate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CasbinRule",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    PType = table.Column<string>(nullable: true),
                    V0 = table.Column<string>(nullable: true),
                    V1 = table.Column<string>(nullable: true),
                    V2 = table.Column<string>(nullable: true),
                    V3 = table.Column<string>(nullable: true),
                    V4 = table.Column<string>(nullable: true),
                    V5 = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CasbinRule", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Requests",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Value = table.Column<string>(nullable: false),
                    DateOfRequest = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Requests", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RuleAttributes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Attributes = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RuleAttributes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DataAccessRequests",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Roles = table.Column<string[]>(type: "text[]", nullable: false),
                    Object = table.Column<string>(nullable: false),
                    Action = table.Column<string>(nullable: false),
                    Attributes = table.Column<string[]>(type: "text[]", nullable: true),
                    DateOfAccess = table.Column<DateTime>(nullable: false),
                    Result = table.Column<bool>(nullable: false),
                    RequestId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DataAccessRequests", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DataAccessRequests_Requests_RequestId",
                        column: x => x.RequestId,
                        principalTable: "Requests",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DataAccessRequests_RequestId",
                table: "DataAccessRequests",
                column: "RequestId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CasbinRule");

            migrationBuilder.DropTable(
                name: "DataAccessRequests");

            migrationBuilder.DropTable(
                name: "RuleAttributes");

            migrationBuilder.DropTable(
                name: "Requests");
        }
    }
}
