using System;
using PatientsRecordsManager.Data.Dtos.Prices;

namespace PatientsRecordsManager.Data.Dtos.Prescribed.LabTests
{
    public class LabTestPurchaseForReturnDto
    {
        public PriceForReturnDto Price { get; set; }
        public PrescribedLabTestForReturnDto PrescribedLabTest { get; set; }
        public DateTime DateOfPurchasing { get; set; }

    }
}