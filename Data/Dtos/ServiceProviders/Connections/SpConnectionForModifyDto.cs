using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PatientsRecordsManager.Data.Dtos.ServiceProviders.Connections
{
    public class SpConnectionForModifyDto
    {
        public DateTime ModifiedAt { get; set; }

        [Required, MinLength(1)]
        public IEnumerable<string> InsuranceCompanies { get; set; }

        public SpConnectionForModifyDto()
        {
            ModifiedAt = DateTime.Now;
        }
    }
}