using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using PatientsRecordsManager.Constants.Enums;

namespace PatientsRecordsManager.Models
{
    public class ServiceProviderConnection
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string ConnectionCode { get; set; }
        [Required]
        [EnumDataType(typeof(SpConnectionStatus))]
        public SpConnectionStatus Status { get; set; }

        [Required]
        public int TpaId { get; set; }
        public ThirdPartyAdministrator Tpa { get; set; }

        [Required]
        public int ServiceProviderId { get; set; }
        public ServiceProvider ServiceProvider { get; set; }

        [Required]
        public DateTime CreatedAt { get; set; }

        public IEnumerable<Visit> Visits { get; set; }
        public IEnumerable<ServiceProviderConnectionInsuranceCompanies> InsuranceCompanies { get; set; }
    }
}