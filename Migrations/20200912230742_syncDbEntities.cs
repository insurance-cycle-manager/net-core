﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PatientsRecordsManager.Migrations
{
    public partial class syncDbEntities : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DrugPurchases_Visits_VisitId",
                table: "DrugPurchases");

            migrationBuilder.DropColumn(
                name: "DrugCode",
                table: "DrugPurchases");

            migrationBuilder.AlterColumn<int>(
                name: "VisitId",
                table: "DrugPurchases",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AddForeignKey(
                name: "FK_DrugPurchases_Visits_VisitId",
                table: "DrugPurchases",
                column: "VisitId",
                principalTable: "Visits",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DrugPurchases_Visits_VisitId",
                table: "DrugPurchases");

            migrationBuilder.AlterColumn<int>(
                name: "VisitId",
                table: "DrugPurchases",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DrugCode",
                table: "DrugPurchases",
                type: "text",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_DrugPurchases_Visits_VisitId",
                table: "DrugPurchases",
                column: "VisitId",
                principalTable: "Visits",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
