using System;
using System.Collections;
using System.Collections.Generic;

namespace PatientsRecordsManager.ABAC.Extensions.Linq
{
    public static class LinqEnumerableExtensions
    {
        public static TSource Aggregate<TSource>(this IEnumerable<TSource> source, Func<TSource, TSource, TSource> func)
        {
            return System.Linq.Enumerable.Aggregate<TSource>(source, func).Visit(Evaluate.Action.READ);
        }
        public static TAccumulate Aggregate<TSource, TAccumulate>(this IEnumerable<TSource> source, TAccumulate seed, Func<TAccumulate, TSource, TAccumulate> func)
        {
            return System.Linq.Enumerable.Aggregate<TSource, TAccumulate>(source, seed, func).Visit(Evaluate.Action.READ);
        }
        public static TResult Aggregate<TSource, TAccumulate, TResult>(this IEnumerable<TSource> source, TAccumulate seed, Func<TAccumulate, TSource, TAccumulate> func, Func<TAccumulate, TResult> resultSelector)
        {
            return System.Linq.Enumerable.Aggregate<TSource, TAccumulate, TResult>(source, seed, func, resultSelector).Visit(Evaluate.Action.READ);
        }

        public static bool All<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
        {
            return System.Linq.Enumerable.All<TSource>(source, predicate).Visit(Evaluate.Action.READ);
        }

        public static bool Any<TSource>(this IEnumerable<TSource> source)
        {
            return System.Linq.Enumerable.Any<TSource>(source).Visit(Evaluate.Action.READ);
        }
        public static bool Any<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
        {
            return System.Linq.Enumerable.Any<TSource>(source, predicate).Visit(Evaluate.Action.READ);
        }

        public static IEnumerable<TSource> Append<TSource>(this IEnumerable<TSource> source, TSource element)
        {
            return System.Linq.Enumerable.Append<TSource>(source, element).Visit(Evaluate.Action.READ);
        }

        public static IEnumerable<TSource> AsEnumerable<TSource>(this IEnumerable<TSource> source)
        {
            return System.Linq.Enumerable.AsEnumerable<TSource>(source).Visit(Evaluate.Action.READ);
        }

        public static double Average<TSource>(this IEnumerable<TSource> source, Func<TSource, int> selector)
        {
            return System.Linq.Enumerable.Average<TSource>(source, selector).Visit(Evaluate.Action.READ);
        }
        public static double Average<TSource>(this IEnumerable<TSource> source, Func<TSource, long> selector)
        {
            return System.Linq.Enumerable.Average<TSource>(source, selector).Visit(Evaluate.Action.READ);
        }
        public static decimal? Average<TSource>(this IEnumerable<TSource> source, Func<TSource, decimal?> selector)
        {
            return System.Linq.Enumerable.Average<TSource>(source, selector).Visit(Evaluate.Action.READ);
        }
        public static float Average<TSource>(this IEnumerable<TSource> source, Func<TSource, float> selector)
        {
            return System.Linq.Enumerable.Average<TSource>(source, selector).Visit(Evaluate.Action.READ);
        }
        public static double? Average<TSource>(this IEnumerable<TSource> source, Func<TSource, int?> selector)
        {
            return System.Linq.Enumerable.Average<TSource>(source, selector).Visit(Evaluate.Action.READ);
        }
        public static float? Average<TSource>(this IEnumerable<TSource> source, Func<TSource, float?> selector)
        {
            return System.Linq.Enumerable.Average<TSource>(source, selector).Visit(Evaluate.Action.READ);
        }
        public static double Average<TSource>(this IEnumerable<TSource> source, Func<TSource, double> selector)
        {
            return System.Linq.Enumerable.Average<TSource>(source, selector).Visit(Evaluate.Action.READ);
        }
        public static double? Average<TSource>(this IEnumerable<TSource> source, Func<TSource, double?> selector)
        {
            return System.Linq.Enumerable.Average<TSource>(source, selector).Visit(Evaluate.Action.READ);
        }
        public static decimal Average<TSource>(this IEnumerable<TSource> source, Func<TSource, decimal> selector)
        {
            return System.Linq.Enumerable.Average<TSource>(source, selector).Visit(Evaluate.Action.READ);
        }
        public static double? Average<TSource>(this IEnumerable<TSource> source, Func<TSource, long?> selector)
        {
            return System.Linq.Enumerable.Average<TSource>(source, selector).Visit(Evaluate.Action.READ);
        }
        public static float? Average(this IEnumerable<float?> source)
        {
            return System.Linq.Enumerable.Average(source);
        }
        public static decimal Average(this IEnumerable<decimal> source)
        {
            return System.Linq.Enumerable.Average(source);
        }
        public static double Average(this IEnumerable<int> source)
        {
            return System.Linq.Enumerable.Average(source);
        }
        public static double Average(this IEnumerable<long> source)
        {
            return System.Linq.Enumerable.Average(source);
        }
        public static double Average(this IEnumerable<double> source)
        {
            return System.Linq.Enumerable.Average(source);
        }
        public static double? Average(this IEnumerable<double?> source)
        {
            return System.Linq.Enumerable.Average(source);
        }
        public static double? Average(this IEnumerable<int?> source)
        {
            return System.Linq.Enumerable.Average(source);
        }
        public static double? Average(this IEnumerable<long?> source)
        {
            return System.Linq.Enumerable.Average(source);
        }
        public static decimal? Average(this IEnumerable<decimal?> source)
        {
            return System.Linq.Enumerable.Average(source);
        }
        public static float Average(this IEnumerable<float> source)
        {
            return System.Linq.Enumerable.Average(source);
        }

        public static IEnumerable<TResult> Cast<TResult>(this IEnumerable source)
        {
            return System.Linq.Enumerable.Cast<TResult>(source.Visit(Evaluate.Action.READ));
        }

        public static IEnumerable<TSource> Concat<TSource>(this IEnumerable<TSource> first, IEnumerable<TSource> second)
        {
            return System.Linq.Enumerable.Concat<TSource>(first.Visit(Evaluate.Action.READ), second);
        }

        public static bool Contains<TSource>(this IEnumerable<TSource> source, TSource value)
        {
            return System.Linq.Enumerable.Contains<TSource>(source.Visit(Evaluate.Action.READ), value);
        }
        public static bool Contains<TSource>(this IEnumerable<TSource> source, TSource value, IEqualityComparer<TSource> comparer)
        {
            return System.Linq.Enumerable.Contains<TSource>(source.Visit(Evaluate.Action.READ), value, comparer);
        }

        public static int Count<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
        {
            return System.Linq.Enumerable.Count<TSource>(source.Visit(Evaluate.Action.READ), predicate);
        }
        public static int Count<TSource>(this IEnumerable<TSource> source)
        {
            return System.Linq.Enumerable.Count<TSource>(source).Visit(Evaluate.Action.READ);
        }

        public static IEnumerable<TSource> DefaultIfEmpty<TSource>(this IEnumerable<TSource> source, TSource defaultValue)
        {
            return System.Linq.Enumerable.DefaultIfEmpty<TSource>(source, defaultValue);
        }
        public static IEnumerable<TSource> DefaultIfEmpty<TSource>(this IEnumerable<TSource> source)
        {
            return System.Linq.Enumerable.DefaultIfEmpty<TSource>(source);
        }

        public static IEnumerable<TSource> Distinct<TSource>(this IEnumerable<TSource> source)
        {
            return System.Linq.Enumerable.Distinct<TSource>(source.Visit(Evaluate.Action.READ));
        }
        public static IEnumerable<TSource> Distinct<TSource>(this IEnumerable<TSource> source, IEqualityComparer<TSource> comparer)
        {
            return System.Linq.Enumerable.Distinct<TSource>(source.Visit(Evaluate.Action.READ), comparer);
        }

        public static TSource ElementAt<TSource>(this IEnumerable<TSource> source, int index)
        {
            return System.Linq.Enumerable.ElementAt<TSource>(source.Visit(Evaluate.Action.READ), index);
        }
        public static TSource ElementAtOrDefault<TSource>(this IEnumerable<TSource> source, int index)
        {
            return System.Linq.Enumerable.ElementAt<TSource>(source.Visit(Evaluate.Action.READ), index);
        }

        public static IEnumerable<TResult> Empty<TResult>()
        {
            return System.Linq.Enumerable.Empty<TResult>();
        }

        public static IEnumerable<TSource> Except<TSource>(this IEnumerable<TSource> first, IEnumerable<TSource> second)
        {
            return System.Linq.Enumerable.Except<TSource>(first.Visit(Evaluate.Action.READ), second);
        }
        public static IEnumerable<TSource> Except<TSource>(this IEnumerable<TSource> first, IEnumerable<TSource> second, IEqualityComparer<TSource> comparer)
        {
            return System.Linq.Enumerable.Except<TSource>(first.Visit(Evaluate.Action.READ), second, comparer);
        }

        public static TSource First<TSource>(this IEnumerable<TSource> source)
        {
            return System.Linq.Enumerable.First<TSource>(source.Visit(Evaluate.Action.READ));
        }
        public static TSource First<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
        {
            return System.Linq.Enumerable.First<TSource>(source.Visit(Evaluate.Action.READ), predicate);
        }
        public static TSource FirstOrDefault<TSource>(this IEnumerable<TSource> source)
        {
            return System.Linq.Enumerable.FirstOrDefault<TSource>(source.Visit(Evaluate.Action.READ));
        }
        public static TSource FirstOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
        {
            return System.Linq.Enumerable.FirstOrDefault<TSource>(source.Visit(Evaluate.Action.READ), predicate);
        }

        public static IEnumerable<TResult> GroupBy<TSource, TKey, TElement, TResult>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector, Func<TKey, IEnumerable<TElement>, TResult> resultSelector, IEqualityComparer<TKey> comparer)
        {
            return System.Linq.Enumerable.GroupBy<TSource, TKey, TElement, TResult>(source.Visit(Evaluate.Action.READ), keySelector, elementSelector, resultSelector, comparer);
        }
        public static IEnumerable<TResult> GroupBy<TSource, TKey, TElement, TResult>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector, Func<TKey, IEnumerable<TElement>, TResult> resultSelector)
        {
            return System.Linq.Enumerable.GroupBy<TSource, TKey, TElement, TResult>(source.Visit(Evaluate.Action.READ), keySelector, elementSelector, resultSelector);
        }
        public static IEnumerable<TResult> GroupBy<TSource, TKey, TResult>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TKey, IEnumerable<TSource>, TResult> resultSelector, IEqualityComparer<TKey> comparer)
        {
            return System.Linq.Enumerable.GroupBy<TSource, TKey, TResult>(source.Visit(Evaluate.Action.READ), keySelector, resultSelector, comparer);
        }
        public static IEnumerable<TResult> GroupBy<TSource, TKey, TResult>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TKey, IEnumerable<TSource>, TResult> resultSelector)
        {
            return System.Linq.Enumerable.GroupBy<TSource, TKey, TResult>(source.Visit(Evaluate.Action.READ), keySelector, resultSelector);
        }
        public static IEnumerable<System.Linq.IGrouping<TKey, TElement>> GroupBy<TSource, TKey, TElement>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector)
        {
            return System.Linq.Enumerable.GroupBy<TSource, TKey, TElement>(source.Visit(Evaluate.Action.READ), keySelector, elementSelector);
        }
        public static IEnumerable<System.Linq.IGrouping<TKey, TSource>> GroupBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, IEqualityComparer<TKey> comparer)
        {
            return System.Linq.Enumerable.GroupBy<TSource, TKey>(source.Visit(Evaluate.Action.READ), keySelector, comparer);
        }
        public static IEnumerable<System.Linq.IGrouping<TKey, TSource>> GroupBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            return System.Linq.Enumerable.GroupBy<TSource, TKey>(source.Visit(Evaluate.Action.READ), keySelector);
        }
        public static IEnumerable<System.Linq.IGrouping<TKey, TElement>> GroupBy<TSource, TKey, TElement>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector, IEqualityComparer<TKey> comparer)
        {
            return System.Linq.Enumerable.GroupBy<TSource, TKey, TElement>(source.Visit(Evaluate.Action.READ), keySelector, elementSelector, comparer);
        }

        public static IEnumerable<TResult> GroupJoin<TOuter, TInner, TKey, TResult>(this IEnumerable<TOuter> outer, IEnumerable<TInner> inner, Func<TOuter, TKey> outerKeySelector, Func<TInner, TKey> innerKeySelector, Func<TOuter, IEnumerable<TInner>, TResult> resultSelector)
        {
            return System.Linq.Enumerable.GroupJoin<TOuter, TInner, TKey, TResult>(outer.Visit(Evaluate.Action.READ), inner.Visit(Evaluate.Action.READ), outerKeySelector, innerKeySelector, resultSelector);
        }
        public static IEnumerable<TResult> GroupJoin<TOuter, TInner, TKey, TResult>(this IEnumerable<TOuter> outer, IEnumerable<TInner> inner, Func<TOuter, TKey> outerKeySelector, Func<TInner, TKey> innerKeySelector, Func<TOuter, IEnumerable<TInner>, TResult> resultSelector, IEqualityComparer<TKey> comparer)
        {
            return System.Linq.Enumerable.GroupJoin<TOuter, TInner, TKey, TResult>(outer.Visit(Evaluate.Action.READ), inner.Visit(Evaluate.Action.READ), outerKeySelector, innerKeySelector, resultSelector, comparer);
        }

        public static IEnumerable<TSource> Intersect<TSource>(this IEnumerable<TSource> first, IEnumerable<TSource> second)
        {
            return System.Linq.Enumerable.Intersect<TSource>(first, second).Visit(Evaluate.Action.READ);
        }
        public static IEnumerable<TSource> Intersect<TSource>(this IEnumerable<TSource> first, IEnumerable<TSource> second, IEqualityComparer<TSource> comparer)
        {
            return System.Linq.Enumerable.Intersect<TSource>(first, second, comparer).Visit(Evaluate.Action.READ);
        }

        public static IEnumerable<TResult> Join<TOuter, TInner, TKey, TResult>(this IEnumerable<TOuter> outer, IEnumerable<TInner> inner, Func<TOuter, TKey> outerKeySelector, Func<TInner, TKey> innerKeySelector, Func<TOuter, TInner, TResult> resultSelector)
        {
            return System.Linq.Enumerable.Join<TOuter, TInner, TKey, TResult>(outer.Visit(Evaluate.Action.READ), inner.Visit(Evaluate.Action.READ), outerKeySelector, innerKeySelector, resultSelector);
        }
        public static IEnumerable<TResult> Join<TOuter, TInner, TKey, TResult>(this IEnumerable<TOuter> outer, IEnumerable<TInner> inner, Func<TOuter, TKey> outerKeySelector, Func<TInner, TKey> innerKeySelector, Func<TOuter, TInner, TResult> resultSelector, IEqualityComparer<TKey> comparer)
        {
            return System.Linq.Enumerable.Join<TOuter, TInner, TKey, TResult>(outer.Visit(Evaluate.Action.READ), inner.Visit(Evaluate.Action.READ), outerKeySelector, innerKeySelector, resultSelector, comparer);
        }

        public static TSource Last<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
        {
            return System.Linq.Enumerable.Last<TSource>(source.Visit(Evaluate.Action.READ), predicate);
        }
        public static TSource Last<TSource>(this IEnumerable<TSource> source)
        {
            return System.Linq.Enumerable.Last<TSource>(source.Visit(Evaluate.Action.READ));
        }
        public static TSource LastOrDefault<TSource>(this IEnumerable<TSource> source)
        {
            return System.Linq.Enumerable.LastOrDefault<TSource>(source.Visit(Evaluate.Action.READ));
        }
        public static TSource LastOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
        {
            return System.Linq.Enumerable.LastOrDefault<TSource>(source.Visit(Evaluate.Action.READ), predicate);
        }

        public static long LongCount<TSource>(this IEnumerable<TSource> source)
        {
            return System.Linq.Enumerable.LongCount<TSource>(source.Visit(Evaluate.Action.READ));
        }
        public static long LongCount<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
        {
            return System.Linq.Enumerable.LongCount<TSource>(source.Visit(Evaluate.Action.READ), predicate);
        }

        public static double? Max<TSource>(this IEnumerable<TSource> source, Func<TSource, double?> selector)
        {
            return System.Linq.Enumerable.Max<TSource>(source.Visit(Evaluate.Action.READ), selector);
        }
        public static long Max<TSource>(this IEnumerable<TSource> source, Func<TSource, long> selector)
        {
            return System.Linq.Enumerable.Max<TSource>(source.Visit(Evaluate.Action.READ), selector);
        }
        public static decimal? Max<TSource>(this IEnumerable<TSource> source, Func<TSource, decimal?> selector)
        {
            return System.Linq.Enumerable.Max<TSource>(source.Visit(Evaluate.Action.READ), selector);
        }
        public static int? Max<TSource>(this IEnumerable<TSource> source, Func<TSource, int?> selector)
        {
            return System.Linq.Enumerable.Max<TSource>(source.Visit(Evaluate.Action.READ), selector);
        }
        public static float? Max(this IEnumerable<float?> source)
        {
            return System.Linq.Enumerable.Max(source);
        }
        public static float? Max<TSource>(this IEnumerable<TSource> source, Func<TSource, float?> selector)
        {
            return System.Linq.Enumerable.Max<TSource>(source.Visit(Evaluate.Action.READ), selector);
        }
        public static float Max<TSource>(this IEnumerable<TSource> source, Func<TSource, float> selector)
        {
            return System.Linq.Enumerable.Max<TSource>(source.Visit(Evaluate.Action.READ), selector);
        }
        public static TResult Max<TSource, TResult>(this IEnumerable<TSource> source, Func<TSource, TResult> selector)
        {
            return System.Linq.Enumerable.Max<TSource, TResult>(source.Visit(Evaluate.Action.READ), selector);
        }
        public static int Max<TSource>(this IEnumerable<TSource> source, Func<TSource, int> selector)
        {
            return System.Linq.Enumerable.Max<TSource>(source.Visit(Evaluate.Action.READ), selector);
        }
        public static long? Max<TSource>(this IEnumerable<TSource> source, Func<TSource, long?> selector)
        {
            return System.Linq.Enumerable.Max<TSource>(source.Visit(Evaluate.Action.READ), selector);
        }
        public static double Max<TSource>(this IEnumerable<TSource> source, Func<TSource, double> selector)
        {
            return System.Linq.Enumerable.Max<TSource>(source.Visit(Evaluate.Action.READ), selector);
        }
        public static double? Max(this IEnumerable<double?> source)
        {
            return System.Linq.Enumerable.Max(source);
        }
        public static TSource Max<TSource>(this IEnumerable<TSource> source)
        {
            return System.Linq.Enumerable.Max<TSource>(source.Visit(Evaluate.Action.READ));
        }
        public static float Max(this IEnumerable<float> source)
        {
            return System.Linq.Enumerable.Max(source);
        }
        public static long? Max(this IEnumerable<long?> source)
        {
            return System.Linq.Enumerable.Max(source);
        }
        public static int? Max(this IEnumerable<int?> source)
        {
            return System.Linq.Enumerable.Max(source);
        }
        public static decimal? Max(this IEnumerable<decimal?> source)
        {
            return System.Linq.Enumerable.Max(source);
        }
        public static long Max(this IEnumerable<long> source)
        {
            return System.Linq.Enumerable.Max(source);
        }
        public static int Max(this IEnumerable<int> source)
        {
            return System.Linq.Enumerable.Max(source);
        }
        public static double Max(this IEnumerable<double> source)
        {
            return System.Linq.Enumerable.Max(source);
        }
        public static decimal Max(this IEnumerable<decimal> source)
        {
            return System.Linq.Enumerable.Max(source);
        }
        public static decimal Max<TSource>(this IEnumerable<TSource> source, Func<TSource, decimal> selector)
        {
            return System.Linq.Enumerable.Max<TSource>(source.Visit(Evaluate.Action.READ), selector);
        }

        public static decimal? Min<TSource>(this IEnumerable<TSource> source, Func<TSource, decimal?> selector)
        {
            return System.Linq.Enumerable.Min<TSource>(source.Visit(Evaluate.Action.READ), selector);
        }
        public static int Min<TSource>(this IEnumerable<TSource> source, Func<TSource, int> selector)
        {
            return System.Linq.Enumerable.Min<TSource>(source.Visit(Evaluate.Action.READ), selector);
        }
        public static long Min<TSource>(this IEnumerable<TSource> source, Func<TSource, long> selector)
        {
            return System.Linq.Enumerable.Min<TSource>(source.Visit(Evaluate.Action.READ), selector);
        }
        public static double? Min<TSource>(this IEnumerable<TSource> source, Func<TSource, double?> selector)
        {
            return System.Linq.Enumerable.Min<TSource>(source.Visit(Evaluate.Action.READ), selector);
        }
        public static double Min<TSource>(this IEnumerable<TSource> source, Func<TSource, double> selector)
        {
            return System.Linq.Enumerable.Min<TSource>(source.Visit(Evaluate.Action.READ), selector);
        }
        public static long? Min<TSource>(this IEnumerable<TSource> source, Func<TSource, long?> selector)
        {
            return System.Linq.Enumerable.Min<TSource>(source.Visit(Evaluate.Action.READ), selector);
        }
        public static float? Min<TSource>(this IEnumerable<TSource> source, Func<TSource, float?> selector)
        {
            return System.Linq.Enumerable.Min<TSource>(source.Visit(Evaluate.Action.READ), selector);
        }
        public static float Min<TSource>(this IEnumerable<TSource> source, Func<TSource, float> selector)
        {
            return System.Linq.Enumerable.Min<TSource>(source.Visit(Evaluate.Action.READ), selector);
        }
        public static TResult Min<TSource, TResult>(this IEnumerable<TSource> source, Func<TSource, TResult> selector)
        {
            return System.Linq.Enumerable.Min<TSource, TResult>(source.Visit(Evaluate.Action.READ), selector);
        }
        public static int? Min<TSource>(this IEnumerable<TSource> source, Func<TSource, int?> selector)
        {
            return System.Linq.Enumerable.Min<TSource>(source.Visit(Evaluate.Action.READ), selector);
        }
        public static decimal Min<TSource>(this IEnumerable<TSource> source, Func<TSource, decimal> selector)
        {
            return System.Linq.Enumerable.Min<TSource>(source.Visit(Evaluate.Action.READ), selector);
        }
        public static double Min(this IEnumerable<double> source)
        {
            return System.Linq.Enumerable.Min(source);
        }
        public static float Min(this IEnumerable<float> source)
        {
            return System.Linq.Enumerable.Min(source);
        }
        public static float? Min(this IEnumerable<float?> source)
        {
            return System.Linq.Enumerable.Min(source);
        }
        public static long? Min(this IEnumerable<long?> source)
        {
            return System.Linq.Enumerable.Min(source);
        }
        public static int? Min(this IEnumerable<int?> source)
        {
            return System.Linq.Enumerable.Min(source);
        }
        public static double? Min(this IEnumerable<double?> source)
        {
            return System.Linq.Enumerable.Min(source);
        }
        public static decimal? Min(this IEnumerable<decimal?> source)
        {
            return System.Linq.Enumerable.Min(source);
        }
        public static long Min(this IEnumerable<long> source)
        {
            return System.Linq.Enumerable.Min(source);
        }
        public static int Min(this IEnumerable<int> source)
        {
            return System.Linq.Enumerable.Min(source);
        }
        public static TSource Min<TSource>(this IEnumerable<TSource> source)
        {
            return System.Linq.Enumerable.Min<TSource>(source.Visit(Evaluate.Action.READ));
        }
        public static decimal Min(this IEnumerable<decimal> source)
        {
            return System.Linq.Enumerable.Min(source);
        }

        public static IEnumerable<TResult> OfType<TResult>(this IEnumerable source)
        {
            return System.Linq.Enumerable.OfType<TResult>(source.Visit(Evaluate.Action.READ));
        }

        public static System.Linq.IOrderedEnumerable<TSource> OrderBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            return System.Linq.Enumerable.OrderBy<TSource, TKey>(source.Visit(Evaluate.Action.READ), keySelector);
        }
        public static System.Linq.IOrderedEnumerable<TSource> OrderBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, IComparer<TKey> comparer)
        {
            return System.Linq.Enumerable.OrderBy<TSource, TKey>(source.Visit(Evaluate.Action.READ), keySelector, comparer);
        }
        public static System.Linq.IOrderedEnumerable<TSource> OrderByDescending<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            return System.Linq.Enumerable.OrderByDescending<TSource, TKey>(source.Visit(Evaluate.Action.READ), keySelector);
        }
        public static System.Linq.IOrderedEnumerable<TSource> OrderByDescending<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, IComparer<TKey> comparer)
        {
            return System.Linq.Enumerable.OrderByDescending<TSource, TKey>(source.Visit(Evaluate.Action.READ), keySelector, comparer);
        }

        public static IEnumerable<TSource> Prepend<TSource>(this IEnumerable<TSource> source, TSource element)
        {
            return System.Linq.Enumerable.Prepend<TSource>(source.Visit(Evaluate.Action.READ), element);
        }

        public static IEnumerable<int> Range(int start, int count)
        {
            return System.Linq.Enumerable.Range(start, count);
        }

        public static IEnumerable<TResult> Repeat<TResult>(TResult element, int count)
        {
            return System.Linq.Enumerable.Repeat<TResult>(element, count);
        }

        public static IEnumerable<TSource> Reverse<TSource>(this IEnumerable<TSource> source)
        {
            return System.Linq.Enumerable.Reverse<TSource>(source.Visit(Evaluate.Action.READ));
        }

        public static IEnumerable<TResult> Select<TSource, TResult>(this IEnumerable<TSource> source, Func<TSource, TResult> selector)
        {
            return System.Linq.Enumerable.Select<TSource, TResult>(source.Visit(Evaluate.Action.READ), selector);
        }
        public static IEnumerable<TResult> Select<TSource, TResult>(this IEnumerable<TSource> source, Func<TSource, int, TResult> selector)
        {
            return System.Linq.Enumerable.Select<TSource, TResult>(source.Visit(Evaluate.Action.READ), selector);
        }
        public static IEnumerable<TResult> SelectMany<TSource, TCollection, TResult>(this IEnumerable<TSource> source, Func<TSource, int, IEnumerable<TCollection>> collectionSelector, Func<TSource, TCollection, TResult> resultSelector)
        {
            return System.Linq.Enumerable.SelectMany<TSource, TCollection, TResult>(source.Visit(Evaluate.Action.READ), collectionSelector, resultSelector);
        }
        public static IEnumerable<TResult> SelectMany<TSource, TResult>(this IEnumerable<TSource> source, Func<TSource, int, IEnumerable<TResult>> selector)
        {
            return System.Linq.Enumerable.SelectMany<TSource, TResult>(source.Visit(Evaluate.Action.READ), selector);
        }
        public static IEnumerable<TResult> SelectMany<TSource, TResult>(this IEnumerable<TSource> source, Func<TSource, IEnumerable<TResult>> selector)
        {
            return System.Linq.Enumerable.SelectMany<TSource, TResult>(source.Visit(Evaluate.Action.READ), selector);
        }
        public static IEnumerable<TResult> SelectMany<TSource, TCollection, TResult>(this IEnumerable<TSource> source, Func<TSource, IEnumerable<TCollection>> collectionSelector, Func<TSource, TCollection, TResult> resultSelector)
        {
            return System.Linq.Enumerable.SelectMany<TSource, TCollection, TResult>(source.Visit(Evaluate.Action.READ), collectionSelector, resultSelector);
        }

        public static bool SequenceEqual<TSource>(this IEnumerable<TSource> first, IEnumerable<TSource> second)
        {
            return System.Linq.Enumerable.SequenceEqual<TSource>(first.Visit(Evaluate.Action.READ), second.Visit(Evaluate.Action.READ));
        }
        public static bool SequenceEqual<TSource>(this IEnumerable<TSource> first, IEnumerable<TSource> second, IEqualityComparer<TSource> comparer)
        {
            return System.Linq.Enumerable.SequenceEqual<TSource>(first.Visit(Evaluate.Action.READ), second.Visit(Evaluate.Action.READ), comparer);
        }

        public static TSource Single<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
        {
            return System.Linq.Enumerable.Single<TSource>(source.Visit(Evaluate.Action.READ), predicate);
        }
        public static TSource Single<TSource>(this IEnumerable<TSource> source)
        {
            return System.Linq.Enumerable.Single<TSource>(source.Visit(Evaluate.Action.READ));
        }
        public static TSource SingleOrDefault<TSource>(this IEnumerable<TSource> source)
        {
            return System.Linq.Enumerable.SingleOrDefault<TSource>(source.Visit(Evaluate.Action.READ));
        }
        public static TSource SingleOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
        {
            return System.Linq.Enumerable.SingleOrDefault<TSource>(source.Visit(Evaluate.Action.READ), predicate);
        }

        public static IEnumerable<TSource> Skip<TSource>(this IEnumerable<TSource> source, int count)
        {
            return System.Linq.Enumerable.Skip<TSource>(source.Visit(Evaluate.Action.READ), count);
        }
        public static IEnumerable<TSource> SkipLast<TSource>(this IEnumerable<TSource> source, int count)
        {
            return System.Linq.Enumerable.SkipLast<TSource>(source.Visit(Evaluate.Action.READ), count);
        }
        public static IEnumerable<TSource> SkipWhile<TSource>(this IEnumerable<TSource> source, Func<TSource, int, bool> predicate)
        {
            return System.Linq.Enumerable.SkipWhile<TSource>(source.Visit(Evaluate.Action.READ), predicate);
        }
        public static IEnumerable<TSource> SkipWhile<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
        {
            return System.Linq.Enumerable.SkipWhile<TSource>(source.Visit(Evaluate.Action.READ), predicate);
        }

        public static float Sum<TSource>(this IEnumerable<TSource> source, Func<TSource, float> selector)
        {
            return System.Linq.Enumerable.Sum<TSource>(source.Visit(Evaluate.Action.READ), selector);
        }
        public static int Sum<TSource>(this IEnumerable<TSource> source, Func<TSource, int> selector)
        {
            return System.Linq.Enumerable.Sum<TSource>(source.Visit(Evaluate.Action.READ), selector);
        }
        public static long Sum<TSource>(this IEnumerable<TSource> source, Func<TSource, long> selector)
        {
            return System.Linq.Enumerable.Sum<TSource>(source.Visit(Evaluate.Action.READ), selector);
        }
        public static decimal? Sum<TSource>(this IEnumerable<TSource> source, Func<TSource, decimal?> selector)
        {
            return System.Linq.Enumerable.Sum<TSource>(source.Visit(Evaluate.Action.READ), selector);
        }
        public static double Sum<TSource>(this IEnumerable<TSource> source, Func<TSource, double> selector)
        {
            return System.Linq.Enumerable.Sum<TSource>(source.Visit(Evaluate.Action.READ), selector);
        }
        public static int? Sum<TSource>(this IEnumerable<TSource> source, Func<TSource, int?> selector)
        {
            return System.Linq.Enumerable.Sum<TSource>(source.Visit(Evaluate.Action.READ), selector);
        }
        public static long? Sum<TSource>(this IEnumerable<TSource> source, Func<TSource, long?> selector)
        {
            return System.Linq.Enumerable.Sum<TSource>(source.Visit(Evaluate.Action.READ), selector);
        }
        public static float? Sum<TSource>(this IEnumerable<TSource> source, Func<TSource, float?> selector)
        {
            return System.Linq.Enumerable.Sum<TSource>(source.Visit(Evaluate.Action.READ), selector);
        }
        public static double? Sum<TSource>(this IEnumerable<TSource> source, Func<TSource, double?> selector)
        {
            return System.Linq.Enumerable.Sum<TSource>(source.Visit(Evaluate.Action.READ), selector);
        }
        public static decimal Sum<TSource>(this IEnumerable<TSource> source, Func<TSource, decimal> selector)
        {
            return System.Linq.Enumerable.Sum<TSource>(source.Visit(Evaluate.Action.READ), selector);
        }
        public static double? Sum(this IEnumerable<double?> source)
        {
            return System.Linq.Enumerable.Sum(source);
        }
        public static float? Sum(this IEnumerable<float?> source)
        {
            return System.Linq.Enumerable.Sum(source);
        }
        public static decimal Sum(this IEnumerable<decimal> source)
        {
            return System.Linq.Enumerable.Sum(source);
        }
        public static double Sum(this IEnumerable<double> source)
        {
            return System.Linq.Enumerable.Sum(source);
        }
        public static int Sum(this IEnumerable<int> source)
        {
            return System.Linq.Enumerable.Sum(source);
        }
        public static float Sum(this IEnumerable<float> source)
        {
            return System.Linq.Enumerable.Sum(source);
        }
        public static decimal? Sum(this IEnumerable<decimal?> source)
        {
            return System.Linq.Enumerable.Sum(source);
        }
        public static int? Sum(this IEnumerable<int?> source)
        {
            return System.Linq.Enumerable.Sum(source);
        }
        public static long? Sum(this IEnumerable<long?> source)
        {
            return System.Linq.Enumerable.Sum(source);
        }
        public static long Sum(this IEnumerable<long> source)
        {
            return System.Linq.Enumerable.Sum(source);
        }

        public static IEnumerable<TSource> Take<TSource>(this IEnumerable<TSource> source, int count)
        {
            return System.Linq.Enumerable.Take<TSource>(source.Visit(Evaluate.Action.READ), count);
        }
        public static IEnumerable<TSource> TakeLast<TSource>(this IEnumerable<TSource> source, int count)
        {
            return System.Linq.Enumerable.TakeLast<TSource>(source.Visit(Evaluate.Action.READ), count);
        }
        public static IEnumerable<TSource> TakeWhile<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
        {
            return System.Linq.Enumerable.TakeWhile<TSource>(source.Visit(Evaluate.Action.READ), predicate);
        }
        public static IEnumerable<TSource> TakeWhile<TSource>(this IEnumerable<TSource> source, Func<TSource, int, bool> predicate)
        {
            return System.Linq.Enumerable.TakeWhile<TSource>(source.Visit(Evaluate.Action.READ), predicate);
        }

        public static System.Linq.IOrderedEnumerable<TSource> ThenBy<TSource, TKey>(this System.Linq.IOrderedEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            return System.Linq.Enumerable.ThenBy<TSource, TKey>(source.Visit(Evaluate.Action.READ), keySelector);
        }
        public static System.Linq.IOrderedEnumerable<TSource> ThenBy<TSource, TKey>(this System.Linq.IOrderedEnumerable<TSource> source, Func<TSource, TKey> keySelector, IComparer<TKey> comparer)
        {
            return System.Linq.Enumerable.ThenBy<TSource, TKey>(source.Visit(Evaluate.Action.READ), keySelector, comparer);
        }
        public static System.Linq.IOrderedEnumerable<TSource> ThenByDescending<TSource, TKey>(this System.Linq.IOrderedEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            return System.Linq.Enumerable.ThenByDescending<TSource, TKey>(source.Visit(Evaluate.Action.READ), keySelector);
        }
        public static System.Linq.IOrderedEnumerable<TSource> ThenByDescending<TSource, TKey>(this System.Linq.IOrderedEnumerable<TSource> source, Func<TSource, TKey> keySelector, IComparer<TKey> comparer)
        {
            return System.Linq.Enumerable.ThenByDescending<TSource, TKey>(source.Visit(Evaluate.Action.READ), keySelector, comparer);
        }

        public static TSource[] ToArray<TSource>(this IEnumerable<TSource> source)
        {
            return System.Linq.Enumerable.ToArray<TSource>(source.Visit(Evaluate.Action.READ));
        }

        public static Dictionary<TKey, TSource> ToDictionary<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            return System.Linq.Enumerable.ToDictionary<TSource, TKey>(source.Visit(Evaluate.Action.READ), keySelector);
        }
        public static Dictionary<TKey, TSource> ToDictionary<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, IEqualityComparer<TKey> comparer)
        {
            return System.Linq.Enumerable.ToDictionary<TSource, TKey>(source.Visit(Evaluate.Action.READ), keySelector, comparer);
        }
        public static Dictionary<TKey, TElement> ToDictionary<TSource, TKey, TElement>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector)
        {
            return System.Linq.Enumerable.ToDictionary<TSource, TKey, TElement>(source.Visit(Evaluate.Action.READ), keySelector, elementSelector);
        }
        public static Dictionary<TKey, TElement> ToDictionary<TSource, TKey, TElement>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector, IEqualityComparer<TKey> comparer)
        {
            return System.Linq.Enumerable.ToDictionary<TSource, TKey, TElement>(source.Visit(Evaluate.Action.READ), keySelector, elementSelector, comparer);
        }

        public static HashSet<TSource> ToHashSet<TSource>(this IEnumerable<TSource> source)
        {
            return System.Linq.Enumerable.ToHashSet<TSource>(source.Visit(Evaluate.Action.READ));
        }
        public static HashSet<TSource> ToHashSet<TSource>(this IEnumerable<TSource> source, IEqualityComparer<TSource> comparer)
        {
            return System.Linq.Enumerable.ToHashSet<TSource>(source.Visit(Evaluate.Action.READ), comparer);
        }

        public static List<TSource> ToList<TSource>(this IEnumerable<TSource> source)
        {
            return System.Linq.Enumerable.ToList<TSource>(source.Visit(Evaluate.Action.READ));
        }

        public static System.Linq.ILookup<TKey, TElement> ToLookup<TSource, TKey, TElement>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector, IEqualityComparer<TKey> comparer)
        {
            return System.Linq.Enumerable.ToLookup<TSource, TKey, TElement>(source.Visit(Evaluate.Action.READ), keySelector, elementSelector, comparer);
        }
        public static System.Linq.ILookup<TKey, TElement> ToLookup<TSource, TKey, TElement>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector)
        {
            return System.Linq.Enumerable.ToLookup<TSource, TKey, TElement>(source.Visit(Evaluate.Action.READ), keySelector, elementSelector);
        }
        public static System.Linq.ILookup<TKey, TSource> ToLookup<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, IEqualityComparer<TKey> comparer)
        {
            return System.Linq.Enumerable.ToLookup<TSource, TKey>(source.Visit(Evaluate.Action.READ), keySelector, comparer);
        }
        public static System.Linq.ILookup<TKey, TSource> ToLookup<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            return System.Linq.Enumerable.ToLookup<TSource, TKey>(source.Visit(Evaluate.Action.READ), keySelector);
        }

        public static IEnumerable<TSource> Union<TSource>(this IEnumerable<TSource> first, IEnumerable<TSource> second)
        {
            return System.Linq.Enumerable.Union<TSource>(first.Visit(Evaluate.Action.READ), second.Visit(Evaluate.Action.READ));
        }
        public static IEnumerable<TSource> Union<TSource>(this IEnumerable<TSource> first, IEnumerable<TSource> second, IEqualityComparer<TSource> comparer)
        {
            return System.Linq.Enumerable.Union<TSource>(first.Visit(Evaluate.Action.READ), second.Visit(Evaluate.Action.READ), comparer);
        }

        public static IEnumerable<TSource> Where<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
        {
            return System.Linq.Enumerable.Where<TSource>(source.Visit(Evaluate.Action.READ), predicate);
        }
        public static IEnumerable<TSource> Where<TSource>(this IEnumerable<TSource> source, Func<TSource, int, bool> predicate)
        {
            return System.Linq.Enumerable.Where<TSource>(source.Visit(Evaluate.Action.READ), predicate);
        }

        public static IEnumerable<(TFirst First, TSecond Second)> Zip<TFirst, TSecond>(this IEnumerable<TFirst> first, IEnumerable<TSecond> second)
        {
            return System.Linq.Enumerable.Zip<TFirst, TSecond>(first.Visit(Evaluate.Action.READ), second.Visit(Evaluate.Action.READ));
        }
        public static IEnumerable<TResult> Zip<TFirst, TSecond, TResult>(this IEnumerable<TFirst> first, IEnumerable<TSecond> second, Func<TFirst, TSecond, TResult> resultSelector)
        {
            return System.Linq.Enumerable.Zip<TFirst, TSecond, TResult>(first.Visit(Evaluate.Action.READ), second.Visit(Evaluate.Action.READ), resultSelector);
        }
    }
}