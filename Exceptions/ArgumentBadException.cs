using System;

namespace PatientsRecordsManager.Exceptions
{
    public class ArgumentBadException : ArgumentException
    {
        public ArgumentBadException()
        {
        }

        public ArgumentBadException(string message) : base(message)
        {

        }

        public ArgumentBadException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}