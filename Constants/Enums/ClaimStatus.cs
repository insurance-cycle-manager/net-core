namespace PatientsRecordsManager.Constants.Enums
{
    public enum ClaimStatus
    {
        Pending,
        Accepted,
        Rejected,
        Canceled
    }
}