using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using AutoMapper;
using PatientsRecordsManager.Resources;
using PatientsRecordsManager.Data.Dtos.Addresses;
using PatientsRecordsManager.Exceptions;
using PatientsRecordsManager.Models;
using PatientsRecordsManager.ABAC.Extensions.EF;
using PatientsRecordsManager.ABAC.Extensions;

namespace PatientsRecordsManager.Data.Repositories
{
    public class AddressRepository : IAddressRepository
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        private readonly Messages _messages;

        public AddressRepository(DataContext context, IMapper mapper, Messages messages)
        {
            _mapper = mapper;
            _context = context;
            _messages = messages;
        }

        public AddressForReturnDto AddAddress(AddressForCreateDto addressForCreate)
        {
            var address = _mapper.Map<Address>(addressForCreate);
            _context.Addresses.Add(address);

            if (_context.SaveChanges() <= 0)
            {
                throw new SaveChangesFailedException();
            }
            return _mapper.Map<AddressForReturnDto>(address);
        }

        public IEnumerable<AddressForReturnDto> GetAddresses(string title, string addressCode)
        {
            var addresses = _context.Addresses.AsQueryable();

            if (!string.IsNullOrWhiteSpace(title))
            {
                addresses = addresses.Where(ad => ad.NormalizedTitle.ToUpper().Contains(title.ToUpper()));
            }

            if (!string.IsNullOrWhiteSpace(addressCode))
            {
                addresses = addresses.Where(ad => ad.AddressCode.ToUpper().Contains(addressCode.ToUpper()));
            }
            return _mapper.Map<IEnumerable<AddressForReturnDto>>(addresses);
        }

        public AddressForReturnDto ModifyAddress(string addressCode, AddressForUpdateDto address)
        {
            var retrieved = GetAddress(addressCode);
            var updated = _mapper.Map<AddressForUpdateDto, Address>(address, retrieved);

            if (_context.SaveChanges() <= 0)
            {
                throw new SaveChangesFailedException();
            }
            return _mapper.Map<AddressForReturnDto>(updated);
        }

        private Address GetAddress(string addressCode)
        {
            return _context.Addresses
                .FirstOrDefault(ad => ad.AddressCode.ToUpper() == (addressCode.ToUpper()))
                ?? throw new ArgumentBadException(_messages.GetString("addressCodeNotFound", addressCode));
        }
    }
}