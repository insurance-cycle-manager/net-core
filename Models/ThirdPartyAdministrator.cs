using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PatientsRecordsManager.Models
{
    public class ThirdPartyAdministrator
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string ThirdPartyCode { get; set; }
        [Required]
        public DateTime JoinedAt { get; set; }
        public DateTime DateOfFounding { get; set; }

        [Required]
        public int SupervisoryComissionId { get; set; }
        public SupervisoryComission SupervisoryComission { get; set; }

        [Required]
        public int StatusId { get; set; }
        public Status Status { get; set; }

        [Required]
        [ForeignKey("Users")]
        public int UserId { get; set; }
        public User User { get; set; }

        [Required]
        [ForeignKey("Roles")]
        public int RoleId { get; set; }
        public Role Role { get; set; }
        public UserRole UserRole { get; set; }

        [Required]
        [ForeignKey("Addresses")]
        public int AddressId { get; set; }
        public UserAddress Address { get; set; }

        [Required]
        [ForeignKey("UserEmails")]
        public int EmailId { get; set; }
        public UserEmail Email { get; set; }

        [Required]
        [ForeignKey("UserPhoneNumbers")]
        public int PhoneNumberId { get; set; }
        public UserPhoneNumber PhoneNumber { get; set; }

        public ICollection<InsuranceConnection> InsuranceConnections { get; set; }
        public ICollection<ServiceProviderConnection> ServiceProvidersConnections { get; set; }
        public IEnumerable<VisitClaim> VisitClaims { get; set; }
    }
}