using System.ComponentModel.DataAnnotations;

namespace PatientsRecordsManager.Data.Dtos.Diagnosis
{
    public class DiagnosisForCreateDto
    {
        [Required]
        public string Description { get; set; }
    }
}