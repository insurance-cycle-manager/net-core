using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PatientsRecordsManager.ABAC.Extensions;
using PatientsRecordsManager.ABAC.Extensions.EF;
using PatientsRecordsManager.Constants;
using PatientsRecordsManager.Data;
using PatientsRecordsManager.Data.Dtos.Prescribed.LabTests;
using PatientsRecordsManager.Extensions;
using PatientsRecordsManager.Models;
using PatientsRecordsManager.Resources;
using PatientsRecordsManager.Utils.Exceptions;

namespace PatientsRecordsManager.Controllers
{
    [ApiController]
    [Authorize(Roles = RolesNames.GetStaffersCommaDelimited)]
    public class LabTestPurchaseController : PrmController
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;
        private readonly Messages _messages;

        public LabTestPurchaseController(DataContext dataContext, IMapper mapper, Messages messages)
        {
            _mapper = mapper;
            _messages = messages;
            _dataContext = dataContext;
        }

        [HttpGet(APIs.DefaultRoute)]
        public IActionResult GetLabTestPurchases()
        {
            var labTestPurchases = _dataContext
                .LabTestPurchases
                .Select(ltp => _mapper.Map<LabTestPurchaseForReturnDto>(ltp));

            return HttpContext.Success(labTestPurchases, _messages.GetString("LabTestPurchasesFeteched"));
        }

        [HttpPost(APIs.AddLabTestPurchase)]
        public IActionResult AddLabTestPurchase(string visitCode, string prescribedLabTestCode)
        {
            var visit = FetchVisit(visitCode);

            if (visit.BaseVisit == null)
            {
                throw new Exception(_messages.GetString("NoBaseVisitFound", visitCode));
            }

            var prescribed = FetchLabTest(prescribedLabTestCode, visit.BaseVisit);

            var purchase = new LabTestPurchase
            {
                Visit = visit,
                PrescribedLabTest = prescribed,
                Price = prescribed.LabTest.Price,
                DateOfPurchasing = DateTime.Now
            };
            prescribed.Purchases.Add(purchase);

            if (_dataContext.SaveChanges() < 1)
                throw new Exception(_messages.GetString("LabTestPurchaseAddFailed"));

            return HttpContext.Success(
                _mapper.Map<LabTestPurchaseForReturnDto>(purchase),
                _messages.GetString("LabTestPurchaseCreated"));
        }


        [HttpGet(APIs.GetLabTestPurchase)]
        public IActionResult GetLabTestPurchase(string visitCode, string prescribedLabTestCode)
        {
            var visit = FetchVisit(visitCode, true);
            var purchases = visit
                .LabTestPurchases
                .Where(p => p.PrescribedLabTest.PrescribedLabTestCode.ToUpper() == (prescribedLabTestCode.ToUpper()));

            return HttpContext.Success(
                _mapper.Map<IEnumerable<LabTestPurchaseForReturnDto>>(purchases),
                _messages.GetString("LabTestPurchaseFetched"));
        }

        private Visit FetchVisit(string visitCode, bool purchases = false)
        {
            var visits = _dataContext.Visits
                .Include(v => v.BaseVisit)
                .AsQueryable();

            if (purchases)
            {
                visits = visits
                    .Include(v => v.LabTestPurchases)
                    .ThenInclude(lp => lp.Price)
                    .Include(v => v.LabTestPurchases)
                    .ThenInclude(lp => lp.PrescribedLabTest);
            }

            return visits.FirstOrDefault(v => v.VisitCode.ToUpper() == (visitCode.ToUpper()))
                ?? throw new NoDataFoundException(_messages.GetString("VisitNotFound2"));
        }

        private PrescribedLabTest FetchLabTest(string prescribedLabTestCode, Visit visit)
        {
            var labTests = _dataContext.PrescribedLabTests
                .Include(l => l.Purchases)
                .Include(l => l.LabTest)
                .ThenInclude(t => t.Price)
                .Where(l => l.PrescribedLabTestCode.ToUpper() == (prescribedLabTestCode.ToUpper()) && l.VisitId == visit.Id)
                .AsQueryable();

            return labTests.FirstOrDefault() ?? throw new NoDataFoundException(_messages.GetString("PrescribedLabTestNotFound"));
        }
    }
}