using System;
using System.ComponentModel.DataAnnotations;
using PatientsRecordsManager.Constants.Enums;

namespace PatientsRecordsManager.Data.Dtos.ServiceProviders.Staffers
{
    public class StafferForCreateDto
    {
        [Required]
        [EnumDataType(typeof(ServiceProviderStafferType))]
        public ServiceProviderStafferType Type { get; set; }
 
        [Required]
        public string NormalizedUserName { get; set; }
 
        [Required]
        public string Title { get; set; }
        
        [Required]
        public int AddressId { get; set; }
        [Required]
        public int EmailId { get; set; }
        [Required]
        public int PhoneNumberId { get; set; }
        public DateTime JoinedAt { get; set; }
        [Required]
        public int UserId { get; set; }
        [Required]
        public int RoleId { get; set; }

        public StafferForCreateDto()
        {
            JoinedAt = DateTime.Now;
        }
    }
}