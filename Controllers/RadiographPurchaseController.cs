using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PatientsRecordsManager.ABAC;
using PatientsRecordsManager.ABAC.Extensions;
using PatientsRecordsManager.ABAC.Extensions.EF;
using PatientsRecordsManager.Constants;
using PatientsRecordsManager.Data;
using PatientsRecordsManager.Data.Dtos.Prescribed.Radiographs;
using PatientsRecordsManager.Extensions;
using PatientsRecordsManager.Models;
using PatientsRecordsManager.Resources;
using PatientsRecordsManager.Utils.Exceptions;

namespace PatientsRecordsManager.Controllers
{
    [ApiController]
    [Authorize(Roles = RolesNames.GetStaffersCommaDelimited)]
    public class RadiographPurchaseController : PrmController
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;
        private readonly Messages _messages;

        public RadiographPurchaseController(DataContext dataContext, IMapper mapper, Messages messages)
        {
            _mapper = mapper;
            _messages = messages;
            _dataContext = dataContext;
        }

        [HttpGet(APIs.DefaultRoute)]
        public IActionResult GetRadiographPurchases()
        {
            var radiographPurchases = _dataContext
                                    .RadiographPurchases
                                    .Select(rp => _mapper.Map<RadiographPurchaseForReturnDto>(rp));

            return HttpContext.Success(radiographPurchases, _messages.GetString("RadiographPurchasesFetched"));
        }

        [HttpPost(APIs.AddRadiographPurchase)]
        public IActionResult AddRadiographPurchase(string visitCode, string prescribedRadiographCode)
        {
            var visit = FetchVisit(visitCode);

            if (visit.BaseVisit == null)
            {
                throw new Exception(_messages.GetString("NoBaseVisitFound", visitCode));
            }
            var prescribed = FetchRadiograph(prescribedRadiographCode, visit.BaseVisit);

            var purchase = new RadiographPurchase
            {
                Visit = visit,
                PrescribedRadiograph = prescribed,
                Price = prescribed.Radiograph.Price,
                DateOfPurchasing = DateTime.Now
            };
            prescribed.Purchases.Add(purchase);

            if (_dataContext.SaveChanges() < 1)
                throw new Exception(_messages.GetString("RadiographPurchaseAddFailed"));

            return HttpContext.Success(
                _mapper.Map<RadiographPurchaseForReturnDto>(purchase),
                _messages.GetString("RadiographPurchaseCreated"));
        }

        [HttpGet(APIs.GetRadiographPurchase)]
        public IActionResult GetRadiographPurchase(string visitCode, string prescribedRadiographCode)
        {
            try
            {
                var visit = FetchVisit(visitCode, true);
                var purchases = visit
                    .RadiographPurchases
                    .AsEnumerable()
                    .Where(p => p.PrescribedRadiograph.PrescribedRadiographCode.ToUpper() == (prescribedRadiographCode.ToUpper()));

                return HttpContext.Success(
                    _mapper.Map<IEnumerable<RadiographPurchaseForReturnDto>>(purchases),
                    _messages.GetString("RadiographPurchaseFetched"));
            }
            catch (NoDataFoundException e)
            {
                return HttpContext.Error(e, e.Message);
            }
        }

        private Visit FetchVisit(string visitCode, bool purchases = false)
        {
            var visits = _dataContext.Visits
                .Include(v => v.BaseVisit)
                .AsQueryable();

            if (purchases)
            {
                visits = visits
                    .Include(v => v.RadiographPurchases)
                    .ThenInclude(lp => lp.Price)
                    .Include(v => v.RadiographPurchases)
                    .ThenInclude(lp => lp.PrescribedRadiograph);
            }

            return visits
                .AsEnumerable()
                .FirstOrDefault(v => v.VisitCode.ToUpper() == (visitCode.ToUpper()))
                ?? throw new NoDataFoundException(_messages.GetString("VisitNotFound2"));
        }

        private PrescribedRadiograph FetchRadiograph(string prescribedRadiographCode, Visit visit)
        {
            var Radiographs = _dataContext.PrescribedRadiographs
                .Include(l => l.Purchases)
                .Include(l => l.Radiograph)
                .ThenInclude(t => t.Price)
                .AsEnumerable()
                .Where(l => l.PrescribedRadiographCode.ToUpper() == (prescribedRadiographCode.ToUpper()) && l.VisitId == visit.Id)
                .AsQueryable();

            return Radiographs.FirstOrDefault() ?? throw new NoDataFoundException(_messages.GetString("PrescribedRadiographNotFound"));
        }
    }
}