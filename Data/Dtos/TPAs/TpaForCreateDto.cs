using System;
using System.ComponentModel.DataAnnotations;
using PatientsRecordsManager.Constants;
using PatientsRecordsManager.Constants.Enums;
using PatientsRecordsManager.Data.Dtos.Addresses;

namespace PatientsRecordsManager.Data.Dtos.TPAs
{
    public class TpaForCreateDto
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public int AddressId { get; set; }
        [Required]
        public int EmailId { get; set; }
        [Required]
        public int PhoneNumberId { get; set; }
        [Required]
        public int UserId { get; set; }
        [Required]
        public int RoleId { get; set; }
        [Required]
        public DateTime JoinedAt { get; set; }
        [Required]
        public int SupervisoryComissionId { get; set; }
        [Required]
        public StatusType Status { get; set; }
    }
}