using System.Collections.Generic;
using PatientsRecordsManager.Data.Dtos.Surgeries;

namespace PatientsRecordsManager.Data.Repositories
{
    public interface ISurgeryRespository
    {
        SurgeryForReturnDto AddSurgery(SurgeryForCreateDto surgery);
        IEnumerable<SurgeryForReturnDto> GetSurgeries();
        SurgeryForReturnDto GetSurgeryDetails(string surgeryCode);
        SurgeryForReturnDto ModifySurgery(string surgeryCode, SurgeryForUpdateDto surgery);
        SurgeryForReturnDto RemoveSurgery(string surgeryCode);
    }
}