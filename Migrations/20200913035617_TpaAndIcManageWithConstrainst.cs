﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PatientsRecordsManager.Migrations
{
    public partial class TpaAndIcManageWithConstrainst : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_InsuranceCompanies_UserEmails_EmailId",
                table: "InsuranceCompanies");

            migrationBuilder.DropForeignKey(
                name: "FK_InsuranceCompanies_UserPhoneNumbers_PhoneNumberId",
                table: "InsuranceCompanies");

            migrationBuilder.DropForeignKey(
                name: "FK_InsuranceCompanies_AspNetRoles_RoleId",
                table: "InsuranceCompanies");

            migrationBuilder.DropForeignKey(
                name: "FK_InsuranceCompanies_AspNetUsers_UserId",
                table: "InsuranceCompanies");

            migrationBuilder.DropForeignKey(
                name: "FK_InsuranceCompanies_UserAddresses_AddressUserId_AddressId1",
                table: "InsuranceCompanies");

            migrationBuilder.DropForeignKey(
                name: "FK_InsuranceCompanies_AspNetUserRoles_UserRoleUserId_UserRoleR~",
                table: "InsuranceCompanies");

            migrationBuilder.DropForeignKey(
                name: "FK_ThirdPartyAdministrators_UserEmails_EmailId",
                table: "ThirdPartyAdministrators");

            migrationBuilder.DropForeignKey(
                name: "FK_ThirdPartyAdministrators_UserPhoneNumbers_PhoneNumberId",
                table: "ThirdPartyAdministrators");

            migrationBuilder.DropForeignKey(
                name: "FK_ThirdPartyAdministrators_AspNetRoles_RoleId",
                table: "ThirdPartyAdministrators");

            migrationBuilder.DropForeignKey(
                name: "FK_ThirdPartyAdministrators_AspNetUsers_UserId",
                table: "ThirdPartyAdministrators");

            migrationBuilder.DropForeignKey(
                name: "FK_ThirdPartyAdministrators_UserAddresses_AddressUserId_Addres~",
                table: "ThirdPartyAdministrators");

            migrationBuilder.DropForeignKey(
                name: "FK_ThirdPartyAdministrators_AspNetUserRoles_UserRoleUserId_Use~",
                table: "ThirdPartyAdministrators");

            migrationBuilder.DropIndex(
                name: "IX_ThirdPartyAdministrators_UserId",
                table: "ThirdPartyAdministrators");

            migrationBuilder.DropIndex(
                name: "IX_ThirdPartyAdministrators_AddressUserId_AddressId1",
                table: "ThirdPartyAdministrators");

            migrationBuilder.DropIndex(
                name: "IX_ThirdPartyAdministrators_UserRoleUserId_UserRoleRoleId",
                table: "ThirdPartyAdministrators");

            migrationBuilder.DropIndex(
                name: "IX_InsuranceCompanies_UserId",
                table: "InsuranceCompanies");

            migrationBuilder.DropIndex(
                name: "IX_InsuranceCompanies_AddressUserId_AddressId1",
                table: "InsuranceCompanies");

            migrationBuilder.DropIndex(
                name: "IX_InsuranceCompanies_UserRoleUserId_UserRoleRoleId",
                table: "InsuranceCompanies");

            migrationBuilder.DropColumn(
                name: "AddressId1",
                table: "ThirdPartyAdministrators");

            migrationBuilder.DropColumn(
                name: "AddressUserId",
                table: "ThirdPartyAdministrators");

            migrationBuilder.DropColumn(
                name: "UserRoleRoleId",
                table: "ThirdPartyAdministrators");

            migrationBuilder.DropColumn(
                name: "UserRoleUserId",
                table: "ThirdPartyAdministrators");

            migrationBuilder.DropColumn(
                name: "AddressId1",
                table: "InsuranceCompanies");

            migrationBuilder.DropColumn(
                name: "AddressUserId",
                table: "InsuranceCompanies");

            migrationBuilder.DropColumn(
                name: "UserRoleRoleId",
                table: "InsuranceCompanies");

            migrationBuilder.DropColumn(
                name: "UserRoleUserId",
                table: "InsuranceCompanies");

            migrationBuilder.AlterColumn<int>(
                name: "UserId",
                table: "ThirdPartyAdministrators",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "RoleId",
                table: "ThirdPartyAdministrators",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PhoneNumberId",
                table: "ThirdPartyAdministrators",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "EmailId",
                table: "ThirdPartyAdministrators",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "AddressId",
                table: "ThirdPartyAdministrators",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "UserId",
                table: "InsuranceCompanies",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "RoleId",
                table: "InsuranceCompanies",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PhoneNumberId",
                table: "InsuranceCompanies",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "EmailId",
                table: "InsuranceCompanies",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "AddressId",
                table: "InsuranceCompanies",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ThirdPartyAdministrators_UserId_AddressId",
                table: "ThirdPartyAdministrators",
                columns: new[] { "UserId", "AddressId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ThirdPartyAdministrators_UserId_RoleId",
                table: "ThirdPartyAdministrators",
                columns: new[] { "UserId", "RoleId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_InsuranceCompanies_UserId_AddressId",
                table: "InsuranceCompanies",
                columns: new[] { "UserId", "AddressId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_InsuranceCompanies_UserId_RoleId",
                table: "InsuranceCompanies",
                columns: new[] { "UserId", "RoleId" },
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_InsuranceCompanies_UserEmails_EmailId",
                table: "InsuranceCompanies",
                column: "EmailId",
                principalTable: "UserEmails",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_InsuranceCompanies_UserPhoneNumbers_PhoneNumberId",
                table: "InsuranceCompanies",
                column: "PhoneNumberId",
                principalTable: "UserPhoneNumbers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_InsuranceCompanies_AspNetRoles_RoleId",
                table: "InsuranceCompanies",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_InsuranceCompanies_AspNetUsers_UserId",
                table: "InsuranceCompanies",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_InsuranceCompanies_UserAddresses_UserId_AddressId",
                table: "InsuranceCompanies",
                columns: new[] { "UserId", "AddressId" },
                principalTable: "UserAddresses",
                principalColumns: new[] { "UserId", "AddressId" },
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_InsuranceCompanies_AspNetUserRoles_UserId_RoleId",
                table: "InsuranceCompanies",
                columns: new[] { "UserId", "RoleId" },
                principalTable: "AspNetUserRoles",
                principalColumns: new[] { "UserId", "RoleId" },
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ThirdPartyAdministrators_UserEmails_EmailId",
                table: "ThirdPartyAdministrators",
                column: "EmailId",
                principalTable: "UserEmails",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ThirdPartyAdministrators_UserPhoneNumbers_PhoneNumberId",
                table: "ThirdPartyAdministrators",
                column: "PhoneNumberId",
                principalTable: "UserPhoneNumbers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ThirdPartyAdministrators_AspNetRoles_RoleId",
                table: "ThirdPartyAdministrators",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ThirdPartyAdministrators_AspNetUsers_UserId",
                table: "ThirdPartyAdministrators",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ThirdPartyAdministrators_UserAddresses_UserId_AddressId",
                table: "ThirdPartyAdministrators",
                columns: new[] { "UserId", "AddressId" },
                principalTable: "UserAddresses",
                principalColumns: new[] { "UserId", "AddressId" },
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ThirdPartyAdministrators_AspNetUserRoles_UserId_RoleId",
                table: "ThirdPartyAdministrators",
                columns: new[] { "UserId", "RoleId" },
                principalTable: "AspNetUserRoles",
                principalColumns: new[] { "UserId", "RoleId" },
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_InsuranceCompanies_UserEmails_EmailId",
                table: "InsuranceCompanies");

            migrationBuilder.DropForeignKey(
                name: "FK_InsuranceCompanies_UserPhoneNumbers_PhoneNumberId",
                table: "InsuranceCompanies");

            migrationBuilder.DropForeignKey(
                name: "FK_InsuranceCompanies_AspNetRoles_RoleId",
                table: "InsuranceCompanies");

            migrationBuilder.DropForeignKey(
                name: "FK_InsuranceCompanies_AspNetUsers_UserId",
                table: "InsuranceCompanies");

            migrationBuilder.DropForeignKey(
                name: "FK_InsuranceCompanies_UserAddresses_UserId_AddressId",
                table: "InsuranceCompanies");

            migrationBuilder.DropForeignKey(
                name: "FK_InsuranceCompanies_AspNetUserRoles_UserId_RoleId",
                table: "InsuranceCompanies");

            migrationBuilder.DropForeignKey(
                name: "FK_ThirdPartyAdministrators_UserEmails_EmailId",
                table: "ThirdPartyAdministrators");

            migrationBuilder.DropForeignKey(
                name: "FK_ThirdPartyAdministrators_UserPhoneNumbers_PhoneNumberId",
                table: "ThirdPartyAdministrators");

            migrationBuilder.DropForeignKey(
                name: "FK_ThirdPartyAdministrators_AspNetRoles_RoleId",
                table: "ThirdPartyAdministrators");

            migrationBuilder.DropForeignKey(
                name: "FK_ThirdPartyAdministrators_AspNetUsers_UserId",
                table: "ThirdPartyAdministrators");

            migrationBuilder.DropForeignKey(
                name: "FK_ThirdPartyAdministrators_UserAddresses_UserId_AddressId",
                table: "ThirdPartyAdministrators");

            migrationBuilder.DropForeignKey(
                name: "FK_ThirdPartyAdministrators_AspNetUserRoles_UserId_RoleId",
                table: "ThirdPartyAdministrators");

            migrationBuilder.DropIndex(
                name: "IX_ThirdPartyAdministrators_UserId_AddressId",
                table: "ThirdPartyAdministrators");

            migrationBuilder.DropIndex(
                name: "IX_ThirdPartyAdministrators_UserId_RoleId",
                table: "ThirdPartyAdministrators");

            migrationBuilder.DropIndex(
                name: "IX_InsuranceCompanies_UserId_AddressId",
                table: "InsuranceCompanies");

            migrationBuilder.DropIndex(
                name: "IX_InsuranceCompanies_UserId_RoleId",
                table: "InsuranceCompanies");

            migrationBuilder.AlterColumn<int>(
                name: "UserId",
                table: "ThirdPartyAdministrators",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "RoleId",
                table: "ThirdPartyAdministrators",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "PhoneNumberId",
                table: "ThirdPartyAdministrators",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "EmailId",
                table: "ThirdPartyAdministrators",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "AddressId",
                table: "ThirdPartyAdministrators",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "AddressId1",
                table: "ThirdPartyAdministrators",
                type: "integer",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "AddressUserId",
                table: "ThirdPartyAdministrators",
                type: "integer",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserRoleRoleId",
                table: "ThirdPartyAdministrators",
                type: "integer",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserRoleUserId",
                table: "ThirdPartyAdministrators",
                type: "integer",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "UserId",
                table: "InsuranceCompanies",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "RoleId",
                table: "InsuranceCompanies",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "PhoneNumberId",
                table: "InsuranceCompanies",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "EmailId",
                table: "InsuranceCompanies",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "AddressId",
                table: "InsuranceCompanies",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "AddressId1",
                table: "InsuranceCompanies",
                type: "integer",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "AddressUserId",
                table: "InsuranceCompanies",
                type: "integer",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserRoleRoleId",
                table: "InsuranceCompanies",
                type: "integer",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserRoleUserId",
                table: "InsuranceCompanies",
                type: "integer",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ThirdPartyAdministrators_UserId",
                table: "ThirdPartyAdministrators",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_ThirdPartyAdministrators_AddressUserId_AddressId1",
                table: "ThirdPartyAdministrators",
                columns: new[] { "AddressUserId", "AddressId1" });

            migrationBuilder.CreateIndex(
                name: "IX_ThirdPartyAdministrators_UserRoleUserId_UserRoleRoleId",
                table: "ThirdPartyAdministrators",
                columns: new[] { "UserRoleUserId", "UserRoleRoleId" });

            migrationBuilder.CreateIndex(
                name: "IX_InsuranceCompanies_UserId",
                table: "InsuranceCompanies",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_InsuranceCompanies_AddressUserId_AddressId1",
                table: "InsuranceCompanies",
                columns: new[] { "AddressUserId", "AddressId1" });

            migrationBuilder.CreateIndex(
                name: "IX_InsuranceCompanies_UserRoleUserId_UserRoleRoleId",
                table: "InsuranceCompanies",
                columns: new[] { "UserRoleUserId", "UserRoleRoleId" });

            migrationBuilder.AddForeignKey(
                name: "FK_InsuranceCompanies_UserEmails_EmailId",
                table: "InsuranceCompanies",
                column: "EmailId",
                principalTable: "UserEmails",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_InsuranceCompanies_UserPhoneNumbers_PhoneNumberId",
                table: "InsuranceCompanies",
                column: "PhoneNumberId",
                principalTable: "UserPhoneNumbers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_InsuranceCompanies_AspNetRoles_RoleId",
                table: "InsuranceCompanies",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_InsuranceCompanies_AspNetUsers_UserId",
                table: "InsuranceCompanies",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_InsuranceCompanies_UserAddresses_AddressUserId_AddressId1",
                table: "InsuranceCompanies",
                columns: new[] { "AddressUserId", "AddressId1" },
                principalTable: "UserAddresses",
                principalColumns: new[] { "UserId", "AddressId" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_InsuranceCompanies_AspNetUserRoles_UserRoleUserId_UserRoleR~",
                table: "InsuranceCompanies",
                columns: new[] { "UserRoleUserId", "UserRoleRoleId" },
                principalTable: "AspNetUserRoles",
                principalColumns: new[] { "UserId", "RoleId" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ThirdPartyAdministrators_UserEmails_EmailId",
                table: "ThirdPartyAdministrators",
                column: "EmailId",
                principalTable: "UserEmails",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ThirdPartyAdministrators_UserPhoneNumbers_PhoneNumberId",
                table: "ThirdPartyAdministrators",
                column: "PhoneNumberId",
                principalTable: "UserPhoneNumbers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ThirdPartyAdministrators_AspNetRoles_RoleId",
                table: "ThirdPartyAdministrators",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ThirdPartyAdministrators_AspNetUsers_UserId",
                table: "ThirdPartyAdministrators",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ThirdPartyAdministrators_UserAddresses_AddressUserId_Addres~",
                table: "ThirdPartyAdministrators",
                columns: new[] { "AddressUserId", "AddressId1" },
                principalTable: "UserAddresses",
                principalColumns: new[] { "UserId", "AddressId" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ThirdPartyAdministrators_AspNetUserRoles_UserRoleUserId_Use~",
                table: "ThirdPartyAdministrators",
                columns: new[] { "UserRoleUserId", "UserRoleRoleId" },
                principalTable: "AspNetUserRoles",
                principalColumns: new[] { "UserId", "RoleId" },
                onDelete: ReferentialAction.Restrict);
        }
    }
}
