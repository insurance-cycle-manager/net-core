using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using PatientsRecordsManager.ABAC.Data;
using PatientsRecordsManager.ABAC.Enforcers;

namespace PatientsRecordsManager.ABAC.Extensions
{
    public static class AbacServicesExtension
    {
        public static IServiceCollection AddAbac(this IServiceCollection services)
        {
            return services
                .AddDbContext<AbacDbContext>(ServiceLifetime.Scoped)
                .AddScoped<RulesRepository>()
                .AddScoped<ActionEnforcer>()
                .AddScoped<DboEnforcer>()
                .AddScoped<PoliciesManager>();
        }

        public static IApplicationBuilder UseAbacMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseWhen(
                context => context.ShouldUseMiddleware(),
                configuration => configuration.UseMiddleware<AbacMiddleware>());
        }
    }
}