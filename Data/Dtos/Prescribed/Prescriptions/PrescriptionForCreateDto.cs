using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PatientsRecordsManager.Data.Dtos.Prescribed.Prescriptions
{
    public class PrescriptionForCreateDto
    {
        [Required]
        public string Note { get; set; }
        [Required, MinLength(1)]
        public IEnumerable<string> Drugs { get; set; }
    }
}