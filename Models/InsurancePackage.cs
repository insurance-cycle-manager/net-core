using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace PatientsRecordsManager.Models
{
    public class InsurancePackage
    {
        public int Id { get; set; }
        public DateTime PackageDate { get; set; }
        public int InsuranceCompanyId { get; set; }
        public InsuranceCompany InsuranceCompany { get; set; }
        public IEnumerable<PatientPackage> PatientPackages { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public string InsurancePackageCode { get; set; }

        public List<PackageDrug> Drugs { get; set; }
        public List<PackageSurgery> Surgeries { get; set; }
        public List<PackageRadiograph> Radiographs { get; set; }
        public List<PackageLabTest> LabTests { get; set; }
        [ForeignKey("Prices")]
        public int? PriceId { get; set; }
        public Price Price { get; set; }

    }
}