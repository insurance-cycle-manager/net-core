using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PatientsRecordsManager.Models
{
    public class LabTest
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string NormalizedName { get; set; }
        [Required]
        public string LabTestCode { get; set; }

        [ForeignKey("Prices")]
        public int? PriceId { get; set; }
        public Price Price { get; set; }

        public List<PrescribedLabTest> Prescribed { get; set; }
        public List<LabTestPurchase> Purchases { get; set; }
        public List<PackageLabTest> Packages { get; set; }
    }
}