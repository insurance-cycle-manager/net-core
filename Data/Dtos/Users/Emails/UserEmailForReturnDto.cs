namespace PatientsRecordsManager.Data.Dtos.Users.Emails
{
    public class UserEmailForReturnDto
    {
        public string Address { get; set; }
        public string EmailCode { get; set; }
        public bool Confirmed { get; set; }
        public string UserName { get; set; }

    }
}