namespace PatientsRecordsManager.Constants.Enums
{
    public enum ServiceProviderStafferType
    {
        Doctor,
        Nurse,
        Pharmacist,
        Radiographer,
        LabSpecialist,
        Receptionist,
        NoType
    }
}