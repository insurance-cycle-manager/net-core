using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using AutoMapper;
using PatientsRecordsManager.ABAC.Extensions;
using PatientsRecordsManager.ABAC.Extensions.EF;
using PatientsRecordsManager.Constants.Enums;
using PatientsRecordsManager.Data.Dtos.ServiceProviders;
using PatientsRecordsManager.Exceptions;
using PatientsRecordsManager.Models;
using PatientsRecordsManager.Resources;

namespace PatientsRecordsManager.Data.Repositories
{
    public class ServiceProviderRepository : IServiceProviderRepository
    {
        private readonly IMapper _mapper;
        private readonly DataContext _context;
        private readonly Messages _messages;

        public ServiceProviderRepository(IMapper mapper, DataContext context, Messages messages)
        {
            _context = context;
            _mapper = mapper;
            _messages = messages;
        }

        public ServiceProviderForReturnDto AddServiceProvider(ServiceProviderForCreateDto serviceProviderForCreate)
        {
            var serviceProvider = _mapper.Map<ServiceProvider>(serviceProviderForCreate);
            serviceProvider.Status = new Status { Name = StatusType.Active };

            _context.ServiceProviders.Add(serviceProvider);

            if (_context.SaveChanges() <= 0)
            {
                throw new SaveChangesFailedException();
            }
            return _mapper.Map<ServiceProviderForReturnDto>(serviceProvider);
        }

        public ServiceProviderForReturnDto DeleteServiceProvider(ServiceProviderType type, int userId)
        {
            var retrieved = _context.ServiceProviders
                .Include(sp => sp.UserRole)
                .FirstOrDefault(sp => sp.Type.Equals(type) && sp.UserRole.UserId == userId);

            if (retrieved == null)
            {
                throw new ArgumentBadException(_messages.GetString("serviceProviderNotFound"));
            }
            _context.ServiceProviders.Remove(retrieved);

            if (_context.SaveChanges() <= 0)
            {
                throw new SaveChangesFailedException();
            }
            return _mapper.Map<ServiceProviderForReturnDto>(retrieved);
        }

        public ServiceProviderForReturnDto GetServiceProviderDetails(ServiceProviderType type, string code)
        {
            var serviceProvider = _context.ServiceProviders
                .Include(sp => sp.Status)
                .Include(sp => sp.Address)
                .ThenInclude(ua => ua.Address)
                .Include(sp => sp.UserRole)
                .ThenInclude(ur => ur.User)
                .Include(sp => sp.Staff)
                .FirstOrDefault(sp => sp.Type == type && sp.ServiceProviderCode.ToUpper() == (code.ToUpper()));

            if (serviceProvider == null)
            {
                throw new ArgumentBadException(_messages.GetString("serviceProviderNotFound"));
            }
            return _mapper.Map<ServiceProviderForReturnDto>(serviceProvider);
        }

        public ServiceProviderForReturnDto GetServiceProviderDetails(ServiceProviderType type, int userId)
        {
            var serviceProvider = _context.ServiceProviders
                .Include(sp => sp.Status)
                .Include(sp => sp.Address)
                .ThenInclude(ua => ua.Address)
                .Include(sp => sp.UserRole)
                .ThenInclude(ur => ur.User)
                .Include(sp => sp.Staff)
                .FirstOrDefault(sp => sp.Type == type && sp.UserId == userId);

            if (serviceProvider == null)
            {
                throw new ArgumentBadException(_messages.GetString("serviceProviderNotFound"));
            }
            return _mapper.Map<ServiceProviderForReturnDto>(serviceProvider);
        }

        public List<ServiceProviderForReturnDto> GetServiceProviders(ServiceProviderType type, string title = null)
        {
            var serviceProviders = _context.ServiceProviders
                .Include(sp => sp.Status)
                .Include(sp => sp.Address)
                .ThenInclude(ua => ua.Address)
                .Include(sp => sp.UserRole)
                .ThenInclude(ur => ur.User)
                .Where(sp => sp.Type == type);

            if (!string.IsNullOrWhiteSpace(title))
            {
                serviceProviders = serviceProviders.Where(sp => sp.NormalizedTitle.ToUpper().Contains(title.ToUpper()));
            }
            return _mapper.Map<List<ServiceProviderForReturnDto>>(serviceProviders);
        }

        public ServiceProviderForReturnDto ModifyServiceProvider(ServiceProviderType type, string code, ServiceProviderForUpdateDto serviceProviderForUpdate)
        {
            if (serviceProviderForUpdate == null)
            {
                throw new ArgumentBadException(_messages.GetString("serviceProviderNotFound"));
            }

            var retrieved = _context.ServiceProviders
                .Include(sp => sp.UserRole)
                .ThenInclude(ur => ur.User)
                .FirstOrDefault(sp => sp.Type == type && sp.ServiceProviderCode.ToUpper() == (code.ToUpper()));

            if (retrieved == null)
            {
                throw new ArgumentBadException(_messages.GetString("serviceProviderNotFound"));
            }
            var updated = UpdateServiceProvider(retrieved, serviceProviderForUpdate);

            if (_context.SaveChanges() <= 0)
            {
                throw new SaveChangesFailedException(_messages.GetString("failedUpdateServiceProvider"));
            }
            return _mapper.Map<ServiceProviderForReturnDto>(updated);
        }

        public ServiceProviderForReturnDto ModifyServiceProvider(ServiceProviderType type, int userId, ServiceProviderForUpdateDto serviceProviderForUpdate)
        {
            if (serviceProviderForUpdate == null)
            {
                throw new ArgumentBadException(_messages.GetString("serviceProviderNotFound"));
            }

            var retrieved = _context.ServiceProviders
                .FirstOrDefault(sp => sp.UserId == userId && sp.Type == type);

            if (retrieved == null)
            {
                throw new ArgumentBadException(_messages.GetString("serviceProviderNotFound"));
            }
            var updated = UpdateServiceProvider(retrieved, serviceProviderForUpdate);

            if (_context.SaveChanges() <= 0)
            {
                throw new SaveChangesFailedException(_messages.GetString("failedUpdateServiceProvider"));
            }
            return _mapper.Map<ServiceProviderForReturnDto>(updated);
        }

        private ServiceProvider UpdateServiceProvider(ServiceProvider source, ServiceProviderForUpdateDto updateDto)
        {
            var sp = _mapper.Map<ServiceProviderForUpdateDto, ServiceProvider>(updateDto, source);
            if (!string.IsNullOrWhiteSpace(updateDto.UserAddressCode))
            {
                sp.Address = GetAddress(source.UserId, updateDto.UserAddressCode);
            }
            if (!string.IsNullOrWhiteSpace(updateDto.UserEmailCode))
            {
                sp.Email = GetEmail(source.UserId, updateDto.UserEmailCode);
            }
            if (!string.IsNullOrWhiteSpace(updateDto.UserPhoneCode))
            {
                sp.PhoneNumber = GetPhone(source.UserId, updateDto.UserPhoneCode);
            }

            return sp;
        }

        private UserAddress GetAddress(int userId, string addressCode)
        {
            return _context.UserAddresses.Include(ua => ua.Address)
                .FirstOrDefault(ua =>
                   ua.Confirmed &&
                   ua.Address.AddressCode.ToUpper() == (addressCode.ToUpper()) &&
                   ua.UserId == userId) ??
                throw new ArgumentBadException(_messages.GetString("userAddressCodeNotFound"));
        }

        private UserEmail GetEmail(int userId, string emailCode)
        {
            return _context.UserEmails
                .FirstOrDefault(ue =>
                    ue.Confirmed &&
                    ue.EmailCode.ToUpper() == (emailCode.ToUpper()) &&
                    ue.UserId == userId) ??
                throw new ArgumentBadException(_messages.GetString("userEmailCodeNotFound"));
        }

        private UserPhoneNumber GetPhone(int userId, string phoneCode)
        {
            return _context.UserPhoneNumbers
                .FirstOrDefault(up =>
                   up.Confirmed &&
                   up.PhoneCode.ToUpper() == (phoneCode.ToUpper()) &&
                   up.UserId == userId) ??
                throw new ArgumentBadException(_messages.GetString("userPhoneCodeNotFound"));
        }

        public ServiceProviderForReturnDto ChangeServiceProviderStatus(ServiceProviderType type, string code, StatusType status)
        {
            var retrieved = _context.ServiceProviders
                .Include(sp => sp.Status)
                .ThenInclude(s => s.PreviousStatus)
                .FirstOrDefault(sp =>
                    sp.Type == type &&
                    sp.ServiceProviderCode.ToUpper() == (code.ToUpper()));

            if (retrieved == null)
            {
                throw new ArgumentBadException(_messages.GetString("serviceProviderNotFound"));
            }
            retrieved.Status = new Status { Name = status, PreviousStatus = retrieved.Status };

            Console.WriteLine(status);
            Console.WriteLine(retrieved.Status.Name);

            if (_context.SaveChanges() <= 0)
            {
                throw new SaveChangesFailedException(_messages.GetString("failedUpdateServiceProvider"));
            }
            return _mapper.Map<ServiceProviderForReturnDto>(retrieved);
        }
    }
}