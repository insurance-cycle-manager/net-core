using System.Collections.Generic;

namespace PatientsRecordsManager.ABAC.Data.Dtos
{
    public class RuleForModifyDto
    {
        public string V0 { get; set; }

        public string V1 { get; set; }

        public string V2 { get; set; }

        public string V3 { get; set; }

        public object V4 { get; set; }
    }
}