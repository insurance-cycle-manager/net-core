namespace PatientsRecordsManager.Data.Dtos.Prices
{
    public class PriceForReturnDto
    {
        public string PriceCode { get; set; }
        public double Value { get; set; }
    }
}