using System;
using System.ComponentModel.DataAnnotations;

namespace PatientsRecordsManager.Data.Dtos.InsuranceCompanies
{
    public class InsuranceCompanyForUpdateDto
    {
        [Required]
        public DateTime? DateOfFounding { get; set; }
        [Required]
        public DateTime? JoinedAt { get; set; }
    }
}