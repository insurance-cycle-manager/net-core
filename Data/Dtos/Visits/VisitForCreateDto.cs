using System;
using System.ComponentModel.DataAnnotations;
using PatientsRecordsManager.Constants.Enums;

namespace PatientsRecordsManager.Data.Dtos.Visits
{
    public class VisitForCreateDto
    {
        public string BaseVisitCode { get; set; }
        [Required]
        public string PatientCode { get; set; }
        [Required]
        public string ServiceProviderCode { get; set; }
        // [Required]
        // public string ServiceStafferCode { get; set; }
        [Required]
        public DateTime VisitDate { get; set; }
        [Required]
        [EnumDataType(typeof(VisitType))]
        public VisitType? Type { get; set; }
        [Required]
        public double? Cost { get; set; }

        #region
        // These fields determine if the visit is Claimable
        // Claimable visit means that it visited under specific connection and package
        // The connection and the package are required in order to review the visit and make sure the is valid
        [Required]
        public string ServiceProviderConnectionCode { get; set; }
        [Required]
        public string PatientPackageCode { get; set; }
        #endregion

        VisitForCreateDto()
        {
            VisitDate = DateTime.Now;
        }
    }
}