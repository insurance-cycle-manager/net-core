using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PatientsRecordsManager.Models
{
    public class Permission
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        public ICollection<RolePermission> Roles { get; set; }
    }
}