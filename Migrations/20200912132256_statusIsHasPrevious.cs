﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PatientsRecordsManager.Migrations
{
    public partial class statusIsHasPrevious : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "PreviousStatusId",
                table: "Statuses",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Statuses_PreviousStatusId",
                table: "Statuses",
                column: "PreviousStatusId");

            migrationBuilder.AddForeignKey(
                name: "FK_Statuses_Statuses_PreviousStatusId",
                table: "Statuses",
                column: "PreviousStatusId",
                principalTable: "Statuses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Statuses_Statuses_PreviousStatusId",
                table: "Statuses");

            migrationBuilder.DropIndex(
                name: "IX_Statuses_PreviousStatusId",
                table: "Statuses");

            migrationBuilder.DropColumn(
                name: "PreviousStatusId",
                table: "Statuses");
        }
    }
}
