﻿using System.ComponentModel.DataAnnotations;

namespace PatientsRecordsManager.Data.Dtos.Users
{
    public class UserForLoginDto
    {
        [Required]
        public string UserName { get; set; }

        [Required]
        [StringLength(maximumLength: 24, MinimumLength = 8)]
        public string Password { get; set; }
    }
}
