using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using NetCasbin;

namespace PatientsRecordsManager.ABAC.Functions
{
    public class MatchSubjects : AbstractFunction
    {
        public MatchSubjects(string name) : base(name)
        {
        }

        protected override Delegate GetFunc()
        {
            Func<string, ClaimsPrincipal, bool> myFunction = (pSubject, rSubject) =>
            {
                if (pSubject == null || (rSubject == null && "anonymous" != pSubject))
                    return false;

                if(pSubject == "**")
                {
                    return true;
                }

                int pInt;
                if (int.TryParse(pSubject, out pInt))
                {
                    int r = Convert.ToInt32(rSubject.FindFirstValue(ClaimTypes.NameIdentifier), CultureInfo.CurrentCulture);
                    if ("anonymous" != pSubject && -1 != pInt && pInt != r)
                    {
                        return false;
                    }
                }
                else
                {
                    if ("anonymous" != pSubject && !rSubject.IsInRole(pSubject))
                    {
                        return false;
                    }
                }
                return true;
            };
            return myFunction;
        }
    }
}