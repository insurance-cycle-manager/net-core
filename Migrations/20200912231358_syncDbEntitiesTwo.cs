﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PatientsRecordsManager.Migrations
{
    public partial class syncDbEntitiesTwo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DrugPurchases_Visits_VisitId",
                table: "DrugPurchases");

            migrationBuilder.DropIndex(
                name: "IX_DrugPurchases_VisitId",
                table: "DrugPurchases");

            migrationBuilder.DropColumn(
                name: "VisitId",
                table: "DrugPurchases");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "VisitId",
                table: "DrugPurchases",
                type: "integer",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_DrugPurchases_VisitId",
                table: "DrugPurchases",
                column: "VisitId");

            migrationBuilder.AddForeignKey(
                name: "FK_DrugPurchases_Visits_VisitId",
                table: "DrugPurchases",
                column: "VisitId",
                principalTable: "Visits",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
