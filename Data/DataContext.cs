﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using PatientsRecordsManager.ABAC;
using PatientsRecordsManager.ABAC.Attributes;
using PatientsRecordsManager.ABAC.Extensions;
using PatientsRecordsManager.Models;

namespace PatientsRecordsManager.Data
{
    public class DataContext : IdentityDbContext<User, Role, int, IdentityUserClaim<int>, UserRole, IdentityUserLogin<int>, IdentityRoleClaim<int>, IdentityUserToken<int>>
    {
        private readonly IMapper mapper;
        private readonly PoliciesManager policiesManager;

        public DbSet<Address> Addresses { get; set; }
        public DbSet<UserEmail> UserEmails { get; set; }
        public DbSet<UserPhoneNumber> UserPhoneNumbers { get; set; }
        public DbSet<UserAddress> UserAddresses { get; set; }
        public DbSet<UserRoleRequest> UserRoleRequests { get; set; }
        public DbSet<InsuranceCompany> InsuranceCompanies { get; set; }
        public DbSet<InsuranceConnection> InsuranceConnections { get; set; }
        [Abac]
        public DbSet<InsurancePackage> InsurancePackages { get; set; }
        public DbSet<Patient> Patients { get; set; }
        public DbSet<PatientPackage> PatientPackages { get; set; }
        public DbSet<Permission> Permissions { get; set; }
        public DbSet<RolePermission> RolePermissions { get; set; }
        public DbSet<ServiceProvider> ServiceProviders { get; set; }
        public DbSet<ServiceStaffer> ServiceStaffers { get; set; }
        public DbSet<ServiceProviderStaffer> ServiceProviderStaffer { get; set; }
        public DbSet<SupervisoryComission> SupervisoryComissions { get; set; }
        public DbSet<ThirdPartyAdministrator> ThirdPartyAdministrators { get; set; }
        public DbSet<Visit> Visits { get; set; }
        public DbSet<VisitClaim> VisitClaims { get; set; }
        public DbSet<ServiceProviderConnection> ServiceProviderConnections { get; set; }
        public DbSet<ServiceProviderConnectionInsuranceCompanies> ServiceProviderConnectionInsuranceCompanies { get; set; }
        public DbSet<VisitDiagnosis> Diagnoses { get; set; }
        public DbSet<Status> Statuses { get; set; }
        public DbSet<Drug> Drugs { get; set; }
        public DbSet<PackageDrug> PackageDrugs { get; set; }
        public DbSet<Price> Prices { get; set; }
        public DbSet<Prescription> Prescriptions { get; set; }
        public DbSet<PrescriptionDrug> PrescriptionDrugs { get; set; }
        public DbSet<PrescriptionPurchase> PrescriptionPurchases { get; set; }
        public DbSet<Surgery> Surgeries { get; set; }
        public DbSet<PrescribedSurgery> PrescribedSurgeries { get; set; }
        public DbSet<SurgeryPurchase> SurgeryPurchases { get; set; }
        public DbSet<PackageSurgery> PackageSrugeries { get; set; }
        public DbSet<Radiograph> Radiographs { get; set; }
        public DbSet<PrescribedRadiograph> PrescribedRadiographs { get; set; }
        public DbSet<RadiographPurchase> RadiographPurchases { get; set; }
        public DbSet<PackageRadiograph> PackageRadiographs { get; set; }
        public DbSet<LabTest> LabTests { get; set; }
        public DbSet<PrescribedLabTest> PrescribedLabTests { get; set; }
        public DbSet<LabTestPurchase> LabTestPurchases { get; set; }
        public DbSet<DrugPurchase> DrugPurchases { get; set; }
        public DbSet<PackageLabTest> PackageLabTests { get; set; }

        public DataContext(DbContextOptions<DataContext> options, IMapper mapper, PoliciesManager policiesManager) : base(options)
        {
            this.policiesManager = policiesManager;
            this.mapper = mapper;
        }

        protected override void OnModelCreating([NotNullAttribute] ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            this.ConfigureEntities(builder);
        }

        public override EntityEntry Add([NotNullAttribute] object entity)
        {
            return base.Add(entity.Visit(Evaluate.Action.WRITE));
        }

        public override EntityEntry<TEntity> Add<TEntity>([NotNullAttribute] TEntity entity) where TEntity : class
        {
            return base.Add<TEntity>(entity.Visit(Evaluate.Action.WRITE));
        }

        public override ValueTask<EntityEntry> AddAsync([NotNullAttribute] object entity, CancellationToken cancellationToken = default)
        {
            return base.AddAsync(entity.Visit(Evaluate.Action.WRITE));
        }

        public override ValueTask<EntityEntry<TEntity>> AddAsync<TEntity>([NotNullAttribute] TEntity entity, CancellationToken cancellationToken = default) where TEntity : class
        {
            return base.AddAsync<TEntity>(entity.Visit(Evaluate.Action.WRITE), cancellationToken);
        }

        public override void AddRange([NotNullAttribute] params object[] entities)
        {
            base.AddRange(entities.Visit(Evaluate.Action.WRITE));
        }

        public override void AddRange([NotNullAttribute] IEnumerable<object> entities)
        {
            base.AddRange(entities.Visit(Evaluate.Action.WRITE));
        }

        public override Task AddRangeAsync([NotNullAttribute] IEnumerable<object> entities, CancellationToken cancellationToken = default)
        {
            return base.AddRangeAsync(entities.Visit(Evaluate.Action.WRITE), cancellationToken);
        }

        public override Task AddRangeAsync([NotNullAttribute] params object[] entities)
        {
            return base.AddRangeAsync(entities.Visit(Evaluate.Action.WRITE));
        }

        public override EntityEntry Attach([NotNullAttribute] object entity)
        {
            return base.Attach(entity.Visit(Evaluate.Action.WRITE));
        }

        public override EntityEntry<TEntity> Attach<TEntity>([NotNullAttribute] TEntity entity) where TEntity : class
        {
            return base.Attach(entity.Visit(Evaluate.Action.WRITE));
        }

        public override void AttachRange([NotNullAttribute] IEnumerable<object> entities)
        {
            base.Attach(entities.Visit(Evaluate.Action.WRITE));
        }

        public override void AttachRange([NotNullAttribute] params object[] entities)
        {
            base.AddRange(entities.Visit(Evaluate.Action.WRITE));
        }

        public override EntityEntry Entry([NotNullAttribute] object entity)
        {
            return base.Entry(entity.Visit(Evaluate.Action.WRITE));
        }

        public override EntityEntry<TEntity> Entry<TEntity>([NotNullAttribute] TEntity entity) where TEntity : class
        {
            return base.Entry(entity.Visit(Evaluate.Action.WRITE));
        }

        public override TEntity Find<TEntity>(params object[] keyValues) where TEntity : class
        {
            System.Linq.Enumerable.Empty<TEntity>().Visit(Evaluate.Action.READ);
            return base.Find<TEntity>(keyValues);
        }

        public override object Find([NotNullAttribute] Type entityType, params object[] keyValues)
        {
            return base.Find(entityType.Visit(Evaluate.Action.READ), keyValues);
        }

        public override ValueTask<object> FindAsync([NotNullAttribute] Type entityType, object[] keyValues, CancellationToken cancellationToken)
        {
            return base.FindAsync(entityType.Visit(Evaluate.Action.READ), keyValues, cancellationToken);
        }

        public override ValueTask<object> FindAsync([NotNullAttribute] Type entityType, params object[] keyValues)
        {
            return base.FindAsync(entityType.Visit(Evaluate.Action.READ), keyValues);
        }

        public override ValueTask<TEntity> FindAsync<TEntity>(object[] keyValues, CancellationToken cancellationToken) where TEntity : class
        {
            System.Linq.Enumerable.Empty<TEntity>().Visit(Evaluate.Action.READ);
            return base.FindAsync<TEntity>(keyValues, cancellationToken);
        }

        public override ValueTask<TEntity> FindAsync<TEntity>(params object[] keyValues) where TEntity : class
        {
            System.Linq.Enumerable.Empty<TEntity>().Visit(Evaluate.Action.READ);
            return base.FindAsync<TEntity>(keyValues);
        }

        public override EntityEntry Remove([NotNullAttribute] object entity)
        {
            return base.Remove(entity.Visit(Evaluate.Action.WRITE));
        }

        public override EntityEntry<TEntity> Remove<TEntity>([NotNullAttribute] TEntity entity) where TEntity : class
        {
            return base.Remove(entity.Visit<TEntity>(Evaluate.Action.WRITE));
        }

        public override void RemoveRange([NotNullAttribute] IEnumerable<object> entities)
        {
            base.RemoveRange(entities.Visit(Evaluate.Action.WRITE));
        }

        public override void RemoveRange([NotNullAttribute] params object[] entities)
        {
            base.RemoveRange(entities.Visit(Evaluate.Action.WRITE));
        }

        public override DbSet<TEntity> Set<TEntity>() where TEntity : class
        {
            return base.Set<TEntity>().Visit(Evaluate.Action.READ);
        }

        public override EntityEntry Update([NotNullAttribute] object entity)
        {
            return base.Update(entity.Visit(Evaluate.Action.WRITE));
        }

        public override EntityEntry<TEntity> Update<TEntity>([NotNullAttribute] TEntity entity) where TEntity : class
        {
            return base.Update(entity.Visit<TEntity>(Evaluate.Action.WRITE));
        }

        public override void UpdateRange([NotNullAttribute] IEnumerable<object> entities)
        {
            base.UpdateRange(entities.Visit(Evaluate.Action.WRITE));
        }

        public override void UpdateRange([NotNullAttribute] params object[] entities)
        {
            base.UpdateRange(entities.Visit(Evaluate.Action.WRITE));
        }
    }
}