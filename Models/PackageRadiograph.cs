namespace PatientsRecordsManager.Models
{
    public class PackageRadiograph
    {
        public int InsurancePackageId { get; set; }
        public InsurancePackage InsurancePackage { get; set; }

        public int RadiographId { get; set; }
        public Radiograph Radiograph { get; set; }
    }
}