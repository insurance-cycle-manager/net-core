using PatientsRecordsManager.Data.Dtos.ServiceProviders.Staff;

namespace PatientsRecordsManager.Data.Repositories
{
    public interface IServiceProviderStaffRepository
    {
        System.Collections.Generic.IEnumerable<MemberForReturnDto> GetStaffByProvider(string spCode);
        System.Collections.Generic.IEnumerable<MemberForReturnDto> GetStaffByProvider(int spUserId);
        System.Collections.Generic.IEnumerable<MemberForReturnDto> GetStaffByStaffer(string stCode);
        System.Collections.Generic.IEnumerable<MemberForReturnDto> GetStaffByStaffer(int stUserId);
        MemberForReturnDto AddStafferMember(int providerId, string providerCode, MemberForAddDto member);
        MemberForReturnDto RemoveStaffer(int providerId, string providerCode, string stafferCode);
        MemberForReturnDto ConfirmStafferMember(string providerCode, int stafferId, string stafferCode);
    }
}