namespace PatientsRecordsManager.Constants.Enums
{
    public enum ServiceProviderStafferStatus
    {
        Pending,
        Confirmed,
        Holding
    }
}