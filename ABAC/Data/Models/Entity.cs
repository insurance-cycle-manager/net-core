using System.Collections.Generic;

namespace PatientsRecordsManager.ABAC.Data.Models
{
    public class Entity
    {
        public string Name { get; set; }
        public IEnumerable<string> Properties { get; set; }
    }
}