using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using PatientsRecordsManager.Constants;
using PatientsRecordsManager.Data;
using PatientsRecordsManager.Extensions;
using PatientsRecordsManager.Models;
using System.Collections.Generic;
using PatientsRecordsManager.Resources;
using PatientsRecordsManager.Data.Dtos.InsuranceConnections;
using System.Linq;
using PatientsRecordsManager.ABAC.Extensions.EF;

namespace PatientsRecordsManager.Controllers
{
    [Route(APIs.InsuranceConnectionsBaseUrl)]
    [ApiController]
    [Authorize]
    public class InsuranceConnectionsController : PrmController
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;
        private readonly Messages _messages;

        public InsuranceConnectionsController(DataContext dataContext, IMapper mapper, Messages messages)
        {
            _mapper = mapper;
            _messages = messages;
            _dataContext = dataContext;
        }

        [HttpPost(APIs.AddConnection)]
        [Authorize(Roles = RolesNames.Administrator)]
        public IActionResult CreateInsuranceConnection([FromBody] IcConnectionForManageDto connectionDto)
        {
            var ic = _dataContext
                .InsuranceCompanies
                .Include(ic => ic.Email)
                .FirstOrDefault(i => i.InsuranceCompanyCode.ToUpper() == connectionDto.InsuranceCompanyCode.ToUpper());

            var tpa = _dataContext
                .ThirdPartyAdministrators
                .Include(tpa => tpa.Email)
                .FirstOrDefault(t => t.ThirdPartyCode.ToUpper() == connectionDto.ThirdPartyCode.ToUpper());

            if (ic == null || tpa == null)
                throw new KeyNotFoundException(_messages.GetString("IcOrTpaNotFound"));

            InsuranceConnection connection = new InsuranceConnection
            {
                InsuranceCompany = ic,
                ThirdPartyAdministrator = tpa,
                DateOfConnection = DateTime.Now
            };
            _dataContext.InsuranceConnections.Add(connection);

            if (_dataContext.SaveChanges() < 1)
            {
                throw new Exception(_messages.GetString("InsuranceConnectionFailed"));
            }

            return HttpContext.Success(
                _mapper.Map<IcConnectionForReturnDto>(connection),
                _messages.GetString("InsuranceConnectionAdded"));
        }

        [HttpDelete(APIs.DeleteConnection)]
        [Authorize(Roles = RolesNames.Administrator)]
        public IActionResult DeleteConnection(string connectionCode)
        {
            var connection = _dataContext.InsuranceConnections
                .Include(c => c.InsuranceCompany)
                .Include(c => c.ThirdPartyAdministrator)
                .FirstOrDefault(c => c.InsuranceConnectionCode.ToUpper() == connectionCode.ToUpper());

            if (connection == null)
                throw new KeyNotFoundException(_messages.GetString("IcOrTpaNotFound"));

            _dataContext.InsuranceConnections.Remove(connection);

            if (_dataContext.SaveChanges() < 1)
                throw new Exception(_messages.GetString("InsuranceConnectionFailed"));

            return HttpContext.Success(
                _mapper.Map<IcConnectionForReturnDto>(connection),
                _messages.GetString("ConnectionDeleted"));
        }
    }
}