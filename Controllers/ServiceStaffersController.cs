using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PatientsRecordsManager.Constants;
using PatientsRecordsManager.Constants.Enums;
using PatientsRecordsManager.Data.Dtos.ServiceProviders.Staffers;
using PatientsRecordsManager.Data.Repositories;
using PatientsRecordsManager.Extensions;
using PatientsRecordsManager.Resources;

namespace PatientsRecordsManager.Controllers
{
    [ApiController]
    [Route(APIs.ServiceStaffersBaseUrl)]
    [Authorize]
    public class ServiceStaffersController : PrmController
    {
        private readonly IServiceStafferRepository _staffersRepository;
        private readonly IServiceProviderStaffRepository _staffRepository;
        private readonly Messages _messages;

        public ServiceStaffersController(IServiceStafferRepository staffer, IServiceProviderStaffRepository staff, Messages messages)
        {
            _messages = messages;
            _staffersRepository = staffer;
            _staffRepository = staff;
        }

        [HttpGet(APIs.GetServiceStaffers)]
        public IActionResult GetServiceStaffersByType(ServiceProviderStafferType stType, [FromQuery] string title)
        {
            return HttpContext.Success(_staffersRepository.GetServiceStaffers(stType, title), _messages.GetString("ServiceStaffersFetched"));
        }

        [HttpGet(APIs.GetMeServiceStafferDetails, Order = 1)]
        [Authorize(Roles = RolesNames.GetStaffersCommaDelimited)]
        public IActionResult GetMeServiceStaffer(ServiceProviderStafferType stType)
        {
            return HttpContext.Success(_staffersRepository.GetServiceStafferDetails(stType, HttpContext.GetUserId()), _messages.GetString("ServiceStafferFetched"));
        }

        [HttpGet(APIs.GetServiceStafferDetails, Order = 2)]
        public IActionResult GetServiceStafferByType(ServiceProviderStafferType stType, string stCode)
        {
            return HttpContext.Success(_staffersRepository.GetServiceStafferDetails(stType, stCode), _messages.GetString("ServiceStafferFetched"));
        }

        [HttpPut(APIs.ModifyMeServiceStaffer, Order = 1)]
        [Authorize(Roles = RolesNames.GetStaffersCommaDelimited)]
        public IActionResult ModifyMyServiceStaffer(ServiceProviderStafferType stType, StafferForUpdateDto staffer)
        {
            return HttpContext.Success(_staffersRepository.ModifyServiceStaffer(stType, HttpContext.GetUserId(), staffer), _messages.GetString("ServiceStafferModifyDone"));
        }

        [HttpPut(APIs.ModifyServiceStaffer, Order = 2)]
        [Authorize(Roles = RolesNames.Administrator)]
        public IActionResult ModifyServiceStaffer(ServiceProviderStafferType stType, string stCode, StafferForUpdateDto staffer)
        {
            return HttpContext.Success(_staffersRepository.ModifyServiceStaffer(stType, stCode, staffer), _messages.GetString("ServiceStafferModifyDone"));
        }

        [HttpPut(APIs.ChangeServiceStafferStatus)]
        [Authorize(Roles = RolesNames.Administrator)]
        public IActionResult ChangeServiceStafferStatus(ServiceProviderStafferType spType, string stCode, StatusType newStatus)
        {
            return HttpContext.Success(
                _staffersRepository.ChangeServiceStafferStatus(spType, stCode, newStatus),
                _messages.GetString("ServiceStafferStatusChangeDone"));
        }


        [HttpGet(APIs.GetStafferMeStaff, Order = 1)]
        [Authorize(Roles = RolesNames.GetStaffersCommaDelimited)]
        public IActionResult GetStafferMeProviders()
        {
            return HttpContext.Success(
                _staffRepository.GetStaffByStaffer(HttpContext.GetUserId()),
                _messages.GetString("StafferStaffFetchDone", ""));
        }

        [HttpGet(APIs.GetStafferStaff, Order = 2)]
        // [Authorize(Roles = RolesNames.Administrator)]
        public IActionResult GetStafferProviders(string stCode)
        {
            return HttpContext.Success(_staffRepository.GetStaffByStaffer(stCode), _messages.GetString("StafferStaffFetchDone", stCode));
        }

        [HttpPut(APIs.ConfirmStaffMember)]
        [Authorize(Roles = RolesNames.GetStaffersCommaDelimitedWithAdmin)]
        public IActionResult ConfirmMembership(string spCode, string stCode)
        {
            var id = HttpContext.GetUserId();
            if (HttpContext.User.IsInRole(RolesNames.Administrator))
            {
                id = -1;
            }
            return HttpContext.Success(_staffRepository.ConfirmStafferMember(spCode, id, stCode), _messages.GetString("MemberConfirmDone"));
        }
    }
}