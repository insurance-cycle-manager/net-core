using System.ComponentModel.DataAnnotations;
using PatientsRecordsManager.Data.Dtos.Addresses;

namespace PatientsRecordsManager.Data.Dtos.Users.Addresses
{
    public class UserAddressForReturnDto
    {
        [Required]
        public AddressForReturnDto Address { get; set; }
        [Required]
        public bool Confirmed { get; set; }
        public string UserName { get; set; }
    }
}