using System;
using System.Collections.Generic;
using PatientsRecordsManager.Data.Dtos.Drugs;
using PatientsRecordsManager.Data.Dtos.Prices;
using PatientsRecordsManager.Data.Dtos.Visits;

namespace PatientsRecordsManager.Data.Dtos.Prescribed.Prescriptions
{
    public class PrescriptionPurchaseForReturnDto
    {
        public VisitForReturnDto Visit { get; set; }
        public PrescriptionForReturnDto Prescription { get; set; }
        public DateTime DateOfPurchasing { get; set; }
        public PriceForReturnDto Price { get; set; }
        public IEnumerable<DrugPurchaseForReturnDto> PurchasedDrugs { get; set; }
    }
}