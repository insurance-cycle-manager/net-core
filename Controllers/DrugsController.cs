using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PatientsRecordsManager.Constants;
using PatientsRecordsManager.Data.Dtos.Drugs;
using PatientsRecordsManager.Data.Repositories;
using PatientsRecordsManager.Extensions;
using PatientsRecordsManager.Resources;
using PatientsRecordsManager.Utils;

namespace PatientsRecordsManager.Controllers
{
    [ApiController]
    [Route(APIs.DefaultRoute)]
    [Authorize]
    public class DrugsController : PrmController
    {
        private readonly IDrugRepository _repository;
        private readonly IMapper _mapper;
        private readonly Messages _messages;

        public DrugsController(IDrugRepository repository, IMapper mapper, Messages messages)
        {
            _repository = repository;
            _mapper = mapper;
            _messages = messages;
        }

        [HttpGet(APIs.GetDrugs)]
        public IActionResult GetDrugs([FromQuery] string name, [FromQuery] string manufacturer)
        {
            return HttpContext.Success(_repository.GetDrugs(name, manufacturer), _messages.GetString("DrugsGetDone"));
        }

        [HttpGet(APIs.GetDrugDetails)]
        public IActionResult GetDrugDetails(string drugCode)
        {
            return HttpContext.Success(_repository.GetDrugDetails(drugCode), _messages.GetString("DrugDetailsGetDone", drugCode));
        }

        [HttpPost(APIs.PostDrug)]
        public IActionResult AddDrug(DrugForCreateDto drug)
        {
            Assert.NotNull(drug);
            if (drug.Price != null && !HttpContext.User.IsInRole(RolesNames.Administrator))
            {
                return HttpContext.Unauthorized(_messages.GetString("RadiographPriceAdmin"));
            }
            return HttpContext.Success(_repository.AddDrug(drug), _messages.GetString("DrugAddDone"));
        }

        [HttpDelete(APIs.DeleteDrug)]
        public IActionResult DeleteDrug(string drugCode)
        {
            bool done = _repository.DeleteDrug(drugCode);
            if (!done)
            {
                return HttpContext.Error(drugCode, _messages.GetString("DrugDeleteFailed", drugCode));
            }
            return HttpContext.Success(drugCode, _messages.GetString("DrugDeleteDone", drugCode));
        }

        [HttpPut(APIs.ModifyDrug)]
        public IActionResult ModifyDrug(string drugCode, DrugForUpdateDto drug)
        {
            Assert.NotNull(drug);
            if (drug.Price != null && !HttpContext.User.IsInRole(RolesNames.Administrator))
            {
                return HttpContext.Unauthorized(_messages.GetString("RadiographPriceAdmin"));
            }
            return HttpContext.Success(_repository.ModifyDrug(drugCode, drug), _messages.GetString("DrugModifyDone", drugCode));
        }
    }
}