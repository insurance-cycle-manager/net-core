using System.Globalization;
using System.IO;
using Microsoft.AspNetCore.Http;
using PatientsRecordsManager.Utils;

namespace PatientsRecordsManager.Extensions
{
    public static class DocumentNameExtension
    {
        public static string GenerateFileName(this IFormFile file)
        {
            Assert.NotNull(file);
            return string.Format(
                    CultureInfo.CurrentCulture,
                    "document_{0}{1}", System.DateTime.Now.Ticks, Path.GetExtension(file.FileName));
        }
    }
}