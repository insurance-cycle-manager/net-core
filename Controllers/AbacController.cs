using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PatientsRecordsManager.ABAC.Data;
using PatientsRecordsManager.ABAC.Data.Models;
using PatientsRecordsManager.Extensions;
using PatientsRecordsManager.Resources;

namespace PatientsRecordsManager.Controllers
{
    [ApiController]
    [Route(AbacBaseRoute)]
    public class AbacController : PrmController
    {
        private const string ModelsNameSpace = "PatientsRecordsManager.Models";
        private const string AbacBaseRoute = "[controller]";
        private const string FetchEntitiesRoute = "entities";

        private readonly RulesRepository rules;
        private readonly IMapper mapper;
        private readonly Messages messages;

        public AbacController(RulesRepository rules, IMapper mapper, Messages messages)
        {
            this.messages = messages;
            this.rules = rules;
            this.mapper = mapper;
        }

        [HttpGet(FetchEntitiesRoute)]
        public IActionResult FetchEntities()
        {
            return HttpContext.Success(FetchListOfEntities(), messages.GetString("abacEntitiesFetched"));
        }

        private IEnumerable<Entity> FetchListOfEntities()
        {
            return Assembly
                .GetExecutingAssembly()
                .GetTypes()
                .Where(e => String.Equals(e.Namespace, ModelsNameSpace, StringComparison.Ordinal))
                .Select(e => new Entity
                {
                    Name = e.Name,
                    Properties = e.GetProperties().Select(p => p.Name)
                });
        }
    }
}