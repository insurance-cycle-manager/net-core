using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PatientsRecordsManager.Constants;
using PatientsRecordsManager.Data.Dtos.Addresses;
using PatientsRecordsManager.Data.Repositories;
using PatientsRecordsManager.Extensions;
using PatientsRecordsManager.Resources;

namespace PatientsRecordsManager.Controllers
{
    [ApiController]
    [Route(APIs.AddressesBaseUrl)]
    [Authorize]
    public class AddressesController : PrmController
    {
        private readonly IAddressRepository _repository;
        private readonly IMapper _mapper;
        private readonly Resources.Messages _messages;

        public AddressesController(IAddressRepository repository, IMapper mapper, Resources.Messages messages)
        {
            _messages = messages;
            _mapper = mapper;
            _repository = repository;
        }

        [HttpGet(APIs.GetAddresses)]
        public IActionResult GetAddresses([FromQuery] string title, [FromQuery] string addressCode)
        {
            return HttpContext.Success(_repository.GetAddresses(title, addressCode), _messages.GetString("addressesFetchDone"));
        }

        [HttpPost(APIs.PostAddress)]
        [Authorize(Roles = RolesNames.Administrator)]
        public IActionResult AddAddress(AddressForCreateDto address)
        {
            return HttpContext.Success(_repository.AddAddress(address), _messages.GetString("addressPostDone"));
        }

        [HttpPut(APIs.ModifyAddress)]
        [Authorize(Roles = RolesNames.Administrator)]
        public IActionResult ModifyAddress(string addressCode, AddressForUpdateDto address)
        {
            return HttpContext.Success(_repository.ModifyAddress(addressCode, address), _messages.GetString("addressPostDone"));
        }
    }
}