using System.Collections.Generic;
using PatientsRecordsManager.Data.Dtos.TPAs;

namespace PatientsRecordsManager.Data.Repositories
{
    public interface ITpaRepository
    {
        IEnumerable<TpaForReturnDto> GetTPAs();
        TpaForReturnDto AddTpa(TpaForCreateDto tpaData);
        TpaForReturnDto RemoveTpa(int tpaUserId);
        TpaForReturnDto ModifyTpa(int tpaUserId, TpaForUpdateDto data);
        TpaForReturnDto ModifyTpa(string tpaCode, TpaForUpdateDto data);
        TpaForReturnDto GetTpaDetails(string code);
        TpaForReturnDto GetTpaDetails(int tpaUserId);
    }
}