using System;

namespace PatientsRecordsManager.Data.Dtos.Prescribed.Surgeries
{
    public class PrescribedSurgeryForUpdateDto
    {
        public string SurgeryCode { get; set; }
        public DateTime ScheduledDate { get; set; }
        public string Note { get; set; }
    }
}