using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using PatientsRecordsManager.Resources;
using PatientsRecordsManager.Constants.Enums;
using PatientsRecordsManager.Data.Dtos.ServiceProviders.Staffers;
using PatientsRecordsManager.Exceptions;
using PatientsRecordsManager.Models;
using System;
using PatientsRecordsManager.ABAC.Extensions.EF;
using PatientsRecordsManager.ABAC.Extensions;

namespace PatientsRecordsManager.Data.Repositories
{
    public class ServiceStafferRepository : IServiceStafferRepository
    {
        private readonly IMapper _mapper;
        private readonly DataContext _context;
        private readonly Messages _messages;

        public ServiceStafferRepository(IMapper mapper, DataContext context, Messages messages)
        {
            _messages = messages;
            _context = context;
            _mapper = mapper;
        }

        public StafferForReturnDto AddServiceStaffer(StafferForCreateDto StafferForCreate)
        {
            var serviceStaffer = _mapper.Map<ServiceStaffer>(StafferForCreate);
            serviceStaffer.Status = new Status { Name = StatusType.Active };
            _context.ServiceStaffers.Add(serviceStaffer);

            if (_context.SaveChanges() <= 0)
            {
                throw new SaveChangesFailedException();
            }
            return _mapper.Map<StafferForReturnDto>(serviceStaffer);
        }

        public StafferForReturnDto DeleteServiceStaffer(ServiceProviderStafferType type, int userId)
        {
            var retrieved = _context.ServiceStaffers
                .FirstOrDefault(staffer => staffer.Type == type && staffer.UserId == userId);

            if (retrieved == null)
            {
                throw new ArgumentBadException(_messages.GetString("serviceStafferNotFound"));
            }
            _context.ServiceStaffers.Remove(retrieved);

            if (_context.SaveChanges() <= 0)
            {
                throw new SaveChangesFailedException();
            }
            return _mapper.Map<StafferForReturnDto>(retrieved);
        }

        public StafferForReturnDto GetServiceStafferDetails(ServiceProviderStafferType type, string stafferCode)
        {
            var serviceStaffer = _context.ServiceStaffers
                .Include(staffer => staffer.ServiceProviders)
                .ThenInclude(sp => sp.ServiceProvider)
                .Include(staffer => staffer.Visits)
                .Include(staffer => staffer.User)
                .FirstOrDefault(staffer =>
                   staffer.Type == type && staffer.ServiceStafferCode.ToUpper() == (stafferCode.ToUpper()));

            if (serviceStaffer == null)
            {
                throw new ArgumentBadException(_messages.GetString("serviceStafferNotFound"));
            }
            return _mapper.Map<StafferForReturnDto>(serviceStaffer);
        }

        public StafferForReturnDto GetServiceStafferDetails(ServiceProviderStafferType type, int userId)
        {
            var serviceStaffer = _context.ServiceStaffers
                .Include(staffer => staffer.ServiceProviders)
                .ThenInclude(stp => stp.ServiceProvider)
                .Include(staffer => staffer.Visits)
                .Include(staffer => staffer.User)
                .FirstOrDefault(staffer => staffer.Type == type && staffer.UserId == userId);

            if (serviceStaffer == null)
            {
                throw new ArgumentBadException(_messages.GetString("serviceStafferNotFound"));
            }
            return _mapper.Map<StafferForReturnDto>(serviceStaffer);
        }

        public List<StafferForReturnDto> GetServiceStaffers(ServiceProviderStafferType type, string title = null)
        {
            IQueryable<ServiceStaffer> serviceStaffers;

            if (type == ServiceProviderStafferType.NoType)
            {
                serviceStaffers = _context.ServiceStaffers;
            }
            else
            {
                serviceStaffers = _context.ServiceStaffers
                   .Where(staffer => staffer.Type == type);
            }

            if (!string.IsNullOrWhiteSpace(title))
            {
                serviceStaffers = serviceStaffers.Where(staffer => staffer.NormalizedTitle.ToUpper().Contains(title.ToUpper()));
            }
            return _mapper.Map<List<StafferForReturnDto>>(serviceStaffers);
        }

        public StafferForReturnDto ModifyServiceStaffer(ServiceProviderStafferType type, string stafferCode, StafferForUpdateDto StafferForUpdate)
        {
            if (StafferForUpdate == null)
            {
                throw new ArgumentBadException(_messages.GetString("stafferNotFound"));
            }

            var retrieved = _context.ServiceStaffers
                .FirstOrDefault(staffer => staffer.Type == type && staffer.ServiceStafferCode.ToUpper() == (stafferCode.ToUpper()));

            if (retrieved == null)
            {
                throw new ArgumentBadException(_messages.GetString("serviceStafferNotFound"));
            }
            var updated = UpdateServiceStaffer(retrieved, StafferForUpdate);

            if (_context.SaveChanges() <= 0)
            {
                throw new SaveChangesFailedException(_messages.GetString("failedUpdateServiceStaffer"));
            }
            return _mapper.Map<StafferForReturnDto>(updated);
        }

        public StafferForReturnDto ModifyServiceStaffer(ServiceProviderStafferType type, int userId, StafferForUpdateDto StafferForUpdate)
        {
            if (StafferForUpdate == null)
            {
                throw new ArgumentBadException(_messages.GetString("stafferNotFound"));
            }

            var retrieved = _context.ServiceStaffers
                .FirstOrDefault(staffer => staffer.UserId == userId && staffer.Type == type);

            if (retrieved == null)
            {
                throw new ArgumentBadException(_messages.GetString("serviceStafferNotFound"));
            }
            var updated = UpdateServiceStaffer(retrieved, StafferForUpdate);

            if (_context.SaveChanges() <= 0)
            {
                throw new SaveChangesFailedException(_messages.GetString("failedUpdateServiceStaffer"));
            }
            return _mapper.Map<StafferForReturnDto>(updated);
        }

        private ServiceStaffer UpdateServiceStaffer(ServiceStaffer source, StafferForUpdateDto updateDto)
        {
            var sp = _mapper.Map<StafferForUpdateDto, ServiceStaffer>(updateDto, source);
            if (!string.IsNullOrWhiteSpace(updateDto.UserAddressCode))
            {
                sp.Address = GetAddress(source.UserId, updateDto.UserAddressCode);
            }
            if (!string.IsNullOrWhiteSpace(updateDto.UserEmailCode))
            {
                sp.Email = GetEmail(source.UserId, updateDto.UserEmailCode);
            }
            if (!string.IsNullOrWhiteSpace(updateDto.UserPhoneCode))
            {
                sp.PhoneNumber = GetPhone(source.UserId, updateDto.UserPhoneCode);
            }
            return sp;
        }

        private UserAddress GetAddress(int userId, string addressCode)
        {
            return _context.UserAddresses.Include(ua => ua.Address)
                .FirstOrDefault(ua =>
                   ua.Confirmed &&
                   ua.Address.AddressCode.ToUpper() == (addressCode.ToUpper()) &&
                   ua.UserId == userId) ??
                throw new ArgumentBadException(_messages.GetString("userAddressCodeNotFound"));
        }

        private UserEmail GetEmail(int userId, string emailCode)
        {
            return _context.UserEmails
                .FirstOrDefault(ue =>
                   ue.UserId == userId &&
                   ue.Confirmed &&
                   ue.EmailCode.ToUpper() == (emailCode.ToUpper())) ??
                throw new ArgumentBadException(_messages.GetString("userEmailCodeNotFound"));
        }

        private UserPhoneNumber GetPhone(int userId, string phoneCode)
        {
            return _context.UserPhoneNumbers
                .FirstOrDefault(up =>
                   up.Confirmed &&
                   up.PhoneCode.ToUpper() == (phoneCode.ToUpper()) &&
                   up.UserId == userId) ??
                throw new ArgumentBadException(_messages.GetString("userPhoneCodeNotFound"));
        }

        public StafferForReturnDto ChangeServiceStafferStatus(ServiceProviderStafferType type, string code, StatusType status)
        {
            var retrieved = _context.ServiceStaffers
                .Include(sp => sp.Status)
                .ThenInclude(s => s.PreviousStatus)
                .FirstOrDefault(sp =>
                    // sp.Type == type &&
                    sp.ServiceStafferCode.ToUpper() == (code.ToUpper()));

            if (retrieved == null)
            {
                throw new ArgumentBadException(_messages.GetString("serviceStafferNotFound", code));
            }
            retrieved.Status = new Status { Name = status, PreviousStatus = retrieved.Status };

            if (_context.SaveChanges() <= 0)
            {
                throw new SaveChangesFailedException(_messages.GetString("failedUpdateServiceStaffer"));
            }
            return _mapper.Map<StafferForReturnDto>(retrieved);
        }

        public List<StafferForReturnDto> GetServiceStaffers()
        {
            var serviceStaffers = _context.ServiceStaffers;

            return _mapper.Map<List<StafferForReturnDto>>(serviceStaffers);
        }
    }
}