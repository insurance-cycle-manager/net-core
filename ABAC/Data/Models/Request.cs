using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PatientsRecordsManager.ABAC.Data.Models
{
    public class Request
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Value { get; set; }
        [Required]
        public DateTime DateOfRequest { get; set; }
        public List<DataAccessRequest> Items { get; set; }

    }
}