using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PatientsRecordsManager.Data;
using PatientsRecordsManager.Data.Dtos.Users.Addresses;
using PatientsRecordsManager.Exceptions;
using PatientsRecordsManager.Extensions;
using PatientsRecordsManager.Models;
using PatientsRecordsManager.Constants;
using System;
using System.Globalization;
using PatientsRecordsManager.ABAC.Extensions.EF;
using PatientsRecordsManager.ABAC.Extensions;

namespace PatientsRecordsManager.Controllers
{
    [ApiController]
    [Authorize]
    [Route(APIs.UserAddressesBaseUrl)]
    public class UserAddressesController : PrmController
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        private readonly Resources.Messages _messages;

        public UserAddressesController(DataContext context, IMapper mapper, Resources.Messages messages)
        {
            _mapper = mapper;
            _context = context;
            _messages = messages;
        }

        [HttpGet(APIs.GetPendingUserAddresses)]
        [Authorize(Roles = RolesNames.Administrator)]
        public IActionResult GetPendingUserAddresses([FromQuery] string userName)
        {
            return HttpContext.Success(GetAllAddresses(false, userName), _messages.GetString("pendingUserAddressesFetchDone"));
        }

        [HttpGet(APIs.GetUserAddresses)]
        public IActionResult GetUserAddresses()
        {
            var user = GetUser(HttpContext.GetUserId());
            return HttpContext.Success(GetAddresses(user), _messages.GetString("userAddressesFetchDone", user.UserName));
        }

        [HttpPost(APIs.PostUserAddress)]
        public IActionResult PostUserAddress(UserAddressForAddDto address)
        {
            if (address == null)
            {
                throw new ArgumentNullException(nameof(address));
            }
            return HttpContext.Success(AddAddress(GetUser(HttpContext.GetUserId()), address), _messages.GetString("userAddressPostDone"));
        }

        [HttpPut(APIs.ConfirmUserAddress)]
        [Authorize(Roles = RolesNames.Administrator)]
        public IActionResult ConfirmUserAddress(string userName, string addressCode)
        {
            return HttpContext.Success(ConfirmAddress(GetUser(userName), addressCode), _messages.GetString("userAddressConfirmDone"));
        }

        [HttpDelete(APIs.DeleteUserAddress)]
        public IActionResult DeleteUserAddress(string addressCode)
        {
            return HttpContext.Success(DeleteAddress(GetUser(HttpContext.GetUserId()), addressCode), _messages.GetString("userAddressDeletedDone"));
        }

        private User GetUser(string username)
        {
            return _context.Users
                .Include(u => u.UserAddresses)
                .ThenInclude(ua => ua.Address)
                .Where(u => u.NormalizedUserName.ToUpper() == username.ToUpper())
                .FirstOrDefault() ??
                throw new ArgumentBadException(_messages.GetString("InvalidUserName", username));
        }

        private User GetUser(int userId)
        {
            return _context.Users
                .Include(u => u.UserAddresses)
                .ThenInclude(ua => ua.Address)
                .FirstOrDefault(u => u.Id.Equals(userId)) ??
                throw new ArgumentBadException(_messages.GetString("invalidUserName"));
        }

        private Address GetAddressByCode(string addressCode)
        {
            return _context.Addresses
                .Where(a => a.AddressCode.ToUpper() == addressCode.ToUpper())
                .FirstOrDefault() ??
                throw new ArgumentBadException(_messages.GetString("addressCodeNotFound", addressCode));
        }

        private UserAddress GetAddressByCode(User user, string addressCode)
        {
            return user.UserAddresses
                .Where(ua => ua.Address.AddressCode.ToUpper() == addressCode.ToUpper())
                .FirstOrDefault() ??
                throw new ArgumentBadException(_messages.GetString("userAddressCodeNotFound", addressCode, user.UserName));
        }

        private List<UserAddressForReturnDto> GetAllAddresses(bool confirmed, string userName = null)
        {
            var addresses = _context.UserAddresses
                .Include(e => e.User)
                .Include(e => e.Address)
                .Where(e => e.Confirmed == confirmed);

            if (!string.IsNullOrWhiteSpace(userName))
            {
                addresses = addresses.Where(e => e.User.UserName.ToUpper() == userName.ToUpper());
            }
            return _mapper.Map<List<UserAddressForReturnDto>>(addresses);
        }

        private List<UserAddressForReturnDto> GetAddresses(User user)
        {
            return _mapper.Map<List<UserAddressForReturnDto>>(user.UserAddresses);
        }

        private UserAddressForReturnDto AddAddress(User user, UserAddressForAddDto userAddressDto)
        {
            var address = _mapper.Map<UserAddress>(userAddressDto);
            address.User = user;
            address.Address = GetAddressByCode(userAddressDto.AddressCode);
            user.UserAddresses.Add(address);

            if (_context.SaveChanges() <= 0)
            {
                throw new SaveChangesFailedException();
            }
            return _mapper.Map<UserAddressForReturnDto>(address);
        }

        private UserAddressForReturnDto ConfirmAddress(User user, string addressCode)
        {
            var address = GetAddressByCode(user, addressCode);
            if (address.Confirmed)
            {
                throw new SaveChangesFailedException(_messages.GetString("UserAddressAlreadyConfirmed"));
            }
            address.Confirmed = true;

            if (_context.SaveChanges() <= 0)
            {
                throw new SaveChangesFailedException();
            }
            return _mapper.Map<UserAddressForReturnDto>(address);
        }

        private bool DeleteAddress(User user, string addressCode)
        {
            var address = GetAddressByCode(user, addressCode);
            user.UserAddresses.Remove(address);

            if (_context.SaveChanges() <= 0)
            {
                throw new SaveChangesFailedException();
            }
            return true;
        }
    }
}