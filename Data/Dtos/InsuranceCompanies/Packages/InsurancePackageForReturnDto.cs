using System;
using System.Collections.Generic;
using PatientsRecordsManager.Data.Dtos.Drugs;
using PatientsRecordsManager.Data.Dtos.LabTests;
using PatientsRecordsManager.Data.Dtos.Prices;
using PatientsRecordsManager.Data.Dtos.Radiographs;
using PatientsRecordsManager.Data.Dtos.Surgeries;

namespace PatientsRecordsManager.Data.Dtos.InsuranceCompanies.Packages
{
    public class InsurancePackageForReturnDto
    {
        public string Name { get; set; }
        public string InsurancePackageCode { get; set; }
        public DateTime PackageDate { get; set; }
        public InsuranceCompanyForReturnDto InsuranceCompany { get; set; }
        public string Category { get; set; }
        public PriceForReturnDto Price { get; set; }
        public List<PackageDrugForReturnDto> Drugs { get; set; }
        public List<PackageLabTestForReturnDto> LabTests { get; set; }
        public List<PackageRadiographForReturnDto> Radiographs { get; set; }
        public List<PackageSurgeryForReturnDto> Surgeries { get; set; }
    }
}