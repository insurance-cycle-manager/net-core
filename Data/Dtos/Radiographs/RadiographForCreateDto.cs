using System.ComponentModel.DataAnnotations;
using PatientsRecordsManager.Data.Dtos.Prices;

namespace PatientsRecordsManager.Data.Dtos.Radiographs
{
    public class RadiographForCreateDto
    {
        [Required]
        public string Name { get; set; }
        public PriceForAddDto Price { get; set; }
    }
}