using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PatientsRecordsManager.ABAC;
using PatientsRecordsManager.ABAC.Extensions.EF;
using PatientsRecordsManager.ABAC.Extensions.Linq;
using PatientsRecordsManager.Data;
using PatientsRecordsManager.Models;

namespace PatientsRecordsManager.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TestController : PrmController
    {
        private readonly DataContext context;
        private readonly PoliciesManager policies;

        public TestController(DataContext context, PoliciesManager policies)
        {
            this.policies = policies;
            this.context = context;
        }

        [HttpGet("async")]
        public async Task<List<InsurancePackage>> TestAsync()
        {
            return await this.context.InsurancePackages
                .ToListAsync()
                .ConfigureAwait(false);
        }

        [HttpGet("sync")]
        public List<InsurancePackage> Test()
        {
            return this.context.InsurancePackages.ToList();
        }

        [HttpPost("sync")]
        public InsurancePackage TestPostSync()
        {
            var p = new InsurancePackage {};
            this.context.InsurancePackages.Add(p);

            return p;
        }

        [HttpGet("authorize")]
        public bool Authorize()
        {
            return this.policies.EvaluateRequestOfAction(HttpContext.TraceIdentifier, HttpContext.User, "/users/me", "POST");
        }

        [HttpGet("hashCode")]
        public bool HashCode()
        {
            var x = this.context.Users;
            Console.WriteLine(x.GetHashCode());
            Console.WriteLine(x.ToList().GetHashCode());
            Console.WriteLine(x.AsEnumerable().GetHashCode());
            Console.WriteLine(x.AsQueryable().GetHashCode());

            return true;
        }
    }
}