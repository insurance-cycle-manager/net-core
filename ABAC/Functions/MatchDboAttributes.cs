using System;
using System.Collections.Generic;
using System.Linq;
using NetCasbin;
using Newtonsoft.Json;
using PatientsRecordsManager.ABAC.Data;
using PatientsRecordsManager.ABAC.Data.Models;

namespace PatientsRecordsManager.ABAC.Functions
{
    public class MatchDboAttributes : AbstractFunction
    {
        private readonly AbacDbContext context;

        public MatchDboAttributes(string name, AbacDbContext context) : base(name)
        {
            this.context = context;
        }

        protected override Delegate GetFunc()
        {
            Func<string, IEnumerable<string>, bool> myFunction = (pString, request) =>
            {
                if (!string.IsNullOrWhiteSpace(pString))
                {
                    int attributesId;

                    if(!int.TryParse(pString, out attributesId))
                    {
                        return false;
                    }

                    RuleAttributes attributes = context.RuleAttributes.FirstOrDefault(r => r.Id == attributesId);

                    if (attributes == null)
                    {
                        return false;
                    }

                    if (attributes.Attributes.Properties().All(p => p.Value.ToObject<bool>() == true))
                    {
                        return true;
                    }
                }
                return false;
            };
            return myFunction;
        }
    }
}