using System.Collections.Generic;

namespace PatientsRecordsManager.Data.Dtos.Prescribed.Prescriptions
{
    public class PrescriptionForUpdateDto
    {
        public string Note { get; set; }
        public IEnumerable<string> Drugs { get; set; } = new List<string>() { };
    }
}