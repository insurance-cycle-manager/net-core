using System.Collections.Generic;
using PatientsRecordsManager.Data.Dtos.Users.Roles;
using PatientsRecordsManager.Data.Dtos.Users.Emails;
using System.ComponentModel.DataAnnotations;
using PatientsRecordsManager.Data.Dtos.Users.Phones;
using PatientsRecordsManager.Data.Dtos.Addresses;
using PatientsRecordsManager.Data.Dtos.Users.Addresses;
using System;

namespace PatientsRecordsManager.Data.Dtos.Users
{
    public class UserForReturnDto
    {
        public string UserName { get; set; }
        [EmailAddress]
        public string Email { get; set; }
        [Phone]
        public string PhoneNumber { get; set; }
        public Uri PhotoUrl { get; set; }
        public IEnumerable<UserEmailForReturnDto> UserEmails { get; set; }
        public IEnumerable<UserPhoneForReturnDto> UserPhoneNumbers { get; set; }
        public IEnumerable<UserAddressForReturnDto> UserAddresses { get; set; }
        public IEnumerable<UserRoleForReturnDto> UserRoles { get; set; }
    }
}