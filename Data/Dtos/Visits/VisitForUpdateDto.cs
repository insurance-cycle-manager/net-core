using System;
using System.ComponentModel.DataAnnotations;
using PatientsRecordsManager.Constants.Enums;

namespace PatientsRecordsManager.Data.Dtos.Visits
{
    public class VisitForUpdateDto
    {
        public string BaseVisitCode { get; set; }
        public string PatientCode { get; set; }
        public string ServiceProviderCode { get; set; }
        public string ServiceStafferCode { get; set; }
        public string ServiceProviderConnectionCode { get; set; }
        public string PatientPackageCode { get; set; }
        public DateTime VisitDate { get; set; }
        [EnumDataType(typeof(VisitType))]
        public VisitType Type { get; set; }
        public double? Cost { get; set; }
    }
}