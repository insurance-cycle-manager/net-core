using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using PatientsRecordsManager.ABAC.Extensions;
using PatientsRecordsManager.ABAC.Extensions.EF;
using PatientsRecordsManager.Data.Dtos.Prescribed.LabTests;
using PatientsRecordsManager.Exceptions;
using PatientsRecordsManager.Models;
using PatientsRecordsManager.Resources;

namespace PatientsRecordsManager.Data.Repositories
{
    public class VisitLabTestRepository : IVisitLabTestRepository
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        private readonly Messages _messages;

        public VisitLabTestRepository(DataContext context, IMapper mapper, Messages messages)
        {
            _mapper = mapper;
            _context = context;
            _messages = messages;
        }

        public PrescribedLabTestForReturnDto AddLabTest(string visitCode, PrescribedLabTestForCreateDto labTestForCreate)
        {
            var visit = _context.Visits.Include(v => v.LabTests)
                .Where(v => v.VisitCode.ToUpper() == (visitCode.ToUpper()))
                .FirstOrDefault();
            if (visit == null)
            {
                throw new ArgumentBadException(_messages.GetString("visitNotFound", visitCode));
            }

            var labTest = _context.LabTests
                .FirstOrDefault(lab => lab.LabTestCode.ToUpper() == (labTestForCreate.LabTestCode.ToUpper()));
            if (labTest == null)
            {
                throw new ArgumentBadException(_messages.GetString("labTestNotFound", visitCode));
            }

            var prescribedLabTest = _mapper.Map<PrescribedLabTest>(labTestForCreate);
            prescribedLabTest.Visit = visit;
            prescribedLabTest.LabTest = labTest;
            visit.LabTests.Add(prescribedLabTest);

            if (_context.SaveChanges() <= 0)
            {
                throw new SaveChangesFailedException();
            }
            return _mapper.Map<PrescribedLabTestForReturnDto>(prescribedLabTest);
        }

        public PrescribedLabTestForReturnDto GetLabTestDetails(string visitCode, string labTestCode)
        {
            return _mapper.Map<PrescribedLabTestForReturnDto>(GetLabTest(visitCode, labTestCode));
        }

        public IEnumerable<PrescribedLabTestForReturnDto> GetLabTests(string visitCode)
        {
            var visit = _context.Visits
                .Include(v => v.LabTests)
                .FirstOrDefault(v => v.VisitCode.ToUpper() == (visitCode.ToUpper()));
            if (visit == null)
            {
                throw new ArgumentBadException(_messages.GetString("visitNotFound", visitCode));
            }
            return _mapper.Map<List<PrescribedLabTestForReturnDto>>(visit.LabTests.AsQueryable());
        }

        public PrescribedLabTestForReturnDto ModifyLabTest(string visitCode, string labTestCode, PrescribedLabTestForUpdateDto labTestForUpdate)
        {
            var retrievedLabTest = GetLabTest(visitCode, labTestCode);
            var updatedLabTest = _mapper.Map<PrescribedLabTestForUpdateDto, PrescribedLabTest>(labTestForUpdate, retrievedLabTest);

            if (_context.SaveChanges() <= 0)
            {
                throw new SaveChangesFailedException();
            }
            return _mapper.Map<PrescribedLabTestForReturnDto>(updatedLabTest);
        }

        public PrescribedLabTestForReturnDto RemoveLabTest(string visitCode, string labTestCode)
        {
            return _mapper.Map<PrescribedLabTestForReturnDto>(_context.PrescribedLabTests.Remove(GetLabTest(visitCode, labTestCode)).Entity);
        }

        private PrescribedLabTest GetLabTest(string visitCode, string labTestCode)
        {
            return _context.PrescribedLabTests
                .Include(l => l.Visit)
                .Where(l => l.Visit.VisitCode.ToUpper() == (visitCode.ToUpper()) && l.PrescribedLabTestCode.ToUpper() == (labTestCode.ToUpper()))
                .FirstOrDefault()
                ?? throw new ArgumentBadException(_messages.GetString("visitLabTestNotFound", visitCode, labTestCode));
        }
    }
}