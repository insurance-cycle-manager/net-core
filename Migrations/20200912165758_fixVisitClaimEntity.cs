﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PatientsRecordsManager.Migrations
{
    public partial class fixVisitClaimEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VisitClaims_Visits_VisitId",
                table: "VisitClaims");

            migrationBuilder.DropIndex(
                name: "IX_VisitClaims_VisitId",
                table: "VisitClaims");

            migrationBuilder.DropColumn(
                name: "VisitId",
                table: "VisitClaims");

            migrationBuilder.CreateIndex(
                name: "IX_Visits_VisitClaimId",
                table: "Visits",
                column: "VisitClaimId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Visits_VisitClaims_VisitClaimId",
                table: "Visits",
                column: "VisitClaimId",
                principalTable: "VisitClaims",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Visits_VisitClaims_VisitClaimId",
                table: "Visits");

            migrationBuilder.DropIndex(
                name: "IX_Visits_VisitClaimId",
                table: "Visits");

            migrationBuilder.AddColumn<int>(
                name: "VisitId",
                table: "VisitClaims",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_VisitClaims_VisitId",
                table: "VisitClaims",
                column: "VisitId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_VisitClaims_Visits_VisitId",
                table: "VisitClaims",
                column: "VisitId",
                principalTable: "Visits",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
