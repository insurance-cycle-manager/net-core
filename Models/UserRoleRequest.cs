using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using PatientsRecordsManager.Constants.Enums;

namespace PatientsRecordsManager.Models
{
    public class UserRoleRequest
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [ForeignKey("Users")]
        public int UserId { get; set; }
        public User User { get; set; }

        [Required]
        [ForeignKey("Roles")]
        public int RoleId { get; set; }
        public Role Role { get; set; }

        [Required]
        [ForeignKey("UserEmails")]
        public int UserEmailId { get; set; }
        public UserEmail UserEmail { get; set; }

        [Required]
        [ForeignKey("UserPhoneNumbers")]
        public int UserPhoneNumberId { get; set; }
        public UserPhoneNumber UserPhoneNumber { get; set; }

        [Required]
        [ForeignKey("Addresses")]
        public int AddressId { get; set; }
        public UserAddress UserAddress { get; set; }

        [Required]
        public string Title { get; set; }

        public RoleRequestStatus Status { get; set; } = RoleRequestStatus.Pending;
    }
}