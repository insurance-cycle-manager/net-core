using System;
using System.Reflection;

namespace PatientsRecordsManager.ABAC.Attributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class AbacAttribute : Attribute
    {
        public AbacAttribute()
        {

        }
    }
}