using System;
using System.Collections.Generic;
using PatientsRecordsManager.Data.Dtos.Users.Addresses;
using PatientsRecordsManager.Data.Dtos.Users.Emails;
using PatientsRecordsManager.Data.Dtos.Users.Phones;

namespace PatientsRecordsManager.Data.Dtos.Users.Accounts
{
    public class UserAccountForUpdateDto
    {
        public Uri PhotoUrl { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public List<UserEmailForModifyDto> UserEmails { get; set; }
        public List<UserPhoneForModifyDto> UserPhones { get; set; }
        public List<UserAddressForModifyDto> UserAddresses { get; set; }
    }
}