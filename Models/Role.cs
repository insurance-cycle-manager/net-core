using System.Collections.Generic;
using Casbin.NET.Adapter.EFCore;
using Microsoft.AspNetCore.Identity;
using PatientsRecordsManager.Data.Dtos;

namespace PatientsRecordsManager.Models
{
    public class Role : IdentityRole<int>
    {
        public IEnumerable<RolePermission> Permissions { get; set; }
        public IEnumerable<UserRole> UserRoles { get; set; }
    }
}