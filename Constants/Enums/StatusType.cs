namespace PatientsRecordsManager.Constants.Enums
{
    public enum StatusType
    {
        Active,
        InActive,
        Blocked,
        UnConfirmed
    }
}