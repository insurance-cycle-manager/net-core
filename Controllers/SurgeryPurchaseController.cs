using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PatientsRecordsManager.ABAC.Extensions;
using PatientsRecordsManager.ABAC.Extensions.EF;
using PatientsRecordsManager.Constants;
using PatientsRecordsManager.Data;
using PatientsRecordsManager.Data.Dtos.Prescribed.Surgeries;
using PatientsRecordsManager.Extensions;
using PatientsRecordsManager.Models;
using PatientsRecordsManager.Resources;
using PatientsRecordsManager.Utils.Exceptions;

namespace PatientsRecordsManager.Controllers
{
    [ApiController]
    [Authorize(Roles = RolesNames.GetStaffersCommaDelimited)]
    public class SurgeryPurchaseController : PrmController
    {
        private readonly IMapper _mapper;
        private readonly DataContext _dataContext;
        private readonly Messages _messages;

        public SurgeryPurchaseController(IMapper mapper, DataContext context, Messages messages)
        {
            _messages = messages;
            _dataContext = context;
            _mapper = mapper;
        }

        [HttpGet(APIs.DefaultRoute)]
        public IActionResult GetSurgeryPurchases([FromQuery] string visitCode,
                                                 [FromQuery] string surgeryCode)
        {
            var purchases = _dataContext.SurgeryPurchases
                .Include(p => p.PrescribedSurgery)
                .ThenInclude(ps => ps.Surgery)
                .Include(p => p.PrescribedSurgery)
                .ThenInclude(ps => ps.Visit)
                .Include(p => p.Price)
                .AsQueryable();

            if (!string.IsNullOrWhiteSpace(visitCode))
            {
                purchases = purchases.Where(p => p.PrescribedSurgery.Visit.VisitCode.ToUpper().Contains(visitCode.ToUpper()));
            }

            if (!string.IsNullOrWhiteSpace(surgeryCode))
            {
                purchases = purchases.Where(p => p.PrescribedSurgery.Surgery.SurgeryCode.ToUpper().Contains(surgeryCode.ToUpper()));
            }

            return HttpContext.Success(
                _mapper.Map<IEnumerable<SurgeryPurchaseForReturnDto>>(purchases.ToList()),
                _messages.GetString("SurgeryPurchasesFetched"));
        }

        [HttpGet(APIs.GetSurgeryPurchase)]
        public IActionResult GetSurgeryPurchase(string visitCode, string prescribedSurgeryCode)
        {
            var visit = FetchVisit(visitCode, true);
            var purchases = visit
                .SurgeryPurchases
                .Where(p => p.PrescribedSurgery.PrescribedSurgeryCode.ToUpper() == (prescribedSurgeryCode.ToUpper()));

            return HttpContext.Success(
                _mapper.Map<IEnumerable<SurgeryPurchaseForReturnDto>>(purchases),
                _messages.GetString("SurgeryPurchaseFetched"));
        }

        [HttpPost(APIs.AddSurgeryPurchase)]
        public IActionResult AddSurgeryPurchase(string visitCode, string prescribedSurgeryCode)
        {
            var visit = FetchVisit(visitCode);

            if (visit.BaseVisit == null)
            {
                throw new Exception(_messages.GetString("NoBaseVisitFound", visitCode));
            }
            var prescribed = FetchSurgery(prescribedSurgeryCode, visit.BaseVisit);

            var purchase = new SurgeryPurchase
            {
                Visit = visit,
                PrescribedSurgery = prescribed,
                Price = prescribed.Surgery.Price,
                DateOfPurchasing = DateTime.Now
            };
            prescribed.Purchases.Add(purchase);

            if (_dataContext.SaveChanges() < 1)
                throw new Exception(_messages.GetString("SurgeryPurchaseAddFailed"));

            return HttpContext.Success(
                _mapper.Map<SurgeryPurchaseForReturnDto>(purchase),
                _messages.GetString("SurgeryPurchaseCreated"));
        }

        private Visit FetchVisit(string visitCode, bool purchases = false)
        {
            var visits = _dataContext.Visits
                .Include(v => v.BaseVisit)
                .AsQueryable();

            if (purchases)
            {
                visits = visits
                    .Include(v => v.SurgeryPurchases)
                    .ThenInclude(lp => lp.Price)
                    .Include(v => v.SurgeryPurchases)
                    .ThenInclude(lp => lp.PrescribedSurgery);
            }

            return visits.FirstOrDefault(v => v.VisitCode.ToUpper() == (visitCode.ToUpper()))
                ?? throw new NoDataFoundException(_messages.GetString("VisitNotFound2"));
        }

        private PrescribedSurgery FetchSurgery(string prescribedSurgeryCode, Visit visit)
        {
            return _dataContext
                .PrescribedSurgeries
                .Include(l => l.Purchases)
                .Include(l => l.Surgery)
                .ThenInclude(t => t.Price)
                .Where(l => l.PrescribedSurgeryCode.ToUpper() == (prescribedSurgeryCode.ToUpper()) && l.VisitId == visit.Id)
                .FirstOrDefault() ?? throw new NoDataFoundException(_messages.GetString("PrescribedSurgeryNotFound"));
        }
    }
}