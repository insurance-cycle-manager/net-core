using System.ComponentModel.DataAnnotations;

namespace PatientsRecordsManager.Data.Dtos.Users.Phones
{
    public class UserPhoneForModifyDto
    {
        [Phone]
        [Required]
        public string Number { get; set; }
    }
}