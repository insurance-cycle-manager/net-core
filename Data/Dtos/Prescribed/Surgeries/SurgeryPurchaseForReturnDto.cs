using System;
using System.ComponentModel.DataAnnotations;
using PatientsRecordsManager.Data.Dtos.Prices;

namespace PatientsRecordsManager.Data.Dtos.Prescribed.Surgeries
{
    public class SurgeryPurchaseForReturnDto
    {
        [Required]
        public DateTime DateOfPurchasing { get; set; }
        [Required]
        public PrescribedSurgeryForReturnDto PrescribedSurgery { get; set; }
        [Required]
        public PriceForReturnDto Price { get; set; }
    }
}