namespace PatientsRecordsManager.Constants
{
    public static class Properties
    {
        internal static string PatientCode = "PatientCode";
        internal static string PatientId = "PatientId";
        internal static string ServiceProviderCode = "ServiceProviderCode";
        internal static string ServiceProviderId = "ServiceProviderId";
        internal static string ServiceProviderConnectionCode = "ServiceProviderConnectionCode";
        internal static string ServiceProviderConnectionId = "ServiceProviderConnectionId";
        internal static string AddressCode = "AddressCode";
        internal static string AddressId = "AddressId";
    }
}