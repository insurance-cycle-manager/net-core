using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using PatientsRecordsManager.Data.Dtos.Prices;

namespace PatientsRecordsManager.Data.Dtos.InsuranceCompanies.Packages
{
    public class InsurancePackageForCreateDto
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string Category { get; set; }

        [Required]
        public PriceForAddDto Price { get; set; }
        public DateTime? PackageDate { get; set; }

        [Required]
        public IEnumerable<string> Drugs { get; set; }

        [Required]
        public IEnumerable<string> LabTests { get; set; }

        [Required]
        public IEnumerable<string> Radiographs { get; set; }

        [Required]
        public IEnumerable<string> Surgeries { get; set; }


    }
}