using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PatientsRecordsManager.Constants;
using PatientsRecordsManager.Data.Dtos.LabTests;
using PatientsRecordsManager.Data.Repositories;
using PatientsRecordsManager.Extensions;
using PatientsRecordsManager.Resources;

namespace PatientsRecordsManager.Controllers
{
    [ApiController]
    [Route(APIs.LabTestsBaseUrl)]
    [Authorize]
    public class LabTestsController : PrmController
    {
        private readonly IMapper _mapper;
        private readonly ILabTestRepository _repository;
        private readonly Messages _messages;

        public LabTestsController(IMapper mapper, ILabTestRepository repository, Messages messages)
        {
            _mapper = mapper;
            _repository = repository;
            _messages = messages;
        }

        [HttpGet(APIs.GetLabTests)]
        public IActionResult GetLabTests([FromQuery] string name)
        {
            return HttpContext.Success(_repository.GetLabTests(name), _messages.GetString("LabTestsGetDone"));
        }

        [HttpGet(APIs.GetLabTestDetails)]
        public IActionResult GetLabTestDetails(string labTestCode)
        {
            return HttpContext.Success(_repository.GetLabTestDetails(labTestCode), _messages.GetString("LabTestDetailsGetDone", labTestCode));
        }

        [HttpPost(APIs.PostLabTest)]
        [Authorize(Roles = RolesNames.Administrator)]
        public IActionResult AddLabTest(LabTestForCreateDto labTest)
        {
            return HttpContext.Success(_repository.AddLabTest(labTest), _messages.GetString("LabTestAddDone"));
        }

        [HttpDelete(APIs.DeleteLabTest)]
        public IActionResult DeleteLabTest(string labTestCode)
        {
            bool done = _repository.DeleteLabTest(labTestCode);
            if (!done)
            {
                return HttpContext.Error(labTestCode, _messages.GetString("LabTestDeleteFailed", labTestCode));
            }
            return HttpContext.Success(labTestCode, _messages.GetString("LabTestDeleteDone", labTestCode));
        }

        [HttpPut(APIs.ModifyLabTest)]
        [Authorize(Roles = RolesNames.Administrator)]
        public IActionResult ModifyLabTest(string labTestCode, LabTestForUpdateDto labTest)
        {
            return HttpContext.Success(_repository.ModifyLabTest(labTestCode, labTest), _messages.GetString("LabTestModifyDone", labTestCode));
        }
    }
}