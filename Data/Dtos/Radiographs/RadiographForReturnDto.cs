using PatientsRecordsManager.Data.Dtos.Prices;

namespace PatientsRecordsManager.Data.Dtos.Radiographs
{
    public class RadiographForReturnDto
    {
        public string RadiographCode { get; set; }
        public string Name { get; set; }
        public PriceForReturnDto Price { get; set; }
    }
}