﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace PatientsRecordsManager.Migrations
{
    public partial class firstMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateSequence<int>(
                name: "AddressNumbers");

            migrationBuilder.CreateSequence<int>(
                name: "DiagnosisNumbers");

            migrationBuilder.CreateSequence<int>(
                name: "DrugsCode");

            migrationBuilder.CreateSequence<int>(
                name: "InsuranceCompaniesNumbers");

            migrationBuilder.CreateSequence<int>(
                name: "InsuranceConnectionsNumbers");

            migrationBuilder.CreateSequence<int>(
                name: "LabTestsCode");

            migrationBuilder.CreateSequence<int>(
                name: "PatientsNumbers");

            migrationBuilder.CreateSequence<int>(
                name: "PrescribedLabTestsCode");

            migrationBuilder.CreateSequence<int>(
                name: "PrescribedRadiographsCode");

            migrationBuilder.CreateSequence<int>(
                name: "PrescribedSurgeriesCode");

            migrationBuilder.CreateSequence<int>(
                name: "PrescriptionsCode");

            migrationBuilder.CreateSequence<int>(
                name: "PricesCode");

            migrationBuilder.CreateSequence<int>(
                name: "RadiographCode");

            migrationBuilder.CreateSequence<int>(
                name: "ServiceProviderConnectionsNumbers");

            migrationBuilder.CreateSequence<int>(
                name: "ServiceProviderNumbers");

            migrationBuilder.CreateSequence<int>(
                name: "ServiceProviderStaffCode");

            migrationBuilder.CreateSequence<int>(
                name: "SurgeriesCode");

            migrationBuilder.CreateSequence<int>(
                name: "ThirdPartyAdministratorsNumbers");

            migrationBuilder.CreateSequence<int>(
                name: "UserEmailsCode");

            migrationBuilder.CreateSequence<int>(
                name: "UserPhoneNumbersCode");

            migrationBuilder.CreateSequence<int>(
                name: "VisitClaimsCode");

            migrationBuilder.CreateSequence<int>(
                name: "VisitNumbers");

            migrationBuilder.CreateTable(
                name: "Addresses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Longitude = table.Column<double>(nullable: true),
                    Latitude = table.Column<double>(nullable: true),
                    Title = table.Column<string>(nullable: false),
                    NormalizedTitle = table.Column<string>(nullable: false),
                    AddressCode = table.Column<string>(nullable: false, defaultValueSql: "'ADD'|| nextval('public.\"AddressNumbers\"')")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Addresses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    PhotoUrl = table.Column<string>(nullable: true),
                    DateOfBirth = table.Column<DateTime>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Diagnoses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    DiagnosisCode = table.Column<string>(nullable: false, defaultValueSql: "'DIA'||nextval('public.\"DiagnosisNumbers\"')"),
                    Description = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Diagnoses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Permissions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Permissions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Prices",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Value = table.Column<double>(nullable: false),
                    PriceCode = table.Column<string>(nullable: false, defaultValueSql: "'PRICE' || nextval('public.\"PricesCode\"')"),
                    PreviousPriceId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Prices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Prices_Prices_PreviousPriceId",
                        column: x => x.PreviousPriceId,
                        principalTable: "Prices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Statuses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(nullable: false),
                    ChangedAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Statuses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SupervisoryComissions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(nullable: true),
                    AddressId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SupervisoryComissions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SupervisoryComissions_Addresses_AddressId",
                        column: x => x.AddressId,
                        principalTable: "Addresses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    RoleId = table.Column<int>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    UserId = table.Column<int>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    RoleId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserAddresses",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    AddressId = table.Column<int>(nullable: false),
                    Confirmed = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserAddresses", x => new { x.UserId, x.AddressId });
                    table.ForeignKey(
                        name: "FK_UserAddresses_Addresses_AddressId",
                        column: x => x.AddressId,
                        principalTable: "Addresses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserAddresses_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserEmails",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    EmailCode = table.Column<string>(nullable: false, defaultValueSql: "'EM'|| nextval('public.\"UserEmailsCode\"')"),
                    Address = table.Column<string>(nullable: false),
                    Confirmed = table.Column<bool>(nullable: false),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserEmails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserEmails_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserPhoneNumbers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    PhoneCode = table.Column<string>(nullable: false, defaultValueSql: "'PH'|| nextval('public.\"UserPhoneNumbersCode\"')"),
                    Number = table.Column<string>(nullable: false),
                    Confirmed = table.Column<bool>(nullable: false),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserPhoneNumbers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserPhoneNumbers_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "RolePermissions",
                columns: table => new
                {
                    RoleId = table.Column<int>(nullable: false),
                    PermissionId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RolePermissions", x => new { x.RoleId, x.PermissionId });
                    table.ForeignKey(
                        name: "FK_RolePermissions_Permissions_PermissionId",
                        column: x => x.PermissionId,
                        principalTable: "Permissions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RolePermissions_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Drugs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    DrugCode = table.Column<string>(nullable: true, defaultValueSql: "'DRUG' || nextval('public.\"DrugsCode\"')"),
                    Name = table.Column<string>(nullable: false),
                    NormalizedName = table.Column<string>(nullable: false),
                    Manufacturer = table.Column<string>(nullable: false),
                    PriceId = table.Column<int>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Drugs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Drugs_Prices_PriceId",
                        column: x => x.PriceId,
                        principalTable: "Prices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "LabTests",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(nullable: false),
                    NormalizedName = table.Column<string>(nullable: false),
                    LabTestCode = table.Column<string>(nullable: false, defaultValueSql: "'LAB' || nextval('public.\"LabTestsCode\"')"),
                    PriceId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LabTests", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LabTests_Prices_PriceId",
                        column: x => x.PriceId,
                        principalTable: "Prices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Radiographs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(nullable: false),
                    NormalizedName = table.Column<string>(nullable: false),
                    RadiographCode = table.Column<string>(nullable: false, defaultValueSql: "'RGPH' || nextval('public.\"RadiographCode\"')"),
                    PriceId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Radiographs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Radiographs_Prices_PriceId",
                        column: x => x.PriceId,
                        principalTable: "Prices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Surgeries",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(nullable: false),
                    NormalizedName = table.Column<string>(nullable: false),
                    SurgeryCode = table.Column<string>(nullable: false, defaultValueSql: "'SUR' || nextval('public.\"SurgeriesCode\"')"),
                    PriceId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Surgeries", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Surgeries_Prices_PriceId",
                        column: x => x.PriceId,
                        principalTable: "Prices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "InsuranceCompanies",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(nullable: false),
                    InsuranceCompanyCode = table.Column<string>(nullable: false, defaultValueSql: "'IC'|| nextval('public.\"InsuranceCompaniesNumbers\"')"),
                    AddressId = table.Column<int>(nullable: true),
                    SupervisoryComissionId = table.Column<int>(nullable: false),
                    StatusId = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false),
                    DateOfFounding = table.Column<DateTime>(nullable: false),
                    JoinedAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InsuranceCompanies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_InsuranceCompanies_Addresses_AddressId",
                        column: x => x.AddressId,
                        principalTable: "Addresses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_InsuranceCompanies_Statuses_StatusId",
                        column: x => x.StatusId,
                        principalTable: "Statuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_InsuranceCompanies_SupervisoryComissions_SupervisoryComissi~",
                        column: x => x.SupervisoryComissionId,
                        principalTable: "SupervisoryComissions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_InsuranceCompanies_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ThirdPartyAdministrators",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(nullable: true),
                    DateOfFounding = table.Column<DateTime>(nullable: false),
                    AddressId = table.Column<int>(nullable: true),
                    SupervisoryComissionId = table.Column<int>(nullable: false),
                    JoinedAt = table.Column<DateTime>(nullable: false),
                    StatusId = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false),
                    ThirdPartyCode = table.Column<string>(nullable: true, defaultValueSql: "'TPA'|| nextval('public.\"ThirdPartyAdministratorsNumbers\"')")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ThirdPartyAdministrators", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ThirdPartyAdministrators_Addresses_AddressId",
                        column: x => x.AddressId,
                        principalTable: "Addresses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ThirdPartyAdministrators_Statuses_StatusId",
                        column: x => x.StatusId,
                        principalTable: "Statuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ThirdPartyAdministrators_SupervisoryComissions_SupervisoryC~",
                        column: x => x.SupervisoryComissionId,
                        principalTable: "SupervisoryComissions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ThirdPartyAdministrators_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Patients",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    PatientCode = table.Column<string>(nullable: false, defaultValueSql: "'PAT'|| nextval('public.\"PatientsNumbers\"')"),
                    Name = table.Column<string>(nullable: false),
                    NormalizedName = table.Column<string>(nullable: false),
                    DateOfBirth = table.Column<DateTime>(nullable: false),
                    Gender = table.Column<int>(nullable: false),
                    AddressId = table.Column<int>(nullable: false),
                    EmailId = table.Column<int>(nullable: false),
                    PhoneNumberId = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false),
                    RoleId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Patients", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Patients_UserEmails_EmailId",
                        column: x => x.EmailId,
                        principalTable: "UserEmails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Patients_UserPhoneNumbers_PhoneNumberId",
                        column: x => x.PhoneNumberId,
                        principalTable: "UserPhoneNumbers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Patients_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Patients_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Patients_UserAddresses_UserId_AddressId",
                        columns: x => new { x.UserId, x.AddressId },
                        principalTable: "UserAddresses",
                        principalColumns: new[] { "UserId", "AddressId" },
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Patients_AspNetUserRoles_UserId_RoleId",
                        columns: x => new { x.UserId, x.RoleId },
                        principalTable: "AspNetUserRoles",
                        principalColumns: new[] { "UserId", "RoleId" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ServiceProviders",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ServiceProviderCode = table.Column<string>(nullable: false, defaultValueSql: "'SP'|| nextval('public.\"ServiceProviderNumbers\"')"),
                    Title = table.Column<string>(nullable: false),
                    NormalizedTitle = table.Column<string>(nullable: false),
                    StatusId = table.Column<int>(nullable: true),
                    JoinedAt = table.Column<DateTime>(nullable: false),
                    Type = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false),
                    RoleId = table.Column<int>(nullable: false),
                    AddressId = table.Column<int>(nullable: false),
                    EmailId = table.Column<int>(nullable: false),
                    PhoneNumberId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServiceProviders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ServiceProviders_UserEmails_EmailId",
                        column: x => x.EmailId,
                        principalTable: "UserEmails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ServiceProviders_UserPhoneNumbers_PhoneNumberId",
                        column: x => x.PhoneNumberId,
                        principalTable: "UserPhoneNumbers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ServiceProviders_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ServiceProviders_Statuses_StatusId",
                        column: x => x.StatusId,
                        principalTable: "Statuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ServiceProviders_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ServiceProviders_UserAddresses_UserId_AddressId",
                        columns: x => new { x.UserId, x.AddressId },
                        principalTable: "UserAddresses",
                        principalColumns: new[] { "UserId", "AddressId" },
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ServiceProviders_AspNetUserRoles_UserId_RoleId",
                        columns: x => new { x.UserId, x.RoleId },
                        principalTable: "AspNetUserRoles",
                        principalColumns: new[] { "UserId", "RoleId" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ServiceStaffers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ServiceStafferCode = table.Column<string>(nullable: false, defaultValueSql: "'SPS'|| nextval('public.\"ServiceProviderStaffCode\"')"),
                    Title = table.Column<string>(nullable: false),
                    NormalizedTitle = table.Column<string>(nullable: false),
                    StatusId = table.Column<int>(nullable: true),
                    JoinedAt = table.Column<DateTime>(nullable: false),
                    Type = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false),
                    RoleId = table.Column<int>(nullable: false),
                    AddressId = table.Column<int>(nullable: false),
                    EmailId = table.Column<int>(nullable: false),
                    PhoneNumberId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServiceStaffers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ServiceStaffers_UserEmails_EmailId",
                        column: x => x.EmailId,
                        principalTable: "UserEmails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ServiceStaffers_UserPhoneNumbers_PhoneNumberId",
                        column: x => x.PhoneNumberId,
                        principalTable: "UserPhoneNumbers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ServiceStaffers_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ServiceStaffers_Statuses_StatusId",
                        column: x => x.StatusId,
                        principalTable: "Statuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ServiceStaffers_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ServiceStaffers_UserAddresses_UserId_AddressId",
                        columns: x => new { x.UserId, x.AddressId },
                        principalTable: "UserAddresses",
                        principalColumns: new[] { "UserId", "AddressId" },
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ServiceStaffers_AspNetUserRoles_UserId_RoleId",
                        columns: x => new { x.UserId, x.RoleId },
                        principalTable: "AspNetUserRoles",
                        principalColumns: new[] { "UserId", "RoleId" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserRoleRequests",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    UserId = table.Column<int>(nullable: false),
                    RoleId = table.Column<int>(nullable: false),
                    UserEmailId = table.Column<int>(nullable: false),
                    UserPhoneNumberId = table.Column<int>(nullable: false),
                    AddressId = table.Column<int>(nullable: false),
                    Title = table.Column<string>(nullable: false),
                    Status = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRoleRequests", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserRoleRequests_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserRoleRequests_UserEmails_UserEmailId",
                        column: x => x.UserEmailId,
                        principalTable: "UserEmails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserRoleRequests_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserRoleRequests_UserPhoneNumbers_UserPhoneNumberId",
                        column: x => x.UserPhoneNumberId,
                        principalTable: "UserPhoneNumbers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserRoleRequests_UserAddresses_UserId_AddressId",
                        columns: x => new { x.UserId, x.AddressId },
                        principalTable: "UserAddresses",
                        principalColumns: new[] { "UserId", "AddressId" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "InsurancePackages",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    PackageDate = table.Column<DateTime>(nullable: false),
                    InsuranceCompanyId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Category = table.Column<string>(nullable: true),
                    InsurancePackageCode = table.Column<string>(nullable: true),
                    PriceId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InsurancePackages", x => x.Id);
                    table.ForeignKey(
                        name: "FK_InsurancePackages_InsuranceCompanies_InsuranceCompanyId",
                        column: x => x.InsuranceCompanyId,
                        principalTable: "InsuranceCompanies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_InsurancePackages_Prices_PriceId",
                        column: x => x.PriceId,
                        principalTable: "Prices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "InsuranceConnections",
                columns: table => new
                {
                    InsuranceCompanyId = table.Column<int>(nullable: false),
                    ThirdPartyAdministratorId = table.Column<int>(nullable: false),
                    InsuranceCompanyEmailId = table.Column<int>(nullable: false),
                    ThirdPartyEmailId = table.Column<int>(nullable: false),
                    DateOfConnection = table.Column<DateTime>(nullable: false),
                    InsuranceConnectionCode = table.Column<string>(nullable: true, defaultValueSql: "'ICC'|| nextval('public.\"InsuranceConnectionsNumbers\"')")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InsuranceConnections", x => new { x.InsuranceCompanyId, x.ThirdPartyAdministratorId });
                    table.ForeignKey(
                        name: "FK_InsuranceConnections_UserEmails_InsuranceCompanyEmailId",
                        column: x => x.InsuranceCompanyEmailId,
                        principalTable: "UserEmails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_InsuranceConnections_InsuranceCompanies_InsuranceCompanyId",
                        column: x => x.InsuranceCompanyId,
                        principalTable: "InsuranceCompanies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_InsuranceConnections_ThirdPartyAdministrators_ThirdPartyAdm~",
                        column: x => x.ThirdPartyAdministratorId,
                        principalTable: "ThirdPartyAdministrators",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_InsuranceConnections_UserEmails_ThirdPartyEmailId",
                        column: x => x.ThirdPartyEmailId,
                        principalTable: "UserEmails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ServiceProviderConnections",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ServiceProviderConnectionCode = table.Column<string>(nullable: false, defaultValueSql: "'SPC'|| nextval('public.\"ServiceProviderConnectionsNumbers\"')"),
                    TpaId = table.Column<int>(nullable: false),
                    ServiceProviderId = table.Column<int>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServiceProviderConnections", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ServiceProviderConnections_ServiceProviders_ServiceProvider~",
                        column: x => x.ServiceProviderId,
                        principalTable: "ServiceProviders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ServiceProviderConnections_ThirdPartyAdministrators_TpaId",
                        column: x => x.TpaId,
                        principalTable: "ThirdPartyAdministrators",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ServiceProviderStaffer",
                columns: table => new
                {
                    ServiceProviderId = table.Column<int>(nullable: false),
                    ServiceStafferId = table.Column<int>(nullable: false),
                    Status = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServiceProviderStaffer", x => new { x.ServiceProviderId, x.ServiceStafferId });
                    table.ForeignKey(
                        name: "FK_ServiceProviderStaffer_ServiceProviders_ServiceProviderId",
                        column: x => x.ServiceProviderId,
                        principalTable: "ServiceProviders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ServiceProviderStaffer_ServiceStaffers_ServiceStafferId",
                        column: x => x.ServiceStafferId,
                        principalTable: "ServiceStaffers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PackageDrugs",
                columns: table => new
                {
                    InsurancePackageId = table.Column<int>(nullable: false),
                    DrugId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PackageDrugs", x => new { x.InsurancePackageId, x.DrugId });
                    table.ForeignKey(
                        name: "FK_PackageDrugs_Drugs_DrugId",
                        column: x => x.DrugId,
                        principalTable: "Drugs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PackageDrugs_InsurancePackages_InsurancePackageId",
                        column: x => x.InsurancePackageId,
                        principalTable: "InsurancePackages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PackageLabTests",
                columns: table => new
                {
                    InsurancePackageId = table.Column<int>(nullable: false),
                    LabTestId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PackageLabTests", x => new { x.InsurancePackageId, x.LabTestId });
                    table.ForeignKey(
                        name: "FK_PackageLabTests_InsurancePackages_InsurancePackageId",
                        column: x => x.InsurancePackageId,
                        principalTable: "InsurancePackages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PackageLabTests_LabTests_LabTestId",
                        column: x => x.LabTestId,
                        principalTable: "LabTests",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PackageRadiographs",
                columns: table => new
                {
                    InsurancePackageId = table.Column<int>(nullable: false),
                    RadiographId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PackageRadiographs", x => new { x.InsurancePackageId, x.RadiographId });
                    table.ForeignKey(
                        name: "FK_PackageRadiographs_InsurancePackages_InsurancePackageId",
                        column: x => x.InsurancePackageId,
                        principalTable: "InsurancePackages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PackageRadiographs_Radiographs_RadiographId",
                        column: x => x.RadiographId,
                        principalTable: "Radiographs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PackageSrugeries",
                columns: table => new
                {
                    InsurancePackageId = table.Column<int>(nullable: false),
                    SurgeryId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PackageSrugeries", x => new { x.InsurancePackageId, x.SurgeryId });
                    table.ForeignKey(
                        name: "FK_PackageSrugeries_InsurancePackages_InsurancePackageId",
                        column: x => x.InsurancePackageId,
                        principalTable: "InsurancePackages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PackageSrugeries_Surgeries_SurgeryId",
                        column: x => x.SurgeryId,
                        principalTable: "Surgeries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PatientPackages",
                columns: table => new
                {
                    PatientId = table.Column<int>(nullable: false),
                    InsurancePackageId = table.Column<int>(nullable: false),
                    DrugId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PatientPackages", x => new { x.PatientId, x.InsurancePackageId });
                    table.ForeignKey(
                        name: "FK_PatientPackages_Drugs_DrugId",
                        column: x => x.DrugId,
                        principalTable: "Drugs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PatientPackages_InsurancePackages_InsurancePackageId",
                        column: x => x.InsurancePackageId,
                        principalTable: "InsurancePackages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PatientPackages_Patients_PatientId",
                        column: x => x.PatientId,
                        principalTable: "Patients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ServiceProviderConnectionInsuranceCompanies",
                columns: table => new
                {
                    SpConnectionId = table.Column<int>(nullable: false),
                    InsuranceCompanyId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServiceProviderConnectionInsuranceCompanies", x => new { x.SpConnectionId, x.InsuranceCompanyId });
                    table.ForeignKey(
                        name: "FK_ServiceProviderConnectionInsuranceCompanies_InsuranceCompan~",
                        column: x => x.InsuranceCompanyId,
                        principalTable: "InsuranceCompanies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ServiceProviderConnectionInsuranceCompanies_ServiceProvider~",
                        column: x => x.SpConnectionId,
                        principalTable: "ServiceProviderConnections",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Visits",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    VisitCode = table.Column<string>(nullable: false, defaultValueSql: "'VIZ'||nextval('public.\"VisitNumbers\"')"),
                    BaseVisitId = table.Column<int>(nullable: true),
                    VisitClaimId = table.Column<int>(nullable: true),
                    PatientId = table.Column<int>(nullable: false),
                    ServiceProviderId = table.Column<int>(nullable: false),
                    ServiceProviderConnectionId = table.Column<int>(nullable: false),
                    ServiceProviderStafferId = table.Column<int>(nullable: false),
                    ServiceProviderStafferServiceProviderId = table.Column<int>(nullable: false),
                    ServiceProviderStafferServiceStafferId = table.Column<int>(nullable: false),
                    PatientPackageId = table.Column<int>(nullable: false),
                    PatientPackagePatientId = table.Column<int>(nullable: false),
                    PatientPackageInsurancePackageId = table.Column<int>(nullable: false),
                    Type = table.Column<string>(nullable: false),
                    Cost = table.Column<double>(nullable: false),
                    VisitDate = table.Column<DateTime>(nullable: false),
                    DiagnosisId = table.Column<int>(nullable: true),
                    ServiceStafferId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Visits", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Visits_Visits_BaseVisitId",
                        column: x => x.BaseVisitId,
                        principalTable: "Visits",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Visits_Diagnoses_DiagnosisId",
                        column: x => x.DiagnosisId,
                        principalTable: "Diagnoses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Visits_Patients_PatientId",
                        column: x => x.PatientId,
                        principalTable: "Patients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Visits_ServiceProviderConnections_ServiceProviderConnection~",
                        column: x => x.ServiceProviderConnectionId,
                        principalTable: "ServiceProviderConnections",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Visits_ServiceProviders_ServiceProviderId",
                        column: x => x.ServiceProviderId,
                        principalTable: "ServiceProviders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Visits_ServiceStaffers_ServiceStafferId",
                        column: x => x.ServiceStafferId,
                        principalTable: "ServiceStaffers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Visits_PatientPackages_PatientPackagePatientId_PatientPacka~",
                        columns: x => new { x.PatientPackagePatientId, x.PatientPackageInsurancePackageId },
                        principalTable: "PatientPackages",
                        principalColumns: new[] { "PatientId", "InsurancePackageId" },
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Visits_ServiceProviderStaffer_ServiceProviderStafferService~",
                        columns: x => new { x.ServiceProviderStafferServiceProviderId, x.ServiceProviderStafferServiceStafferId },
                        principalTable: "ServiceProviderStaffer",
                        principalColumns: new[] { "ServiceProviderId", "ServiceStafferId" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PrescribedLabTests",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    PrescribedLabTestCode = table.Column<string>(nullable: false, defaultValueSql: "'PRES_LAB' || nextval('public.\"PrescribedLabTestsCode\"')"),
                    VisitId = table.Column<int>(nullable: false),
                    LabTestId = table.Column<int>(nullable: false),
                    ScheduledDate = table.Column<DateTime>(nullable: false),
                    Note = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PrescribedLabTests", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PrescribedLabTests_LabTests_LabTestId",
                        column: x => x.LabTestId,
                        principalTable: "LabTests",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PrescribedLabTests_Visits_VisitId",
                        column: x => x.VisitId,
                        principalTable: "Visits",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PrescribedRadiographs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    PrescribedRadiographCode = table.Column<string>(nullable: false, defaultValueSql: "'PRES_RGPH' || nextval('public.\"PrescribedRadiographsCode\"')"),
                    VisitId = table.Column<int>(nullable: false),
                    RadiographId = table.Column<int>(nullable: false),
                    ScheduledDate = table.Column<DateTime>(nullable: false),
                    Note = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PrescribedRadiographs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PrescribedRadiographs_Radiographs_RadiographId",
                        column: x => x.RadiographId,
                        principalTable: "Radiographs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PrescribedRadiographs_Visits_VisitId",
                        column: x => x.VisitId,
                        principalTable: "Visits",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PrescribedSurgeries",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    PrescribedSurgeryCode = table.Column<string>(nullable: false, defaultValueSql: "'PRES_SUR' || nextval('public.\"PrescribedSurgeriesCode\"')"),
                    VisitId = table.Column<int>(nullable: false),
                    SurgeryId = table.Column<int>(nullable: false),
                    ScheduledDate = table.Column<DateTime>(nullable: false),
                    Note = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PrescribedSurgeries", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PrescribedSurgeries_Surgeries_SurgeryId",
                        column: x => x.SurgeryId,
                        principalTable: "Surgeries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PrescribedSurgeries_Visits_VisitId",
                        column: x => x.VisitId,
                        principalTable: "Visits",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Prescriptions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    PrescriptionCode = table.Column<string>(nullable: false, defaultValueSql: "'PRES' || nextval('public.\"PrescriptionsCode\"')"),
                    VisitId = table.Column<int>(nullable: false),
                    Note = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Prescriptions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Prescriptions_Visits_VisitId",
                        column: x => x.VisitId,
                        principalTable: "Visits",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VisitClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    VisitClaimCode = table.Column<string>(nullable: false, defaultValueSql: "'CL'||nextval('public.\"VisitClaimsCode\"')"),
                    VisitId = table.Column<int>(nullable: false),
                    ServiceProviderId = table.Column<int>(nullable: false),
                    ThirdPartyAdministratorId = table.Column<int>(nullable: false),
                    InsuranceCompanyId = table.Column<int>(nullable: false),
                    DateOfClaiming = table.Column<DateTime>(nullable: false),
                    Status = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VisitClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VisitClaims_InsuranceCompanies_InsuranceCompanyId",
                        column: x => x.InsuranceCompanyId,
                        principalTable: "InsuranceCompanies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_VisitClaims_ServiceProviders_ServiceProviderId",
                        column: x => x.ServiceProviderId,
                        principalTable: "ServiceProviders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_VisitClaims_ThirdPartyAdministrators_ThirdPartyAdministrato~",
                        column: x => x.ThirdPartyAdministratorId,
                        principalTable: "ThirdPartyAdministrators",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_VisitClaims_Visits_VisitId",
                        column: x => x.VisitId,
                        principalTable: "Visits",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LabTestPurchases",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    PrescribedLabTestId = table.Column<int>(nullable: false),
                    DateOfPurchasing = table.Column<DateTime>(nullable: false),
                    PriceId = table.Column<int>(nullable: false),
                    VisitId = table.Column<int>(nullable: false),
                    LabTestId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LabTestPurchases", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LabTestPurchases_LabTests_LabTestId",
                        column: x => x.LabTestId,
                        principalTable: "LabTests",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_LabTestPurchases_PrescribedLabTests_PrescribedLabTestId",
                        column: x => x.PrescribedLabTestId,
                        principalTable: "PrescribedLabTests",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_LabTestPurchases_Prices_PriceId",
                        column: x => x.PriceId,
                        principalTable: "Prices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_LabTestPurchases_Visits_VisitId",
                        column: x => x.VisitId,
                        principalTable: "Visits",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "RadiographPurchases",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    PrescribedRadiographId = table.Column<int>(nullable: false),
                    DateOfPurchasing = table.Column<DateTime>(nullable: false),
                    PriceId = table.Column<int>(nullable: false),
                    VisitId = table.Column<int>(nullable: false),
                    RadiographId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RadiographPurchases", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RadiographPurchases_PrescribedRadiographs_PrescribedRadiogr~",
                        column: x => x.PrescribedRadiographId,
                        principalTable: "PrescribedRadiographs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RadiographPurchases_Prices_PriceId",
                        column: x => x.PriceId,
                        principalTable: "Prices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RadiographPurchases_Radiographs_RadiographId",
                        column: x => x.RadiographId,
                        principalTable: "Radiographs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RadiographPurchases_Visits_VisitId",
                        column: x => x.VisitId,
                        principalTable: "Visits",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SurgeryPurchases",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    PrescribedSurgeryId = table.Column<int>(nullable: false),
                    DateOfPurchasing = table.Column<DateTime>(nullable: false),
                    PriceId = table.Column<int>(nullable: false),
                    VisitId = table.Column<int>(nullable: false),
                    SurgeryId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SurgeryPurchases", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SurgeryPurchases_PrescribedSurgeries_PrescribedSurgeryId",
                        column: x => x.PrescribedSurgeryId,
                        principalTable: "PrescribedSurgeries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SurgeryPurchases_Prices_PriceId",
                        column: x => x.PriceId,
                        principalTable: "Prices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SurgeryPurchases_Surgeries_SurgeryId",
                        column: x => x.SurgeryId,
                        principalTable: "Surgeries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SurgeryPurchases_Visits_VisitId",
                        column: x => x.VisitId,
                        principalTable: "Visits",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PrescriptionDrugs",
                columns: table => new
                {
                    PrescriptionId = table.Column<int>(nullable: false),
                    DrugId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PrescriptionDrugs", x => new { x.PrescriptionId, x.DrugId });
                    table.ForeignKey(
                        name: "FK_PrescriptionDrugs_Drugs_DrugId",
                        column: x => x.DrugId,
                        principalTable: "Drugs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PrescriptionDrugs_Prescriptions_PrescriptionId",
                        column: x => x.PrescriptionId,
                        principalTable: "Prescriptions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PrescriptionPurchases",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    PrescriptionId = table.Column<int>(nullable: false),
                    DateOfPurchasing = table.Column<DateTime>(nullable: false),
                    PriceId = table.Column<int>(nullable: false),
                    VisitId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PrescriptionPurchases", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PrescriptionPurchases_Prescriptions_PrescriptionId",
                        column: x => x.PrescriptionId,
                        principalTable: "Prescriptions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PrescriptionPurchases_Prices_PriceId",
                        column: x => x.PriceId,
                        principalTable: "Prices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PrescriptionPurchases_Visits_VisitId",
                        column: x => x.VisitId,
                        principalTable: "Visits",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DrugPurchases",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    DrugId = table.Column<int>(nullable: false),
                    DrugCode = table.Column<string>(nullable: true),
                    PriceId = table.Column<int>(nullable: false),
                    PrescriptionPurchaseId = table.Column<int>(nullable: false),
                    DateOfPurchasing = table.Column<DateTime>(nullable: false),
                    VisitId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DrugPurchases", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DrugPurchases_Drugs_DrugId",
                        column: x => x.DrugId,
                        principalTable: "Drugs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DrugPurchases_PrescriptionPurchases_PrescriptionPurchaseId",
                        column: x => x.PrescriptionPurchaseId,
                        principalTable: "PrescriptionPurchases",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DrugPurchases_Prices_PriceId",
                        column: x => x.PriceId,
                        principalTable: "Prices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DrugPurchases_Visits_VisitId",
                        column: x => x.VisitId,
                        principalTable: "Visits",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Addresses_AddressCode",
                table: "Addresses",
                column: "AddressCode",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Addresses_NormalizedTitle",
                table: "Addresses",
                column: "NormalizedTitle",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Diagnoses_DiagnosisCode",
                table: "Diagnoses",
                column: "DiagnosisCode",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_DrugPurchases_DrugId",
                table: "DrugPurchases",
                column: "DrugId");

            migrationBuilder.CreateIndex(
                name: "IX_DrugPurchases_PrescriptionPurchaseId",
                table: "DrugPurchases",
                column: "PrescriptionPurchaseId");

            migrationBuilder.CreateIndex(
                name: "IX_DrugPurchases_PriceId",
                table: "DrugPurchases",
                column: "PriceId");

            migrationBuilder.CreateIndex(
                name: "IX_DrugPurchases_VisitId",
                table: "DrugPurchases",
                column: "VisitId");

            migrationBuilder.CreateIndex(
                name: "IX_Drugs_DrugCode",
                table: "Drugs",
                column: "DrugCode",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Drugs_Name",
                table: "Drugs",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Drugs_PriceId",
                table: "Drugs",
                column: "PriceId");

            migrationBuilder.CreateIndex(
                name: "IX_InsuranceCompanies_AddressId",
                table: "InsuranceCompanies",
                column: "AddressId");

            migrationBuilder.CreateIndex(
                name: "IX_InsuranceCompanies_InsuranceCompanyCode",
                table: "InsuranceCompanies",
                column: "InsuranceCompanyCode",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_InsuranceCompanies_StatusId",
                table: "InsuranceCompanies",
                column: "StatusId");

            migrationBuilder.CreateIndex(
                name: "IX_InsuranceCompanies_SupervisoryComissionId",
                table: "InsuranceCompanies",
                column: "SupervisoryComissionId");

            migrationBuilder.CreateIndex(
                name: "IX_InsuranceCompanies_UserId",
                table: "InsuranceCompanies",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_InsuranceConnections_InsuranceCompanyEmailId",
                table: "InsuranceConnections",
                column: "InsuranceCompanyEmailId");

            migrationBuilder.CreateIndex(
                name: "IX_InsuranceConnections_ThirdPartyAdministratorId",
                table: "InsuranceConnections",
                column: "ThirdPartyAdministratorId");

            migrationBuilder.CreateIndex(
                name: "IX_InsuranceConnections_ThirdPartyEmailId",
                table: "InsuranceConnections",
                column: "ThirdPartyEmailId");

            migrationBuilder.CreateIndex(
                name: "IX_InsurancePackages_PriceId",
                table: "InsurancePackages",
                column: "PriceId");

            migrationBuilder.CreateIndex(
                name: "IX_InsurancePackages_InsuranceCompanyId_Name",
                table: "InsurancePackages",
                columns: new[] { "InsuranceCompanyId", "Name" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_LabTestPurchases_LabTestId",
                table: "LabTestPurchases",
                column: "LabTestId");

            migrationBuilder.CreateIndex(
                name: "IX_LabTestPurchases_PrescribedLabTestId",
                table: "LabTestPurchases",
                column: "PrescribedLabTestId");

            migrationBuilder.CreateIndex(
                name: "IX_LabTestPurchases_PriceId",
                table: "LabTestPurchases",
                column: "PriceId");

            migrationBuilder.CreateIndex(
                name: "IX_LabTestPurchases_VisitId",
                table: "LabTestPurchases",
                column: "VisitId");

            migrationBuilder.CreateIndex(
                name: "IX_LabTests_LabTestCode",
                table: "LabTests",
                column: "LabTestCode",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_LabTests_Name",
                table: "LabTests",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_LabTests_NormalizedName",
                table: "LabTests",
                column: "NormalizedName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_LabTests_PriceId",
                table: "LabTests",
                column: "PriceId");

            migrationBuilder.CreateIndex(
                name: "IX_PackageDrugs_DrugId",
                table: "PackageDrugs",
                column: "DrugId");

            migrationBuilder.CreateIndex(
                name: "IX_PackageLabTests_LabTestId",
                table: "PackageLabTests",
                column: "LabTestId");

            migrationBuilder.CreateIndex(
                name: "IX_PackageRadiographs_RadiographId",
                table: "PackageRadiographs",
                column: "RadiographId");

            migrationBuilder.CreateIndex(
                name: "IX_PackageSrugeries_SurgeryId",
                table: "PackageSrugeries",
                column: "SurgeryId");

            migrationBuilder.CreateIndex(
                name: "IX_PatientPackages_DrugId",
                table: "PatientPackages",
                column: "DrugId");

            migrationBuilder.CreateIndex(
                name: "IX_PatientPackages_InsurancePackageId",
                table: "PatientPackages",
                column: "InsurancePackageId");

            migrationBuilder.CreateIndex(
                name: "IX_Patients_EmailId",
                table: "Patients",
                column: "EmailId");

            migrationBuilder.CreateIndex(
                name: "IX_Patients_PatientCode",
                table: "Patients",
                column: "PatientCode",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Patients_PhoneNumberId",
                table: "Patients",
                column: "PhoneNumberId");

            migrationBuilder.CreateIndex(
                name: "IX_Patients_RoleId",
                table: "Patients",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_Patients_UserId_AddressId",
                table: "Patients",
                columns: new[] { "UserId", "AddressId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Patients_UserId_RoleId",
                table: "Patients",
                columns: new[] { "UserId", "RoleId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_PrescribedLabTests_LabTestId",
                table: "PrescribedLabTests",
                column: "LabTestId");

            migrationBuilder.CreateIndex(
                name: "IX_PrescribedLabTests_PrescribedLabTestCode",
                table: "PrescribedLabTests",
                column: "PrescribedLabTestCode",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_PrescribedLabTests_VisitId",
                table: "PrescribedLabTests",
                column: "VisitId");

            migrationBuilder.CreateIndex(
                name: "IX_PrescribedRadiographs_PrescribedRadiographCode",
                table: "PrescribedRadiographs",
                column: "PrescribedRadiographCode",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_PrescribedRadiographs_RadiographId",
                table: "PrescribedRadiographs",
                column: "RadiographId");

            migrationBuilder.CreateIndex(
                name: "IX_PrescribedRadiographs_VisitId",
                table: "PrescribedRadiographs",
                column: "VisitId");

            migrationBuilder.CreateIndex(
                name: "IX_PrescribedSurgeries_SurgeryId",
                table: "PrescribedSurgeries",
                column: "SurgeryId");

            migrationBuilder.CreateIndex(
                name: "IX_PrescribedSurgeries_VisitId",
                table: "PrescribedSurgeries",
                column: "VisitId");

            migrationBuilder.CreateIndex(
                name: "IX_PrescriptionDrugs_DrugId",
                table: "PrescriptionDrugs",
                column: "DrugId");

            migrationBuilder.CreateIndex(
                name: "IX_PrescriptionPurchases_PrescriptionId",
                table: "PrescriptionPurchases",
                column: "PrescriptionId");

            migrationBuilder.CreateIndex(
                name: "IX_PrescriptionPurchases_PriceId",
                table: "PrescriptionPurchases",
                column: "PriceId");

            migrationBuilder.CreateIndex(
                name: "IX_PrescriptionPurchases_VisitId",
                table: "PrescriptionPurchases",
                column: "VisitId");

            migrationBuilder.CreateIndex(
                name: "IX_Prescriptions_VisitId",
                table: "Prescriptions",
                column: "VisitId");

            migrationBuilder.CreateIndex(
                name: "IX_Prices_PreviousPriceId",
                table: "Prices",
                column: "PreviousPriceId");

            migrationBuilder.CreateIndex(
                name: "IX_Prices_PriceCode",
                table: "Prices",
                column: "PriceCode",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_RadiographPurchases_PrescribedRadiographId",
                table: "RadiographPurchases",
                column: "PrescribedRadiographId");

            migrationBuilder.CreateIndex(
                name: "IX_RadiographPurchases_PriceId",
                table: "RadiographPurchases",
                column: "PriceId");

            migrationBuilder.CreateIndex(
                name: "IX_RadiographPurchases_RadiographId",
                table: "RadiographPurchases",
                column: "RadiographId");

            migrationBuilder.CreateIndex(
                name: "IX_RadiographPurchases_VisitId",
                table: "RadiographPurchases",
                column: "VisitId");

            migrationBuilder.CreateIndex(
                name: "IX_Radiographs_Name",
                table: "Radiographs",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Radiographs_NormalizedName",
                table: "Radiographs",
                column: "NormalizedName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Radiographs_PriceId",
                table: "Radiographs",
                column: "PriceId");

            migrationBuilder.CreateIndex(
                name: "IX_Radiographs_RadiographCode",
                table: "Radiographs",
                column: "RadiographCode",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_RolePermissions_PermissionId",
                table: "RolePermissions",
                column: "PermissionId");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceProviderConnectionInsuranceCompanies_InsuranceCompan~",
                table: "ServiceProviderConnectionInsuranceCompanies",
                column: "InsuranceCompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceProviderConnections_ServiceProviderConnectionCode",
                table: "ServiceProviderConnections",
                column: "ServiceProviderConnectionCode",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ServiceProviderConnections_ServiceProviderId",
                table: "ServiceProviderConnections",
                column: "ServiceProviderId");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceProviderConnections_TpaId",
                table: "ServiceProviderConnections",
                column: "TpaId");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceProviders_EmailId",
                table: "ServiceProviders",
                column: "EmailId");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceProviders_PhoneNumberId",
                table: "ServiceProviders",
                column: "PhoneNumberId");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceProviders_RoleId",
                table: "ServiceProviders",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceProviders_ServiceProviderCode",
                table: "ServiceProviders",
                column: "ServiceProviderCode",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ServiceProviders_StatusId",
                table: "ServiceProviders",
                column: "StatusId");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceProviders_UserId_AddressId",
                table: "ServiceProviders",
                columns: new[] { "UserId", "AddressId" });

            migrationBuilder.CreateIndex(
                name: "IX_ServiceProviders_UserId_RoleId",
                table: "ServiceProviders",
                columns: new[] { "UserId", "RoleId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ServiceProviderStaffer_ServiceStafferId",
                table: "ServiceProviderStaffer",
                column: "ServiceStafferId");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceStaffers_EmailId",
                table: "ServiceStaffers",
                column: "EmailId");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceStaffers_PhoneNumberId",
                table: "ServiceStaffers",
                column: "PhoneNumberId");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceStaffers_RoleId",
                table: "ServiceStaffers",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceStaffers_ServiceStafferCode",
                table: "ServiceStaffers",
                column: "ServiceStafferCode",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ServiceStaffers_StatusId",
                table: "ServiceStaffers",
                column: "StatusId");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceStaffers_UserId_AddressId",
                table: "ServiceStaffers",
                columns: new[] { "UserId", "AddressId" });

            migrationBuilder.CreateIndex(
                name: "IX_ServiceStaffers_UserId_RoleId",
                table: "ServiceStaffers",
                columns: new[] { "UserId", "RoleId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_SupervisoryComissions_AddressId",
                table: "SupervisoryComissions",
                column: "AddressId");

            migrationBuilder.CreateIndex(
                name: "IX_Surgeries_Name",
                table: "Surgeries",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Surgeries_NormalizedName",
                table: "Surgeries",
                column: "NormalizedName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Surgeries_PriceId",
                table: "Surgeries",
                column: "PriceId");

            migrationBuilder.CreateIndex(
                name: "IX_Surgeries_SurgeryCode",
                table: "Surgeries",
                column: "SurgeryCode",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_SurgeryPurchases_PrescribedSurgeryId",
                table: "SurgeryPurchases",
                column: "PrescribedSurgeryId");

            migrationBuilder.CreateIndex(
                name: "IX_SurgeryPurchases_PriceId",
                table: "SurgeryPurchases",
                column: "PriceId");

            migrationBuilder.CreateIndex(
                name: "IX_SurgeryPurchases_SurgeryId",
                table: "SurgeryPurchases",
                column: "SurgeryId");

            migrationBuilder.CreateIndex(
                name: "IX_SurgeryPurchases_VisitId",
                table: "SurgeryPurchases",
                column: "VisitId");

            migrationBuilder.CreateIndex(
                name: "IX_ThirdPartyAdministrators_AddressId",
                table: "ThirdPartyAdministrators",
                column: "AddressId");

            migrationBuilder.CreateIndex(
                name: "IX_ThirdPartyAdministrators_StatusId",
                table: "ThirdPartyAdministrators",
                column: "StatusId");

            migrationBuilder.CreateIndex(
                name: "IX_ThirdPartyAdministrators_SupervisoryComissionId",
                table: "ThirdPartyAdministrators",
                column: "SupervisoryComissionId");

            migrationBuilder.CreateIndex(
                name: "IX_ThirdPartyAdministrators_ThirdPartyCode",
                table: "ThirdPartyAdministrators",
                column: "ThirdPartyCode",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ThirdPartyAdministrators_UserId",
                table: "ThirdPartyAdministrators",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserAddresses_AddressId",
                table: "UserAddresses",
                column: "AddressId");

            migrationBuilder.CreateIndex(
                name: "IX_UserEmails_Address",
                table: "UserEmails",
                column: "Address",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserEmails_EmailCode",
                table: "UserEmails",
                column: "EmailCode",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserEmails_UserId",
                table: "UserEmails",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserPhoneNumbers_Number",
                table: "UserPhoneNumbers",
                column: "Number",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserPhoneNumbers_PhoneCode",
                table: "UserPhoneNumbers",
                column: "PhoneCode",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserPhoneNumbers_UserId",
                table: "UserPhoneNumbers",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserRoleRequests_RoleId",
                table: "UserRoleRequests",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_UserRoleRequests_UserEmailId",
                table: "UserRoleRequests",
                column: "UserEmailId");

            migrationBuilder.CreateIndex(
                name: "IX_UserRoleRequests_UserPhoneNumberId",
                table: "UserRoleRequests",
                column: "UserPhoneNumberId");

            migrationBuilder.CreateIndex(
                name: "IX_UserRoleRequests_UserId_AddressId",
                table: "UserRoleRequests",
                columns: new[] { "UserId", "AddressId" });

            migrationBuilder.CreateIndex(
                name: "IX_VisitClaims_InsuranceCompanyId",
                table: "VisitClaims",
                column: "InsuranceCompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_VisitClaims_ServiceProviderId",
                table: "VisitClaims",
                column: "ServiceProviderId");

            migrationBuilder.CreateIndex(
                name: "IX_VisitClaims_ThirdPartyAdministratorId",
                table: "VisitClaims",
                column: "ThirdPartyAdministratorId");

            migrationBuilder.CreateIndex(
                name: "IX_VisitClaims_VisitClaimCode",
                table: "VisitClaims",
                column: "VisitClaimCode",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_VisitClaims_VisitId",
                table: "VisitClaims",
                column: "VisitId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Visits_BaseVisitId",
                table: "Visits",
                column: "BaseVisitId");

            migrationBuilder.CreateIndex(
                name: "IX_Visits_DiagnosisId",
                table: "Visits",
                column: "DiagnosisId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Visits_PatientId",
                table: "Visits",
                column: "PatientId");

            migrationBuilder.CreateIndex(
                name: "IX_Visits_ServiceProviderConnectionId",
                table: "Visits",
                column: "ServiceProviderConnectionId");

            migrationBuilder.CreateIndex(
                name: "IX_Visits_ServiceProviderId",
                table: "Visits",
                column: "ServiceProviderId");

            migrationBuilder.CreateIndex(
                name: "IX_Visits_ServiceStafferId",
                table: "Visits",
                column: "ServiceStafferId");

            migrationBuilder.CreateIndex(
                name: "IX_Visits_VisitCode",
                table: "Visits",
                column: "VisitCode",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Visits_PatientPackagePatientId_PatientPackageInsurancePacka~",
                table: "Visits",
                columns: new[] { "PatientPackagePatientId", "PatientPackageInsurancePackageId" });

            migrationBuilder.CreateIndex(
                name: "IX_Visits_ServiceProviderStafferServiceProviderId_ServiceProvi~",
                table: "Visits",
                columns: new[] { "ServiceProviderStafferServiceProviderId", "ServiceProviderStafferServiceStafferId" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "DrugPurchases");

            migrationBuilder.DropTable(
                name: "InsuranceConnections");

            migrationBuilder.DropTable(
                name: "LabTestPurchases");

            migrationBuilder.DropTable(
                name: "PackageDrugs");

            migrationBuilder.DropTable(
                name: "PackageLabTests");

            migrationBuilder.DropTable(
                name: "PackageRadiographs");

            migrationBuilder.DropTable(
                name: "PackageSrugeries");

            migrationBuilder.DropTable(
                name: "PrescriptionDrugs");

            migrationBuilder.DropTable(
                name: "RadiographPurchases");

            migrationBuilder.DropTable(
                name: "RolePermissions");

            migrationBuilder.DropTable(
                name: "ServiceProviderConnectionInsuranceCompanies");

            migrationBuilder.DropTable(
                name: "SurgeryPurchases");

            migrationBuilder.DropTable(
                name: "UserRoleRequests");

            migrationBuilder.DropTable(
                name: "VisitClaims");

            migrationBuilder.DropTable(
                name: "PrescriptionPurchases");

            migrationBuilder.DropTable(
                name: "PrescribedLabTests");

            migrationBuilder.DropTable(
                name: "PrescribedRadiographs");

            migrationBuilder.DropTable(
                name: "Permissions");

            migrationBuilder.DropTable(
                name: "PrescribedSurgeries");

            migrationBuilder.DropTable(
                name: "Prescriptions");

            migrationBuilder.DropTable(
                name: "LabTests");

            migrationBuilder.DropTable(
                name: "Radiographs");

            migrationBuilder.DropTable(
                name: "Surgeries");

            migrationBuilder.DropTable(
                name: "Visits");

            migrationBuilder.DropTable(
                name: "Diagnoses");

            migrationBuilder.DropTable(
                name: "ServiceProviderConnections");

            migrationBuilder.DropTable(
                name: "PatientPackages");

            migrationBuilder.DropTable(
                name: "ServiceProviderStaffer");

            migrationBuilder.DropTable(
                name: "ThirdPartyAdministrators");

            migrationBuilder.DropTable(
                name: "Drugs");

            migrationBuilder.DropTable(
                name: "InsurancePackages");

            migrationBuilder.DropTable(
                name: "Patients");

            migrationBuilder.DropTable(
                name: "ServiceProviders");

            migrationBuilder.DropTable(
                name: "ServiceStaffers");

            migrationBuilder.DropTable(
                name: "InsuranceCompanies");

            migrationBuilder.DropTable(
                name: "Prices");

            migrationBuilder.DropTable(
                name: "UserEmails");

            migrationBuilder.DropTable(
                name: "UserPhoneNumbers");

            migrationBuilder.DropTable(
                name: "UserAddresses");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "Statuses");

            migrationBuilder.DropTable(
                name: "SupervisoryComissions");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "Addresses");

            migrationBuilder.DropSequence(
                name: "AddressNumbers");

            migrationBuilder.DropSequence(
                name: "DiagnosisNumbers");

            migrationBuilder.DropSequence(
                name: "DrugsCode");

            migrationBuilder.DropSequence(
                name: "InsuranceCompaniesNumbers");

            migrationBuilder.DropSequence(
                name: "InsuranceConnectionsNumbers");

            migrationBuilder.DropSequence(
                name: "LabTestsCode");

            migrationBuilder.DropSequence(
                name: "PatientsNumbers");

            migrationBuilder.DropSequence(
                name: "PrescribedLabTestsCode");

            migrationBuilder.DropSequence(
                name: "PrescribedRadiographsCode");

            migrationBuilder.DropSequence(
                name: "PrescribedSurgeriesCode");

            migrationBuilder.DropSequence(
                name: "PrescriptionsCode");

            migrationBuilder.DropSequence(
                name: "PricesCode");

            migrationBuilder.DropSequence(
                name: "RadiographCode");

            migrationBuilder.DropSequence(
                name: "ServiceProviderConnectionsNumbers");

            migrationBuilder.DropSequence(
                name: "ServiceProviderNumbers");

            migrationBuilder.DropSequence(
                name: "ServiceProviderStaffCode");

            migrationBuilder.DropSequence(
                name: "SurgeriesCode");

            migrationBuilder.DropSequence(
                name: "ThirdPartyAdministratorsNumbers");

            migrationBuilder.DropSequence(
                name: "UserEmailsCode");

            migrationBuilder.DropSequence(
                name: "UserPhoneNumbersCode");

            migrationBuilder.DropSequence(
                name: "VisitClaimsCode");

            migrationBuilder.DropSequence(
                name: "VisitNumbers");
        }
    }
}
