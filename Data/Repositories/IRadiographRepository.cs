using System.Collections.Generic;
using PatientsRecordsManager.Data.Dtos.Radiographs;

namespace PatientsRecordsManager.Data.Repositories
{
    public interface IRadiographRepository
    {
        RadiographForReturnDto AddRadiograph(RadiographForCreateDto radiograph);
        RadiographForReturnDto ModifyRadiograph(string radiographCode, RadiographForUpdateDto radiograph);
        bool DeleteRadiograph(string radiographCode);
        RadiographForReturnDto GetRadiographDetails(string radiographCode);
        IEnumerable<RadiographForReturnDto> GetRadiographs(string name);
    }
}