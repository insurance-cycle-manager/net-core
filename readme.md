<p align="center">
  <div align="center">
    <a href="" rel="noopener">
        <img width=132px height=100px src="./Documentation/Images/logo.png"  alt="Patient Records Manager">
    </a>
  </div>
</p>

<h3 align="center">Patients Records Manager</h3>

<div align="center">

[![Status](https://img.shields.io/badge/status-active-success.svg)]()
[![GitHub Issues](https://img.shields.io/github/issues/kylelobo/The-Documentation-Compendium.svg)](https://github.com/kylelobo/The-Documentation-Compendium/issues)
[![GitHub Pull Requests](https://img.shields.io/github/issues-pr/kylelobo/The-Documentation-Compendium.svg)](https://github.com/kylelobo/The-Documentation-Compendium/pulls)
[![License](https://img.shields.io/badge/license-MIT-blue.svg)](/LICENSE)

</div>

---

<p align="center">
    This software can be used as an information bank for all medical data. 
    We can integrate different medical data sources to manage and control the healthcare providing workflows.
</p>

## 📝 Table of Contents

- [About](#about)
- [Getting Started](#getting_started)
- [Deployment](#deployment)
- [Usage](#usage)
- [Built Using](#built_using)
- [Documentation](../Documentation/APIs.md)
- [Contributing](../CONTRIBUTING.md)
- [Authors](#authors)
- [Acknowledgments](#acknowledgement)

## 🧐 About <a name = "about"></a>

This software “Medicare” will be used to organize patient’s medical data, and to define rules to
control access it. For example, this software can be considered as a workbook containing all
medical information for patients. Only those who are permitted, are able to access (Add, Delete,
Modify) this workbook.
This software is flexible enough to allow healthcare providers to define access policies as much
as their business needs.
“Medicare” provides unified platform to control medical data. In addition, this software takes
care of urgent and emergency cases.
However, this system is a combination of two subsystems:

- Access Control Management Subsystem.
- Medical Records Management Subsystem.

## 🏁 Getting Started <a name = "getting_started"></a>

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See [deployment](#deployment) for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them.

```
Give examples
```

### Installing

A step by step series of examples that tell you how to get a development env running.

Say what the step will be

```
Give the example
```

And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo.

## 🔧 Running the tests <a name = "tests"></a>

Explain how to run the automated tests for this system.

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## 🎈 Usage <a name="usage"></a>

Add notes about how to use the system.

## 🚀 Deployment <a name = "deployment"></a>

Add additional notes about how to deploy this on a live system.

## ⛏️ Built Using <a name = "built_using"></a>

- [NetCore](https://dotnet.microsoft.com/apps/aspnet/) - Asp NET Core 3.1
- [PostgresSQL](https://www.postgresql.org/) - PostgreSQL
- [Flutter](https://flutter.dev/) - Flutter

## ✍️ Authors <a name = "authors"></a>

- [@nazeer](https://github.com/nazeer-allahham) - Team Leader & Back-end Developer

See also the list of [contributors](http://git.dev.sy/n.lahham/medical-records-management/-/graphs/master) who participated in this project.

## 🎉 Acknowledgements <a name = "acknowledgement"></a>

- Hat tip to anyone whose code was used
- Inspiration
- References
