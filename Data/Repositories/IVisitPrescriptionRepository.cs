using System.Collections.Generic;
using System.Threading.Tasks;
using PatientsRecordsManager.Data.Dtos.Prescribed.Prescriptions;

namespace PatientsRecordsManager.Data.Repositories
{
    public interface IVisitPrescriptionRepository
    {
        PrescriptionForReturnDto AddPrescription(string visitCode, PrescriptionForCreateDto prescription);
        IEnumerable<PrescriptionForReturnDto> GetPrescriptions(string visitCode);
        PrescriptionForReturnDto GetPrescriptionDetails(string visitCode, string prescriptionCode);
        PrescriptionForReturnDto ModifyPrescription(string visitCode, string prescriptionCode, PrescriptionForUpdateDto prescription);
        bool RemovePrescription(string visitCode, string prescriptionCode);
    }
}