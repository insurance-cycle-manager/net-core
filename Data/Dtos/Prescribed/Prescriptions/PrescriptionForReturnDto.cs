using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using PatientsRecordsManager.Data.Dtos.Drugs;

namespace PatientsRecordsManager.Data.Dtos.Prescribed.Prescriptions
{
    public class PrescriptionForReturnDto
    {
        [Required]
        public string PrescriptionCode { get; set; }

        [Required]
        public string VisitCode { get; set; }

        [Required]
        public string Note { get; set; }

        public IEnumerable<DrugForReturnDto> Drugs { get; set; }
        public IEnumerable<PrescriptionPurchaseForReturnDto> Purchases { get; set; }
    }
}