using System;
using System.ComponentModel.DataAnnotations;
using PatientsRecordsManager.Constants.Enums;

namespace PatientsRecordsManager.Data.Dtos.ServiceProviders
{
    public class ServiceProviderForCreateDto
    {
        [Required]
        [EnumDataType(typeof(ServiceProviderType))]
        public ServiceProviderType Type { get; set; }
        [Required]
        public string NormalizedUserName { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public int AddressId { get; set; }
        [Required]
        public int EmailId { get; set; }
        [Required]
        public int PhoneNumberId { get; set; }
        public DateTime JoinedAt { get; set; }
        [Required]
        public int UserId { get; set; }
        [Required]
        public int RoleId { get; set; }

        public ServiceProviderForCreateDto()
        {
            JoinedAt = DateTime.Now;
        }
    }
}