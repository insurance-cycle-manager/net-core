using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using PatientsRecordsManager.Constants;
using PatientsRecordsManager.Models;

namespace PatientsRecordsManager.Data.Seeds
{
    public class Seeder
    {
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<Role> _roleManager;
        private readonly DataContext _context;

        public Seeder(UserManager<User> userManager, RoleManager<Role> roleManager, DataContext context)
        {
            _context = context;
            _roleManager = roleManager;
            _userManager = userManager;
        }

        public void SeedUsers()
        {
            if (!_userManager.Users.Any())
            {
                var data = System.IO.File.ReadAllText("Data\\Seeds\\Users.json");
                var users = JsonConvert.DeserializeObject<List<User>>(data);

                var roles = _roleManager.Roles.ToList();

                var rand = new System.Random();
                foreach (var user in users)
                {
                    _userManager.CreateAsync(user, "A123456a").Wait();
                    _userManager.AddToRoleAsync(user, roles[rand.Next(roles.Count)].Name).Wait();
                }

                var administrator = new User
                {
                    UserName = "Admin",
                };
                IdentityResult result = _userManager.CreateAsync(administrator, "Admin123").Result;

                if (result.Succeeded)
                {
                    var admin = _userManager.FindByNameAsync(administrator.UserName).Result;
                    _userManager.AddToRoleAsync(admin, RolesNames.Administrator).Wait();
                }
            }
        }

        public void SeedRoles()
        {
            var roles = new List<Role>
                {
                    new Role{ Name = "User" },
                    new Role{ Name = "Administrator" },
                    new Role{ Name = "InsuranceCompany" },
                    new Role{ Name = "TPA" },
                    new Role{ Name = "Doctor" },
                    new Role{ Name = "Nurse" },
                    new Role{ Name = "Pharmacist" },
                    new Role{ Name = "LabSpecialist" },
                    new Role{ Name = "Patient" },
                    new Role{ Name = "Radiographer" },
                    new Role{ Name = "Hospital"},
                    new Role{ Name = "Pharmacy"},
                    new Role{ Name = "Clinic"},
                    new Role{ Name = "Laboratory"},
                    new Role{ Name = "RadiographOffice"},
                };

            foreach (var role in roles)
            {
                _roleManager.CreateAsync(role).Wait();
            }
        }

        public void SeedUserRoles()
        {
            var users = _userManager.Users
                .Include(u => u.UserRoles)
                .ToList();
            var roles = _roleManager.Roles.ToList();
            var rand = new System.Random();

            foreach (var user in users)
            {
                for (int i = 0; i < 3; i++)
                {
                    var email = new UserEmail
                    {
                        Address = string.Format(CultureInfo.CurrentCulture, "{0}{1}@mail.com", user.UserName, i + 1).ToLower(CultureInfo.CurrentCulture),
                        UserId = user.Id
                    };

                    if (i == 0)
                    {
                        user.Email = email.Address;
                    }

                    _context.UserEmails.Add(email);
                }
            }
            _context.SaveChanges();
        }

        private List<Price> SeedPrices()
        {
            var data = System.IO.File.ReadAllText("Data\\Seeds\\Prices.json");
            var prices = JsonConvert.DeserializeObject<List<Price>>(data);

            prices.ForEach(price => _context.Prices.Add(price));

            _context.SaveChanges();
            return prices;
        }

        public void SeedDrugs()
        {
            var prices = SeedPrices();

            var data = System.IO.File.ReadAllText("Data\\Seeds\\Drugs.json");
            var drugs = JsonConvert.DeserializeObject<List<Drug>>(data);

            var rand = new System.Random();
            drugs.ForEach(drug =>
            {
                drug.Price = prices[rand.Next(prices.Count)];
                drug.NormalizedName = drug.Name.Normalize();
                _context.Drugs.Add(drug);
            });

            _context.SaveChanges();
        }

        public void SeedRadiographs()
        {
            var data = System.IO.File.ReadAllText("Data\\Seeds\\Radiographs.json");
            var radiographs = JsonConvert.DeserializeObject<List<Radiograph>>(data);

            radiographs.ForEach(radiograph =>
            {
                radiograph.NormalizedName = radiograph.Name.Normalize();
                _context.Radiographs.Add(radiograph);
            });

            _context.SaveChanges();
        }

        public void SeedDefaulValues()
        {
            _context.SupervisoryComissions.Add(new SupervisoryComission { Name = "DefaulSupervisoryComission" });
            _context.SaveChanges();
        }
    }
}