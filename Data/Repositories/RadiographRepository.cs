using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using PatientsRecordsManager.ABAC.Extensions;
using PatientsRecordsManager.ABAC.Extensions.EF;
using PatientsRecordsManager.Data.Dtos.Radiographs;
using PatientsRecordsManager.Models;
using PatientsRecordsManager.Resources;

namespace PatientsRecordsManager.Data.Repositories
{
    public class RadiographRepository : IRadiographRepository
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        private readonly Messages _messages;

        public RadiographRepository(DataContext context, IMapper mapper, Messages messages)
        {
            _mapper = mapper;
            _context = context;
            _messages = messages;
        }

        public RadiographForReturnDto AddRadiograph(RadiographForCreateDto radiographForCreate)
        {
            var radiograph = _mapper.Map<Radiograph>(radiographForCreate);
            _context.Radiographs.Add(radiograph);

            if (_context.SaveChanges() <= 0)
            {
                throw new Exception(_messages.GetString("saveFailed"));
            }
            return _mapper.Map<RadiographForReturnDto>(radiograph);
        }

        public bool DeleteRadiograph(string radiographCode)
        {
            var radiograph = GetRadiograph(radiographCode);
            if (radiograph == null)
            {
                return false;
            }
            _context.Radiographs.Remove(radiograph);
            if (_context.SaveChanges() <= 0)
            {
                return false;
            }
            return true;
        }

        public RadiographForReturnDto GetRadiographDetails(string radiographCode)
        {
            return _mapper.Map<RadiographForReturnDto>(GetRadiograph(radiographCode));
        }

        public IEnumerable<RadiographForReturnDto> GetRadiographs(string name)
        {
            var radiographs = _context.Radiographs.AsQueryable();
            if (!string.IsNullOrWhiteSpace(name))
            {
                radiographs = radiographs.Where(r => r.NormalizedName.ToUpper().Contains(name.ToUpper()));
            }
            return _mapper.Map<List<RadiographForReturnDto>>(radiographs);
        }

        public RadiographForReturnDto ModifyRadiograph(string radiographCode, RadiographForUpdateDto radiographForUpdate)
        {
            var retrievedRadiograph = GetRadiograph(radiographCode);
            var radiograph = _mapper.Map<RadiographForUpdateDto, Radiograph>(radiographForUpdate, retrievedRadiograph);

            if (_context.SaveChanges() <= 0)
            {
                throw new Exception(_messages.GetString("saveFailed"));
            }
            return _mapper.Map<RadiographForReturnDto>(radiograph);
        }

        private Radiograph GetRadiograph(string radiographCode)
        {
            var radiograph = _context.Radiographs
                .Include(r => r.Price)
                .FirstOrDefault(e => e.RadiographCode.ToUpper() == (radiographCode.ToUpper()));

            if (radiograph == null)
            {
                throw new Exception(_messages.GetString("radiographNotFound", radiographCode));
            }
            return radiograph;
        }
    }
}