using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PatientsRecordsManager.Constants;
using PatientsRecordsManager.Data.Dtos.Prescribed.Radiographs;
using PatientsRecordsManager.Data.Repositories;
using PatientsRecordsManager.Extensions;
using PatientsRecordsManager.Resources;

namespace PatientsRecordsManager.Controllers
{
    [ApiController]
    [Route(APIs.VisitRadiographsBaseUrl)]
    [Authorize]
    public class VisitRadiographsController : PrmController
    {
        private readonly IVisitRadiographRepository _repository;
        private readonly Messages _messages;

        public VisitRadiographsController(IVisitRadiographRepository repository, Messages messages)
        {
            _repository = repository;
            _messages = messages;
        }

        [HttpPost(APIs.PostRadiograph)]
        public IActionResult AddRadiograph(string visitCode, PrescribedRadiographForCreateDto radiograph)
        {
            return HttpContext.Success(_repository.AddRadiograph(visitCode, radiograph), _messages.GetString("CompleteAddVisitRadiograph"));
        }

        [HttpPut(APIs.ModifyRadiograph)]
        public IActionResult ModifyRadiograph(string visitCode, string radiographCode, PrescribedRadiographForUpdateDto radiograph)
        {
            return HttpContext.Success(_repository.ModifyRadiograph(visitCode, radiographCode, radiograph), _messages.GetString("CompleteUpdateVisitRadiograph"));
        }

        [HttpDelete(APIs.DeleteRadiograph)]
        public IActionResult DeleteRadiograph(string visitCode, string radiographCode)
        {
            return HttpContext.Success(_repository.RemoveRadiograph(visitCode, radiographCode), _messages.GetString("CompleteDeleteVisitRadiograph"));
        }

        [HttpGet(APIs.GetRadiographs)]
        public IActionResult GetRadiographs(string visitCode)
        {
            return HttpContext.Success(_repository.GetRadiographs(visitCode), _messages.GetString("CompleteFetchVisitRadiographs"));
        }

        [HttpGet(APIs.GetRadiographDetails)]
        public IActionResult GetRadiograph(string visitCode, string radiographCode)
        {
            return HttpContext.Success(_repository.GetRadiographDetails(visitCode, radiographCode), _messages.GetString("CompleteFetchVisitRadiographDetails", radiographCode));
        }
    }
}