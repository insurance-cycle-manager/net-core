using System.ComponentModel.DataAnnotations;

namespace PatientsRecordsManager.Models
{
    public class UserEmail
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string EmailCode { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public bool Confirmed { get; set; } = false;
        [Required]
        public int UserId { get; set; }
        public User User { get; set; }
    }
}