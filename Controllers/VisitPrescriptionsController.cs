using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PatientsRecordsManager.Constants;
using PatientsRecordsManager.Data;
using PatientsRecordsManager.Data.Dtos.Prescribed.Prescriptions;
using PatientsRecordsManager.Data.Repositories;
using PatientsRecordsManager.Extensions;

namespace PatientsRecordsManager.Controllers
{
    [ApiController]
    [Route(APIs.VisitPrescriptionsBaseUrl)]
    [Authorize]
    public class VisitPrescriptionsController : PrmController
    {
        private readonly IVisitPrescriptionRepository _repository;
        private readonly Resources.Messages _messages;

        public VisitPrescriptionsController(IVisitPrescriptionRepository repository, Resources.Messages messages)
        {
            _repository = repository;
            _messages = messages;
        }

        [HttpGet(APIs.GetPrescriptions)]
        public IActionResult GetVisitPrescriptions(string visitCode)
        {
            return HttpContext.Success(
                _repository.GetPrescriptions(visitCode),
                _messages.GetString("prescriptionsFetchDone"));
        }

        [HttpGet(APIs.GetPrescription)]
        public IActionResult GetVisitPrescription(string visitCode, string prescriptionCode)
        {
            return HttpContext.Success(
                _repository.GetPrescriptionDetails(visitCode, prescriptionCode),
                _messages.GetString("prescriptionFetchDone", visitCode, prescriptionCode));
        }

        [HttpPost(APIs.AddPrescription)]
        public IActionResult AddVisitPrescription(string visitCode, PrescriptionForCreateDto prescription)
        {
            return HttpContext.Success(
                _repository.AddPrescription(visitCode, prescription),
                _messages.GetString("prescriptionAddDone", visitCode));
        }

        [HttpPut(APIs.ModifyPrescription)]
        public IActionResult ModifyVisitPrescription(string visitCode, string prescriptionCode, PrescriptionForUpdateDto prescription)
        {
            return HttpContext.Success(
                _repository.ModifyPrescription(visitCode, prescriptionCode, prescription),
                _messages.GetString("prescriptionModifyDone", visitCode, prescriptionCode));
        }

        [HttpDelete(APIs.DeletePrescription)]
        public IActionResult RemoveVisitPrescription(string visitCode, string prescriptionCode)
        {
            return HttpContext.Success(
                _repository.RemovePrescription(visitCode, prescriptionCode),
                _messages.GetString("prescriptionRemoveDone", visitCode, prescriptionCode));
        }
    }
}