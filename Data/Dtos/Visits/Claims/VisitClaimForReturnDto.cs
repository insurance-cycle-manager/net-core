namespace PatientsRecordsManager.Data.Dtos.Visits.Claims
{
    public class VisitClaimForReturnDto
    {
        public string VisitClaimCode { get; set; }
        public string VisitCode { get; set; }
        public string Status { get; set; }
    }
}