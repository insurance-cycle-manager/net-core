﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PatientsRecordsManager.Migrations
{
    public partial class removeStatusFromVisit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Type",
                table: "Visits");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Type",
                table: "Visits",
                type: "text",
                nullable: false,
                defaultValue: "");
        }
    }
}
