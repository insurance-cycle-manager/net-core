using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Microsoft.Extensions.Logging;
using PatientsRecordsManager.ABAC.Data;
using PatientsRecordsManager.ABAC.Data.Models;
using PatientsRecordsManager.ABAC.Functions;

namespace PatientsRecordsManager.ABAC.Enforcers
{
    public class ActionEnforcer : MyEnforcer
    {
        public ActionEnforcer(AbacDbContext context, ILogger<MyEnforcer> logger) : base(".\\ABAC\\Models\\act.config", context, logger)
        {
            this.AddFunction("subMatch", new MatchSubjects("subMatch"));
            this.EnableEnforce(true);
        }

        protected override DataAccessRequestDetails ComposeRequestData(params object[] rValues)
        {
            // ClaimsPrincipal sub, object o, string act
            if (rValues.Length != 3)
                throw new ArgumentException(ComposeRequestDataValuesLength);

            IEnumerable<string> roles = (rValues[0] as ClaimsPrincipal).Claims.Select(c => c.Value);
            string o = $"{rValues[1]}";
            string act = $"{rValues[2]}";


            return new DataAccessRequestDetails
            {
                Roles = roles.ToArray(),
                Object = o,
                Attributes = null,
                Action = act
            };
        }
    }
}