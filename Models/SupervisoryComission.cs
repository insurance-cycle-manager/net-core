using System.Collections.Generic;

namespace PatientsRecordsManager.Models
{
    public class SupervisoryComission
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Address Address { get; set; }
        public ICollection<ThirdPartyAdministrator> ThirdPartyAdministrators { get; set; }
        public ICollection<InsuranceCompany> InsuranceCompanies { get; set; }


    }
}