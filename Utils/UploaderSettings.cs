namespace PatientsRecordsManager.Utils
{
    public class UploaderSettings
    {
        public string Internal { get; set; }
        public string External { get; set; }
    }
}