using System.Collections.Generic;
using PatientsRecordsManager.Constants.Enums;
using PatientsRecordsManager.Data.Dtos.ServiceProviders.Staffers;

namespace PatientsRecordsManager.Data.Repositories
{
    public interface IServiceStafferRepository
    {
        List<StafferForReturnDto> GetServiceStaffers(ServiceProviderStafferType type, string title = null);
        List<StafferForReturnDto> GetServiceStaffers();
        StafferForReturnDto GetServiceStafferDetails(ServiceProviderStafferType type, string code);
        StafferForReturnDto GetServiceStafferDetails(ServiceProviderStafferType type, int userId);
        StafferForReturnDto AddServiceStaffer(StafferForCreateDto serviceStafferForCreate);
        StafferForReturnDto ModifyServiceStaffer(ServiceProviderStafferType type, string code, StafferForUpdateDto serviceStafferForUpdate);
        StafferForReturnDto ModifyServiceStaffer(ServiceProviderStafferType type, int userId, StafferForUpdateDto serviceStafferForUpdate);
        StafferForReturnDto DeleteServiceStaffer(ServiceProviderStafferType type, int userId);
        StafferForReturnDto ChangeServiceStafferStatus(ServiceProviderStafferType type, string code, StatusType status);
    }
}