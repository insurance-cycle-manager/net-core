using System;
using System.Diagnostics.CodeAnalysis;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using PatientsRecordsManager.ABAC.Extensions;
using PatientsRecordsManager.Resources;

namespace PatientsRecordsManager.ABAC
{
    public class AbacMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly Messages _messages;

        public AbacMiddleware(RequestDelegate next, Messages messages)
        {
            _next = next;
            _messages = messages;
        }

        public async Task Invoke([NotNullAttribute] HttpContext context, PoliciesManager policies)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));
            if (policies == null)
                throw new ArgumentNullException(nameof(policies));

            ClaimsPrincipal subject = context.User;
            string obj = context.Request.Path;
            string action = context.Request.Method;

            if (!policies.EvaluateRequestOfAction(context.TraceIdentifier, subject, obj, action))
            {
                await context.UnauthorizedMiddleware(_messages.GetString("unauthorized")).ConfigureAwait(false);
            }
            else
            {
                await _next(context).ConfigureAwait(false);
            }
        }
    }
}