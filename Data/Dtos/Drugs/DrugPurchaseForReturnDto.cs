using System;
using PatientsRecordsManager.Data.Dtos.Prescribed.Prescriptions;
using PatientsRecordsManager.Data.Dtos.Prices;

namespace PatientsRecordsManager.Data.Dtos.Drugs
{
    public class DrugPurchaseForReturnDto
    {
        public DrugForReturnDto Drug { get; set; }
        public PriceForReturnDto Price { get; set; }
        public PrescriptionPurchaseForReturnDto PrescriptionPurchase { get; set; }
        public DateTime DateOfPurchasing { get; set; }
    }
}