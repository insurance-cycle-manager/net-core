namespace PatientsRecordsManager.Models
{
    public class PackageLabTest
    {
        public int InsurancePackageId { get; set; }
        public InsurancePackage InsurancePackage { get; set; }

        public int LabTestId { get; set; }
        public LabTest LabTest { get; set; }
    }
}