namespace PatientsRecordsManager.Models
{
    public class PackageSurgery
    {
        public int InsurancePackageId { get; set; }
        public InsurancePackage InsurancePackage { get; set; }
        public int SurgeryId { get; set; }
        public Surgery Surgery { get; set; }
    }
}