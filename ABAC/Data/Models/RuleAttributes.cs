using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json.Linq;

namespace PatientsRecordsManager.ABAC.Data.Models
{
    public class RuleAttributes
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public JObject Attributes { get; set; }
    }
}