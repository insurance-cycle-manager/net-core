using System;
using System.Linq;
using AutoMapper;
using PatientsRecordsManager.Resources;
using PatientsRecordsManager.Data.Dtos.Prices;
using PatientsRecordsManager.Exceptions;
using PatientsRecordsManager.Models;
using PatientsRecordsManager.ABAC.Extensions.EF;
using PatientsRecordsManager.ABAC.Extensions;

namespace PatientsRecordsManager.Data.Repositories
{
    public class PriceRepository : IPriceRepository
    {
        private readonly IMapper _mapper;
        private readonly DataContext _context;
        private readonly Messages _messages;

        public PriceRepository(DataContext context, IMapper mapper, Messages messages)
        {
            _context = context;
            _mapper = mapper;
            _messages = messages;
        }

        public PriceForReturnDto AddPrice(PriceForAddDto priceForAdd, string previousPriceCode = null)
        {
            var price = _mapper.Map<Price>(priceForAdd);

            if (!string.IsNullOrWhiteSpace(previousPriceCode))
            {
                price.PreviousPrice = GetPriceEntity(previousPriceCode);
            }
            _context.Prices.Add(price);

            if (_context.SaveChanges() <= 0)
            {
                throw new SaveChangesFailedException();
            }
            return _mapper.Map<PriceForReturnDto>(price);
        }

        public PriceForReturnDto GetPrice(string priceCode)
        {
            return _mapper.Map<PriceForReturnDto>(GetPriceEntity(priceCode));
        }

        private Price GetPriceEntity(string priceCode)
        {
            return _context.Prices
                .FirstOrDefault(p => p.PriceCode.ToUpper() == (priceCode.ToUpper())) ??
                throw new ArgumentBadException(_messages.GetString("priceNotFound", priceCode));
        }
    }
}