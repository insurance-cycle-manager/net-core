using System;

namespace PatientsRecordsManager.Data.Dtos.Prescribed.LabTests
{
    public class PrescribedLabTestForUpdateDto
    {
        public string LabTestCode { get; set; }
        public DateTime ScheduledDate { get; set; }
        public string Note { get; set; }
    }
}