using PatientsRecordsManager.Data.Dtos.Prices;

namespace PatientsRecordsManager.Data.Dtos.Drugs
{
    public class DrugForReturnDto
    {
        public string DrugCode { get; set; }
        public string Name { get; set; }
        public string Manufacturer { get; set; }
        public PriceForReturnDto Price { get; set; }
    }
}