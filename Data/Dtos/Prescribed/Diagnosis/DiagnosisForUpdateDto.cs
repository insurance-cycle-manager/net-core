using System.ComponentModel.DataAnnotations;

namespace PatientsRecordsManager.Data.Dtos.Diagnosis
{
    public class DiagnosisForUpdateDto
    {
        [Required]
        public string Description { get; set; }
    }
}