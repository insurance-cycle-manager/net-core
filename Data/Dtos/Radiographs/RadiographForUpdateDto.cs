using PatientsRecordsManager.Data.Dtos.Prices;

namespace PatientsRecordsManager.Data.Dtos.Radiographs
{
    public class RadiographForUpdateDto
    {
        public string Name { get; set; }
        public PriceForAddDto Price { get; set; }
    }
}