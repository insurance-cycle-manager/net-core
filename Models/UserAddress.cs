using System.Collections.Generic;

namespace PatientsRecordsManager.Models
{
    public class UserAddress
    {
        public int UserId { get; set; }
        public User User { get; set; }

        public int AddressId { get; set; }
        public Address Address { get; set; }

        public bool Confirmed { get; set; }

        public ThirdPartyAdministrator Tpa { get; set; }
        public InsuranceCompany InsuranceCompnay { get; set; }
        public Patient Patient { get; set; }
        public IEnumerable<ServiceProvider> ServiceProviders { get; set; }
        public IEnumerable<ServiceStaffer> Staffers { get; set; }
        public IEnumerable<UserRoleRequest> Requests { get; set; }
    }
}