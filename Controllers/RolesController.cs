using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PatientsRecordsManager.ABAC.Data;
using PatientsRecordsManager.ABAC.Extensions;
using PatientsRecordsManager.ABAC.Extensions.EF;
using PatientsRecordsManager.Data.Dtos.Roles;
using PatientsRecordsManager.Extensions;
using PatientsRecordsManager.Models;
using PatientsRecordsManager.Resources;
using PatientsRecordsManager.Utils;

namespace PatientsRecordsManager.Controllers
{
    [ApiController]
    [Route(Constants.APIs.DefaultRoute)]
    public class RolesController : PrmController
    {
        private readonly RulesRepository _rules;
        private readonly IMapper _mapper;
        private readonly RoleManager<Role> _roleManager;
        private readonly Messages _messages;

        public RolesController(IMapper mapper, RulesRepository policies, RoleManager<Role> roleManager, Messages messages)
        {
            _mapper = mapper;
            _rules = policies;
            _roleManager = roleManager;
            _messages = messages;
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Index()
        {
            var roles = _roleManager.Roles.ToList();
            var toSendRoles = new List<RoleForReturnDto>();
            roles.ForEach((role) =>
            {
                toSendRoles.Add(_mapper.Map<RoleForReturnDto>(role));
            });
            return HttpContext.Success(toSendRoles, _messages.GetString("rolesFetchDone"));
        }

        [HttpPost]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> Add(RoleForCreateDto roleForCreate)
        {
            Role role = _mapper.Map<Role>(roleForCreate);
            await _roleManager.CreateAsync(role).ConfigureAwait(false);

            return HttpContext.Success(role, _messages.GetString("roleAddDone"));
        }

        [HttpDelete]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> Delete(string roleName)
        {
            Role role = await _roleManager.Roles
                .FirstOrDefaultAsync(r => r.NormalizedName.ToUpper() == (roleName.ToUpper()))
                .ConfigureAwait(false);

            if (role != null)
            {
                throw new KeyNotFoundException(_messages.GetString("UnknownRoleName"));
            }

            await _roleManager.DeleteAsync(role).ConfigureAwait(false);
            return HttpContext.Success(role, _messages.GetString("roleDeleteDone", roleName));
        }

        [HttpPut]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> Update(string roleName, RoleForUpdateDto roleForUpdate)
        {
            Assert.NotNull(roleForUpdate);

            Role role = await _roleManager.Roles
                .FirstOrDefaultAsync(r => r.NormalizedName.ToUpper() == (roleName.ToUpper()))
                .ConfigureAwait(false);

            if (role == null)
            {
                throw new KeyNotFoundException(_messages.GetString("UnknownRoleName"));
            }

            var result = await _roleManager.UpdateAsync(role).ConfigureAwait(false);
            if (!result.Succeeded)
            {
                return HttpContext.Error(result.Errors, _messages.GetString("saveFailed"));
            }
            return HttpContext.Success(roleForUpdate, _messages.GetString("roleUpdateDone", roleName));
        }
    }
}