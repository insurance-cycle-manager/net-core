using System;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace PatientsRecordsManager.ABAC.Extensions
{
    public static class Evaluate
    {
        public enum Action
        {
            READ,
            WRITE,
        }

        public static Microsoft.EntityFrameworkCore.DbSet<TSource> Visit<TSource>(this Microsoft.EntityFrameworkCore.DbSet<TSource> source, Action action) where TSource : class
        {
            if (source == null)
            {
                throw new NullReferenceException();
            }

            Visit(source.GetType().GenericTypeArguments[0], action);
            return source;
        }

        public static System.Collections.Generic.IEnumerable<TSource> Visit<TSource>(this System.Collections.Generic.IEnumerable<TSource> source, Action action)
        {
            if (source == null)
            {
                throw new NullReferenceException();
            }

            Visit(source.GetType().GenericTypeArguments[0], action);
            return source;
        }

        public static System.Linq.IQueryable<TSource> Visit<TSource>(this System.Linq.IQueryable<TSource> source, Action action)
        {
            if (source == null)
                throw new NullReferenceException();

            Visit(source.GetType().GenericTypeArguments[0], action);
            return source;
        }

        public static TEntity Visit<TEntity>(this TEntity entity, Action action)
        {
            if (entity == null)
                throw new NullReferenceException();

            Visit(entity.GetType(), action);
            return entity;
        }

        public static object[] Visit(this object[] entities, Action action)
        {
            if (entities == null)
                throw new NullReferenceException();

            if (entities.Length > 0)
                Visit(entities.First().GetType(), action);
            return entities;
        }

        public static object Visit(this object entity, Action action)
        {
            if (entity == null)
                throw new NullReferenceException();

            Visit(entity.GetType(), action);
            return entity;
        }


        public static Type Visit(this Type type, Action action)
        {
            if (type == null)
            {
                throw new NullReferenceException(nameof(type));
            }

            var context = new HttpContextAccessor { }.HttpContext;

            var sub = context.User;
            var obj = type.FullName;
            var attrs = type.GetProperties().Select(p => p.Name);

            context.RequestServices
                .GetService<PoliciesManager>()
                .EvaluateRequestOfDbo(context.TraceIdentifier, sub, obj, attrs, action.ToString());

            return type;
        }
    }
}