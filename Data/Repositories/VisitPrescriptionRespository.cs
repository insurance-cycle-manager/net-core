using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using PatientsRecordsManager.ABAC.Extensions;
using PatientsRecordsManager.ABAC.Extensions.EF;
using PatientsRecordsManager.Data.Dtos.Prescribed.Prescriptions;
using PatientsRecordsManager.Exceptions;
using PatientsRecordsManager.Models;
using PatientsRecordsManager.Resources;
using PatientsRecordsManager.Utils;

namespace PatientsRecordsManager.Data.Repositories
{
    public class VisitPrescriptionRepository : IVisitPrescriptionRepository
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        private readonly Messages _messages;

        public VisitPrescriptionRepository(DataContext context, IMapper mapper, Messages messages)
        {
            _mapper = mapper;
            _context = context;
            _messages = messages;
        }

        public PrescriptionForReturnDto AddPrescription(string visitCode, PrescriptionForCreateDto prescriptionForCreate)
        {
            Assert.NotNull(prescriptionForCreate);
            var visit = GetVisit(visitCode);

            var prescription = _mapper.Map<Prescription>(prescriptionForCreate);

            if (prescriptionForCreate.Drugs.Any())
            {
                var drugs = _context.Drugs.Where(d => prescriptionForCreate.Drugs.Contains(d.DrugCode)).ToList();
                drugs.ForEach(drug => _context.PrescriptionDrugs.Add(new PrescriptionDrug
                {
                    Prescription = prescription,
                    Drug = drug
                }));
            }
            visit.Prescriptions.Add(prescription);

            if (_context.SaveChanges() <= 0)
            {
                throw new Exception(_messages.GetString("saveFailed"));
            }
            return _mapper.Map<PrescriptionForReturnDto>(prescription);
        }

        public PrescriptionForReturnDto GetPrescriptionDetails(string visitCode, string prescriptionCode)
        {
            var prescription = GetPrescription(visitCode, prescriptionCode);

            if (prescription == null)
            {
                throw new Exception(_messages.GetString("prescriptionNotFound", visitCode, prescriptionCode));
            }
            return _mapper.Map<PrescriptionForReturnDto>(prescription);
        }

        public IEnumerable<PrescriptionForReturnDto> GetPrescriptions(string visitCode)
        {
            return _mapper.Map<IEnumerable<PrescriptionForReturnDto>>(GetVisit(visitCode).Prescriptions);
        }

        public PrescriptionForReturnDto ModifyPrescription(string visitCode, string prescriptionCode, PrescriptionForUpdateDto prescriptionForUpdate)
        {
            Assert.NotNull(prescriptionForUpdate);
            var prescription = GetPrescription(visitCode, prescriptionCode);

            prescription.Note = prescriptionForUpdate.Note ?? prescription.Note;

            if (prescriptionForUpdate.Drugs.Any())
            {
                var prescriptionDrugs = prescription.Drugs.Select(d => d.Drug.DrugCode);
                var selectedDrugs = prescriptionForUpdate.Drugs ?? Array.Empty<string>();

                // Add
                _context.Drugs
                    .Where(d => selectedDrugs.Except(prescriptionDrugs).Contains(d.DrugCode))
                    .ForEachAsync(drug =>
                    {
                        prescription.Drugs.Add(new PrescriptionDrug()
                        {
                            Prescription = prescription,
                            Drug = drug
                        });
                    });

                // Delete
                prescription.Drugs.RemoveAll(pd => !selectedDrugs.Contains(pd.Drug.DrugCode));
            }

            if (_context.SaveChanges() <= 0)
            {
                throw new SaveChangesFailedException();
            }
            return _mapper.Map<PrescriptionForReturnDto>(prescription);
        }

        public bool RemovePrescription(string visitCode, string prescriptionCode)
        {
            var visit = GetVisit(visitCode);
            var prescription = visit.Prescriptions.FirstOrDefault(p => p.PrescriptionCode.ToUpper() == (prescriptionCode.ToUpper()));

            if (prescription == null)
            {
                throw new ArgumentBadException(_messages.GetString("PrescriptionCodeNotFound", prescriptionCode));
            }
            visit.Prescriptions.Remove(prescription);

            if (_context.SaveChanges() <= 0)
            {
                throw new SaveChangesFailedException();
            }
            return prescription != null;
        }

        private Visit GetVisit(string visitCode)
        {
            var visit = _context.Visits
                .Include(v => v.Prescriptions)
                .FirstOrDefault(v => v.VisitCode.ToUpper() == (visitCode.ToUpper()));

            Console.WriteLine(visit.Prescriptions.Count);
            if (visit == null)
            {
                throw new ArgumentBadException(_messages.GetString("visitNotFound", visitCode));
            }
            return visit;
        }

        private Prescription GetPrescription(string visitCode, string prescriptionCode)
        {
            return _context.Prescriptions
                .Include(p => p.Visit)
                .Include(p => p.Drugs)
                .ThenInclude(pd => pd.Drug)
                .Where(p => p.PrescriptionCode.ToUpper() == (prescriptionCode.ToUpper()) && p.Visit.VisitCode.ToUpper() == (visitCode.ToUpper()))
                .FirstOrDefault()
                ?? throw new ArgumentBadException(_messages.GetString("prescriptionNotFound", visitCode, prescriptionCode));
        }
    }
}