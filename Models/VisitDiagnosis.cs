using System.ComponentModel.DataAnnotations;

namespace PatientsRecordsManager.Models
{
    public class VisitDiagnosis
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string DiagnosisCode { get; set; }
        [Required]
        public string Description { get; set; }
        public Visit Visit { get; set; }
    }
}