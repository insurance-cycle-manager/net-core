using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PatientsRecordsManager.Models
{
    public class SurgeryPurchase
    {
        [Key]
        public int Id { get; set; }
        
        [Required]
        public DateTime DateOfPurchasing { get; set; }

        [Required]
        public int PrescribedSurgeryId { get; set; }
        public PrescribedSurgery PrescribedSurgery { get; set; }

        [Required]
        [ForeignKey("Prices")]
        public int PriceId { get; set; }
        public Price Price { get; set; }
        
        [Required]
        [ForeignKey("Visits")]
        public int VisitId { get; set; }
        public Visit Visit { get; set; }
    }
}