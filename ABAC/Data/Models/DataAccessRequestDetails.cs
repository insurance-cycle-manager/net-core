using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;

namespace PatientsRecordsManager.ABAC.Data.Models
{
    public class DataAccessRequestDetails
    {
        [Required]
        [Column(TypeName = "text[]")]
        public string[] Roles { get; set; }

        [Required]
        public string Object { get; set; }

        [Required]
        public string Action { get; set; }

        [AllowNull]
        [Column(TypeName = "text[]")]
        public string[] Attributes { get; set; }
    }
}