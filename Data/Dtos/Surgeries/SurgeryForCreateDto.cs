using PatientsRecordsManager.Data.Dtos.Prices;

namespace PatientsRecordsManager.Data.Dtos.Surgeries
{
    public class SurgeryForCreateDto
    {
        public string Name { get; set; }
        public PriceForAddDto Price { get; set; }
    }
}