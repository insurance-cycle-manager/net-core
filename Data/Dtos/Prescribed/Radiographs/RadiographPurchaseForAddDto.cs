using System;
using System.ComponentModel.DataAnnotations;
using PatientsRecordsManager.Data.Dtos.Prices;

namespace PatientsRecordsManager.Data.Dtos.Prescribed.Radiographs
{
    public class RadiographPurchaseForAddDto
    {
        [Required]
        public DateTime DateOfPurchasing { get; set; }

        [Required]
        public PriceForAddDto Price { get; set; }
        public string VisitCode { get; set; }
    }
}