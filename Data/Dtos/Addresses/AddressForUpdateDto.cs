namespace PatientsRecordsManager.Data.Dtos.Addresses
{
    public class AddressForUpdateDto
    {
        public double? Longitude { get; set; }
        public double? Latitude { get; set; }
        public string Title { get; set; }
    }
}