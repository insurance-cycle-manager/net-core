using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PatientsRecordsManager.ABAC.Data;
using PatientsRecordsManager.Extensions;
using PatientsRecordsManager.Resources;

namespace PatientsRecordsManager.ABAC.Controllers
{
    [ApiController]
    [Route(BaseRoute)]
    // [Authorize(Roles = "SupervisoryCommission")]
    [AllowAnonymous]
    public class RequestsController : ControllerBase
    {
        private const string BaseRoute = "abac/[controller]";
        private const string FetchRulesRoute = "";

        private readonly RulesRepository rules;
        private readonly Messages messages;

        public RequestsController(RulesRepository rules, Messages messages)
        {
            this.messages = messages;
            this.rules = rules;
        }

        [HttpGet(FetchRulesRoute)]
        public IActionResult FetchRequests()
        {
            return HttpContext.Success(rules.GetRequests(), messages.GetString("rulesRequestsFetched"));
        }

    }
}