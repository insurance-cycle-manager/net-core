using System;
using PatientsRecordsManager.Data.Dtos.Prices;

namespace PatientsRecordsManager.Data.Dtos.Prescribed.Radiographs
{
    public class RadiographPurchaseForReturnDto
    {
        public DateTime DateOfPurchasing { get; set; }
        public PriceForReturnDto Price { get; set; }
        public PrescribedRadiographForReturnDto PrescribedRadiograph { get; set; }
    }
}