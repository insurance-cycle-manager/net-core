using System.ComponentModel.DataAnnotations;

namespace PatientsRecordsManager.Data.Dtos.Users.Emails
{
    public class UserEmailForModifyDto
    {
        [Required]
        [EmailAddress]
        public string Address { get; set; }
    }
}