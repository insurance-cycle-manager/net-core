using System.ComponentModel.DataAnnotations;

namespace PatientsRecordsManager.Data.Dtos.ServiceProviders.Staffers
{
    public class StafferForUpdateDto
    {
        [Required]
        public string Title { get; set; }
        public string UserAddressCode { get; set; }
        public string UserEmailCode { get; set; }
        public string UserPhoneCode { get; set; }
    }
}