using PatientsRecordsManager.Constants.Enums;
using PatientsRecordsManager.Data.Dtos.ServiceProviders.Staffers;

namespace PatientsRecordsManager.Data.Dtos.ServiceProviders.Staff
{
    public class MemberForReturnDto
    {
        public ServiceProviderStafferStatus Status { get; set; }
        public StafferForReturnDto Staffer { get; set; }
        public ServiceProviderForReturnDto ServiceProvider { get; set; }
    }
}