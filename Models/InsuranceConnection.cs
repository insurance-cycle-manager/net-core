using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PatientsRecordsManager.Models
{
    public class InsuranceConnection
    {
        [Required]
        public string InsuranceConnectionCode { get; set; }

        [Required]
        public int InsuranceCompanyId { get; set; }
        public InsuranceCompany InsuranceCompany { get; set; }

        [Required]
        public int ThirdPartyAdministratorId { get; set; }
        public ThirdPartyAdministrator ThirdPartyAdministrator { get; set; }

        [Required]
        public DateTime DateOfConnection { get; set; }
    }
}