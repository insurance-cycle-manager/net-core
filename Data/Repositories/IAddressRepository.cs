using PatientsRecordsManager.Data.Dtos.Addresses;

namespace PatientsRecordsManager.Data.Repositories
{
    public interface IAddressRepository
    {
        AddressForReturnDto AddAddress(AddressForCreateDto address);
        AddressForReturnDto ModifyAddress(string addressCode, AddressForUpdateDto address);
        System.Collections.Generic.IEnumerable<AddressForReturnDto> GetAddresses(string title, string addressCode);
    }
}