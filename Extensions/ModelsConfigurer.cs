using Microsoft.EntityFrameworkCore;
using PatientsRecordsManager.Models;

namespace PatientsRecordsManager.Data
{
    public static class ModelsConfigurer
    {
        public static void ConfigureEntities(this DataContext context, ModelBuilder builder)
        {
            ConfigureAddress(builder);

            ConfigureSupervisoryCommission(builder);
            ConfigureIdentityRelation(builder);
            ConfigureInsuranceCompany(builder);
            ConfigureInsuranceConnection(builder);
            ConfigureInsurancePackage(builder);
            ConfigureInsurancePackageDrugs(builder);
            ConfigurePatient(builder);
            ConfigurePatientPackage(builder);
            ConfigureStatuses(builder);
            ConfigureThirdPartyAdministrator(builder);
            ConfigureEmail(builder);
            ConfigurePhoneNumber(builder);
            ConfigureServiceProviders(builder);
            ConfigureServiceProviderConnections(builder);
            ConfigureServiceProviderConnectionIcs(builder);

            ConfigureVisits(builder);
            ConfigureVisitClaims(builder);
            ConfigureVisitDiagnosis(builder);
            ConfigureVisitPrescriptions(builder);
            ConfigureVisitPrescriptionPurchases(builder);
            ConfigureVisitSurgeries(builder);

            ConfigurePrices(builder);
            ConfigureDrug(builder);
            ConfigureLabTest(builder);

            ConfigureSurgery(builder);
            ConfigurePackageSurgery(builder);
            ConfigurePackageRadiographs(builder);
            ConfigureRadiograph(builder);
            ConfigurePackageLabTest(builder);
            ConfigurePurchase(builder);
        }

        private static void ConfigurePurchase(ModelBuilder builder)
        {
            ConfigureLabTestPurchase(builder);
            ConfigureSurgeryPurchase(builder);
            ConfigureRadiographPurchases(builder);
            ConfigurePrescriptionPurchase(builder);
            ConfigureDrugPurchase(builder);
        }

        private static void ConfigureDrugPurchase(ModelBuilder builder)
        {
            builder.Entity<DrugPurchase>(drug =>
            {
                drug.Property(dp => dp.Id)
                    .ValueGeneratedOnAdd();

                drug.HasOne(dp => dp.Drug)
                    .WithMany(d => d.Purchases)
                    .HasForeignKey(dp => dp.DrugId)
                    .IsRequired(true);

                drug.HasOne(dp => dp.PrescriptionPurchase)
                    .WithMany(pp => pp.PurchasedDrugs)
                    .HasForeignKey(dp => dp.PrescriptionPurchaseId)
                    .IsRequired(true);
            });
        }

        private static void ConfigurePrescriptionPurchase(ModelBuilder builder)
        {
            builder.Entity<PrescriptionPurchase>(table =>
               table.HasOne(pp => pp.Visit)
               .WithMany(v => v.PrescriptionPurchases)
               .IsRequired(true));
        }

        private static void ConfigureRadiographPurchases(ModelBuilder builder)
        {
            builder.Entity<RadiographPurchase>(table =>
               table.HasOne(rp => rp.Visit)
               .WithMany(v => v.RadiographPurchases)
               .IsRequired(true));
        }

        private static void ConfigureSurgeryPurchase(ModelBuilder builder)
        {
            builder.Entity<SurgeryPurchase>(table =>
               table.HasOne(sp => sp.Visit)
               .WithMany(v => v.SurgeryPurchases)
               .IsRequired(true));
        }

        private static void ConfigureLabTestPurchase(ModelBuilder builder)
        {
            builder.Entity<LabTestPurchase>(table =>
               table.HasOne(ltp => ltp.Visit)
               .WithMany(v => v.LabTestPurchases)
               .IsRequired(true));
        }

        private static void ConfigurePackageLabTest(ModelBuilder builder)
        {
            builder.Entity<PackageLabTest>(table =>
            {
                table.HasKey(plt => new
                {
                    plt.InsurancePackageId,
                    plt.LabTestId
                });

                table.HasOne(plt => plt.LabTest)
                    .WithMany(lt => lt.Packages)
                    .IsRequired(true);

                table.HasOne(plt => plt.InsurancePackage)
                    .WithMany(p => p.LabTests)
                    .IsRequired(true);

            });
        }
        private static void ConfigureLabTest(ModelBuilder builder)
        {
            builder.Entity<LabTest>(table =>
            {
                table.Property(s => s.Id)
                    .ValueGeneratedOnAdd();

                table.HasIndex(s => s.Name).IsUnique();
                table.HasIndex(s => s.NormalizedName).IsUnique();
                table.HasIndex(s => s.LabTestCode).IsUnique();

                builder.HasSequence<int>("LabTestsCode");
                table.Property(d => d.LabTestCode)
                    .HasDefaultValueSql(@"'LAB' || nextval('public.""LabTestsCode""')");
            });

            builder.Entity<PrescribedLabTest>(table =>
            {
                table.Property(s => s.Id).ValueGeneratedOnAdd();

                table.HasIndex(s => s.PrescribedLabTestCode).IsUnique();
                builder.HasSequence<int>("PrescribedLabTestsCode");
                table.Property(d => d.PrescribedLabTestCode)
                    .HasDefaultValueSql(@"'PRES_LAB' || nextval('public.""PrescribedLabTestsCode""')");

                table.HasOne(l => l.Visit)
                    .WithMany(v => v.LabTests)
                    .HasForeignKey(l => l.VisitId);

                table.HasOne(l => l.LabTest)
                    .WithMany(e => e.Prescribed)
                    .HasForeignKey(l => l.LabTestId)
                    .IsRequired(true);
            });

            builder.Entity<LabTestPurchase>(table =>
            {
                table.Property(s => s.Id).ValueGeneratedOnAdd();
                table.HasOne(p => p.PrescribedLabTest)
                    .WithMany(lp => lp.Purchases)
                    .HasForeignKey(p => p.PrescribedLabTestId)
                    .IsRequired(true);
            });
        }

        private static void ConfigureRadiograph(ModelBuilder builder)
        {
            builder.Entity<Radiograph>(table =>
            {
                table.Property(r => r.Id).ValueGeneratedOnAdd();
                table.HasIndex(r => r.Name).IsUnique();
                table.HasIndex(r => r.NormalizedName).IsUnique();
                table.HasIndex(r => r.RadiographCode).IsUnique();

                builder.HasSequence<int>("RadiographCode");
                table.Property(d => d.RadiographCode)
                    .HasDefaultValueSql(@"'RGPH' || nextval('public.""RadiographCode""')");
            });

            builder.Entity<PrescribedRadiograph>(table =>
            {
                table.Property(s => s.Id).ValueGeneratedOnAdd();

                table.HasIndex(s => s.PrescribedRadiographCode).IsUnique();
                builder.HasSequence<int>("PrescribedRadiographsCode");
                table.Property(d => d.PrescribedRadiographCode)
                    .HasDefaultValueSql(@"'PRES_RGPH' || nextval('public.""PrescribedRadiographsCode""')");

                table.HasOne(l => l.Visit)
                    .WithMany(v => v.Radiographs)
                    .HasForeignKey(l => l.VisitId);

                table.HasOne(l => l.Radiograph)
                    .WithMany(e => e.Prescribed)
                    .HasForeignKey(l => l.RadiographId)
                    .IsRequired(true);
            });

            builder.Entity<RadiographPurchase>(table =>
            {
                table.Property(s => s.Id).ValueGeneratedOnAdd();
                table.HasOne(p => p.PrescribedRadiograph)
                    .WithMany(lp => lp.Purchases)
                    .HasForeignKey(p => p.PrescribedRadiographId)
                    .IsRequired(true);
            });
        }

        private static void ConfigurePackageRadiographs(ModelBuilder builder)
        {
            builder.Entity<PackageRadiograph>(table =>
            {
                table.HasKey(pr =>
                   new
                   {
                       pr.InsurancePackageId,
                       pr.RadiographId
                   }
                );

                table.HasOne(pr => pr.Radiograph)
                    .WithMany(r => r.Packages)
                    .IsRequired(true);

                table.HasOne(pr => pr.InsurancePackage)
                    .WithMany(p => p.Radiographs)
                    .IsRequired(true);
            });
        }

        private static void ConfigurePackageSurgery(ModelBuilder builder)
        {
            builder.Entity<PackageSurgery>(table =>
            {
                table.HasKey(ps => new
                {
                    ps.InsurancePackageId,
                    ps.SurgeryId
                });

                table.HasOne(ps => ps.Surgery)
                    .WithMany(s => s.Packages)
                    .HasForeignKey(ps => ps.SurgeryId)
                    .IsRequired(true);

                table.HasOne(ps => ps.InsurancePackage)
                    .WithMany(s => s.Surgeries)
                    .HasForeignKey(ps => ps.InsurancePackageId)
                    .IsRequired(true);
            });
        }

        private static void ConfigureSurgery(ModelBuilder builder)
        {
            builder.Entity<Surgery>(surgery =>
            {
                surgery.Property(s => s.Id).ValueGeneratedOnAdd();

                surgery.HasIndex(s => s.Name).IsUnique();
                surgery.HasIndex(s => s.NormalizedName).IsUnique();
                surgery.HasIndex(s => s.SurgeryCode).IsUnique();

                builder.HasSequence<int>("SurgeriesCode");
                surgery.Property(d => d.SurgeryCode)
                    .HasDefaultValueSql(@"'SUR' || nextval('public.""SurgeriesCode""')");
            });
        }

        private static void ConfigureDrug(ModelBuilder builder)
        {
            builder.Entity<Drug>(table =>
               table.HasIndex(drug => drug.DrugCode)
               .IsUnique(true)
            );
        }

        private static void ConfigureAddress(ModelBuilder builder)
        {
            builder.Entity<Address>(address =>
            {
                address.Property(ad => ad.Title)
                    .IsRequired();

                // address.Property(ad => ad.Longitude)
                //     .IsRequired();

                // address.Property(ad => ad.Latitude)
                //     .IsRequired();

                address.HasIndex(ad => ad.NormalizedTitle).IsUnique();

                address.HasIndex(ad => ad.AddressCode).IsUnique();
                builder.HasSequence<int>("AddressNumbers");
                address.Property(ad => ad.AddressCode)
                    .HasDefaultValueSql(@"'ADD'|| nextval('public.""AddressNumbers""')");
            });

            builder.Entity<UserAddress>(userAddress =>
            {
                userAddress.HasKey(ua => new
                {
                    ua.UserId,
                    ua.AddressId
                });

                userAddress.HasOne(ua => ua.User)
                    .WithMany(u => u.UserAddresses)
                    .HasForeignKey(ua => ua.UserId)
                    .IsRequired();

                userAddress.HasOne(ua => ua.Address)
                    .WithMany(a => a.UserAddresses)
                    .HasForeignKey(ua => ua.AddressId)
                    .IsRequired();
            });
        }

        private static void ConfigureVisits(ModelBuilder builder)
        {
            builder.Entity<Visit>((visit) =>
            {
                visit.HasOne(v => v.BaseVisit)
                    .WithMany(bv => bv.SubVisits)
                    .HasForeignKey(v => v.BaseVisitId)
                    .IsRequired(false);

                visit.HasOne(v => v.ServiceProvider)
                    .WithMany(sp => sp.Visits)
                    .HasForeignKey(v => v.ServiceProviderId)
                    .IsRequired();

                visit.HasOne(v => v.Patient)
                    .WithMany(p => p.Visits)
                    .HasForeignKey(v => v.PatientId)
                    .IsRequired();

                visit.HasOne(v => v.ServiceProviderConnection)
                    .WithMany(spc => spc.Visits)
                    .HasForeignKey(v => v.ServiceProviderConnectionId)
                    .IsRequired();

                visit.HasOne(v => v.Diagnosis)
                    .WithOne(d => d.Visit)
                    .IsRequired(false);

                visit.HasOne(v => v.VisitClaim)
                    .WithOne(c => c.Visit)
                    .IsRequired(false);

                visit.HasIndex(v => v.VisitCode)
                    .IsUnique(true);

            });

            builder.HasSequence<int>("VisitNumbers");

            builder.Entity<Visit>()
                .Property(v => v.VisitCode)
                .HasDefaultValueSql(@"'VIZ'||nextval('public.""VisitNumbers""')");
        }

        private static void ConfigureVisitClaims(ModelBuilder builder)
        {
            builder.Entity<VisitClaim>((claim) =>
            {
                claim.Property(c => c.Id).ValueGeneratedOnAdd();

                claim.HasOne(c => c.ServiceProvider)
                    .WithMany(sp => sp.VisitClaims)
                    .HasForeignKey(c => c.ServiceProviderId)
                    .IsRequired();

                claim.HasOne(c => c.ThirdPartyAdministrator)
                    .WithMany(tpa => tpa.VisitClaims)
                    .HasForeignKey(c => c.ThirdPartyAdministratorId)
                    .IsRequired();

                builder.HasSequence<int>("VisitClaimsCode");
                claim.Property(c => c.VisitClaimCode)
                    .HasDefaultValueSql(@"'CL'||nextval('public.""VisitClaimsCode""')");
                claim.HasIndex(c => c.VisitClaimCode).IsUnique();
            });

        }

        private static void ConfigureVisitDiagnosis(ModelBuilder builder)
        {
            builder.Entity<VisitDiagnosis>(diagnosis =>
            {
                diagnosis.Property(d => d.Id)
                    .ValueGeneratedOnAdd();

                diagnosis.HasIndex(d => d.DiagnosisCode)
                    .IsUnique(true);

                builder.HasSequence<int>("DiagnosisNumbers");
                diagnosis.Property(v => v.DiagnosisCode)
                    .HasDefaultValueSql(@"'DIA'||nextval('public.""DiagnosisNumbers""')");
            });
        }

        private static void ConfigureVisitPrescriptions(ModelBuilder builder)
        {
            builder.Entity<Drug>(drug =>
            {
                drug.Property(d => d.Id)
                    .ValueGeneratedOnAdd();

                drug.HasIndex(d => d.Name).IsUnique();
                drug.HasIndex(d => d.DrugCode).IsUnique();

                builder.HasSequence<int>("DrugsCode");
                drug.Property(d => d.DrugCode)
                    .HasDefaultValueSql(@"'DRUG' || nextval('public.""DrugsCode""')");
            });

            builder.Entity<Prescription>(prescription =>
            {
                prescription.Property(p => p.Id)
                    .ValueGeneratedOnAdd();

                prescription.HasOne(p => p.Visit)
                    .WithMany(v => v.Prescriptions)
                    .HasForeignKey(p => p.VisitId)
                    .IsRequired(true);

                builder.HasSequence<int>("PrescriptionsCode");
                prescription.Property(d => d.PrescriptionCode)
                    .HasDefaultValueSql(@"'PRES' || nextval('public.""PrescriptionsCode""')");
            });

            builder.Entity<PrescriptionDrug>(prescriptionDrug =>
            {
                prescriptionDrug.HasKey(pd => new
                {
                    pd.PrescriptionId,
                    pd.DrugId
                });

                prescriptionDrug.HasOne(pd => pd.Prescription)
                    .WithMany(p => p.Drugs)
                    .HasForeignKey(pd => pd.PrescriptionId)
                    .IsRequired(true);

                prescriptionDrug.HasOne(pd => pd.Drug)
                    .WithMany(d => d.Prescriptions)
                    .HasForeignKey(pd => pd.DrugId)
                    .IsRequired(true);
            });
        }

        private static void ConfigureVisitPrescriptionPurchases(ModelBuilder builder)
        {
            builder.Entity<PrescriptionPurchase>(prescriptionPurchase =>
            {
                prescriptionPurchase.Property(pp => pp.Id)
                    .ValueGeneratedOnAdd();

                prescriptionPurchase.HasOne(pp => pp.Prescription)
                    .WithMany(p => p.PrescriptionPurchases)
                    .HasForeignKey(pp => pp.PrescriptionId)
                    .IsRequired(true);

                prescriptionPurchase.HasMany(pp => pp.PurchasedDrugs)
                    .WithOne(d => d.PrescriptionPurchase)
                    .HasForeignKey(d => d.PrescriptionPurchaseId)
                    .IsRequired(true);
            });
        }

        private static void ConfigureVisitSurgeries(ModelBuilder builder)
        {
            builder.Entity<PrescribedSurgery>(pSurgery =>
            {
                pSurgery.Property(ps => ps.Id).ValueGeneratedOnAdd();

                pSurgery.HasOne(p => p.Visit)
                    .WithMany(v => v.Surgeries)
                    .HasForeignKey(p => p.VisitId)
                    .IsRequired(true);

                pSurgery.HasOne(p => p.Surgery)
                    .WithMany(v => v.Surgeries)
                    .HasForeignKey(p => p.SurgeryId)
                    .IsRequired(true);

                builder.HasSequence<int>("PrescribedSurgeriesCode");
                pSurgery.Property(d => d.PrescribedSurgeryCode)
                    .HasDefaultValueSql(@"'PRES_SUR' || nextval('public.""PrescribedSurgeriesCode""')");
            });

            builder.Entity<SurgeryPurchase>(sPurchase =>
            {
                sPurchase.Property(sp => sp.Id).ValueGeneratedOnAdd();
                sPurchase.HasOne(sp => sp.PrescribedSurgery)
                    .WithMany(s => s.Purchases)
                    .HasForeignKey(sp => sp.PrescribedSurgeryId);
            });
        }

        private static void ConfigureServiceProviderConnectionIcs(ModelBuilder builder)
        {
            builder.Entity<ServiceProviderConnectionInsuranceCompanies>()
                .HasKey(spc => new
                {
                    spc.SpConnectionId,
                    spc.InsuranceCompanyId
                });
        }

        private static void ConfigureInsurancePackageDrugs(ModelBuilder builder)
        {
            builder.Entity<PackageDrug>()
                .HasKey(pd => new
                {
                    pd.InsurancePackageId,
                    pd.DrugId
                });
        }

        private static void ConfigureServiceProviders(ModelBuilder builder)
        {

            builder.Entity<ServiceProvider>(table =>
            {

                table.Property(sp => sp.Id)
                    .ValueGeneratedOnAdd();

                table.HasIndex(sps => new { sps.UserId, sps.Type }).IsUnique(true);

                table.HasOne(sp => sp.Status);

                table.HasOne(sp => sp.UserRole)
                    .WithOne(ur => ur.ServiceProvider)
                    .HasForeignKey<ServiceProvider>(sp => new
                    {
                        sp.UserId,
                        sp.RoleId
                    });

                table.HasOne(sp => sp.Address)
                    .WithMany(ua => ua.ServiceProviders)
                    .HasForeignKey(sp => new
                    {
                        sp.UserId,
                        sp.AddressId
                    });

                table.HasIndex(sp => sp.ServiceProviderCode)
                    .IsUnique(true);

                builder.HasSequence<int>("ServiceProviderNumbers");

                builder.Entity<ServiceProvider>()
                    .Property(sp => sp.ServiceProviderCode)
                    .HasDefaultValueSql(@"'SP'|| nextval('public.""ServiceProviderNumbers""')");
            });

            builder.Entity<ServiceProviderStaffer>(table =>
            {
                table.HasKey(sps => new { sps.ServiceProviderId, sps.ServiceStafferId });

                table.HasOne(sps => sps.ServiceProvider)
                    .WithMany(sp => sp.Staff)
                    .HasForeignKey(sps => sps.ServiceProviderId)
                    .IsRequired();

                table.HasOne(sps => sps.Staffer)
                    .WithMany(staffer => staffer.ServiceProviders)
                    .HasForeignKey(sps => sps.ServiceStafferId)
                    .IsRequired();
            });

            builder.Entity<ServiceStaffer>(staffer =>
            {
                staffer.Property(sps => sps.Id).ValueGeneratedOnAdd();

                staffer.HasIndex(sps => new { sps.UserId, sps.Type }).IsUnique(true);

                staffer.HasOne(sps => sps.UserRole)
                    .WithOne(ur => ur.ServiceStaffer)
                    .HasForeignKey<ServiceStaffer>(sp => new
                    {
                        sp.UserId,
                        sp.RoleId
                    });

                staffer.HasOne(sp => sp.Address)
                    .WithMany(ua => ua.Staffers)
                    .HasForeignKey(sp => new
                    {
                        sp.UserId,
                        sp.AddressId
                    });

                staffer.HasIndex(s => s.ServiceStafferCode).IsUnique(true);
                builder.HasSequence<int>("ServiceProviderStaffCode");
                staffer.Property(s => s.ServiceStafferCode)
                    .HasDefaultValueSql(@"'SPS'|| nextval('public.""ServiceProviderStaffCode""')");
            });

        }

        private static void ConfigureServiceProviderConnections(ModelBuilder builder)
        {

            builder.Entity<ServiceProviderConnection>(table =>
            {

                table.Property(spc => spc.Id)
                    .ValueGeneratedOnAdd();

                table.HasOne(spc => spc.Tpa)
                    .WithMany(tpa => tpa.ServiceProvidersConnections)
                    .HasForeignKey(spc => spc.TpaId)
                    .IsRequired();

                table.HasOne(spc => spc.ServiceProvider)
                    .WithMany(sp => sp.Connections)
                    .HasForeignKey(spc => spc.ServiceProviderId)
                    .IsRequired();

                table.HasIndex(spc => new { spc.TpaId, spc.ServiceProviderId }).IsUnique(true);
                table.HasIndex(spc => spc.ConnectionCode).IsUnique(true);

                builder.HasSequence<int>("ServiceProviderConnectionsNumbers");
                table.Property(spc => spc.ConnectionCode)
                    .HasDefaultValueSql(@"'SPC'|| nextval('public.""ServiceProviderConnectionsNumbers""')");
            });

        }

        private static void ConfigureEmail(ModelBuilder builder)
        {
            builder.Entity<UserEmail>(email =>
            {
                email.Property(e => e.Id).ValueGeneratedOnAdd();
                email.HasIndex(e => e.Address).IsUnique();

                email.HasOne(e => e.User)
                    .WithMany(u => u.UserEmails)
                    .HasForeignKey(e => e.UserId)
                    .IsRequired();

                builder.HasSequence<int>("UserEmailsCode");
                email.Property(e => e.EmailCode).HasDefaultValueSql(@"'EM'|| nextval('public.""UserEmailsCode""')");
                email.HasIndex(e => e.EmailCode).IsUnique();
            });

        }

        private static void ConfigurePhoneNumber(ModelBuilder builder)
        {
            builder.Entity<UserPhoneNumber>(phoneNumber =>
            {
                phoneNumber.Property(p => p.Id).ValueGeneratedOnAdd();
                phoneNumber.HasIndex(p => p.Number).IsUnique();

                phoneNumber.HasOne(ph => ph.User)
                    .WithMany(u => u.UserPhoneNumbers)
                    .HasForeignKey(ph => ph.UserId)
                    .IsRequired();

                builder.HasSequence<int>("UserPhoneNumbersCode");
                phoneNumber.Property(p => p.PhoneCode).HasDefaultValueSql(@"'PH'|| nextval('public.""UserPhoneNumbersCode""')");
                phoneNumber.HasIndex(p => p.PhoneCode).IsUnique();
            });

        }

        private static void ConfigureThirdPartyAdministrator(ModelBuilder builder)
        {
            builder.Entity<ThirdPartyAdministrator>(table =>
            {
                table.HasIndex(tpa => tpa.ThirdPartyCode).IsUnique();

                builder.HasSequence<int>("ThirdPartyAdministratorsNumbers");
                table.Property(tpa => tpa.ThirdPartyCode)
                    .HasDefaultValueSql(@"'TPA'|| nextval('public.""ThirdPartyAdministratorsNumbers""')");

                table.HasOne(tpa => tpa.Status);

                table.HasOne(tpa => tpa.SupervisoryComission)
                    .WithMany(sc => sc.ThirdPartyAdministrators)
                    .HasForeignKey(tpa => tpa.SupervisoryComissionId)
                    .IsRequired(true);

                table.HasOne(tpa => tpa.UserRole)
                    .WithOne(ur => ur.ThirdPartyAdministrator)
                    .HasForeignKey<ThirdPartyAdministrator>(tpa => new
                    {
                        tpa.UserId,
                        tpa.RoleId
                    });

                table.HasOne(tpa => tpa.Address)
                    .WithOne(ua => ua.Tpa)
                    .HasForeignKey<ThirdPartyAdministrator>(tpa => new
                    {
                        tpa.UserId,
                        tpa.AddressId
                    });
            });
        }

        private static void ConfigurePatient(ModelBuilder builder)
        {
            builder.Entity<Patient>(patient =>
            {
                patient.Property(p => p.Id).ValueGeneratedOnAdd();

                patient.HasOne(p => p.Address)
                    .WithOne(ua => ua.Patient)
                    .HasForeignKey<Patient>(p => new
                    {
                        p.UserId,
                        p.AddressId
                    });

                patient.HasMany(p => p.PatientPackages)
                    .WithOne(pp => pp.Patient)
                    .HasForeignKey(pp => pp.PatientId)
                    .IsRequired();

                patient.HasIndex(p => p.PatientCode).IsUnique(true);
                builder.HasSequence<int>("PatientsNumbers");
                patient.Property(p => p.PatientCode).HasDefaultValueSql(@"'PAT'|| nextval('public.""PatientsNumbers""')");

                patient.HasOne(p => p.UserRole)
                    .WithOne(ur => ur.Patient)
                    .HasForeignKey<Patient>(p => new
                    {
                        p.UserId,
                        p.RoleId
                    });
            });
        }

        private static void ConfigureInsurancePackage(ModelBuilder builder)
        {
            builder.Entity<InsurancePackage>()
                .Property(ip => ip.Id)
                .ValueGeneratedOnAdd();

            builder.Entity<InsurancePackage>()
                .HasMany(ip => ip.PatientPackages)
                .WithOne(pp => pp.InsurancePackage)
                .HasForeignKey(pp => pp.InsurancePackageId)
                .IsRequired();

            builder.Entity<InsurancePackage>()
                .HasOne(ip => ip.InsuranceCompany);

            builder.Entity<InsurancePackage>()
                .HasIndex(p => new { p.InsuranceCompanyId, p.Name })
                .IsUnique(true);

            builder.HasSequence<int>("InsurancePackagesNumbers");

            builder.Entity<InsurancePackage>()
                .Property(ic => ic.InsurancePackageCode)
                .HasDefaultValueSql(@"'ICP'|| nextval('public.""InsurancePackagesNumbers""')");
        }

        private static void ConfigureSupervisoryCommission(ModelBuilder builder)
        {
            builder.Entity<SupervisoryComission>()
                .HasMany(sc => sc.InsuranceCompanies)
                .WithOne(ic => ic.SupervisoryComission)
                .HasForeignKey(sc => sc.SupervisoryComissionId)
                .IsRequired();
        }

        private static void ConfigureInsuranceCompany(ModelBuilder builder)
        {
            builder.Entity<InsuranceCompany>(table =>
            {
                table.Property(c => c.Id)
                    .ValueGeneratedOnAdd();

                table.HasIndex(ic => ic.InsuranceCompanyCode).IsUnique();

                builder.HasSequence<int>("InsuranceCompaniesNumbers");
                table.Property(ic => ic.InsuranceCompanyCode)
                    .HasDefaultValueSql(@"'IC'|| nextval('public.""InsuranceCompaniesNumbers""')");

                table.HasOne(ic => ic.Status);

                table.HasOne(ic => ic.SupervisoryComission)
                    .WithMany(sc => sc.InsuranceCompanies)
                    .HasForeignKey(ic => ic.SupervisoryComissionId)
                    .IsRequired(true);

                table.HasOne(ic => ic.UserRole)
                    .WithOne(ur => ur.InsuranceCompany)
                    .HasForeignKey<InsuranceCompany>(ic => new
                    {
                        ic.UserId,
                        ic.RoleId
                    });

                table.HasOne(ic => ic.Address)
                    .WithOne(ua => ua.InsuranceCompnay)
                    .HasForeignKey<InsuranceCompany>(ic => new
                    {
                        ic.UserId,
                        ic.AddressId
                    });
            });
        }

        private static void ConfigureStatuses(ModelBuilder builder)
        {
            builder.Entity<Status>()
                .Property(s => s.Id)
                .ValueGeneratedOnAdd();
        }

        private static void ConfigureIdentityRelation(ModelBuilder builder)
        {
            builder.Entity<User>()
                .Property(u => u.Id)
                .ValueGeneratedOnAdd();

            builder.Entity<User>()
                .HasMany(u => u.UserEmails)
                .WithOne(e => e.User)
                .IsRequired();

            builder.Entity<UserEmail>()
                .Property(e => e.Id)
                .ValueGeneratedOnAdd();

            builder.Entity<UserRole>()
                .HasKey(ur => new { ur.UserId, ur.RoleId });

            builder.Entity<UserRole>()
                .HasOne(ur => ur.Role)
                .WithMany(r => r.UserRoles)
                .HasForeignKey(ur => ur.RoleId)
                .IsRequired();

            builder.Entity<UserRole>()
                .HasOne(ur => ur.User)
                .WithMany(u => u.UserRoles)
                .HasForeignKey(ur => ur.UserId)
                .IsRequired();

            builder.Entity<RolePermission>()
                .HasKey(rp => new { rp.RoleId, rp.PermissionId });

            builder.Entity<RolePermission>()
                .HasOne(rp => rp.Role)
                .WithMany(r => r.Permissions)
                .HasForeignKey(rp => rp.RoleId)
                .IsRequired();

            builder.Entity<RolePermission>()
                .HasOne(rp => rp.Permission)
                .WithMany(p => p.Roles)
                .HasForeignKey(rp => rp.PermissionId)
                .IsRequired();

            builder.Entity<UserRoleRequest>(request =>
            {
                request.Property(req => req.Id).ValueGeneratedOnAdd();

                request.HasOne(req => req.User)
                    .WithMany(u => u.UserRoleRequests)
                    .HasForeignKey(req => req.UserId);

                // request.HasOne(req => req.Role)
                //     .WithMany(r => r.UserRoleRequests)
                //     .HasForeignKey(req => req.RoleId);

                request.HasOne(req => req.UserAddress)
                    .WithMany(ua => ua.Requests)
                    .HasForeignKey(req => new
                    {
                        UserId = req.UserId,
                        AddressId = req.AddressId
                    });
            });
        }

        private static void ConfigureInsuranceConnection(ModelBuilder builder)
        {
            builder.Entity<InsuranceConnection>()
                .HasKey(c => new
                {
                    c.InsuranceCompanyId,
                    c.ThirdPartyAdministratorId
                });

            builder.Entity<InsuranceConnection>()
                .HasOne(c => c.InsuranceCompany)
                .WithMany(ic => ic.InsuranceConnections)
                .HasForeignKey(c => c.InsuranceCompanyId)
                .IsRequired();

            builder.Entity<InsuranceConnection>()
                .HasOne(c => c.ThirdPartyAdministrator)
                .WithMany(tpa => tpa.InsuranceConnections)
                .HasForeignKey(c => c.ThirdPartyAdministratorId)
                .IsRequired();

            builder.HasSequence<int>("InsuranceConnectionsNumbers");

            builder.Entity<InsuranceConnection>()
                .Property(c => c.InsuranceConnectionCode)
                .HasDefaultValueSql(@"'ICC'|| nextval('public.""InsuranceConnectionsNumbers""')");
        }

        private static void ConfigurePatientPackage(ModelBuilder builder)
        {
            builder.Entity<PatientPackage>()
                .HasKey(t => new { t.PatientId, t.InsurancePackageId });

            builder.Entity<PatientPackage>()
                .HasOne(pp => pp.Patient)
                .WithMany(p => p.PatientPackages)
                .HasForeignKey(pp => pp.PatientId);

            builder.Entity<PatientPackage>()
                .HasOne(pp => pp.InsurancePackage)
                .WithMany(ip => ip.PatientPackages)
                .HasForeignKey(pp => pp.InsurancePackageId);
        }

        private static void ConfigurePrices(ModelBuilder builder)
        {
            builder.Entity<Price>(price =>
            {
                price.Property(p => p.Id).ValueGeneratedOnAdd();
                price.HasIndex(p => p.PriceCode).IsUnique();

                builder.HasSequence<int>("PricesCode");
                price.Property(d => d.PriceCode).HasDefaultValueSql(@"'PRICE' || nextval('public.""PricesCode""')");
            });
        }
    }
}