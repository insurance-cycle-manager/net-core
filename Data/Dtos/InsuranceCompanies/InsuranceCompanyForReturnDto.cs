using System;
using PatientsRecordsManager.Data.Dtos.Addresses;
using PatientsRecordsManager.Data.Dtos.Users.Addresses;
using PatientsRecordsManager.Data.Dtos.Users.Emails;
using PatientsRecordsManager.Data.Dtos.Users.Phones;

namespace PatientsRecordsManager.Data.Dtos.InsuranceCompanies
{
    public class InsuranceCompanyForReturnDto
    {
        public string Name { get; set; }
        public DateTime DateOfFounding { get; set; }
        public DateTime JoinedAt { get; set; }
        public UserAddressForReturnDto Address { get; set; }
        public UserEmailForReturnDto Email { get; set; }
        public UserPhoneForReturnDto PhoneNumber { get; set; }
        public string SupervisoryComission { get; set; }
        public string Status { get; set; }
        public string InsuranceCompanyCode { get; set; }
    }
}