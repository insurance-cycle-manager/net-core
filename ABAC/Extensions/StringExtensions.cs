using System;

namespace PatientsRecordsManager.ABAC.Extensions
{
    public static class StringExtensions
    {
        private static bool ILike(this string source, string other)
        {
            if (source == null)
                throw new NullReferenceException(nameof(source));

            if (other == null)
                throw new ArgumentNullException(nameof(source));

            return source.ToUpper() == other.ToUpper();
        }

        private static bool IMatch(this string source, string other)
        {
            if (source == null)
                throw new NullReferenceException(nameof(source));

            if (other == null)
                throw new ArgumentNullException(nameof(source));

            return source.ToUpper().Contains(other.ToUpper());
        }
    }
}