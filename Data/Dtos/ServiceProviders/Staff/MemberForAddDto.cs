using System.ComponentModel.DataAnnotations;

namespace PatientsRecordsManager.Data.Dtos.ServiceProviders.Staff
{
    public class MemberForAddDto
    {
        [Required]
        public string StafferCode { get; set; }
    }
}