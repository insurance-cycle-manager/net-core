using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PatientsRecordsManager.ABAC.Extensions;
using PatientsRecordsManager.ABAC.Extensions.EF;
using PatientsRecordsManager.Constants;
using PatientsRecordsManager.Data;
using PatientsRecordsManager.Data.Dtos.Users.Phones;
using PatientsRecordsManager.Exceptions;
using PatientsRecordsManager.Extensions;
using PatientsRecordsManager.Models;
using PatientsRecordsManager.Resources;

namespace PatientsRecordsManager.Controllers
{
    [ApiController]
    [Authorize]
    [Route(APIs.UserPhonesBaseUrl)]
    public class UserPhonesController : PrmController
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        private readonly Messages _messages;

        public UserPhonesController(DataContext context, IMapper mapper, Messages messages)
        {
            _messages = messages;
            _mapper = mapper;
            _context = context;
        }

        [HttpGet(APIs.GetPendingUserPhones)]
        [Authorize(Roles = RolesNames.Administrator)]
        public IActionResult GetPendingUserPhoneNumbers([FromQuery] string userName)
        {
            return HttpContext.Success(GetAllPhoneNumbers(false, userName), _messages.GetString("PendingUserPhonesFetchDone"));
        }

        [HttpGet(APIs.GetUserPhones)]
        public IActionResult GetUserPhoneNumbers()
        {
            var user = GetUser(HttpContext.GetUserId());
            return HttpContext.Success(GetPhoneNumbers(user), _messages.GetString("UserPhonesFetchDone", user.UserName));
        }

        [HttpPost(APIs.PostUserPhone)]
        public IActionResult PostUserPhone(UserPhoneForAddDto phone)
        {
            return HttpContext.Success(AddPhoneNumber(GetUser(HttpContext.GetUserId()), phone), _messages.GetString("UserPhonePostDone"));
        }

        [HttpPut(APIs.ModifyUserPhone)]
        public IActionResult ModifyUserPhone(string phoneCode, UserPhoneForModifyDto phone)
        {
            return HttpContext.Success(ModifyPhoneNumber(GetUser(HttpContext.GetUserId()), phoneCode, phone), _messages.GetString("UserPhoneModifyDone"));
        }

        [HttpPut(APIs.ConfirmUserPhone)]
        [Authorize(Roles = RolesNames.Administrator)]
        public IActionResult ConfirmUserPhone(string userName, string phoneCode)
        {
            return HttpContext.Success(ConfirmPhoneNumber(GetUser(userName), phoneCode), _messages.GetString("UserPhoneConfirmDone"));
        }

        [HttpDelete(APIs.DeleteUserPhone)]
        public IActionResult DeleteUserPhone(string phoneCode)
        {
            return HttpContext.Success(DeletePhoneNumber(GetUser(HttpContext.GetUserId()), phoneCode), _messages.GetString("UserPhoneDeletedDone"));
        }

        private User GetUser(string username)
        {
            return _context.Users
                .Include(u => u.UserPhoneNumbers)
                .FirstOrDefault(u => u.NormalizedUserName.ToUpper() == (username.ToUpper())) ?? throw new ArgumentBadException(_messages.GetString("InvalidUserName", username));
        }

        private User GetUser(int userId)
        {
            return _context.Users
                .Include(u => u.UserPhoneNumbers)
                .FirstOrDefault(u => u.Id.Equals(userId)) ?? throw new ArgumentBadException(_messages.GetString("InvalidUserName", userId));
        }

        private UserPhoneNumber GetPhoneNumberByCode(User user, string phoneCode)
        {
            return user.UserPhoneNumbers
                .FirstOrDefault(ph => ph.PhoneCode.ToUpper() == (phoneCode.ToUpper())) ?? throw new ArgumentBadException(_messages.GetString("UserPhoneCodeNotFound"));
        }

        private List<UserPhoneForReturnDto> GetAllPhoneNumbers(bool confirmed, string userName = null)
        {
            var phones = _context.UserPhoneNumbers
                .Include(ph => ph.User)
                .Where(ph => ph.Confirmed == confirmed);

            if (!string.IsNullOrWhiteSpace(userName))
            {
                phones = phones.Where(ph => ph.User.UserName.ToUpper() == (userName.ToUpper()));
            }
            return _mapper.Map<List<UserPhoneForReturnDto>>(phones);
        }

        private List<UserPhoneForReturnDto> GetPhoneNumbers(User user)
        {
            return _mapper.Map<List<UserPhoneForReturnDto>>(user.UserPhoneNumbers);
        }

        private UserPhoneForReturnDto AddPhoneNumber(User user, UserPhoneForAddDto userPhoneDto)
        {
            var phone = _mapper.Map<UserPhoneNumber>(userPhoneDto);
            user.UserPhoneNumbers.Add(phone);

            if (_context.SaveChanges() <= 0)
            {
                throw new SaveChangesFailedException();
            }
            return _mapper.Map<UserPhoneForReturnDto>(phone);
        }

        private UserPhoneForReturnDto ModifyPhoneNumber(User user, string phoneCode, UserPhoneForModifyDto userPhoneDto)
        {
            var retrievedPhone = GetPhoneNumberByCode(user, phoneCode);
            var phone = _mapper.Map<UserPhoneForModifyDto, UserPhoneNumber>(userPhoneDto, retrievedPhone);
            phone.Confirmed = false;

            if (_context.SaveChanges() <= 0)
            {
                throw new SaveChangesFailedException();
            }
            return _mapper.Map<UserPhoneForReturnDto>(phone);
        }

        private UserPhoneForReturnDto ConfirmPhoneNumber(User user, string phoneCode)
        {
            var phone = GetPhoneNumberByCode(user, phoneCode);
            if (phone.Confirmed)
            {
                throw new SaveChangesFailedException(_messages.GetString("UserPhoneAlreadyConfirmed"));
            }
            phone.Confirmed = true;

            if (_context.SaveChanges() <= 0)
            {
                throw new SaveChangesFailedException();
            }
            return _mapper.Map<UserPhoneForReturnDto>(phone);
        }

        private bool DeletePhoneNumber(User user, string phoneCode)
        {
            var phone = GetPhoneNumberByCode(user, phoneCode);
            user.UserPhoneNumbers.Remove(phone);

            if (_context.SaveChanges() <= 0)
            {
                throw new SaveChangesFailedException();
            }
            return true;
        }
    }
}