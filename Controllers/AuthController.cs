﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using PatientsRecordsManager.Constants;
using PatientsRecordsManager.Data.Dtos.Users;
using PatientsRecordsManager.Models;
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using PatientsRecordsManager.Data;
using PatientsRecordsManager.Extensions;
using System.Linq;
using PatientsRecordsManager.Resources;
using PatientsRecordsManager.Utils;
using PatientsRecordsManager.ABAC.Extensions.EF;

namespace PatientsRecordsManager.Controllers
{
    [ApiController]
    [Route(APIs.DefaultRoute)]
    [AllowAnonymous]
    public class AuthController : PrmController
    {
        private readonly IConfiguration _config;
        private readonly IMapper _mapper;
        private readonly DataContext _context;
        private readonly Messages _messages;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;

        public AuthController(IConfiguration configuration,
                              IMapper mapper,
                              DataContext context,
                              Messages messages,
                              UserManager<User> userManager,
                              SignInManager<User> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _config = configuration;
            _mapper = mapper;
            _context = context;
            _messages = messages;
        }


        [HttpPost(APIs.Register)]
        public async Task<IActionResult> Register(UserForRegisterDto userForRegister)
        {
            Assert.NotNull(userForRegister);
            var userToCreate = _mapper.Map<User>(userForRegister);
            var result = await _userManager.CreateAsync(userToCreate, userForRegister.Password).ConfigureAwait(false);

            if (result.Succeeded)
            {
                // var result2 = await _userManager.AddToRoleAsync(userToCreate, userForRegister.RoleName);
                var result2 = await _userManager.AddToRoleAsync(userToCreate, RolesNames.User).ConfigureAwait(false);

                if (result2.Succeeded)
                {
                    var userForReturn = _mapper.Map<UserForReturnDto>(GetUser(userForRegister.UserName));

                    return HttpContext.Success(userForReturn, _messages.GetString("UserCreated"));
                }
                return HttpContext.Error(result.Errors, _messages.GetString("FailedUpdateUserRoles"));
            }
            return HttpContext.Error(result.Errors, _messages.GetString("RegistrationFailed"));
        }

        [HttpPost(APIs.Login)]
        public async Task<IActionResult> Login(UserForLoginDto userForLogin)
        {
            Assert.NotNull(userForLogin);
            var user = await _userManager.FindByNameAsync(userForLogin.UserName).ConfigureAwait(false);
            var result = await _signInManager.CheckPasswordSignInAsync(user, userForLogin.Password, false).ConfigureAwait(false);

            if (result.Succeeded)
            {
                var appUser = GetUser(userForLogin.UserName);
                var userForReturn = _mapper.Map<UserForReturnDto>(appUser);

                var token = appUser.GenerateJwtMethod(_config, _userManager);

                return HttpContext.Success(new
                {
                    user = userForReturn,
                    token = token
                }, _messages.GetString("UserLogged"));
            }
            return HttpContext.Unauthorized(_messages.GetString("InavalidUserCredentials"));
        }

        private User GetUser(string username)
        {
            var appUser = _userManager.Users
                .Include(u => u.UserAddresses)
                .ThenInclude(ua => ua.Address)
                .Include(u => u.UserEmails)
                .Include(u => u.UserPhoneNumbers)
                .Include(u => u.UserRoles)
                .ThenInclude(ur => ur.Role)
                .ThenInclude(r => r.Permissions)
                .ThenInclude(rp => rp.Permission)
                .FirstOrDefault(u => u.NormalizedUserName.ToUpper() == username.ToUpper());
            return appUser;
        }
    }
}
