using System;
using System.ComponentModel.DataAnnotations;
using PatientsRecordsManager.Constants.Enums;

namespace PatientsRecordsManager.Models
{
    public class Status
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [EnumDataType(typeof(StatusType))]
        public StatusType Name { get; set; }
        public DateTime ChangedAt { get; set; }
        
        public int? PreviousStatusId { get; set; }
        public Status PreviousStatus { get; set; }

        public Status()
        {
            ChangedAt = DateTime.Now;
        }
    }

}