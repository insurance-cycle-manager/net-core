using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PatientsRecordsManager.Models
{
    public class Price
    {
        [Key]
        public int Id { get; set; }
        
        [Required]
        public double Value { get; set; }
        [Required]
        public string PriceCode { get; set; }
        
        [ForeignKey("Prices")]
        public int? PreviousPriceId { get; set; }
        public Price PreviousPrice { get; set; }
    }
}