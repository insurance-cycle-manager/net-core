using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PatientsRecordsManager.Models
{
    public class Radiograph
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string NormalizedName { get; set; }
        [Required]
        public string RadiographCode { get; set; }

        [ForeignKey("Prices")]
        public int? PriceId { get; set; }
        public Price Price { get; set; }

        public List<PrescribedRadiograph> Prescribed { get; set; }
        public List<RadiographPurchase> Purchases { get; set; }
        public List<PackageRadiograph> Packages { get; set; }
    }
}