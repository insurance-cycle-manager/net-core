using System;
using System.Collections.Generic;
using PatientsRecordsManager.Constants.Enums;
using PatientsRecordsManager.Data.Dtos.Addresses;
using PatientsRecordsManager.Data.Dtos.Users;
using PatientsRecordsManager.Data.Dtos.Users.Emails;
using PatientsRecordsManager.Data.Dtos.Users.Phones;

namespace PatientsRecordsManager.Data.Dtos.ServiceProviders.Staffers
{
    public class StafferForReturnDto
    {
        public string ServiceStafferCode { get; set; }
        public string Title { get; set; }
        public string Type { get; set; }
        public DateTime JoinedAt { get; set; }
        public StatusType Status { get; set; }
        public AddressForReturnDto Address { get; set; }
        public UserEmailForReturnDto Email { get; set; }
        public UserPhoneForReturnDto PhoneNumber { get; set; }
        public UserForReturnDto User { get; set; }
        public IEnumerable<ServiceProviderForReturnDto> ServiceProviders { get; set; }
        public IEnumerable<Visits.VisitForReturnDto> Visits { get; set; }
    }
}