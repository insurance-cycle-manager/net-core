using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PatientsRecordsManager.ABAC.Data.Models
{
    public class DataAccessRequest : DataAccessRequestDetails, IComparable<DataAccessRequestDetails>
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public DateTime DateOfAccess { get; set; }

        [Required]
        public bool Result { get; set; }

        [ForeignKey("Requests")]
        public int RequestId { get; set; }
        public Request Request { get; set; }

        public int CompareTo(DataAccessRequestDetails other)
        {
            if (other == null)
            {
                throw new ArgumentNullException();
            }

            var cc = this.Object.CompareTo(other.Object);
            if (cc != 0)
            {
                return cc;
            }

            cc = this.Action.CompareTo(other.Action);
            if (cc != 0)
            {
                return cc;
            }

            cc = this.Roles.Length.CompareTo(other.Roles.Length);
            if (cc != 0)
            {
                return cc;
            }

            cc = this.Attributes.Length.CompareTo(other.Attributes.Length);
            if (cc != 0)
            {
                return cc;
            }
            return 0;
        }
    }
}