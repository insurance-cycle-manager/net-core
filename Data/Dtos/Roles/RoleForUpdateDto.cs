using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PatientsRecordsManager.Data.Dtos.Roles
{
    public class RoleForUpdateDto
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public IEnumerable<string> UsersNames { get; set; }
    }
}