using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using PatientsRecordsManager.Constants.Enums;

namespace PatientsRecordsManager.Models
{
    public class VisitClaim
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string VisitClaimCode { get; set; }

        public Visit Visit { get; set; }

        [Required]
        [ForeignKey("ServiceProviders")]
        public int ServiceProviderId { get; set; }
        public ServiceProvider ServiceProvider { get; set; }

        [Required]
        [ForeignKey("ThirdPartyAdministrators")]
        public int ThirdPartyAdministratorId { get; set; }
        public ThirdPartyAdministrator ThirdPartyAdministrator { get; set; }

        [Required]
        [ForeignKey("InsuranceCompanies")]
        public int InsuranceCompanyId { get; set; }
        public InsuranceCompany InsuranceCompany { get; set; }

        [Required]
        public DateTime DateOfClaiming { get; set; }

        [Required]
        [EnumDataType(typeof(ClaimStatus))]
        public ClaimStatus Status { get; set; } = ClaimStatus.Pending;
    }
}
