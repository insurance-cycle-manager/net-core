using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using PatientsRecordsManager.ABAC.Extensions;
using PatientsRecordsManager.ABAC.Extensions.EF;
using PatientsRecordsManager.Data.Dtos.Prescribed.Radiographs;
using PatientsRecordsManager.Exceptions;
using PatientsRecordsManager.Models;
using PatientsRecordsManager.Resources;
using PatientsRecordsManager.Utils;

namespace PatientsRecordsManager.Data.Repositories
{
    public class VisitRadiographRepository : IVisitRadiographRepository
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        private readonly Messages _messages;

        public VisitRadiographRepository(DataContext context, IMapper mapper, Messages messages)
        {
            _mapper = mapper;
            _context = context;
            _messages = messages;
        }

        public PrescribedRadiographForReturnDto AddRadiograph(string visitCode, PrescribedRadiographForCreateDto radiographForCreate)
        {
            Assert.NotNull(radiographForCreate);
            var visit = GetVisit(visitCode);
            var radiograph = GetRadiograph(radiographForCreate.RadiographCode);

            var prescribedRadiograph = _mapper.Map<PrescribedRadiograph>(radiographForCreate);
            prescribedRadiograph.Visit = visit;
            prescribedRadiograph.Radiograph = radiograph;

            visit.Radiographs.Add(prescribedRadiograph);

            if (_context.SaveChanges() <= 0)
            {
                throw new SaveChangesFailedException();
            }
            return _mapper.Map<PrescribedRadiographForReturnDto>(prescribedRadiograph);
        }

        public PrescribedRadiographForReturnDto GetRadiographDetails(string visitCode, string radiographCode)
        {
            return _mapper.Map<PrescribedRadiographForReturnDto>(GetPrescribedRadiograph(visitCode, radiographCode));
        }

        public IEnumerable<PrescribedRadiographForReturnDto> GetRadiographs(string visitCode)
        {
            return _mapper.Map<List<PrescribedRadiographForReturnDto>>(GetVisit(visitCode).Radiographs.ToList());
        }

        public PrescribedRadiographForReturnDto ModifyRadiograph(string visitCode, string radiographCode, PrescribedRadiographForUpdateDto radiographForUpdate)
        {
            Assert.NotNull(radiographForUpdate);

            var prescribedRadiograph = _mapper.Map<PrescribedRadiographForUpdateDto, PrescribedRadiograph>(
                    radiographForUpdate,
                    GetPrescribedRadiograph(visitCode, radiographCode));

            var radiograph = GetRadiograph(radiographForUpdate.RadiographCode);
            prescribedRadiograph.Radiograph = radiograph;

            if (_context.SaveChanges() <= 0)
            {
                throw new SaveChangesFailedException();
            }
            return _mapper.Map<PrescribedRadiographForReturnDto>(prescribedRadiograph);
        }

        public PrescribedRadiographForReturnDto RemoveRadiograph(string visitCode, string radiographCode)
        {
            return _mapper.Map<PrescribedRadiographForReturnDto>(_context.PrescribedRadiographs.Remove(GetPrescribedRadiograph(visitCode, radiographCode)).Entity);
        }

        private Visit GetVisit(string visitCode)
        {
            var visit = _context.Visits
                .Include(v => v.Radiographs)
                .AsEnumerable()
                .FirstOrDefault(v => v.VisitCode.ToUpper() == (visitCode.ToUpper()));

            if (visit == null)
            {
                throw new ArgumentBadException(_messages.GetString("visitNotFound", visitCode));
            }
            return visit;
        }

        private PrescribedRadiograph GetPrescribedRadiograph(string visitCode, string radiographCode)
        {
            var visit = GetVisit(visitCode);
            var radiograph = visit.Radiographs
                .AsEnumerable()
                .FirstOrDefault(r => r.PrescribedRadiographCode.ToUpper() == (radiographCode.ToUpper()));

            if (radiograph == null)
            {
                throw new ArgumentBadException(_messages.GetString("visitRadiographNotFound", visitCode, radiographCode));
            }
            return radiograph;
        }

        private Radiograph GetRadiograph(string radiographCode)
        {
            return _context.Radiographs
                .FirstOrDefault(r => r.RadiographCode.ToUpper() == (radiographCode.ToUpper()))
                ?? throw new ArgumentBadException(_messages.GetString("radiographNotFound", radiographCode));
        }
    }
}