using System;

namespace PatientsRecordsManager.Data.Dtos.Prescribed.Radiographs
{
    public class PrescribedRadiographForUpdateDto
    {
        public string RadiographCode { get; set; }
        public DateTime ScheduledDate { get; set; }
        public string Note { get; set; }
    }
}