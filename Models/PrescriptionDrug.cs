using System.ComponentModel.DataAnnotations;

namespace PatientsRecordsManager.Models
{
    public class PrescriptionDrug
    {
        [Required]
        public int PrescriptionId { get; set; }
        public Prescription Prescription { get; set; }
        
        [Required]
        public int DrugId { get; set; }
        public Drug Drug { get; set; }
    }
}