using System.ComponentModel.DataAnnotations;

namespace PatientsRecordsManager.ABAC.Data.Dtos
{
    public class RuleForCreateDto
    {
        [Required]
        public string V0 { get; set; }

        [Required]
        public string V1 { get; set; }

        [Required]
        public string V2 { get; set; }

        [Required]
        public string V3 { get; set; }

        public virtual string V4 { get; set; }

        // public virtual string V5 { get; set; }
    }
}