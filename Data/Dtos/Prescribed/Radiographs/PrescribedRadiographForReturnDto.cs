using System;
using System.ComponentModel.DataAnnotations;
using PatientsRecordsManager.Data.Dtos.Radiographs;
using PatientsRecordsManager.Data.Dtos.Visits;

namespace PatientsRecordsManager.Data.Dtos.Prescribed.Radiographs
{
    public class PrescribedRadiographForReturnDto
    {
        [Required]
        public string PrescribedRadiographCode { get; set; }
        
        [Required]
        public string VisitCode { get; set; }
        public VisitForReturnDto Visit { get; set; }

        [Required]
        public string RadiographCode { get; set; }
        public RadiographForReturnDto LabTest { get; set; }

        [Required]
        public DateTime ScheduledDate { get; set; }
        
        [Required]
        public string Note { get; set; }
    }
}