using Casbin.NET.Adapter.EFCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PatientsRecordsManager.ABAC.Data.Models;

namespace PatientsRecordsManager.ABAC.Data
{
    public class AbacDbContext : CasbinDbContext<int>
    {
        public const string pType = "p";
        public const string act = "act";
        public const string dbo = "dbo";

        // public new DbSet<Rule> CasbinRule { get; set; }
        public DbSet<RuleAttributes> RuleAttributes { get; set; }
        public DbSet<Request> Requests { get; set; }
        public DbSet<DataAccessRequest> DataAccessRequests { get; set; }

        public AbacDbContext(IConfiguration configuration) : base(new DbContextOptionsBuilder<CasbinDbContext<int>>()
            .UseNpgsql(configuration.GetConnectionString("AbacConnection"))
            .Options)
        {
            Database.EnsureCreated();
        }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            if (builder == null)
            {
                throw new System.ArgumentNullException(nameof(builder));
            }

            base.OnModelCreating(builder);

            // builder.Entity<CasbinRule<int>>(rules =>
            // {
            //     rules.Property(r => r.Id).ValueGeneratedOnAdd();
            //     rules.HasIndex(r => new { r.V0, r.V1, r.V4, r.V5 }).IsUnique(true);

            // builder.HasSequence<int>("RuleCodeNums");
            // rules.Property(r => r.RuleCode)
            //     .HasDefaultValueSql(@"'RULE' || nextval('public.""RuleCodeNums""')");

            // System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(r.V4)))
            // rules.Property(r => r.V4)
            //     .HasConversion(
            //         v => System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(v)),
            //         v => System.Text.Encoding.UTF8.GetString(System.Convert.FromBase64String(v))
            //     );
            // });

            builder.Entity<RuleAttributes>(attrs =>
            {
                attrs.Property(r => r.Id).ValueGeneratedOnAdd();

                attrs.Property(r => r.Attributes)
                    .HasConversion(
                        v => JsonConvert.SerializeObject(v, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }),
                        v => JsonConvert.DeserializeObject<JObject>(v, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore })
                    );
            });

            builder.Entity<Request>(requests =>
            {
                requests.HasMany(r => r.Items)
                    .WithOne(a => a.Request)
                    .HasForeignKey(r => r.RequestId)
                    .IsRequired(false);
            });

            builder.Entity<DataAccessRequest>(accesses =>
            {

            });
        }

    }
}