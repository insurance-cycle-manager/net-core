using System;
using System.Collections.Generic;
using PatientsRecordsManager.Data.Dtos.Addresses;
using PatientsRecordsManager.Data.Dtos.InsuranceConnections;
using PatientsRecordsManager.Data.Dtos.ServiceProviders.Connections;
using PatientsRecordsManager.Data.Dtos.Users.Addresses;
using PatientsRecordsManager.Data.Dtos.Users.Emails;
using PatientsRecordsManager.Data.Dtos.Users.Phones;
using PatientsRecordsManager.Data.Dtos.Visits.Claims;

namespace PatientsRecordsManager.Data.Dtos.TPAs
{
    public class TpaForReturnDto
    {
        public string Name { get; set; }
        public DateTime DateOfFounding { get; set; }
        public DateTime JoinedAt { get; set; }
        public UserAddressForReturnDto Address { get; set; }
        public UserEmailForReturnDto Email { get; set; }
        public UserPhoneForReturnDto PhoneNumber { get; set; }
        public string SupervisoryComission { get; set; }
        public string Status { get; set; }
        public string ThirdPartyCode { get; set; }
        public IEnumerable<IcConnectionForReturnDto> InsuranceConnections { get; set; }
        public IEnumerable<SpConnectionForReturnDto> ServiceProvidersConnections { get; set; }
        public IEnumerable<VisitClaimForReturnDto> VisitClaims { get; set; }
    }
}