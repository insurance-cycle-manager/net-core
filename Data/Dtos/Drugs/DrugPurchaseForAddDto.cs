using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using PatientsRecordsManager.Data.Dtos.Prices;

namespace PatientsRecordsManager.Data.Dtos.Drugs
{
    public class DrugPurchaseForAddDto
    {
        [Required]
        public DateTime DateOfPurchasing { get; set; }

        [Required]
        public PriceForAddDto Price { get; set; }

        [Required]
        public IEnumerable<string> DrugCodes { get; set; }
        public string VisitCode { get; set; }
    }
}