using System;
using System.ComponentModel.DataAnnotations;
using PatientsRecordsManager.Data.Dtos.Surgeries;
using PatientsRecordsManager.Data.Dtos.Visits;

namespace PatientsRecordsManager.Data.Dtos.Prescribed.Surgeries
{
    public class PrescribedSurgeryForReturnDto
    {
        [Required]
        public string PrescribedSurgeryCode { get; set; }
        
        [Required]
        public string VisitCode { get; set; }
        public VisitForReturnDto Visit { get; set; }

        [Required]
        public string SurgeryCode { get; set; }
        public SurgeryForReturnDto Surgery { get; set; }

        [Required]
        public DateTime ScheduledDate { get; set; }
        
        [Required]
        public string Note { get; set; }
    }
}