using System.ComponentModel.DataAnnotations;

namespace PatientsRecordsManager.Data.Dtos.RolesRequests
{
    public class AddUserToRoleDto : RequestUserRoleDto
    {
        [Required]
        public string UserName { get; set; }
    }
}