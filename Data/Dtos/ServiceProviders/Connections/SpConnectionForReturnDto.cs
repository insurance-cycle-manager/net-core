using System;
using System.Collections.Generic;
using PatientsRecordsManager.Constants.Enums;
using PatientsRecordsManager.Data.Dtos.InsuranceCompanies;
using PatientsRecordsManager.Data.Dtos.TPAs;
using PatientsRecordsManager.Data.Dtos.Visits;

namespace PatientsRecordsManager.Data.Dtos.ServiceProviders.Connections
{
    public class SpConnectionForReturnDto
    {
        public string ConnectionCode { get; set; }
        public DateTime CreatedAt { get; set; }
        public SpConnectionStatus Status { get; set; }
        public ServiceProviderForReturnDto ServiceProvider { get; set; }
        public TpaForReturnDto TPA { get; set; }
        public IEnumerable<InsuranceCompanyForReturnDto> InsuranceCompanies { get; set; }
        public IEnumerable<VisitForReturnDto> Visits { get; set; }
    }
}