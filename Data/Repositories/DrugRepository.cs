using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using PatientsRecordsManager.ABAC.Extensions;
using PatientsRecordsManager.ABAC.Extensions.EF;
using PatientsRecordsManager.Data.Dtos.Drugs;
using PatientsRecordsManager.Models;
using PatientsRecordsManager.Resources;

namespace PatientsRecordsManager.Data.Repositories
{
    public class DrugRepository : IDrugRepository
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        private readonly Messages _messages;
        private readonly IPriceRepository _prices;

        public DrugRepository(DataContext context, IMapper mapper, Messages messages, IPriceRepository prices)
        {
            _prices = prices;
            _mapper = mapper;
            _context = context;
            _messages = messages;
        }

        public DrugForReturnDto AddDrug(DrugForCreateDto drugForCreate)
        {
            if (drugForCreate == null)
            {
                throw new ArgumentNullException(nameof(drugForCreate));
            }

            var drug = _mapper.Map<Drug>(drugForCreate);
            _context.Drugs.Add(drug);

            if (_context.SaveChanges() <= 0)
            {
                throw new Exception(_messages.GetString("saveFailed"));
            }
            return _mapper.Map<DrugForReturnDto>(drug);
        }

        public bool DeleteDrug(string drugCode)
        {
            var drug = GetDrug(drugCode);
            if (drug == null)
            {
                return false;
            }

            _context.Drugs.Remove(drug);

            if (_context.SaveChanges() <= 0)
            {
                return false;
            }
            return true;
        }

        public DrugForReturnDto GetDrugDetails(string drugCode)
        {
            return _mapper.Map<DrugForReturnDto>(GetDrug(drugCode));
        }

        public IEnumerable<DrugForReturnDto> GetDrugs(string name, string manufacturer)
        {
            var drugs = _context.Drugs.AsQueryable();
            if (!string.IsNullOrWhiteSpace(name))
            {
                drugs = drugs.Where(e => e.Name.ToUpper().Contains(name.ToUpper()));
            }

            if (!string.IsNullOrWhiteSpace(manufacturer))
            {
                drugs = drugs.Where(e => e.Manufacturer.ToUpper().Contains(manufacturer.ToUpper()));
            }
            return _mapper.Map<List<DrugForReturnDto>>(drugs);
        }

        public DrugForReturnDto ModifyDrug(string drugCode, DrugForUpdateDto drugForUpdate)
        {
            if (drugForUpdate == null)
            {
                throw new ArgumentNullException(nameof(drugForUpdate));
            }

            var retrievedDrug = GetDrug(drugCode);
            _mapper.Map<DrugForUpdateDto, Drug>(drugForUpdate, retrievedDrug);

            if (_context.SaveChanges() <= 0)
            {
                throw new Exception(_messages.GetString("saveFailed"));
            }
            return _mapper.Map<DrugForReturnDto>(retrievedDrug);
        }

        private Drug GetDrug(string drugCode)
        {
            var drug = _context.Drugs
                .Include(d => d.Price)
                .FirstOrDefault(e => e.DrugCode.ToUpper() == (drugCode.ToUpper()));
            if (drug == null)
            {
                throw new Exception(_messages.GetString("drugNotFound", drugCode));
            }
            return drug;
        }
    }
}