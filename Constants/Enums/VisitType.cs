namespace PatientsRecordsManager.Constants.Enums
{
    public enum VisitType
    {
        CheckUp,
        FollowUp,
        Urgent,
        PurchaseLabTest,
        PurchaseRadiograph,
        PurchaseSurgery,
        PurchasePrescription
    }
}