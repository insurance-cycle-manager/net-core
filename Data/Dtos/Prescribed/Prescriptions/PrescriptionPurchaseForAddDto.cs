using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using PatientsRecordsManager.Data.Dtos.Prices;

namespace PatientsRecordsManager.Data.Dtos.Prescribed.Prescriptions
{
    public class PrescriptionPurchaseForAddDto
    {
        [Required, MinLength(1)]
        public IEnumerable<string> PurchasedDrugs { get; set; }
    }
}