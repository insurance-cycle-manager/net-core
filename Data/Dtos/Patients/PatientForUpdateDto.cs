using System;
using System.ComponentModel.DataAnnotations;
using PatientsRecordsManager.Constants.Enums;
using PatientsRecordsManager.Data.Dtos.Addresses;

namespace PatientsRecordsManager.Data.Dtos.Patients
{
    public class PatientForUpdateDto
    {
        [Required]
        public DateTime? DateOfBirth { get; set; }

        [Required]
        [EnumDataType(typeof(Gender))]
        public Gender? Gender { get; set; }
    }
}