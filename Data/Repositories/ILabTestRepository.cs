using System.Collections.Generic;
using PatientsRecordsManager.Data.Dtos.LabTests;

namespace PatientsRecordsManager.Data.Repositories
{
    public interface ILabTestRepository
    {
        LabTestForReturnDto AddLabTest(LabTestForCreateDto labTest);
        LabTestForReturnDto ModifyLabTest(string labTestCode, LabTestForUpdateDto labTest);
        bool DeleteLabTest(string labTestCode);
        LabTestForReturnDto GetLabTestDetails(string labTestCode);
        IEnumerable<LabTestForReturnDto> GetLabTests(string name);
    }
}