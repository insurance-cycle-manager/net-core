namespace PatientsRecordsManager.Constants.Enums
{
    public enum RoleRequestResponse
    {
        Reject,
        Approve
    }

    public enum RoleRequestStatus
    {
        Pending,
        Canceled,
        Approved,
        Rejected,
        Removed,
    }
}