using System.ComponentModel.DataAnnotations;

namespace PatientsRecordsManager.Data.Dtos.Prices
{
    public class PriceForAddDto
    {
        [Required]
        public double Value { get; set; }
    }
}