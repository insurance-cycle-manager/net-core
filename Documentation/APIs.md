<h3 align="center">Patients Records Manager</h3>

## 📝 Table of Contents

- [User](#user)

  - [User Information](#user-information)
    - [User Addresses](#user-addresses)
    - [User Emails](#user-emails)
    - [User Phones](#user-phones)
  - [User Roles](#user-roles)

- [Templates](#templates)

  - [Drugs](#drugs)
  - [Surgeries](#surgeries)
  - [LabTests](#labTests)
  - [Radiographs](#radiographs)

- [ِAddresses](#addresses)

- [Service Providers](#service-providers)
- [Service Staffers](#service-staffers)
- [Service Providers' Staffers](#service-providers-staff)
- [Service Providers' Connections](#sp-connections)

- [Patients](#patients)
- [Patients' Packages](#patients-packages)

- [Third-Party Administrators](#tpas)

- [Insurance Companies](#ics)
- [Insurance Companies' Connections](#ics-connections)
- [Insurance Companies' Packages](#ics-packages)

- [Visits](#visits)

  - [Visit Prescriptions](#visit-prescriptions)
  - [Visit Prescribed Lab Tests](#visit-labTests)
  - [Visit Prescribed Surgeries](#visit-surgeries)
  - [Visit Prescribed Radiographs](#visit-radiographs)
  - [Visit Claims](#visit-claims)
  - [Visit Prescriptions Purchase](#visit-prescriptions-purchase)
  - [Visit Prescribed Lab Tests Purchase](#visit-labTests-purchase)
  - [Visit Prescribed Surgeries Purchase](#visit-surgeries-purchase)
  - [Visit Prescribed Radiographs Purchase](#visit-radiographs-purchase)

- [Statistics](#stats)

## 👦👩‍🦰 User Management <a name = "user"></a>

### User Information <a name = "user-information"></a>

- Get Information
  Get account information for the given user name

  - GET: Users/:userName
  - Only admins are allowed

- Get Information (User)
  Get the authenticated user account information

  - GET: Users/me
  - Any authenticated user can use this API

- Modify Information
  Modify account information for the given user name

  - PUT: Users/:userName
  - Only admins are allowed
  - Fields
    - PhotoUrl
    - Email (Main Email)
      - Must be already added email
    - PhoneNumber
      - Must be already addred phone number
    - UserEmails
      - List of email addresses
    - UserPhones
      - List of phone numbers
    - UserAddresses
      - List of address codes

- Modify Information (User)
  Modify the authenticated user account information

  - PUT: Users/me
  - Any authenticated user can use this API
  - Fields
    - PhotoUrl
    - Email (Main Email)
      - Must be already added email
    - PhoneNumber
      - Must be already addred phone number
    - UserEmails
      - List of email addresses
    - UserPhones
      - List of phone numbers
    - UserAddresses
      - List of address codes

#### User Addresses <a name = "user-addresses"></a>

Manage the addresses associated with the user

- GET: UserAddresses/Pending

  - Get list of users' addresses which need to confirm
  - Only admins are allowed

- GET: UserAddresses/

  - Get list of addresses associated with the authenticated user

- POST: UserAddresses/

  - Add new address into the authenticated user's addresses
  - Fields
    - Address Code

- PUT: UserAddresses/:username/:addressCode/Confirm

  - Only admins are allowed

- DELETE: UserAddresses/:addressCode
  - Delete address of the authenticated user

#### User Emails <a name = "user-emails"></a>

Manage the emails associated with the user

- GET: UserEmails/Pending

  - Get list of users' emails which need to confirm
  - Only admins are allowed

- GET: UserEmails/

  - Get list of emails associated with the authenticated user

- POST: UserEmails/

  - Add new address into the authenticated user's emails
  - Fields
    - Email Address

- PUT: UserEmails/:emailCode

  - Update address of the authenticated user
  - Fields
    - Email Address (New Email Address)

- PUT: UserEmails/:username/:emailCode/Confirm

  - Only admins are allowed

- DELETE: UserEmails/:emailCode
  - Delete address of the authenticated user

#### User Phones <a name = "user-phones"></a>

Manage the phones associated with the user

- GET: UserPhones/Pending

  - Get list of users' phones which need to confirm
  - Only admins are allowed

- GET: UserPhones/

  - Get list of phones associated with the authenticated user

- POST: UserPhones/

  - Add new phone number into the authenticated user's phones
  - Fields
    - Number

- PUT: UserPhones/:phoneCode

  - Update phone number of the authenticated user
  - Fields
    - Number (New Number)

- PUT: UserPhones/:username/:phoneCode/Confirm

  - Only admins are allowed

- DELETE: UserPhones/:phoneCode
  - Delete phone number of the authenticated user

### User Roles <a name = "user-roles"></a>

#### Get Roles Requests <a name = "roles-requests-get"></a>

Fetch list of pending roles requests.

- GET: /Admin/UserRoles/
- Only admins are allowed
- Params
  - Status.
    - Enum (PENDING, CANCELED, APPROVED, REJECTED)

#### Get User Roles Requests <a name = "user-roles-requests-get"></a>

Fetch list of pending roles requests associated with the authenticated user.

- GET: /UserRoles/
- Params
  - Status.
    - Enum (PENDING, CANCELED, APPROVED, REJECTED)

#### Request User Role <a name = "request-user-role"></a>

Request a specific role for the authenticated user.

- POST: /UserRoles/
- Fields
  - RoleName.
  - PreferredEmail.
  - PreferredPhoneNumber.
  - PreferredAddressCode.
  - Title.

#### Assign User To Role <a name = "assign-user-to-role"></a>

Add a specific user into a specific role.

- POST: /Admin/UserRoles/
- Only admins are allowed
- Fields
  - All fields from [Request User Role](#request-user-role)
  - UserName

#### Remove User Role <a name = "user-role-remove"></a>

Remove a specific role from the authenticated user's roles.

- DELETE: /UserRoles/:RoleName

#### Un Assign User From Role <a name = "unassign-user-from-role"></a>

Unassign a specific user from a specific role.

- DELETE: /Admin/UserRoles/:UserName/:RoleName
- Only admins are allowed

#### Manage Roles Requests <a name = "manage-roles-requests"></a>

Manage a pending role request.

- POST: /Admin/UserRoles/
- Only admins are allowed
- Fields
  - UserName.
  - RoleName.
  - Response.
    - Enum (REJECT, APPROVE)

## 🧐 Templates and Constants <a name = "templates"></a>

### Drugs <a name = "drugs"></a>

- Add Drug
  add new drug template.

  - POST: /drugs/
  - The required fields
    - Name.
    - Manufacturer.

- Get Drugs
  fetch list of drugs templates

  - GET: /drugs/

- Current Drug
  drug code is required.

  - Details
    get more details about the specified drug.

    - GET: /drugs/:code/

  - Update drug
    update the specified drug.

    - PUT: /drugs/:code/
    - The required fields
      - Name.
      - Manufacturer.
      - Price.
        - Administrators only allowed.

  - Delete drug
    delete the specified drug

    - DELETE: /drugs/:code/

### Surgeries <a name = "surgeries"></a>

- Add Surgery
  add new surgery template.

  - POST: /surgeries/
  - The required fields
    - Name.

- Get Surgeries
  fetch list of surgeries templates

  - GET: /surgeries/

- Current Surgery
  surgery code is required.

  - Details
    get more details about the specified surgery.

    - GET: /surgeries/:code/

  - Update surgery
    update the specified surgery.

    - PUT: /surgeries/:code/
    - The required fields
      - Name.
      - Price.
        - Administrators only allowed.

  - Delete surgery
    delete the specified surgery

    - DELETE: /surgeries/:code/

### Lab Tests <a name = "labTests"></a>

- Add Lab Test
  add new lab test template.

  - POST: /labtests/
  - The required fields
    - Name.

- Get Lab Tests
  fetch list of labtests templates

  - GET: /labtests/

- Current Lab Test
  lab test code is required.

  - Details
    get more details about the specified lab test.

    - GET: /labtests/:code/

  - Update lab test
    update the specified lab test.

    - PUT: /labtests/:code/
    - The required fields
      - Name.
      - Price.
        - Administrators only allowed.

  - Delete lab test
    delete the specified lab test

    - DELETE: /labtests/:code/

### Radiographs <a name = "radiographs"></a>

- Add Radiograph
  add new lab test template.

  - POST: /radiographs/
  - The required fields
    - Name.

- Get Radiographs
  fetch list of radiographs templates

  - GET: /radiographs/

- Current Radiograph
  radiograph code is required.

  - Details
    get more details about the specified radiograph.

    - GET: /radiographs/:code/

  - Update radiograph
    update the specified radiograph.

    - PUT: /radiographs/:code/
    - The required fields
      - Name.
      - Price.
        - Administrators only allowed.

  - Delete radiograph
    delete the specified radiograph

    - DELETE: /radiographs/:code/

## Addresses <a name = "addresses"></a>

### Get Addresses <a name = "get-addresses"></a>

Fetch list of addresses

- GET: /addresses/
- Any authentecated user is allowed to use it.
- Params:
  - Title.
  - AddressCode.

### Add Address <a name = "add-address"></a>

Add & save new address

- POST: /addresses/
- Only admins are allowed.
- Fields:
  - Title.
  - Longitude (Optional).
  - Latitude (Optional).

### Modify Address <a name = "modify-address"></a>

Update an already exist address

- POST: /addresses/:addressCode
- Only admins are allowed.
- Fields:
  - Title.
  - Longitude (Optional).
  - Latitude (Optional).

## 👨‍⚕️👩‍⚕️ Service Providers <a name = "service-providers"></a>

### Service Provider Type <a name = "service-provider-type"></a>

Available service provider type:

- HOSPITAL.
- PHARMACY.
- LABORATORY.
- RADIOGRAPH_OFFICE.
- CLINIC.

### Get Service Providers <a name = "get-service-providers"></a>

Fetch list of service providers

- GET: ServiceProviders/:spType
  - spType must be one of [Avaialabe Types](#service-provider-type)
- Any authenticated user is allowed.
- Params
  - Title.
    - Filter service providers by Title

### Get Service Provider's Details <a name = "get-service-provider"></a>

Get a specific service provider's details

- GET: ServiceProviders/:spType/:spCode
  - spType must be one of [Avaialabe Types](#service-provider-type)
- Any authenticated user is allowed.

### Get Service Provider's Details (User) <a name = "get-service-provider-user"></a>

Get details of service provider associated with authenticated user

- GET: ServiceProviders/:spType/Me
  - spType must be one of [Avaialabe Types](#service-provider-type)

### Modify Service Provider's Details <a name = "modify-service-provider"></a>

Update a specific service provider's details

- PUT: ServiceProviders/:spType/:spCode
  - spType must be one of [Avaialabe Types](#service-provider-type)
- Any authenticated user is allowed.
- Fields (Not required but they're expected)
  - Title
  - UserAddressCode
  - UserEmailCode
  - UserPhoneCode

### Modify Service Provider's Details (User) <a name = "modify-service-provider-user"></a>

Update service provider's details for the record that associated with authenticated user.

- PUT: ServiceProviders/:spType/me
  - spType must be one of [Avaialabe Types](#service-provider-type)
- User must be already in role corresponding with :spType.
- Fields (Not required but they're expected)
  - Title
  - UserAddressCode
  - UserEmailCode
  - UserPhoneCode

## Service Staffers <a name = "service-staffers"></a>

### Service Staffer Type <a name = "service-staffer-type"></a>

Available service staffers type:

- DOCTOR.
- NURSE.
- PHARMACIST.
- RADIOGRAPHER.
- LAB_SPECIALIST.
- RECEPTIONIST.

### Get Staffers <a name = "get-service-staffers"></a>

Fetch list of service staffers

- GET: ServiceStaffers/:sType
  - sType must be one of [Avaialabe Types](#service-staffer-type)
- Any authenticated user is allowed.
- Params
  - Title.
    - Filter service staffers by Title

### Get Service Staffer Details <a name = "get-service-staffer"></a>

Get a specific service staffer's details

- GET: ServiceStaffers/:sType/:username
  - sType must be one of [Avaialabe Types](#service-staffer-type)
- Any authenticated user is allowed.

### Get Service Staffer's Details (User) <a name = "get-service-staffer-user"></a>

Get details of service staffer associated with authenticated user

- GET: ServiceStaffers/:spType/Me
  - spType must be one of [Avaialabe Types](#service-staffer-type)

### Modify Service Staffer's Details <a name = "modify-service-staffer"></a>

Update a specific service staffer's details

- PUT: ServiceStaffers/:spType/:spCode
  - spType must be one of [Avaialabe Types](#service-staffer-type)
- Any authenticated user is allowed.
- Fields (Not required but they're expected)
  - Title
  - UserAddressCode
  - UserEmailCode
  - UserPhoneCode

### Modify Service Staffer's Details (User) <a name = "modify-service-staffer-user"></a>

Update service staffer's details for the record that associated with authenticated user.

- PUT: ServiceStaffers/:spType/me
  - spType must be one of [Avaialabe Types](#service-staffer-type)
- User must be already in role corresponding with :spType.
- Fields (Not required but they're expected)
  - Title
  - UserAddressCode
  - UserEmailCode
  - UserPhoneCode

## 👨‍⚕️👩‍⚕️ Service Providers' Staff <a name = "service-providers-staff"></a>

### Get Service Provider's Staffers By Provider <a name = "get-staff-by-provider"></a>

Fetch list of members associated with the specfic service provider.

- GET: ServiceProviders/:spCode/Staff
- Any authenticated user is allowed.

### Get Service Provider's Staffers By Staffer <a name = "get-staff-by-staffer"></a>

Fetch list of members associated with the specfic service satffer.

- GET: ServiceStaffers/:stCode/Staff
- Any authenticated user is allowed.

### Add Member to Staff <a name = "add-staff-member"></a>

Add a specific member (Service Staffer) into service provider's staff.
After adding the member' status will be Pending waiting for staffer confirmation.

- POST: ServiceProviders/:spCode/Staff
- Authenticated User.
  - Admins are allowed to add any staffer to any provider.
  - Normal user allowed only to add staffers into their own.
- Fields
  - StafferCode

### Confirm member <a name = "confirm-staff-member"></a>

Confirm

- PUT: ServiceStaffers/:stCode/Staff/:spCode
- Authenticated User.
  - Admins are allowed to confirm any membership.
  - Normal user allowed only to confirm their own.

### Delete member <a name = "delete-staff-member"></a>

Delete member from the specific provider's staff.

- DELETE: ServiceProviders/:spCode/Staff/:stCode
- Authenticated User.
  - Admins are allowed to delete any membership.
  - Normal user allowed only to delete from their own.

## 🧒 Service Providers' Connections <a name = "sp-connections"></a>

- Fetch Connections
  Fetch list of connections recorded in our databases

  - GET: spConnections/
  - Params
    - status: filter connections' status
      - Pending, Approved, Rejected, TpaDeactivated, SpDeactivated;
    - sp: filter connections' service provider by their title
    - tpa: filter connections' tpa by its name
  - Authenticated Users only allowed

- Fetch SP Connections
  Fetch list of connections associated with the authenticated sp

  - GET: spConnections/sp/:spType/me/
  - Params
    - type: filter type of sp
      - Hospital, Pharmacy, ...
  - Authenticated Users only allowed

- Fetch TPA Connections
  Fetch list of connections associated with the authenticated tpa

  - GET: spConnections/tpa/me/
  - Authenticated Users only allowed

- Invite TPA to connection
  ask a specific TPA to connect with the authenticated user (sp)

  - POST: spConnections/sp/:spType/me/
  - Required Fields
    - TPA Code.
    - List of Insurance Companies (IcCode)
  - Optional Fields
    - CreatedAt (Has Default Vaule (NOW))

- Invite SP to connection
  ask a specific SP to connect with the authenticated user (tpa)

  - POST: spConnections/tpa/me/
  - Required Fields
    - Sp Code.
    - List of Insurance Companies (IcCode)
  - Optional Fields
    - CreatedAt (Has Default Vaule (NOW))

- Modify connection as SP
  ask the specific TPA to modify the already existed connection with the authenticated SP

  - PUT: spConnections/sp/:spType/me/:connectionCode
  - Required Fields
    - List of Insurance Companies (IcCode)

- Modify connection as TPA
  ask the specific SP to modify the already existed connection with the authenticated TPA

  - PUT: spConnections/tpa/me/:connectionCode
  - Required Fields
    - List of Insurance Companies (IcCode)

- Change connection request as SP
  change staus of the determined connection as SP

  - PUT: spConnections/sp/:spType/me/:connectionCode/status
  - Required Fields
    - status: filter connections' status
      - Pending, Approved, Rejected, TpaDeactivated, SpDeactivated;

- Change connection request as TPA
  change staus of the determined connection as TPA
  - PUT: spConnections/tpa/me/:connectionCode/status
  - Required Fields
    - status: filter connections' status
      - Pending, Approved, Rejected, TpaDeactivated, SpDeactivated;

## 🤧 Patients <a name = "patients"></a>

### Patient Gender Enum <a name = "patients-genders"></a>

Supported genders for a specific patient

- Male
- Female
- Unknown (Mouaz)

### Get Patients <a name = "patients-fetch"></a>

Fetch list of Third-party administrators

- GET: /patients
- Any authenticated user is allowed.

### Get Patient's Details <a name = "patients-details"></a>

Get a specific tpa's details

- GET: /patients/:patientCode
- Only admins are allowed.

### Get Patient's Details (User) <a name = "patients-details-me"></a>

Get details of Patient associated with authenticated user

- GET: /patients/me

### Modify Patient's Details <a name = "patients-modify"></a>

Update a specific Patient's details

- PUT: /patients/:patientCode
- Only admins are allowed.
- Fields (Not required but expected)
  - DateOfBirth
  - Gender
    - Must be [Avaialabe Gender](#patients-genders)

### Modify Patient's Details (User) <a name = "patients-modify-me"></a>

Update tpa's details for the record that associated with authenticated user.

- PUT: /patients/me
- User must be already in role 'Patient'.
- Fields (Not required but they're expected)
  - DateOfBirth
  - Gender
    - Must be [Avaialabe Gender](#patients-genders)

## 🤵 Patients' Packages <a name = "patients-packages"></a>

### Add Package <a name = "patients-packages-add"></a>

Subscribe the specified patient into a specific package

- POST: /patients/:patientCode/packages
- Only admins are allowed.
- Fields
  - PackageCode
    - Code must be valid and belongs to an IC which is already connected with the patient

### Add Package (User) <a name = "patients-packages-add-me"></a>

Subscribe the authenticated patient into a specific package

- POST: /patients/me/packages
- Any authenticated user (User must be already Patient).
- Fields
  - PackageCode
    - Code must be valid.

### Suspend Package (User) <a name = "patients-packages-suspend-me"></a>

Suspend a specific package of the authenticated patient

- POST: /patients/me/packages/:packageCode
- Any authenticated user
  - User must be already Patient.
  - Package code must be for an already added package

## 🧒 Third-party Administrators <a name = "tpas"></a>

### Get TPAs <a name = "tpas-fetch"></a>

Fetch list of Third-party administrators

- GET: /thirdPartyAdministrators
- Any authenticated user is allowed.

### Get TPA's Details <a name = "tpas-details"></a>

Get a specific tpa's details

- GET: /thirdPartyAdministrators/:tpaCode
- Only admins are allowed.

### Get TPA's Details (User) <a name = "tpas-details-me"></a>

Get details of TPA associated with authenticated user

- GET: /thirdPartyAdministrators/me

### Modify TPA's Details <a name = "tpas-modify"></a>

Update a specific TPA's details

- PUT: /thirdPartyAdministrators/:tpaCode
- Only admins are allowed.
- Fields (Not required but expected)
  - JoinedDate
  - DateOfFounding

### Modify TPA's Details (User) <a name = "tpas-modify-me"></a>

Update tpa's details for the record that associated with authenticated user.

- PUT: /thirdPartyAdministrators/me
- User must be already in role 'TPA'.
- Fields (Not required but they're expected)
  - JoinedDate
  - DateOfFounding

## 🧒 Insurance Companies <a name = "ics"></a>

### Get ICs <a name = "ics-fetch"></a>

Fetch list of Insurance Companies

- GET: /insuranceCompanies
- Any authenticated user is allowed.

### Get IC's Details <a name = "ics-details"></a>

Get a specific tpa's details

- GET: /insuranceCompanies/:icCode
- Only admins are allowed.

### Get IC's Details (User) <a name = "ics-details-me"></a>

Get details of IC associated with authenticated user

- GET: /insuranceCompanies/me

### Modify IC's Details <a name = "ics-modify"></a>

Update a specific IC's details

- PUT: /insuranceCompanies/:icCode
- Only admins are allowed.
- Fields (Not required but expected)
  - JoinedDate
  - DateOfFounding

### Modify IC's Details (User) <a name = "ics-modify-me"></a>

Update tpa's details for the record that associated with authenticated user.

- PUT: /insuranceCompanies/me
- User must be already in role 'InsuranceCompany'.
- Fields (Not required but they're expected)
  - JoinedDate
  - DateOfFounding

## 🧒 Insurance Companies' Connections <a name = "ics-connections"></a>

- Add New Connection
  adding new connection between a specific insurance company and TPA.

  - POST: insuranceConnections/
  - Required Fields
    - InsuranceCompanyCode.
    - ThirdPartyCode

- Delete Connection
  delete a specific connection from our records.

  - DELETE: insuranceConnections/:connectionCode/

## 🧒🧒 Insurance Companies' Packages <a name = "ics-packages"></a>

- Get Packages
  Fetch list of package for all insuarance companies

  - GET: /insurancePackages/
  - Any authenticated user is allowed

- Get IC Packages
  Fetch list of package for a given ic code

  - GET: /insurancePackages/:companyCode
  - Any authenticated user is allowed

- Get an IC Package
  Fetch a specific package for a given ic code

  - GET: /insurancePackages/:companyCode/:packageName
  - Any authenticated user is allowed

- Add Package
  Add new package into the specific insurance comapny

  - POST: /insurancePackages/:companyCode/
  - Only admins are allowed
  - Fields
    - Name
    - Category
    - Price
    - Drugs
      - List of drug codes supported by the new package
    - LabTests
      - List of labtest codes supported by the new package
    - Radiographs
      - List of radiograph codes supported by the new package
    - Surgeries
      - List of surgery codes supported by the new package

- Modify Package
  Modify specified package of the specific insurance comapny

  - PUT: /insurancePackages/:companyCode/:packageName
  - Only admins are allowed
  - Fields
    - Category
    - Price
    - Drugs
      - List of drug codes supported by the new package
    - LabTests
      - List of labtest codes supported by the new package
    - Radiographs
      - List of radiograph codes supported by the new package
    - Surgeries
      - List of surgery codes supported by the new package

- Delete Package
  Delete specified package of the specific insurance comapny

  - DELETE: /insurancePackages/:companyCode/:packageName
  - Only admins are allowed

- Manage Patient's Package
  Manage the status of a specific package for specific patient

  - PUT: /insurancePackages/me/manage
  - Only Insurance Companies are able
  - Fields
    - PatientCode
    - PackageCode
    - Status
      - Approved: IC approved the request
      - Rejected: IC rejected the request

## 🧐 Visits <a name = "visits"></a>

- Add Visit
  add new visit into our records.

  - POST: /visits/:stType/me
  - Only Service Staffers are allowed to call this
  - Fields
    - BaseVisitCode (Optional)
    - PatientCode
    - ServiceProviderCode
    - ServiceStafferCode
    - VisitType
    - Cost
    - ServiceProviderConnectionCode
    - PatientPackageCode

- Fetch visits
  fetch list of recorded visits

  - GET: /visits/
  - Any authenticated user is allowed
  - Params
    - visitType

### Prescriptions <a name = "visit-prescriptions"></a>

- Add Prescription
  add new lab test to the specified visit.

  - POST: /visits/:visitCode/prescriptions/
  - The required fields
    - Note.
    - List of Drugs (Drug Code).

- Get Prescriptions
  fetch list of prescriptions for the specified visit

  - GET: /visits/:visitCode/prescriptions/

- Current Prescription
  lab test code is required.

  - Details
    get more details about the specified lab test.

    - GET: /visits/:visitCode/prescriptions/:prescriptionCode/

  - Update lab test
    update the specified lab test for the specified visit.

    - PUT: /visits/:visitCode/prescriptions/:prescriptionCode/
    - The required fields
      - Note.
      - List of Drugs (Drug Code).

  - Delete Prescription
    delete the specified lab test for the specified visit

    - DELETE: /visits/:visitCode/prescriptions/:prescriptionCode/

### Lab Tests <a name = "visit-labTests"></a>

- Add Lab Test
  add new lab test to the specified visit.

  - POST: /visits/:visitCode/labtests/
  - The required fields
    - LabTestCode.
    - ScheduledDate.
    - Note.

- Get Lab Tests
  fetch list of labtests for the specified visit

  - GET: /visits/:visitCode/labtests/

- Current Lab Test
  lab test code is required.

  - Details
    get more details about the specified lab test.

    - GET: /visits/:visitCode/labtests/:prescribedLabTestCode/

  - Update lab test
    update the specified lab test for the specified visit.

    - PUT: /visits/:visitCode/labtests/:prescribedLabTestCode/
    - The required fields
      - LabTestCode.
      - ScheduledDate.
      - Note.

  - Delete lab test
    delete the specified lab test for the specified visit

    - DELETE: /visits/:visitCode/labtests/:prescribedLabTestCode/

### Surgeries <a name = "visit-surgeries"></a>

- Add Surgery
  add new surgery to the specified visit.

  - POST: /visits/:visitCode/surgeries/
  - The required fields
    - SurgeryTestCode.
    - ScheduledDate.
    - Note.

- Get Surgerys
  fetch list of surgeries for the specified visit

  - GET: /visits/:visitCode/surgeries/

- Current Surgery
  surgery code is required.

  - Details
    get more details about the specified surgery.

    - GET: /visits/:visitCode/surgeries/:prescribedSurgeryCode/

  - Update surgery
    update the specified surgery for the specified visit.

    - PUT: /visits/:visitCode/surgeries/:prescribedSurgeryCode/
    - The required fields
      - SurgeryTestCode.
      - ScheduledDate.
      - Note.

  - Delete surgery
    delete the specified surgery for the specified visit

    - DELETE: /visits/:visitCode/surgeries/:prescribedSurgeryCode/

### Radiographs <a name = "visit-radiographs"></a>

- Add Radiograph
  add new Radiograph to the specified visit.

  - POST: /visits/:visitCode/radiographs/
  - The required fields
    - RadiographCode.
    - ScheduledDate.
    - Note.

- Get Radiographs
  fetch list of radiographs for the specified visit

  - GET: /visits/:visitCode/radiographs/

- Current Radiograph
  radiograph code is required.

  - Details
    get more details about the specified radiograph.

    - GET: /visits/:visitCode/radiographs/:prescribedRadiographCode/

  - Update radiograph
    update the specified radiograph for the specified visit.

    - PUT: /visits/:visitCode/radiographs/:prescribedRadiographCode/
    - The required fields
      - RadiographCode.
      - ScheduledDate.
      - Note.

  - Delete radiograph
    delete the specified radiograph for the specified visit

    - DELETE: /visits/:visitCode/radiographs/:prescribedRadiographCode/

### Claims <a name = "visit-claims"></a>

- Add Claim
  send a the specific visit' code to TPA

  - POST: /visits/:visitCode/claim
  - The required fields
    - Empty body

- Get Claims
  fetch list of claims (Only TPAs are allowed)

  - GET: claims/:claimCode/

- Manage Claim
  manage current claim status.

  - PUT: claims/:claimCode/
  - The required fields
    - Status
      - Enum type

### Visit Prescriptions Purchase <a name = "visit-prescriptions-purchase"></a>

- Add Visit's Prescription Purchase

  - POST: /visits/:visitCode/prescriptions/:prescriptionCode/purchase
  - :visitCode must be assocaited with a base visit, which has a prescribed lab test (:prescriptionCode)
  - Only service staffers are allowed to use this API

- Fetch Visit's Prescription Purchases

  - GET: /visits/:visitCode/prescriptions/:prescriptionCode/purchase
  - :visitCode must be assocaited with a base visit, which has a prescribed lab test (:prescriptionCode)
  - Only service staffers are allowed to use this API

### Visit Prescribed Lab Tests Purchase <a name = "visit-labTests-purchase"></a>

- Add Visit's Lab Test Purchase

  - POST: /visits/:visitCode/labTests/:prescribedLabTestCode/purchase
  - :visitCode must be assocaited with a base visit, which has a prescribed lab test (:prescribedLabTestCode)
  - Only service staffers are allowed to use this API

- Fetch Visit's Lab Test Purchases

  - GET: /visits/:visitCode/labTests/:prescribedLabTestCode/purchase
  - :visitCode must be assocaited with a base visit, which has a prescribed lab test (:prescribedLabTestCode)
  - Only service staffers are allowed to use this API

### Visit Prescribed Surgeries Purchase <a name = "visit-surgeries-purchase"></a>

- Add Visit's Surgery Purchase

  - POST: /visits/:visitCode/surgeries/:prescribedSurgeryCode/purchase
  - :visitCode must be assocaited with a base visit, which has a prescribed lab test (:prescribedSurgeryCode)
  - Only service staffers are allowed to use this API

- Fetch Visit's Surgery Purchases

  - GET: /visits/:visitCode/surgeries/:prescribedSurgeryCode/purchase
  - :visitCode must be assocaited with a base visit, which has a prescribed lab test (:prescribedSurgeryCode)
  - Only service staffers are allowed to use this API

### Visit Prescribed Radiographs Purchase <a name = "visit-radiographs-purchase"></a>

- Add Visit's Radiograph Purchase

  - POST: /visits/:visitCode/radiographs/:prescribedRadiographCode/purchase
  - :visitCode must be assocaited with a base visit, which has a prescribed lab test (:prescribedRadiographCode)
  - Only service staffers are allowed to use this API

- Fetch Visit's Radiograph Purchases

  - GET: /visits/:visitCode/radiographs/:prescribedRadiographCode/purchase
  - :visitCode must be assocaited with a base visit, which has a prescribed lab test (:prescribedRadiographCode)
  - Only service staffers are allowed to use this API

### Statistics <a name="stats"></a>

- Get Purchase Counts
  Get purchase counts by type

  - GET: /stats/PurchaseCounts

- Get Purchase Amounts
  Get purchase amounts by type

  - GET: /stats/PurchaseAmounts

- Get Visit Counts
  Get visit count by visit type

  - GET: /stats/VisitCounts

- Get Service Provider Counts by Role
  Get SP count by role (Doctor, Pharmacisit etc...)

  - GET: /stats/SPCounts

- Get Patient Counts by Gender
  Get patient count by gender

  - GET: /stats/Genders

- Get Access Requests by Object Accessed

  - GET: /stats/ACCounts

- Get Access Requests by Result
  Get Access request by result (permitted / Allowed)
  - GET: /stats/permitted
