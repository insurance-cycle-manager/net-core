using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace PatientsRecordsManager.ABAC.Extensions
{
    public static class AbacHttpContextExtensions
    {
        internal static bool ShouldUseMiddleware(this HttpContext context)
        {
            var path = context.Request.Path.ToString();

            if (path.Contains("auth", StringComparison.CurrentCultureIgnoreCase))
                return false;
            else if (path.Contains("policies", StringComparison.CurrentCultureIgnoreCase))
                return false;
            else
                return true;
        }

        public static async Task UnauthorizedMiddleware(this HttpContext context, string msg)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            context.Response.StatusCode = StatusCodes.Status406NotAcceptable;
            context.Response.Headers.Add("Access-Control-Expose-Headers", "User-Unauthorized");
            // context.Response.Headers.Add("Message", msg);
            context.Response.Headers.Add("Access-Control-Allow-Origin", "*");
            context.Response.Headers.Add("Content-Type", "application/json");

            object o = new
            {
                Data = new
                {
                    RequestId = context.TraceIdentifier,
                    EmergencyAccessAllowed = false,
                    Issuer = "MMMM4AbacServices"
                },
                Message = msg,
                Url = context.Request.Path
            };
            await context.Response.Body.WriteAsync(System.Text.Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(o)));
        }
    }
}