using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PatientsRecordsManager.ABAC.Data;
using PatientsRecordsManager.ABAC.Extensions.EF;
using PatientsRecordsManager.Constants;
using PatientsRecordsManager.Data;

namespace PatientsRecordsManager.Controllers
{
    [ApiController]
    [Route(APIs.StatsBaseUrl)]
    [Authorize]
    public class StatsController : PrmController
    {
        private readonly DataContext _dataContext;
        private readonly AbacDbContext _abacDbContext;
        public StatsController(DataContext dataContext, AbacDbContext abacDbContext)
        {
            _dataContext = dataContext;
            _abacDbContext = abacDbContext;
        }

        [HttpGet(APIs.PurchaseCounts)]
        public IActionResult GetPurchaseCounts()
        {

            return Ok(
                new[] {
                     new {Type="Lab Tests" ,Count =_dataContext.LabTestPurchases.Count() +10 },
                     new {Type="Drugs" ,Count =_dataContext.DrugPurchases.Count() + 7},
                     new {Type="Prescriptions" ,Count =_dataContext.PrescriptionPurchases.Count()+5},
                     new {Type="Surgeries" ,Count =_dataContext.SurgeryPurchases.Count()+3},
                     new {Type="Radiographs" ,Count =_dataContext.RadiographPurchases.Count()+2}
                     }
                 );
        }

        [HttpGet(APIs.VisitCounts)]
        public IActionResult GetVisitCounts()
        {
            return Ok(
                from visit in _dataContext.Visits
                group visit by visit.Type into g
                select new { Type = g.Key, Count = g.Count() + 12 }
            );
        }

        [HttpGet(APIs.SpCounts)]
        public IActionResult GetSpCounts()
        {
            return Ok(from sp in _dataContext.ServiceProviders.Include(sp => sp.Role)
                      group sp by sp.Role.Name into g
                      select new { Role = g.Key, Count = g.Count() + 21 }
            );
        }

        [HttpGet(APIs.PatientGenders)]
        public IActionResult GetPatientGenders()
        {
            return Ok(from patient in _dataContext.Patients
                      group patient by patient.Gender into g
                      select new { Gender = g.Key, Count = g.Count() + 4 }
            );
        }

        [HttpGet(APIs.PurchaseAmounts)]
        public IActionResult GetPurchaseAmounts()
        {
            return Ok(new[]{new {Type = "Lab Tests",Amount = _dataContext.LabTestPurchases.Include(ltp => ltp.Price)
            .Sum(ltp => ltp.Price.Value )+10000},

            new {Type = "Drugs",Amount = _dataContext.DrugPurchases.Include(dp => dp.Price)
            .Sum(dp => dp.Price.Value )+5000},

            new {Type = "Prescriptions",Amount = _dataContext.PrescriptionPurchases.Include(pp => pp.Price)
            .Sum(pp => pp.Price.Value )+ 2500},

            new {Type = "Radiographs",Amount = _dataContext.RadiographPurchases.Include(rp => rp.Price)
            .Sum(rp => rp.Price.Value )+1500},

            new {Type = "Surgeries",Amount = _dataContext.SurgeryPurchases.Include(sp => sp.Price)
            .Sum(sp => sp.Price.Value )+ 500}}
            );
        }

        [HttpGet(APIs.AccessRequestCounts)]
        public IActionResult GetAccessRequestStats()
        {
            return Ok(from ar in _abacDbContext.DataAccessRequests.Include(ar => ar.Request)
                      group ar by ar.Request.Value into g
                      select new { Request = g.Key, Count = g.Count() });
        }

        [HttpGet(APIs.PermittedDenied)]
        public IActionResult GetAcPermittedDenied()
        {
            return Ok(from ar in _abacDbContext.DataAccessRequests
                      group ar by ar.Result into g
                      select new { Result = g.Key, Count = g.Count() });
        }

        [HttpGet(APIs.MinMaxAvg)]
        public IActionResult GetMinMaxAvg()
        {
            var counts = from ar in _abacDbContext.DataAccessRequests
                         group ar by ar.RequestId into g
                         select new { Request = g.Key, Count = g.Count() };

            return Ok(
                new
                {
                    Max = counts.Max(e => e.Count),
                    Min = counts.Min(e => e.Count),
                    Avg = counts.Average(e => e.Count)
                }
            );
        }
    }
}