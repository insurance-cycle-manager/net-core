using System;
using System.Collections.Generic;
using PatientsRecordsManager.Data.Dtos.Prices;

namespace PatientsRecordsManager.Data.Dtos.InsuranceCompanies.Packages
{
    public class InsurancePackageForUpdateDto
    {
        public string Category { get; set; }

        public PriceForAddDto Price { get; set; }
        public DateTime? PackageDate { get; set; }

        public IEnumerable<string> Drugs { get; set; }

        public IEnumerable<string> LabTests { get; set; }

        public IEnumerable<string> Radiographs { get; set; }

        public IEnumerable<string> Surgeries { get; set; }
    }
}