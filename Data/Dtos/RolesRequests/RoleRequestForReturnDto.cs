using System.ComponentModel.DataAnnotations;
using PatientsRecordsManager.Constants.Enums;
using PatientsRecordsManager.Data.Dtos.Addresses;
using PatientsRecordsManager.Data.Dtos.Roles;
using PatientsRecordsManager.Data.Dtos.Users;
using PatientsRecordsManager.Data.Dtos.Users.Emails;
using PatientsRecordsManager.Data.Dtos.Users.Phones;

namespace PatientsRecordsManager.Data.Dtos.RolesRequests
{
    public class RoleRequestForReturnDto
    {
        [Required]
        public UserForReturnDto User { get; set; }
        [Required]
        public RoleForReturnDto Role { get; set; }
        [Required]
        public UserEmailForReturnDto Email { get; set; }
        [Required]
        public UserPhoneForReturnDto Phone { get; set; }
        public AddressForReturnDto Address { get; set; }
        [Required]
        public RoleRequestStatus Status { get; set; }
    }
}